Package: BiocParallel
Type: Package
Title: Bioconductor facilities for parallel evaluation
Version: 1.12.0
Authors@R: c(
    person("Bioconductor Package Maintainer",
        email="maintainer@bioconductor.org", role="cre"),
    person("Martin", "Morgan", role="aut"),
    person("Valerie", "Obenchain", role="aut"),
    person("Michel", "Lang", email="michellang@gmail.com", role="aut"),
    person("Ryan", "Thompson", email="rct@thompsonclan.org", role="aut"))
Description: This package provides modified versions and novel
    implementation of functions for parallel evaluation, tailored to
    use with Bioconductor objects.
URL: https://github.com/Bioconductor/BiocParallel
BugReports: https://github.com/Bioconductor/BiocParallel/issues
biocViews: Infrastructure
License: GPL-2 | GPL-3
SystemRequirements: C++11
Depends: methods
Imports: stats, utils, futile.logger, parallel, snow
Suggests: BiocGenerics, tools, foreach, BatchJobs, BBmisc, doParallel,
        Rmpi, GenomicRanges, RNAseqData.HNRNPC.bam.chr14,
        TxDb.Hsapiens.UCSC.hg19.knownGene, VariantAnnotation,
        Rsamtools, GenomicAlignments, ShortRead, codetools, RUnit,
        BiocStyle, knitr
Collate: AllGenerics.R BiocParallelParam-class.R bploop.R
        ErrorHandling.R log.R bpbackend-methods.R bpisup-methods.R
        bplapply-methods.R bpmapply-methods.R bpiterate-methods.R
        bpschedule-methods.R bpstart-methods.R bpstop-methods.R
        bpvec-methods.R bpvectorize-methods.R bpworkers-methods.R
        bpaggregate-methods.R bpvalidate.R SnowParam-class.R
        MulticoreParam-class.R register.R SerialParam-class.R
        DoparParam-class.R SnowParam-utils.R BatchJobsParam-class.R
        progress.R ipcmutex.R utilities.R
LinkingTo: BH
VignetteBuilder: knitr
NeedsCompilation: yes
Packaged: 2017-10-30 23:36:29 UTC; biocbuild
Author: Bioconductor Package Maintainer [cre],
  Martin Morgan [aut],
  Valerie Obenchain [aut],
  Michel Lang [aut],
  Ryan Thompson [aut]
Maintainer: Bioconductor Package Maintainer <maintainer@bioconductor.org>
Built: R 3.4.4; x86_64-pc-linux-gnu; 2018-12-17 09:36:28 UTC; unix
