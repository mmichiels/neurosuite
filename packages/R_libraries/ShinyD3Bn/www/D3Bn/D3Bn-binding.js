// IIFE (not necessary but good practice)
//(function(){

  // Create a generic output binding instance
  var d3BnBinding = new Shiny.InputBinding();

  // Our output el class is : shinyD3Bn
  var D3BN_CLASS = ".shinyD3Bn";

  // This should be a variable somehow
  // Width and height in pixels
  var svg_width = 500,
      svg_height = 500;

  //Set up the colour scale
  var color = d3.scale.category20();

  // Configurable params
  var node_rad = 30;
  var node_padding = 1;
  var link_width = 1;

  var bnData = {};

  // Finding method: Using jQuery find method
  d3BnBinding.find = function(scope) {
    return $(scope).find(D3BN_CLASS);
  };
  
  // TODO: Input action have not been defined yet
  d3BnBinding.getValue = function(el) {
    
    var $el = $(el);
    var action = $el.data("action");
    
    if(action){
      $el.data("action",null);
      return(action);
    }
    else
      return(null);
  };
  
  // Suscribe function: Calls callback function when a change oocurs (not rate policy applies)
  d3BnBinding.subscribe = function(el, callback) {
    $(el).on('change.d3BnBinding', function(event,action) {
      $(el).data("action",action);
      callback(false);
    });
  };
  
  // Unsuscribe
  d3BnBinding.unsubscribe = function(el) {
    $(el).off('.d3BnBinding');
  };
  
  // Function to call when a message from the server
  // is received. mostly to change the appearance or the graph
  d3BnBinding.receiveMessage = function(el, data) {
    this.renderValue(el, data);
  };
  
  // Get current state of the bn
  // TODO: To be defined
  d3BnBinding.getState = function(el){
    return(0);
  };
  
  
  d3BnBinding.initialize = function(el) {
    
      var $el = $(el);
    
      //Set up the force layout
     force = d3.layout.force()
            .charge(-1000)
            .linkDistance(200)
            //.linkStrength(0.5)
            .size([$el.parent().width(), $el.parent().height()]);

      // If it is not initialized we have to build all svg stuff
      svg = d3.select(el).append("svg")
      .attr("width", $el.parent().width())
      .attr("height", $el.parent().height());

      // Create marker for arrows
      svg.append("defs").append("marker")
          .attr("id", "arrowHead")
          .attr("refX", 0)
          .attr("refY", 0)
          .attr("orient", "auto")
          .attr("markerWidth", 12)
          .attr("markerHeight", 12)
          .attr("viewBox", "0 -5 10 10")
          .style("overflow", "visible")
          .append("path")
          .attr("d", "M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z")
          .attr("transform", "matrix(-1.1,0,0,-1.1,-1.1,0)")
          .style("fill-rule", "evenodd")
          .style("stroke", "#999")
          .style("stroke-width", 0.625)
          .style("stroke-linejoin", "round")
          .style("opacity", "1");

      // Add svg to state
      $el.data("toggle",0);
      $el.data("force",force);
  };
  
  
  // Renderer
  /* This function will be called every time we receive a new output values
  *  for rendering (call from server).
  *  · The el argument is an element of the array returned by find method (div tag)
  *  · Data is the conversion from R using RJSONIO with, at least, the following
       fields:
         - nodes: Set of nodes.
         - arcs: Arcs between nodes
         - config: Plot config params
     - Detailed data structure available in json schema files and documentation
  */
  d3BnBinding.renderValue = function(el, data){

    var $el = $(el);

    // Code that will be executed on each call.

    // Get svg
    var svg = d3.select(el).select("svg");
    var force =  $el.data("force");
    
    // Set svg and force parameters
    svg.attr("width", $el.parent().width())
       .attr("height", $el.parent().height());

    // Force layout params
    force.charge(data.params.layout.charge)
         .linkDistance(data.params.layout.linkdist)
         .size([$el.parent().width(), $el.parent().height()]); 
    
    var layout_mode = data.params.layout.layout;
    var layout_overlap = data.params.layout.avoidOverlap;

    // Phase 0: Parse data
    var graph = data;

    //Creates the graph data structure out of the json data
    force.nodes(graph.nodes)
        .links(graph.arcs)
        .start();
        
    $el.data("linked",markovBlanket(graph));

    // Phase 1: Enter phase -> Build nodes and links

    // Create all the line svgs but without locations yet (links)
    svg.selectAll(".link")
        .data(graph.arcs)
        .enter().append("line")
        .attr("class", "link")
        .style("marker-end", "url(#arrowHead)")
        .style("stroke-width", data.params.arc.width);

    // Remove unnc arcs
    svg.selectAll(".link")
            .data(graph.arcs)
            .exit().remove();

    // Select all links
    var link = svg.selectAll(".link");
    $el.data("links",link);


    // A lo bestia
    svg.selectAll(".node").remove();
    
    // Create all nodes but again wo locations
    var newNodes = svg.selectAll(".node")
        .data(graph.nodes)
        .enter().append("g")
        .attr("class", "node")
        .call(force.drag);

    // Tip
    if(data.params.node.tooltip){
            var nodetip = d3.tip()
                  .attr('class', 'd3-tip')
                  //.offset([-10, 0])
                  .html(function(d) {
                    return "<strong> <span style='color:white'>" + d.name + "</strong>";
                });

            svg.call(nodetip);

            newNodes.on('mouseover', nodetip.show)
            .on('mouseout',  nodetip.hide);
    }
    
    newNodes.on('dblclick',function(){
          var toggle = connectedNodes(d3.select(this).node().__data__,
                         $el.data("toggle"),
                         $el.data("linked"),
                         $el.data("nodeLabels"),
                         $el.data("nodeCircles"),
                         $el.data("links"));
          /// Update toggle value               
          $el.data("toggle",toggle);
    })
    .on('contextmenu',d3.contextMenu(createMenu(d3.select(this).node().__data__, data.params.menu )));
    


    // If layout is hierachical compute focus
    var hierscale;
    if(layout_mode == "hierachical"){
      hierscale = hierRankScale(graph.nodes, data.params.svg.height );
      force.gravity(0);
    }


    // Remove nodes
    //svg.selectAll(".node")
    //   .data(graph.nodes)
    //   .exit().remove();

    // Create circles for new nodes
    newNodes.append("circle")
        .attr("class", "nodecircle")
        .attr("r", data.params.node.radius );
        

    // Create labels
    newNodes.append("text")
        .attr("class", "nodetext")
        .attr("dy", ".35em")
        .attr("fill", "white")
        //.attr("stroke", "black")
        .style("font-size", function (d) {
          return Math.min( 1.75*data.params.node.radius,  ( ( 3.5 * data.params.node.radius ) / d.name.length  ) ) + "px";
        });
        
    // Select all nodes
    var node = svg.selectAll(".node");

    // Phase 2: Update (Update nodes and labels)
    
    // Select color
    var colorsel = data.params.node.color;
    var colfun;
    if(colorsel == "group")
      colfun = getGroup;
    else if (colorsel == "class")
      colfun = getClass;
    else  
      colfun = getType;
    
      
    // Select all labels and circles
    var nodeCircles = node.selectAll("circle")
                      .style("fill", function (d){ return color(colfun(d)); });
    $el.data("nodeCircles",nodeCircles);

    var nodeLabels = node.selectAll("text")
                     .text(function (d) {return d.name;});
    $el.data("nodeLabels",nodeLabels);

    //Now we are giving the SVGs co-ordinates - the force layout is generating the co-ordinates which this code is using to update the attributes of the SVG elements
    force.on("tick", function () {

                         link.attr("x1", function (d) {
                            var deltaX = d.target.x - d.source.x,
                                deltaY = d.target.y - d.source.y,
                                dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
                             return d.source.x + (data.params.node.radius * (deltaX/dist) );
                         })
                             .attr("y1", function (d) {
                                var deltaX = d.target.x - d.source.x,
                                deltaY = d.target.y - d.source.y,
                                dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
                                
                             return d.source.y + (data.params.node.radius * (deltaY/dist) );
                         })
                             .attr("x2", function (d) {
                                var deltaX = d.target.x - d.source.x,
                                deltaY = d.target.y - d.source.y,
                                dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
                             return d.target.x - ( (data.params.node.radius+2) * (deltaX/dist) );
                         })
                             .attr("y2", function (d) {
                                var deltaX = d.target.x - d.source.x,
                                deltaY = d.target.y - d.source.y,
                                dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
                             return d.target.y - ( (data.params.node.radius+2) * (deltaY/dist) );
                         });

                         nodeCircles.attr("cx", function (d) {
                             return d.x;
                         })
                             .attr("cy", function (d) {
                             return d.y;
                         });

                         nodeLabels.attr("x", function (d) {
                             return d.x;
                         })
                             .attr("y", function (d) {
                             return d.y;
                         });
                         
                         // Custom node gravity
                        if(layout_mode == "hierachical")
                            node.each(customHierGravity(0.1, hierscale, data.params.svg.width ) );

                        // Avoid node overlap
                        if(layout_overlap)
                          node.each(collide(graph,0.5,data.params.node.radius, data.params.node.padding));
    });
    
    // Initialize
    for (var i = 50; i > 0; --i) force.tick();

  };
//});

// Register our new binding
Shiny.inputBindings.register(d3BnBinding, "shiny.BnD3");


