// Function that computes the mb for each node
function markovBlanket(graph) {

    var linkedIndex = {};

    // Nodes are linked with themselves
    for (i = 0; i < graph.nodes.length; i++) {
        linkedIndex[i + "," + i] = 1;
    }

    // Descs & Parents
    graph.arcs.forEach(function (d) {
        linkedIndex[d.source.index + "," + d.target.index] = 1;
        linkedIndex[d.target.index + "," + d.source.index] = 1;
    });

    // Sibilings
    graph.arcs.forEach(function (d) {
        var parId = d.source.index;
        var sonId = d.target.index;
        graph.arcs.forEach(function (e) {
            if (e.target.index == sonId) {
                linkedIndex[parId + "," + e.source.index] = 1;
            }
        })
    });

    return linkedIndex;
}

//This function looks up whether a pair are linked
function neighboring(a, b, linked) {
    return linked[a.index + "," + b.index];
}


// Trigger connected nodes
function connectedNodes(d,
                        toggle,
                        linked,
                        labels,
                        circles,
                        links) {

    if (toggle === 0) {

        labels.style("opacity", function (o) {
            return neighboring(d, o, linked) | neighboring(o, d, linked) ? 1 : 0.1;
        });
        circles.style("opacity", function (o) {
            return neighboring(d, o, linked) | neighboring(o, d, linked) ? 1 : 0.1;
        });

        links.style("opacity", function (o) {
            return (neighboring(d, o.source, linked) & neighboring(d, o.target, linked)) ? 1 : 0.1;
        });

        //Reduce the op
        return 1;
    } else {
        //Put them back to opacity=1
        labels.style("opacity", 1);
        circles.style("opacity", 1);
        links.style("opacity", 1);
        return 0;
    }
}

function collide(graph,alpha,rad,padding) {
    // Build quadtree to search
    var quadtree = d3.geom.quadtree(graph.nodes);
    return function (d) {
        var rb = 2 * rad + padding,
            nx1 = d.x - rb,
            nx2 = d.x + rb,
            ny1 = d.y - rb,
            ny2 = d.y + rb;
        quadtree.visit(function (quad, x1, y1, x2, y2) {
            if (quad.point && (quad.point !== d)) {
                var x = d.x - quad.point.x,
                    y = d.y - quad.point.y,
                    l = Math.sqrt(x * x + y * y);
                if (l < rb) {
                    l = (l - rb) / l * alpha;
                    d.x -= x *= l;
                    d.y -= y *= l;
                    quad.point.x += x;
                    quad.point.y += y;
                }
            }
            return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
        });
    };
}

// Custom gravity function for hierarchy dispay
// Move nodes toward cluster focus, but x grav affect when node
// is close to the height
function customHierGravity(alpha, scale, width ) {
  return function(d) {
    d.y += (scale(d.rank) - d.y) * alpha;
    d.x += ( (width/2) - d.x)/Math.max(1,( scale(d.rank) - d.y)) * alpha*0.1;
    d.x += 0;
  };
}

// Hierarchy focuses
function hierRankScale(nodes, height){
  var maxRank = 0;
  // Get max rank
  for(var i=0; i<nodes.length; i++){
    if(nodes[i].rank > maxRank) maxRank = nodes[i].rank;
  }
  // Return scale
  return d3.scale.ordinal()
    .domain(d3.range(maxRank+1))
    .rangePoints([0, height], 1);
}

// Node color functions

// By "group"
function getGroup(d){
  return d.group | 1;
}

// By Class
function getClass(d){
  if(d.isClass)
    return 1;
  else
    return 2;
}

// by Type
function getType(d){
  return d.type | 1;
}