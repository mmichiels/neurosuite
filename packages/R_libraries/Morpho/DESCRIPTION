Package: Morpho
Type: Package
Title: Calculations and Visualisations Related to Geometric
        Morphometrics
Version: 2.6
Date: 2018-04-19
Authors@R: c(
    person("Stefan", "Schlager",, "zarquon42@gmail.com", c("aut", "cre", "cph")),
    person("Gregory", "Jefferis",,, c("ctb")),
    person("Dryden", "Ian",,,c("cph"))
    )
Description: A toolset for Geometric Morphometrics and mesh processing. This
    includes (among other stuff) mesh deformations based on reference points,
    permutation tests, detection of outliers, processing of sliding
    semi-landmarks and semi-automated surface landmark placement.
Suggests: car, lattice, shapes, testthat
Depends: R (>= 3.2.0)
Imports: Rvcg (>= 0.7), rgl (>= 0.93.963), foreach (>= 1.4.0), Matrix
        (>= 1.0-1), MASS, parallel, doParallel (>= 1.0.6), colorRamps,
        Rcpp, graphics, grDevices, methods, stats, utils
LinkingTo: Rcpp, RcppArmadillo (>= 0.4)
Copyright: see COPYRIGHTS file for details
License: GPL-2
BugReports: https://github.com/zarquon42b/Morpho/issues
LazyLoad: yes
URL: https://github.com/zarquon42b/Morpho
Encoding: UTF-8
RoxygenNote: 6.0.1
NeedsCompilation: yes
Packaged: 2018-04-24 05:12:24 UTC; hornik
Author: Stefan Schlager [aut, cre, cph],
  Gregory Jefferis [ctb],
  Dryden Ian [cph]
Maintainer: Stefan Schlager <zarquon42@gmail.com>
Repository: CRAN
Date/Publication: 2018-04-24 07:13:41
Built: R 3.4.4; x86_64-pc-linux-gnu; 2019-02-14 09:12:44 UTC; unix
