FROM ubuntu:16.04

#-----Postgres_backups:-----------
RUN apt-get update
RUN apt-get -y install software-properties-common python-software-properties wget

RUN add-apt-repository 'deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main'
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
    apt-key add -
RUN apt-get update
RUN apt-get -y install postgresql

RUN apt-get update && \
    apt-get install -y && \
    apt-get -y install rsyslog nano cron

#To work in every host computer
#RUN sed -i '/session    required   pam_loginuid.so/c\#session    required   pam_loginuid.so' /etc/pam.d/cron
RUN rm /etc/pam.d/cron
COPY ./backups/cron_daemon /etc/pam.d/cron
#RUN rm /etc/rsyslog.d/50-default.conf
#COPY ./backups/50-default.conf /etc/rsyslog.d/50-default.conf
COPY ./backups/pg_backups/crontab_db /etc/cron.d/
COPY ./backups/dj_backups/crontab_dj /etc/cron.d/
COPY ./neurosuite_dj/apps/microscopic_images/helpers/cron_delete_old_maps_multimap/crontab_clean_old_maps_multimap /etc/cron.d/
COPY ./neurosuite_dj/apps/microscopic_images/helpers/cron_delete_old_chunked_uploads/crontab_clean_old_chunked_uploads /etc/cron.d/
COPY ./neurosuite_dj/apps/morpho_analyzer/helpers/stats/crontab_delete_old_plots /etc/cron.d/
#Important note: Cron jobs added to /etc/cron.d/ will not show up in the "crontab -l" command but they are still detected and working
RUN chmod 0644 /etc/cron.d/*
#-----------------------------

ENTRYPOINT ["/neurosuite_backups/docker_backups_and_cron_entrypoint.sh"]
