#!/usr/bin/env bash

docker system prune -a -f
docker volume ls -qf dangling=true | xargs -r docker volume rm
docker rm -v $(sudo docker ps -a -q -f status=exited)
docker rmi -f  $(sudo docker images -f "dangling=true" -q)