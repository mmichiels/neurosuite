#!/bin/bash
source ~/.bashrc
conda activate conda_py364

readonly HOST='db'
readonly PORT=5432

rstudio-server stop
rstudio-server start

until nc -z $HOST $PORT; do
    echo "$(date) - waiting for postgres..."
    sleep 1
done
echo "$(date) - Postgres is UP!"

#configure_rstudio_server
rstudio-server stop
rstudio-server start
userdel rstudio
adduser --disabled-password --gecos "" rstudio
echo rstudio:rstudio | chpasswd

Xvfb :7 -screen 0 1280x1024x24 &

#Run shiny-server
/usr/bin/shiny-server > /dev/null 2>&1 &

#Install rpy2. #We install this in the docker_web_entrypoint.sh because it needs to be installed after the installation of python and R
pip install rpy2==2.9.2

#Workaround to fix kombu 4.2 to break Celery 4.1
pip uninstall -y kombu
pip install kombu==4.1.0

readonly WORKDIR=/neurosuite/neurosuite_dj

#$WORKDIR/fix_aldryn_bootstrap3_docker.sh #Running this line in development breaks the initialization of this container (it waits for the debugger to unlock). This line is neccesary for running manage.py without errors (i.e. running Celery beat tasks like manage.py clearsession)

function run_celery {
    cd $WORKDIR
    export C_FORCE_ROOT='true'
    celery -A neurosuite_dj beat -l info > /dev/null 2>&1 &
    celery -A neurosuite_dj worker -l info -Ofair
}

run_celery