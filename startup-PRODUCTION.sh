#!/bin/bash

startup() {
  echo "NeuroSuites startup PRODUCTION starts"
  echo "IMPORTANT!!!: Make sure DEBUG=False in base.py"

  echo "IMPORTANT: If this is your first run: Rename neurosuite_dj/dummy_credentials_secrets/ to neurosuite_dj/credentials_secrets/* and fill the text in each file with your own users/passwords. Django_admin files are not neccesary if you don't want a Django superuser account to be created, so delete or rename them if you don't want a Django admin account to be created."
  echo "If you are with DEBUG=True and you have just to do a migration, maybe you need to uncomment the line 10 of manage.py in order to continue the DEBUG breakpoint and not hangs up"
  echo "If you want to debug production, be sure that DEBUG=True and neurosuite_uwsgi_docker-PRODUCTION.ini has processes=1 and no threads"
  echo "If you need to generate a new TLS certificate for a new domain, please use the docker-compose.yml file and nginx.tmpl that end in '-new-tls-certificate'"

  cd /home/workspace/PRODUCTION_NEUROSUITES/neurosuite/
  echo "Stopping and removing all containers..."
  docker-compose -f docker-compose-PRODUCTION.yml down --remove-orphans

  #echo "Removing  all docker data containers..."
  #Neccesary when the app is deployed in a different computer
  docker volume rm neurosuite_pg_data
  docker volume rm neurosuite_nginx_proxy_conf
  docker volume rm neurosuite_nginx_proxy_vhost
  docker volume rm neurosuite_nginx_proxy_html
  #docker volume rm neurosuite_prometheus_data
  #docker volume rm neurosuite_logs_data

  #Necessary when nginx, rabbitMQ, rstudio-server, postgresql and shiny-server are deployed in the host machine:
  sudo /etc/init.d/nginx stop
  systemctl stop rabbitmq-server
  systemctl stop rstudio-server
  service postgresql stop
  service shiny-server stop

  #Enable ufw (Ubuntu's firewall) to block all incoming traffic except the Docker traffic
  echo "Enabling firewall: deny all incoming traffic except Docker traffic"
  echo "Warning: ssh port will also be blocked. To change this behaviour, edit this startup.sh file commenting the ufw lines"
  ufw enable
  ufw default deny incoming
  ufw reload

  #When dclient is configured:
  #systemctl restart ddclient #commented to avoid assigning a DNS type A to our domain (now we only assign a CNAM pointing to upm.es subdomain)

  echo "Deploying neurosuite..."
  docker-compose -f docker-compose-PRODUCTION.yml up --remove-orphans --build --force-recreate
  echo "Neurosuite finished!"
}

startup
