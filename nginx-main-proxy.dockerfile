FROM nginx:1.17.7

RUN rm /etc/nginx/nginx.conf
COPY nginx_main_proxy.conf /etc/nginx/nginx.conf

RUN apt-get update -y
RUN apt-get install -y openssl
RUN mkdir /etc/nginx/certs_localhost
WORKDIR /etc/nginx/certs_localhost
RUN openssl req -x509 -out localhost.crt -keyout localhost.key -newkey rsa:2048 -nodes -sha256 -subj '/CN=localhost'


