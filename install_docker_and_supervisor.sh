#!/bin/bash

#------------Docker--------------------
apt-get update && \
apt-get install -y \
    apt-transport-https \
  ca-certificates \
    curl \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

apt-key fingerprint 0EBFCD88

add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"

apt-get update
apt-get install -y docker-ce
apt-get update


#------------Docker-Compose--------------------
curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose


#------------------Supervisor---------------------
apt-get update
apt-get install -y supervisor

apt-get install -y --reinstall supervisor
service supervisor start

supervisorctl reread

#Edit /etc/supervisor/supervisord.conf with the following  (uncommented):

#[program:PRODUCTION_NEUROSUITES]
#command=/home/workspace/PRODUCTION_NEUROSUITES/neurosuite/startup-PRODUCTION.sh
#autostart=true
#autorestart=true
#stderr_logfile=/var/log/supervisor/PRODUCTION_NEUROSUITES_error.log
#stdout_logfile=/var/log/supervisor/PRODUCTION_NEUROSUITES_out.log
#
#[unix_http_server]
#file=/tmp/supervisor.sock   ; the path to the socket file
#
#[supervisord]
#logfile=/tmp/supervisord.log ; main log file; default $CWD/supervisord.log
#logfile_maxbytes=50MB        ; max main logfile bytes b4 rotation; default 50MB
#logfile_backups=10           ; # of main logfile backups; 0 means none, default 10
#loglevel=info                ; log level; default info; others: debug,warn,trace
#pidfile=/tmp/supervisord.pid ; supervisord pidfile; default supervisord.pid
#nodaemon=false               ; start in foreground if true; default false
#minfds=1024                  ; min. avail startup file descriptors; default 1024
#minprocs=200                 ; min. avail process descriptors;default 200
#[rpcinterface:supervisor]
#supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface
#
#[supervisorctl]
#serverurl=unix:///tmp/supervisor.sock ; use a unix:// URL  for a unix socket
#


#Then:
#apt-get install -y --reinstall supervisor
#supervisorctl reread
#service supervisor start
#supervisorctl start PRODUCTION_NEUROSUITES
#To check the status and logs:
#supervisorctl status
#apt-get install -y multitail
#multitail /var/log/supervisor/PRODUCTION_NEUROSUITES_error.log /var/log/supervisor/PRODUCTION_NEUROSUITES_out.log