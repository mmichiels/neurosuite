FROM nginx:1.17.7

RUN rm /etc/nginx/nginx.conf
COPY nginx_neurosuite.conf /etc/nginx/nginx.conf

COPY ./neurosuite_dj/credentials_secrets/nginx_services_htpasswd/ /etc/nginx/passwords/

