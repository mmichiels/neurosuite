#!/bin/bash
set -e


psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
  CREATE DATABASE neurosuite_db_psql;
  CREATE USER root WITH PASSWORD 'root';
  ALTER ROLE root SET client_encoding TO 'utf8';
  ALTER ROLE root SET default_transaction_isolation TO 'read committed';
  ALTER ROLE root SET timezone TO 'UTC';
  GRANT ALL PRIVILEGES ON DATABASE neurosuite_db_psql TO root;
EOSQL

psql neurosuite_db_psql < /pg_backups/neurosuite_db_psql_backup.sql
