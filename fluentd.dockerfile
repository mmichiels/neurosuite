FROM fluent/fluentd:v1.1-debian
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get update && \
    apt-get install -y && \
    apt-get install -y build-essential && \
    apt-get install -y libgeoip-dev && \
    apt-get install -y libmaxminddb-dev && \
    apt-get install make libcurl4-gnutls-dev --yes && \
    apt-get install -y libcurl4-gnutls-dev && \
    apt-get install -y ruby2.3-dev && \
    apt-get install -y gcc

RUN ["gem", "install", "fluent-plugin-elasticsearch", "--no-rdoc", "--no-ri", "--version", "2.7.0"]
RUN ["gem", "install", "fluent-plugin-geoip"]