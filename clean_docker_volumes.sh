#!/usr/bin/env bash

docker -H unix:///root/altdocker.socketsystem prune -a -f
docker -H unix:///root/altdocker.socketvolume ls -qf dangling=true | xargs -r docker -H unix:///root/altdocker.socket volume rm
docker -H unix:///root/altdocker.socket rm -v $(sudo docker -H unix:///root/altdocker.socket ps -a -q -f status=exited)
docker -H unix:///root/altdocker.socket rmi -f  $(sudo docker -H unix:///root/altdocker.socket images -f "dangling=true" -q)