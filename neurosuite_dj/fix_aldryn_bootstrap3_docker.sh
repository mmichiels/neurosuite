#!/bin/bash

readonly ALDRYN_BOOTSTRAP_PATH=/opt/conda/envs/conda_py364/lib/python3.6/site-packages/aldryn_bootstrap3

readonly WORKDIR_DJANGO=/neurosuite/neurosuite_dj

cp $WORKDIR_DJANGO/fix_aldryn_bootstrap/context.html $ALDRYN_BOOTSTRAP_PATH/templates/admin/aldryn_bootstrap3/widgets/context.html

cp $WORKDIR_DJANGO/fix_aldryn_bootstrap/link_or_button.html $ALDRYN_BOOTSTRAP_PATH/templates/admin/aldryn_bootstrap3/widgets/link_or_button.html

cp $WORKDIR_DJANGO/fix_aldryn_bootstrap/size.html $ALDRYN_BOOTSTRAP_PATH/templates/admin/aldryn_bootstrap3/widgets/size.html

cp $WORKDIR_DJANGO/fix_aldryn_bootstrap/widgets.py $ALDRYN_BOOTSTRAP_PATH/widgets.py
