#!/bin/bash

WORKDIR=/neurosuite/neurosuite_dj/static/neurosuite

function minify_and_zopfli_gzip () {
    cd $WORKDIR

    RED='\033[0;31m'
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color

    #It's not neccesary to minify the HTML here. It is minified dynamically via django-htmlmin

    #-------------Minify JS--------------------------
    npm install uglify-js -g

    EXTENSION_OUTPUT_JS='MINIFIED_JS'
    find ./ -name "*.js" | while read fname; do
      printf "${GREEN}Minifying JS: ${NC} ${fname}\n"
      uglifyjs $fname --compress drop_console,dead_code=true --ie8 -o $fname.$EXTENSION_OUTPUT_JS
      if [ -f $fname.$EXTENSION_OUTPUT_JS ]; then
        rm $fname
        mv $fname.$EXTENSION_OUTPUT_JS $fname
      fi
    done

    #-------------Minify CSS--------------------------
    npm install -g crass

    EXTENSION_OUTPUT_CSS='MINIFIED_CSS'
    find ./ -name "*.css" | while read fname; do
      printf "${GREEN}Minifying CSS: ${NC} ${fname}\n"
      crass $fname > $fname.$EXTENSION_OUTPUT_CSS
      if [ -f $fname.$EXTENSION_OUTPUT_CSS ]; then
        rm $fname
        mv $fname.$EXTENSION_OUTPUT_CSS $fname
      fi
    done

    #-------------Compress to gzip with zopfli--------------------------
    apt-get install -y zopfli

    $EXTENSION_GZIP='gz'
    find ./ -name "*.css" -o -name "*.js" | while read fname; do
      printf "${GREEN}Compressing to gzip with Zopfli: ${NC} ${fname}\n"
      if [ -f $fname.$EXTENSION_GZIP ]; then
        rm $fname.$EXTENSION_GZIP #To update the previous gzip file
      fi
      zopfli --i1000 $fname
      #rm $fname
    done

}

minify_and_zopfli_gzip
