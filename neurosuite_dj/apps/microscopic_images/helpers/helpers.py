import os
import io
from base64 import b64encode
import base64
from io import BytesIO as IO
import json
import zipfile
from io import BytesIO
from django.conf import settings
import requests
import shutil
import subprocess
import platform
import pyvips
import math
import tempfile
from PIL import Image
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
#from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import logging
from selenium.webdriver.remote.remote_connection import LOGGER
#LOGGER.setLevel(logging.WARNING)

from celery.result import AsyncResult
from celery.task.control import revoke
import time
from datetime import datetime
from celery import shared_task, current_task

import pandas as pd

from chunked_upload.exceptions import ChunkedUploadError


def process_image_multimap(image):
    dir_output = settings.MULTIMAP_DIR + os.sep + image["id"]
    #Create dir_output if not exists:
    if not os.path.exists(settings.MULTIMAP_DIR):
        os.makedirs(settings.MULTIMAP_DIR)

    configuration_json = {
        "configuration_json": {},
        "error": "",
        "configuration_name": "",
        "type": "map",
    }
    try:
        image_vips = pyvips.Image.new_from_file(image["path"])
        image_vips.dzsave(image["name"], basename=dir_output, layout="google", tile_size=256, centre=False)
    except:
        configuration_json["error"] = "Error processing this image. Please select another image or download the image and upload it."
        return configuration_json

    zoom_levels = os.listdir(dir_output)
    zoom_levels.remove("blank.png")
    max_native_zoom = max(zoom_levels)

    tile_url_template = settings.MULTIMAP_URL + os.sep + image["id"] + "/{z}/{y}/{x}.jpg"
    configuration_json["configuration_json"] = {
        "type": "map",
        "layers": {
            image["id"]: {
                "name": image["name"],
                "image_id": image["id"],
                "type": "tileLayer",
                "tileUrlTemplate": tile_url_template,
                "baseLayer": True,
                "options": {
                    "attribution": image["name"],
                    "minNativeZoom": "0",
                    "maxNativeZoom": max_native_zoom,
                    "maxZoom": max_native_zoom
                }
            }
        },
        "basePath": settings.HOST_URL_WITH_PORT
    }

    #Move original image tu multimap maps folder (it will be needed for analyzing tools like points detection):
    shutil.move(image["path"], os.path.join(dir_output, "original_image"))

    """
    try:
        os.remove(image["path"])  # Original image is no longer needed because now we have the image splitted by vips dzsave
    except OSError:
        return False
    """

    configuration_json["configuration_name"] = image["name"]
    configuration_json_path = dir_output + os.sep + "configuration.json"
    with open(configuration_json_path, 'w') as outfile:
        json.dump(configuration_json, outfile)

    return configuration_json

def process_layer_multimap(image):
    dir_output = settings.MULTIMAP_DIR + os.sep + image["id"]

    configuration_json = {
        "configuration_json": {},
        "error": "",
        "configuration_name": "",
        "type": "layer",
    }
    try:
        image_vips = pyvips.Image.new_from_file(image["path"])
        image_vips.dzsave(image["name"], basename=dir_output, layout="google", tile_size=256, centre=False)
    except:
        configuration_json["error"] = "Error processing this image. Please select another image or download the image and upload it."
        return configuration_json

    zoom_levels = os.listdir(dir_output)
    zoom_levels.remove("blank.png")
    max_native_zoom = max(zoom_levels)

    tile_url_template = settings.MULTIMAP_URL + os.sep + image["id"] + "/{z}/{y}/{x}.jpg"
    configuration_json["configuration_json"] = {
        image["id"]: {
            "baseLayer": False,
            "name": image["name"],
            "image_id": image["id"],
            "type": "tileLayer",
            "url": tile_url_template,
            "options": {
                "attribution": image["name"],
                "minNativeZoom": "0",
                "maxNativeZoom": max_native_zoom,
                "maxZoom": max_native_zoom
            }
        }
    }

    #Move original image tu multimap maps folder (it will be needed for analyzing tools like points detection):
    shutil.move(image["path"], os.path.join(dir_output, "original_image"))

    """
    try:
        os.remove(image["path"])  # Original image is no longer needed because now we have the image splitted by vips dzsave
    except OSError:
        return False
    """

    configuration_json["configuration_name"] = image["name"]
    configuration_json_path = dir_output + os.sep + "configuration.json"
    with open(configuration_json_path, 'w') as outfile:
        json.dump(configuration_json, outfile)

    return configuration_json

def add_layer_json_multimap(configuration_map_json, configuration_layer_json, configuration_name):
    if configuration_layer_json["error"] == "":
        configuration_map_json["layers"].update(configuration_layer_json["configuration_json"])

    configuration_json = {
        "configuration_json": configuration_map_json,
        "type": "map",
        "configuration_name": configuration_name,
        "error": "",
        "configuration_warning": configuration_layer_json["error"]
    }

    return configuration_json


def change_drawing_layer_json_multimap(configuration_map_json, configuration_drawing_layer_json, configuration_name):
    error = True
    if ("name" in configuration_drawing_layer_json and configuration_drawing_layer_json["name"] == "drawnItems") and (not "error" in configuration_drawing_layer_json):
        configuration_map_json["layers"]["drawnItems"] = configuration_drawing_layer_json
        error = False

    configuration_json = {
        "configuration_json": configuration_map_json,
        "error": "",
        "configuration_name": configuration_name,
        "type": "map",
    }

    if error:
        if "error" in configuration_drawing_layer_json and configuration_drawing_layer_json["error"] != "":
            configuration_json["configuration_warning"] = configuration_drawing_layer_json["error"]
        else:
            configuration_json["configuration_warning"] = "Invalid drawing layer loaded"

    return configuration_json



def download_file_from_url(image):
    try:
        if image["url_download"].startswith("data:image/jpeg;base64,"):
            image_base64_data = image["url_download"].replace("data:image/jpeg;base64,", "")
            image_bytes = base64.b64decode(image_base64_data)
            with open(image["path"], 'wb') as f:
                f.write(image_bytes)
        else:
            # NOTE the stream=True parameter
            r = requests.get(image["url_download"], stream=True)

            request_chunk_size = 500000 #50MB chunks
            request_total_size = int(r.headers['Content-length'])
            number_chunks = request_total_size / request_chunk_size
            with open(image["path"], 'wb') as f:
                for i, chunk in enumerate(r.iter_content(chunk_size=request_chunk_size)): #10MB chunks
                    start_time = time.time()

                    if chunk: # filter out keep-alive new chunks
                        f.write(chunk)
                        #f.flush()
                        end_time = time.time()
                        if current_task:
                            process_percent = int(100 * float(i) / float(number_chunks))
                            estimated_time_finish = round((end_time - start_time) * (float(number_chunks) - float(i)), 2)
                            print ("process_percent: ", process_percent, "Task: ", current_task)
                            current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                                              'estimated_time_finish': estimated_time_finish})
    except Exception as e:
        print(e)
        result = {"error": "Error downloading the image: {0}".format(e), "result": image["path"]}
        return result

    result = {"error": "", "result": image["path"]}

    return result

def validate_disk_space(new_upload_bytes=None):
    statvfs = os.statvfs('/')
    free_available_disk_space = statvfs.f_frsize * statvfs.f_bavail  # Number of free bytes that ordinary users are allowed to use (excl. reserved space)
    if new_upload_bytes is not None:
        free_available_disk_space -= new_upload_bytes
    if (free_available_disk_space < settings.LIMIT_FREE_DISK_SPACE):
        raise ChunkedUploadError(status=400, detail='Low space on the server disk')


def run_tool_points_detection_multimap(options_algorithm, args_algorithm):
    configuration_json = {
        "configuration_json": {},
        "error": "",
        "configuration_name": "",
        "type": "layer",
    }

    imagej_full_command = ["export DISPLAY=:7 &&", "java", '-jar', "ij.jar", '-batchpath', "ObjectDetector.ijm"]
    args_string = ""
    for key, value in args_algorithm.items():
        args_string += value + "#"
    imagej_full_command.append(args_string)
    imagej_cmd = ' '.join(imagej_full_command)

    if current_task:
        process_percent = int(25)
        estimated_time_finish =  round(120, 2)
        print ("process_percent: ", process_percent, "Task: ", current_task)
        current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                          'estimated_time_finish': estimated_time_finish})

    output = subprocess.Popen(imagej_cmd, cwd=options_algorithm["imagej_path"], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output_imagej_cmd, output_imagej_cmd_err  = output.communicate()
    if output_imagej_cmd != b'_DONE_\n':
        configuration_json["error"] = "Error detecting points in this image. Try to change the algorithm parameters (like minimum or maximum radius). Perhaps there are no visible points or there are too many points."
        return configuration_json

    if current_task:
        process_percent = int(65)
        estimated_time_finish =  round(60, 2)
        print ("process_percent: ", process_percent, "Task: ", current_task)
        current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                          'estimated_time_finish': estimated_time_finish})

    image_vips = pyvips.Image.new_from_file(options_algorithm["image_path"])
    image_width = image_vips.width
    image_height = image_vips.height
    max_image_width_height = max(image_width, image_height)

    url_csv_points = settings.MULTIMAP_URL + os.sep + options_algorithm["image_id"] + os.sep + options_algorithm["folder_output"] + os.sep + options_algorithm["folder_output_points"]
    url_csv_points = os.path.join(url_csv_points, "points_original_image.csv")
    csv_filepath = settings.MULTIMAP_DIR + os.sep + options_algorithm["image_id"] + os.sep + options_algorithm["folder_output"] + os.sep + options_algorithm["folder_output_points"]
    csv_filepath = os.path.join(csv_filepath, "points_original_image.csv")
    try:
        total_number_points = sum(1 for line in open(csv_filepath))
    except:
        configuration_json["error"] = "Error processing this image. Please select another image or download the image and upload it."
        return configuration_json

    tile_size = 256
    log_max =  math.log(max_image_width_height, 2)
    max_int = math.ceil(log_max)
    size_coord = (max_image_width_height * tile_size) / pow(2, max_int)

    if current_task:
        process_percent = int(85)
        estimated_time_finish =  round(30, 2)
        print ("process_percent: ", process_percent, "Task: ", current_task)
        current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                          'estimated_time_finish': estimated_time_finish})

    configuration_json["configuration_json"] = {
        options_algorithm["image_id"] + "_points_csv": {
            "name": str(total_number_points) + "_points_" + options_algorithm["image_name"],
            "cmd": imagej_cmd,
            "type": "csvTiles",
            "url": url_csv_points,
            "options": {
                "tileSize": max_image_width_height,
                "size": max_image_width_height,
                "bounds": [
                    [-size_coord, 0],
                    [0, size_coord]
                ],
                "localRS": True,
                "grid": True,
                "color": "blue",
                "fillColor": "blue",
                "radius": 5,
                "columns": {
                    "x": 0,
                    "y": 1
                }
            }
        }
    }

    return configuration_json



def screenshot_element(full_html_client, element_id_client):
    if current_task:
        process_percent = int(30)
        estimated_time_finish =  round(20, 2)
        print ("process_percent: ", process_percent, "Task: ", current_task)
        current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                          'estimated_time_finish': estimated_time_finish})

    result = ""
    options = Options()
    # FIREFOX
    options.log.level = 'trace'
    options.add_argument("--headless")

    fp = webdriver.FirefoxProfile()
    fp.set_preference("browser.tabs.remote.autostart", False)
    fp.set_preference("browser.tabs.remote.autostart.1", False)
    fp.set_preference("browser.tabs.remote.autostart.2", False)
    #fp.set_preference("network.proxy.no_proxies_on", "localhost, 127.0.0.1")
    firefox_capabilities = DesiredCapabilities.FIREFOX
    firefox_capabilities['marionette'] = True


    binary = FirefoxBinary('/usr/bin/firefox/firefox')

    try:
        if current_task:
            process_percent = int(60)
            estimated_time_finish =  round(15, 2)
            print ("process_percent: ", process_percent, "Task: ", current_task)
            current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                              'estimated_time_finish': estimated_time_finish,
                                                              'pid': os.getpid()
                                                              })

        executable_path = "/geckodriver/geckodriver"
        driver = webdriver.Firefox(firefox_options=options, firefox_profile=fp, capabilities=firefox_capabilities, executable_path=executable_path,
                                   firefox_binary=binary, log_path='/dev/null')

        driver.set_window_size(2000, 1768) # optional
        # driver.execute_script("document.body.style.zoom='250%'")
        driver.set_page_load_timeout(30)
        with tempfile.NamedTemporaryFile(mode='w', suffix=".html") as tmp_file:
            tmp_file.write(full_html_client)
            filename = "file:///" + tmp_file.name
            driver.get(filename)

            image = take_screenshot_firefox(driver, firefox_options=options, firefox_binary=binary, executable_path=executable_path, filename=filename, retry=10)

            map_element = driver.find_element_by_id(element_id_client)

            map_location = map_element.location
            map_size = map_element.size

            driver.quit()

            if current_task:
                process_percent = int(90)
                estimated_time_finish = round(5, 2)
                print ("process_percent: ", process_percent, "Task: ", current_task)
                current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                                  'estimated_time_finish': estimated_time_finish})

            im = Image.open(BytesIO(image))  # uses PIL library to open image in memory

            left = map_location['x']
            top = map_location['y']
            right = map_location['x'] + map_size['width']
            bottom = map_location['y'] + map_size['height']

            im = im.crop((int(left), int(top), int(right), int(bottom)))  # defines crop points
            imgByteArr = io.BytesIO()
            im.save(imgByteArr, format='PNG')
            imgByteArr = imgByteArr.getvalue()

            result = {"error": "", "result": imgByteArr}

    except Exception as e:
        print(e)
        try:
            driver.title #Driver is still alive
            driver.quit()
        except WebDriverException:
            pass #Driver is dead

        result = {"error": "Error exporting the image, please retry. {0}".format(str(e)), "result": ""}

    return result

def take_screenshot_firefox(driver, firefox_options, firefox_binary, executable_path, filename, retry=10):
    try:
        image = driver.get_screenshot_as_png()
    except Exception as e:
        print(e)
        if current_task:
            process_percent = int(80 - retry)
            estimated_time_finish =  round(15 - retry, 2)
            print ("process_percent: ", process_percent, "Task: ", current_task)
            current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                              'estimated_time_finish': estimated_time_finish})
        time.sleep(1);
        retry -= 1
        if retry > 0:
            driver = webdriver.Firefox(firefox_options=firefox_options, executable_path=executable_path,
                                       firefox_binary=firefox_binary)
            driver.get(filename)
            take_screenshot_firefox(driver, firefox_options, firefox_binary, executable_path, filename, retry)
        else:
            raise Exception("")

    return image

