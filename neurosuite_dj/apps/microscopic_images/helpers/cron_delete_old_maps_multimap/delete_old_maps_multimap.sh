#!/bin/bash

LOGS_TO_DOCKER_LOG='/proc/1/fd/1' #For Docker

exec &>> $LOGS_TO_DOCKER_LOG
echo "" &>> $LOGS_TO_DOCKER_LOG
#Examples:
#Remove files older than 10 days:
# -mtime 9
#Remove files older than 24 hours (1440 minutes):
# -mmin +1439

PATH_MAPS_MULTIMAP='/neurosuite_dj/media/multimap_maps/*'
DELETE_OLD_TIME=60 #Delete folders older than 60 minutes

echo "CRON: Cleaning old maps MultiMap"
find $PATH_MAPS_MULTIMAP -mmin +$DELETE_OLD_TIME -delete
