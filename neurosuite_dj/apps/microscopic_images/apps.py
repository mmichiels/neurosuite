from django.apps import AppConfig


class MicroscopicImagesConfig(AppConfig):
    name = 'microscopic_images'
