from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


@apphook_pool.register  # register the application
class MicroscopicImagesApphook(CMSApp):
    app_name = "apps.microscopic_images"
    name = _("Microscopic images")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["apps.microscopic_images.urls"]