from django.conf.urls import url

from . import views


urlpatterns = [
    # -----MultiMap---
    url(r'^multimap', views.multimap, name='multimap'),
    url(r'^multimap_json_loaded', views.multimap_json_loaded, name='multimap_json_loaded'),
    url(r'^api_chunked_upload/?$',  views.MyChunkedUploadView.as_view(), name='api_chunked_upload'),
    url(r'^load_map_image_computer_multimap/?$',  views.Load_map_image_computer_multimap.as_view(),name='load_map_image_computer_multimap'),
    url(r'^load_map_image_url_multimap/?$',  views.load_map_image_url_multimap, name='load_map_image_url_multimap'),
    url(r'^load_layer_image_computer_multimap/?$', views.Load_layer_image_computer_multimap.as_view(), name='load_layer_image_computer_multimap'),
    url(r'^load_layer_image_url_multimap/?$', views.load_layer_image_url_multimap, name='load_layer_image_url_multimap'),
    url(r'^load_drawing_layer_multimap/?$', views.Load_drawing_layer_multimap.as_view(), name='load_drawing_layer_multimap'),
    url(r'^tool_points_multimap/?$', views.tool_points_multimap, name='tool_points_multimap'),

    #Miscellaneous
    url(r'^screenshot_element/?$', views.screenshot_element, name='screenshot_element'),

    #------Root----
    url(r'^$', views.index_microscopic_images, name='index_microscopic_images'),
]