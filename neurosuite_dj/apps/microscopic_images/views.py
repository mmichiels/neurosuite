import io
import json
import logging
import os
import zipfile
import base64
from django.conf import settings
from django.contrib.sessions.models import Session
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.http import HttpResponse
from django.http import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseRedirect,
    Http404
)
import time
import datetime
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.utils.encoding import smart_str
from django.views import View
from django.views.generic.base import TemplateView
import neurosuite_dj.helpers as global_helpers
from io import BytesIO
from io import BytesIO as IO
import requests
import tempfile
from PIL import Image
from selenium import webdriver
#from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

logging.basicConfig(level = logging.DEBUG)
import pyvips
import subprocess
import platform

from celery.result import AsyncResult
import apps.microscopic_images.tasks as tasks
from celery.task.control import revoke

import pandas as pd


from chunked_upload.views import ChunkedUploadView, ChunkedUploadCompleteView
from chunked_upload.exceptions import ChunkedUploadError
from .models import MyChunkedUpload
from .helpers import helpers



# ----Index----------
def index_microscopic_images(request):
    data = {}

    return render(request, 'index_microscopic_images.html', data)

def multimap(request):
    data = {}

    return render(request, 'multimap.html', data)

def multimap_json_loaded(request):
    data = {}
    data["configuration_json_received"] = request.session["configuration_json"]

    return render(request, 'multimap.html', data)

class MyChunkedUploadView(ChunkedUploadView):

    model = MyChunkedUpload
    field_name = 'the_file'

    def check_permissions(self, request):
        # Allow non authenticated users to make uploads
        pass

    def pre_save(self, chunked_upload, request, new=False):
        #If there is low free disk space, the upload will not continue
        global_helpers.create_chunked_upload_folder()
        helpers.validate_disk_space()

class Load_map_image_computer_multimap(ChunkedUploadCompleteView):

    model = MyChunkedUpload

    def check_permissions(self, request):
        # Allow non authenticated users to make uploads
        pass

    #The last call to the frontend
    def get_response_data(self, chunked_upload, request):
        image = {
            "id": chunked_upload.upload_id,
            "name": os.path.splitext(chunked_upload.filename)[0],
            "path": chunked_upload.file.name,
        }

        #This could be a Celery task:
        configuration_json = helpers.process_image_multimap(image)


        return configuration_json

def load_map_image_url_multimap(request):
    if (not request.is_ajax()): #1st time running this method, it will enters here
        if (request.POST.get("task_done_frontend", "") == "1"):
            if 'task_id' in request.POST.keys() and request.POST['task_id']:
                data = get_worker_task_result(request.POST['task_id'])
                context = {}
                if not "configuration_json" in request.session:
                    configuration_json = {
                        "configuration_json": {},
                        "error": "Error downloading the image.",
                        "configuration_name": "",
                        "type": "map",
                    }
                else:
                    configuration_json = request.session["configuration_json"]
                #del request.session["configuration_json"]
                context = {}
                json_to_send = json.dumps(configuration_json)
                context["configuration_json_received"] = json_to_send

                return render(request, 'multimap.html', context)

        image_url_download = request.POST.get("image_url_download_multimap", "")
        create_session_if_not_exists(request)

        if not image_url_download.startswith("data:image/jpeg;base64,"):
            # If there is low free disk space, the upload will not continue
            try:
                r = requests.get(image_url_download, stream=True)
                new_upload_bytes = int(r.headers['Content-length'])
                helpers.validate_disk_space(new_upload_bytes=new_upload_bytes)
            except Exception as e:
                context = {}
                json_to_send = json.dumps({"error": "Error uploading the image. Try another image or download the image and upload it"})
                context["configuration_json_received"] = json_to_send
                return render(request, 'multimap.html', context)

        job = tasks.load_map_image_url_multimap.delay(request.session.session_key, image_url_download)
        #job = tasks.load_map_image_url_multimap(request.session.session_key, image_url_download)

        task = {}
        task["name"] = "MultiMap - Load image from URL"
        task["description"] = "Loading and processing the image from the URL"
        task["url_on_loading"] = "/micro/load_map_image_url_multimap/"
        task["id"] = job.id
        context = {}
        context["task"] = task

        return return_micro_loading_worker_task_html(request, context)
    else: #Rest of time running this method, it will enters here
        return process_loading_worker(request)


class Load_layer_image_computer_multimap(ChunkedUploadCompleteView):

    model = MyChunkedUpload

    def check_permissions(self, request):
        # Allow non authenticated users to make uploads
        pass

    #The last call to the frontend
    def get_response_data(self, chunked_upload, request):
        image = {
            "id": chunked_upload.upload_id,
            "name": os.path.splitext(chunked_upload.filename)[0],
            "path": chunked_upload.file.name,
        }

        #This could be a Celery task:
        configuration_layer_json = helpers.process_layer_multimap(image)
        configuration_map_json = request.POST.get("configuration_map_json", "")
        configuration_name = request.POST.get("configuration_name", "")
        configuration_map_json = json.loads(configuration_map_json)
        configuration_map_json = helpers.add_layer_json_multimap(configuration_map_json, configuration_layer_json, configuration_name)

        return configuration_map_json


def load_layer_image_url_multimap(request):
    if (not request.is_ajax()): #1st time running this method, it will enters here
        if (request.POST.get("task_done_frontend", "") == "1"):
            if 'task_id' in request.POST.keys() and request.POST['task_id']:
                data = get_worker_task_result(request.POST['task_id'])
                context = {}
                if not "configuration_json" in request.session:
                    configuration_json = {
                        "configuration_json": {},
                        "error": "Error downloading the image.",
                        "configuration_name": "",
                        "type": "map",
                    }
                else:
                    configuration_json = request.session["configuration_json"]
                #del request.session["configuration_json"]
                context = {}
                json_to_send = json.dumps(configuration_json)
                context["configuration_json_received"] = json_to_send

                return render(request, 'multimap.html', context)

        image_url_download = request.POST.get("image_url_layer_download_multimap", "")
        configuration_map_json_string = request.POST.get("configuration_json_in_html", "")
        configuration_name = request.POST.get("configuration_name_in_html", "")
        create_session_if_not_exists(request)

        if not image_url_download.startswith("data:image/jpeg;base64,"):
            # If there is low free disk space, the upload will not continue
            try:
                r = requests.get(image_url_download, stream=True)
                new_upload_bytes = int(r.headers['Content-length'])
                helpers.validate_disk_space(new_upload_bytes=new_upload_bytes)
            except Exception as e:
                context = {}
                configuration_json_send = {
                    "configuration_json": json.loads(configuration_map_json_string),
                    "type": "map",
                    "configuration_name": configuration_name,
                    "error": "",
                    "configuration_warning": "Error uploading the image. Try another image or download the image and upload it",
                }
                json_to_send = json.dumps(configuration_json_send)
                context["configuration_json_received"] = json_to_send
                return render(request, 'multimap.html', context)

        job = tasks.load_layer_image_url_multimap.delay(request.session.session_key, image_url_download, configuration_map_json_string, configuration_name)
        #job = tasks.load_layer_image_url_multimap(request.session.session_key, image_url_download, configuration_map_json_string, configuration_name)

        task = {}
        task["name"] = "MultiMap - Load layer image from URL"
        task["description"] = "Loading and processing the image from the URL"
        task["url_on_loading"] = "/micro/load_layer_image_url_multimap/"
        task["id"] = job.id
        context = {}
        context["task"] = task

        return return_micro_loading_worker_task_html(request, context)
    else: #Rest of time running this method, it will enters here
        return process_loading_worker(request)

class Load_drawing_layer_multimap(ChunkedUploadCompleteView):

    model = MyChunkedUpload

    def check_permissions(self, request):
        # Allow non authenticated users to make uploads
        pass

    #The last call to the frontend
    def get_response_data(self, chunked_upload, request):

        file_json = chunked_upload.file.read()
        try:
            configuration_drawing_layer_json = json.loads(file_json)
        except Exception as e:
            configuration_drawing_layer_json = {"error": "Invalid json file loaded"}
        configuration_map_json = request.POST.get("configuration_map_json", "")
        configuration_name = request.POST.get("configuration_name", "")
        configuration_map_json_original = json.loads(configuration_map_json)
        configuration_map_json = helpers.change_drawing_layer_json_multimap(configuration_map_json_original, configuration_drawing_layer_json, configuration_name)

        return configuration_map_json


def tool_points_multimap(request):
    if (not request.is_ajax()): #1st time running this method, it will enters here
        if (request.POST.get("task_done_frontend", "") == "1"):
            if 'task_id' in request.POST.keys() and request.POST['task_id']:
                data = get_worker_task_result(request.POST['task_id'])
                context = {}
                if not "configuration_json" in request.session:
                    configuration_json = {
                        "configuration_json": {},
                        "error": "Error downloading the image.",
                        "configuration_name": "",
                        "type": "map",
                    }
                else:
                    configuration_json = request.session["configuration_json"]
                context = {}
                json_to_send = json.dumps(configuration_json)
                context["configuration_json_received"] = json_to_send
                #del request.session["configuration_json"]

                return render(request, 'multimap.html', context)

        options_algorithm = {}
        args_algorithm = {}
        options_algorithm["imagej_path"] = settings.IMAGE_J_PATH
        options_algorithm["imagej_plugins_path"] = settings.IMAGE_J_PLUGINS_PATH

        options_algorithm["image_id"] = request.POST.get("actual_image_id", "")
        options_algorithm["image_name"] = request.POST.get("actual_image_name", "")
        options_algorithm["image_name_path"] = options_algorithm["image_id"] + os.sep + "original_image"
        options_algorithm["image_path"] = settings.MULTIMAP_DIR + os.sep + options_algorithm["image_name_path"]
        options_algorithm["folder_output"] = "output_object_detection"
        options_algorithm["dir_output"] = settings.MULTIMAP_DIR + os.sep + options_algorithm[
            "image_id"] + os.sep + options_algorithm["folder_output"]
        options_algorithm["folder_output_objects"] = "objects"
        options_algorithm["folder_output_points"] = "points"
        options_algorithm["dir_output_objects"] = os.path.join(options_algorithm["dir_output"], options_algorithm["folder_output_objects"])
        options_algorithm["dir_output_points"] = os.path.join(options_algorithm["dir_output"], options_algorithm["folder_output_points"])
        # Create dir_output:
        if not os.path.exists(options_algorithm["dir_output"]):
            os.makedirs(options_algorithm["dir_output"])
        options_algorithm["object_detector_macro"] = settings.IMAGE_J_MACROS_PATH + os.sep + "ObjectDetector.ijm"
        options_algorithm["configuration_map_json_string"] = request.POST.get("configuration_map_json", "")

        args_algorithm["mode"] = "0"  # 0 = SINGLE_IMAGE
        args_algorithm["path"] = options_algorithm["image_path"]
        args_algorithm["minimum_radius"] = request.POST.get("tool_points_minimum_radius", "")
        args_algorithm["maximum_radius"] = request.POST.get("tool_points_maximum_radius", "")
        args_algorithm["by"] = request.POST.get("tool_points_by", "")
        args_algorithm["threshold_method"] = request.POST.get("tool_points_threshold_method", "")
        args_algorithm["minimum"] = request.POST.get("tool_points_minimum", "")
        args_algorithm["maximum"] = request.POST.get("tool_points_maximum", "")
        args_algorithm["fraction"] = request.POST.get("tool_points_fraction", "")
        args_algorithm["tollerance"] = request.POST.get("tool_points_tollerance", "")
        args_algorithm["dir_output"] = options_algorithm["dir_output"]
        create_session_if_not_exists(request)

        job = tasks.tool_points_multimap.delay(request.session.session_key, options_algorithm, args_algorithm)
        #job = tasks.tool_points_multimap(request.session.session_key, options_algorithm, args_algorithm)



        task = {}
        task["name"] = "MultiMap - Points detection"
        task["description"] = "Detecting points..."
        task["url_on_loading"] = "/micro/tool_points_multimap/"
        task["id"] = job.id
        context = {}
        context["task"] = task

        return return_micro_loading_worker_task_html(request, context)
    else: #Rest of time running this method, it will enters here
        return process_loading_worker(request)

# ---------------------Miscellaneous-----------------------------------------------
def screenshot_element(request):
    if (not request.is_ajax()): #1st time running this method, it will enters here
        if (request.POST.get("task_done_frontend", "") == "1"):
            if 'task_id' in request.POST.keys() and request.POST['task_id']:
                data = get_worker_task_result(request.POST['task_id'])
                context = {}
                screenshot_multimap= request.session["screenshot_multimap"]
                if "done" in screenshot_multimap:
                    context = {}
                    configuration_json_to_send = {
                        "configuration_json": screenshot_multimap["configuration_json"],
                        "error": "",
                        "configuration_name": screenshot_multimap["configuration_name"],
                        "type": "map",
                    }
                    context["configuration_json_received"] = json.dumps(configuration_json_to_send)
                    # del request.session["configuration_json"]

                    return render(request, 'multimap.html', context)

                if screenshot_multimap["screenshot"]["error"] != "":
                    configuration_json_to_send = {
                        "configuration_json": screenshot_multimap["configuration_json"],
                        "error": "",
                        "configuration_name": screenshot_multimap["configuration_name"],
                        "configuration_warning": screenshot_multimap["screenshot"]["error"],
                        "type": "map",
                    }
                    json_to_send = json.dumps(configuration_json_to_send)
                    context["configuration_json_received"] = json_to_send

                    return render(request, 'multimap.html', context)
                else:
                    screenshot_multimap["done"] = True
                    request.session.save()
                    request.session.modified = True
                    response = HttpResponse(content_type='application/force-download')
                    response['Content-Disposition'] = 'attachment;filename="{0}.png"'.format(screenshot_multimap["screenshot_name"])
                    response.write(screenshot_multimap["screenshot"]["result"])

                    return response

        full_html_client = request.POST.get("full_html_client", "")
        full_html_client = (base64.b64decode(full_html_client)).decode("utf8", "ignore")
        full_html_client = full_html_client.replace(settings.HOST_URL_WITH_PORT, settings.PROJECT_ROOT)
        full_html_client = full_html_client.replace("/static", os.path.join(settings.PROJECT_ROOT, "static/"))
        full_html_client = "<!doctype html><html>" + full_html_client + "</html>"
        element_id_client = request.POST.get("element_id_client", "")
        screenshot_name = request.POST.get("screenshot_name", "")
        configuration_map_json_string = request.POST.get("configuration_map_json_image", "")
        configuration_name = request.POST.get("configuration_name_json_image", "")
        create_session_if_not_exists(request)

        job = tasks.screenshot_element.delay(request.session.session_key, full_html_client, element_id_client, screenshot_name, configuration_map_json_string, configuration_name)
        #job = tasks.screenshot_element(request.session.session_key, full_html_client, element_id_client, screenshot_name, configuration_map_json_string, configuration_name)

        task = {}
        task["name"] = "Multimap - Export map as image"
        task["description"] = "Exporting the map..."
        task["url_on_loading"] = "/micro/screenshot_element/"
        task["id"] = job.id
        context = {}
        context["task"] = task
        context["cancel_possible"] = 0

        return return_micro_loading_worker_task_html(request, context)
    else: #Rest of time running this method, it will enters here
        return process_loading_worker(request)



# ---------------------Worker task general methods-----------------------------------------------
def process_loading_worker(request):
    if 'task_id' in request.POST.keys() and request.POST['task_id']:
        try:
            data = get_worker_task_result(request.POST['task_id'])
            if data and "done" in data and data["done"]:
                data["processed_django"] = True

            json_data = json.dumps(data)
            return HttpResponse(json_data, content_type='application/json')
        except ValueError:  # includes simplejson.decoder.JSONDecodeError
            print ('Decoding JSON has failed')
            return return_micro_loading_worker_task_html(request, context)
    else:
        data = 'No task_id in the request'
        context = {}
        return return_micro_loading_worker_task_html(request, context)

def return_micro_loading_worker_task_html(request, context):
    context["sidebar"] = "sidebar_select_software_micro.html"
    context["send_to_background_url"] = "/micro"
    if not "cancel_possible" in context:
        context["cancel_possible"] = 1
    return render(request, "loading_worker_task.html", context)


def get_worker_task_result(task_id):
    task = AsyncResult(task_id)
    data = task.result or task.state

    return data

def revoke_worker_task(request):
    task_id = request.POST.get("task_id", "")
    result = revoke(task_id, terminate=True, signal='SIGKILL')

    data = {"result": result}
    json_data = json.dumps(data)
    return HttpResponse(json_data, content_type='application/json')

def create_session_if_not_exists(request):
    if request.session.session_key is None:
        request.session.save()
        request.session.modified = True
