from celery import shared_task,current_task
from celery.contrib import rdb
from celery.result import AsyncResult
from celery.task.control import revoke
import json
from django.contrib.sessions.models import Session
from django.contrib.sessions.backends.db import SessionStore
from .helpers import helpers
import time
import os
from django.conf import settings


@shared_task
def load_map_image_url_multimap(session_id, image_url_download):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)

    time_now = time.strftime("%Y_%m_%d_%H_%M_%S")
    image_name = os.path.splitext(image_url_download.split('/')[-1])[0]
    image_id = image_name + "_" + time_now

    image = {
        "id": image_id,
        "name": image_name,
        "path": os.path.join(settings.CHUNKED_UPLOAD_PATH, image_id),
        "url_download": image_url_download,
    }

    result = helpers.download_file_from_url(image)
    configuration_json = {}
    if result["error"] != "":
        configuration_json["error"] = result["error"]
    else:
        configuration_json = helpers.process_image_multimap(image)

    session["configuration_json"] = configuration_json

    session.save()
    session.modified = True
    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id
              }

    return result

@shared_task
def load_layer_image_url_multimap(session_id, image_url_download, configuration_map_json_string, configuration_name):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)

    time_now = time.strftime("%Y_%m_%d_%H_%M_%S")
    image_name = os.path.splitext(image_url_download.split('/')[-1])[0]
    image_id = image_name + "_" + time_now

    image = {
        "id": image_id,
        "name": image_name,
        "path": os.path.join(settings.CHUNKED_UPLOAD_PATH, image_id),
        "url_download": image_url_download,
    }

    result = helpers.download_file_from_url(image)
    if result["error"] != "":
        configuration_layer_json["error"] = result["error"]
    else:
        configuration_layer_json = helpers.process_layer_multimap(image)
    configuration_map_json = json.loads(configuration_map_json_string)
    configuration_json = helpers.add_layer_json_multimap(configuration_map_json, configuration_layer_json, configuration_name)

    session["configuration_json"] = configuration_json

    session.save()
    session.modified = True
    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id
              }

    return result

@shared_task
def tool_points_multimap(session_id, options_algorithm, args_algorithm):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)

    configuration_csv_tiles_json = helpers.run_tool_points_detection_multimap(options_algorithm, args_algorithm)
    configuration_map_json_string = options_algorithm["configuration_map_json_string"]
    configuration_name = options_algorithm["image_name"]
    configuration_map_json = json.loads(configuration_map_json_string)

    configuration_json = helpers.add_layer_json_multimap(configuration_map_json, configuration_csv_tiles_json, configuration_name)

    session["configuration_json"] = configuration_json
    session.save()
    session.modified = True
    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id
              }

    return result

@shared_task
def screenshot_element(session_id, full_html_client, element_id_client, screenshot_name, configuration_map_json_string, configuration_name):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)

    #Get PID of this task (worker):
    pid_worker = os.getpid()
    print("Worker PID: ", pid_worker)

    result = helpers.screenshot_element(full_html_client, element_id_client)
    session["screenshot_multimap"] = {"screenshot": result, "screenshot_name": screenshot_name, "configuration_json": configuration_map_json_string, "configuration_name": configuration_name}

    session.save()
    session.modified = True
    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id,
              "pid": pid_worker,
              }

    return result


