from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


@apphook_pool.register  # register the application
class MorphoAnalyzerApphook(CMSApp):
    app_name = "apps.morpho_analyzer"
    name = _("Morpho analyzer")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["apps.morpho_analyzer.urls"]