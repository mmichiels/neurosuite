from celery import task,shared_task,current_task
from numpy import random
from scipy.fftpack import fft
from celery.contrib import rdb
import json
from .helpers.get_data import neuromorpho_api_client
import pydevd
from .helpers.get_data.get_data import *
from .helpers.get_data.get_features import *
from datetime import datetime, timezone
import subprocess
from django.core import management
from django.conf import settings
from django.contrib.sessions.models import Session
from django.contrib.sessions.backends.db import SessionStore
import apps.morpho_analyzer.helpers.filter_data.check_neurons as check_neurons
from .helpers import helpers
import pandas as pd
import neurosuite_dj.helpers_global.dataset as dataset_helper
import neurosuite_dj.helpers_global.datasets_user as datasets_user_helper
from apps.morpho_analyzer.helpers.stats import stats_helpers
from apps.morpho_analyzer.helpers.machine_learning import machine_learning_helpers as ml_helpers
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import io as bn_io
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import models as bn_models
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import learn_structure as bn_learn_structure
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import learn_parameters as bn_learn_parameters
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import plotting as bn_plotting
from apps.morpho_analyzer.helpers.machine_learning.supervised_classification.evaluation import supervised_classification_metrics
from apps.morpho_analyzer.helpers.machine_learning.supervised_classification import models as supervised_class_models
from apps.morpho_analyzer.helpers.machine_learning.non_probabilistic_clustering import models as non_probabilistic_clustering
from apps.morpho_analyzer.helpers.preprocess import preprocess as preprocess

@shared_task
def search_neuromorpho_query(query_data, custom_options, session_id):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)
    print ("query_data is", query_data)
    print ("custom_options is", custom_options)

    if query_data == "":
        query_data = "{}"
    if custom_options == "":
        custom_options = "{}"
    query_data_json = json.loads(query_data)
    custom_options_json = json.loads(custom_options)

    print ("Query data", query_data)
    print ("Custom options", custom_options)
    #Send query to neuromorpho
    #query_params = neuromorpho_api_client.parse_json_to_neuromorpho_query(query_data_json)
    results = neuromorpho_api_client.search_neurons_query(query_data_json, custom_options_json)
    file_extension = ".swc"
    i = 0
    neurons_user = []
    if isinstance(results, list):
        for neuron in results:
            neuron = {
                "name": neuron["neuron_name"],
                "data_file": {
                    "file": neuromorpho_api_client.get_url_swc_neuron(neuron),
                    "extension": file_extension,
                    "lazy_load": True
                },
                "additional_data": neuron,
            }

            neurons_user.append(neuron)
            #print ("Storing in user session neuron from neuromorpho:", neuron["name"])
            i += 1

    label_column_id = "neuron_name"
    label_instances = "Neurons"
    datasets_user = datasets_user_helper.DatasetsUser(app_name=settings.MORPHO_ANALYZER_APP_NAME, has_input_dataset=True, has_morpho_reconstructions=True)
    dataframe_input_all = helpers.neurons_additional_data_to_dataframe(neurons_user, label_column_id=label_column_id)
    datasets_user.global_label_column_id = label_column_id
    datasets_user.global_label_instances = label_instances

    session["neurons_user"] = neurons_user
    session.save()
    session.modified = True
    dataset_helper.initialize_all_dataframes_data(session_id, datasets_user, dataframe_input_all)
    datasets_user.save(session)

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id
              }

    return result

@shared_task
def morpho_l_measure(session_id, valid_neurons):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)
    datasets_user = session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

    if datasets_user.has_morpho_reconstructions:
        neurons_user = session["neurons_user"]
        data_l_measure = get_features(neurons_user, valid_neurons)
        dataset = dataset_helper.Dataset("l_measure", session_id, app_name=settings.MORPHO_ANALYZER_APP_NAME)
        dataset.load(data_l_measure["dataframe"], label_column_id="Neuron name", label_instances=datasets_user.global_label_instances, instances_error=data_l_measure["data_errors"])
        datasets_user.add([dataset])

        session.save()
        session.modified = True
        datasets_user.save(session)

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id
              }

    return result

@shared_task
def morpho_validator_neurostr(session_id):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)
    neurons_user = session["neurons_user"]

    neurostr_validator_neurons = check_neurons.check_neurons(neurons_user)
    session["neurostr_validator_neurons"] = neurostr_validator_neurons

    session.save()
    session.modified = True

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id
              }




    return result

@shared_task
def morpho_all_format_converter_neurostr(session_id, output_format, correct, simplification_algorithm):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)
    neurons_user = session["neurons_user"]

    converted_neurons = helpers.morpho_all_format_converter_neurostr(neurons_user, output_format, correct, simplification_algorithm)
    session["converted_neurons"] = converted_neurons

    session.save()
    session.modified = True
    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id
              }

    return result

@shared_task
def morpho_download_all_original_swc_neuromorpho(session_id):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)
    neurons_user = session["neurons_user"]

    original_neurons_swc = helpers.morpho_download_all_original_swc_neuromorpho(neurons_user)
    session["original_neurons_swc"] = original_neurons_swc

    session.save()
    session.modified = True
    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id
              }

    return result

@shared_task
def morpho_repair_all_neurons_3DBasalRM(session_id, output_format):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)
    neurons_user = session["neurons_user"]

    neurons_repaired = helpers.morpho_repair_all_neurons_3DBasalRM(neurons_user, output_format)
    session["neurons_repaired"] = neurons_repaired

    session.save()
    session.modified = True
    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id
              }

    return result

@shared_task
def morpho_gabaclassifier(session_id, valid_neurons):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)
    neurons_user = session["neurons_user"]

    gabaclassifier_results = helpers.morpho_classify_all_interneuron_gabaclassifier(neurons_user, valid_neurons)
    session["gabaclassifier_results"] = gabaclassifier_results

    session.save()
    session.modified = True

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id
              }


    return result

@shared_task
def ml_bn_upload_dataset(session_id, params):
    print ("SESSION_ID:", session_id)
    current_task.update_state(state='PROGRESS',meta={'process_percent': 0, 'estimated_time_finish': 0})

    bn_name = params["bn_name"]
    datasets_names = params["datasets_names"]
    dataframes_features = params["dataframes_features"]
    dataframes_features_classes = params["dataframes_features_classes"]
    discretize = params["discretize"]

    session = SessionStore(session_key=session_id)

    data = {}
    try:
        if bn_name == "":
            bn_name = "BN"
        if discretize:
            discretize_method = params["discretize_method"]
        if len(datasets_names) == 0:
            data["error"] = True
            data["error_message"] = "Error: Select a dataset and features or upload a bayesian network file"
        elif not dataframes_features["selected_all"] and len(dataframes_features["values"]) < 2:
            data["error"] = True
            data["error_message"] = "Error: Select two or more features; Or upload a bayesian network file"
        else:
            datasets_user = session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

            if dataframes_features["selected_all"]:
                all_columns_dataframe = None
            else:
                all_columns_dataframe = list(set(dataframes_features["values"] + dataframes_features_classes))
                all_columns_dataframe.sort()

            dataframe = dataset_helper.merge_session_dataframes(datasets_user, datasets_names,
                                                                all_columns_dataframe, include_ids=False)
            if discretize:
                discretize_parameters = params["discretize_method_parameters"]
                dataframe = ml_helpers.discretize_data(dataframe, dataframes_features_classes, discretize_method,
                                                       discretize_parameters)

            dataset = dataset_helper.Dataset("ml_bayesian_network", session.session_key,
                                             app_name=settings.MORPHO_ANALYZER_APP_NAME, delete=True)
            dataset.load(dataframe)

            bn = bn_models.BayesianNetwork(name=bn_name, dataset=dataset, is_uploaded_bn_file=False,
                                           model_original_import={}, features_classes=dataframes_features_classes, session_id=session_id)

            session["ml_bayesian_network"] = bn
            session.save()
            session.modified = True
            data["error"] = False
    except Exception as e:
        data["error"] = True
        data["error_message"] = str(e)

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id,
              "data_processed": data
              }

    return result

@shared_task
def ml_bn_learn_structure(session_id, params):
    print ("SESSION_ID:", session_id)
    current_task.update_state(state='PROGRESS',meta={'process_percent': 0, 'estimated_time_finish': 0})

    structure_algorithm = params["structure_algorithm"]
    structure_algorithm_parameters = params["structure_algorithm_parameters"]
    backend = structure_algorithm_parameters["backend"]

    session = SessionStore(session_key=session_id)

    try:
        data = {}
        bn = session["ml_bayesian_network"]

        bn_dataframe = bn.dataset.get_dataframe()
        bn_structure = bn_learn_structure.LearnStructure.get_structure_learning_class(structure_algorithm,
                                                                                      structure_algorithm_parameters,
                                                                                      bn_dataframe, bn.data_type, bn.features_classes,
                                                                                      session_id=session_id)
        bn.graph = bn_structure.run(backend)
        bn.weights_info = bn.init_graph_edges_weights(bn.graph)
        bn.order_topological = bn.set_topological_order(bn.graph)
        bn.original_graph = bn.graph.copy()

        bn_plot = bn_plotting.BnPlot(bn)
        data = bn_plot.draw_networkx_sigmajs(layout_name="dot")
        data["error"] = False
    except Exception as e:
        data["error"] = True
        data["error_message"] = str(e)

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id,
              "data_processed": data
              }

    session.save()
    session.modified = True

    return result

@shared_task
def ml_bn_learn_parameters(session_id, params):
    print ("SESSION_ID:", session_id)
    current_task.update_state(state='PROGRESS',meta={'process_percent': 0, 'estimated_time_finish': 0})

    parameters_algorithm = params["parameters_algorithm"]
    algorithm_parameters = params["algorithm_parameters"]
    backend = algorithm_parameters["backend"]

    session = SessionStore(session_key=session_id)

    try:
        data = {}
        bn = session["ml_bayesian_network"]

        bn_dataframe = bn.dataset.get_dataframe()
        bn_parameters = bn_learn_parameters.LearnParameters.get_parameters_learning_class(parameters_algorithm, bn_dataframe, bn.data_type, bn.graph, algorithm_parameters)
        bn.parameters = bn_parameters.run(backend)
        bn.set_nodes_parameters_continuous(bn.parameters)

        bn_plot = bn_plotting.BnPlot(bn)
        data = bn_plot.draw_networkx_sigmajs(layout_name="dot")
        data["error"] = False
    except Exception as e:
        data["error"] = True
        data["error_message"] = str(e)

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id,
              "data_processed": data
              }

    session.save()
    session.modified = True

    return result

@shared_task
def read_dataset(session_id, dataset_params):
    print ("SESSION_ID:", session_id)
    session = SessionStore(session_key=session_id)
    dataset_helper.fix_last_character_encoding_tus(dataset_params["path"])

    datasets_user = datasets_user_helper.DatasetsUser(dataset_params["app"])
    dataframe_input_all = dataset_helper.pandas_read_dataset(dataset_params["path"])
    dataframe_input_all, label_column_id, label_instances = dataset_helper.get_label_column_id_and_instances(dataframe_input_all, dataset_params["label_column_id"], dataset_params["label_instances"])
    datasets_user.global_label_column_id = label_column_id
    datasets_user.global_label_instances = label_instances

    os.remove(dataset_params["path"])  # Remove output file because now is saved in the datasets session folder
    session.save()
    session.modified = True
    dataset_helper.initialize_all_dataframes_data(session_id, datasets_user, dataframe_input_all)
    datasets_user.save(session) #TODO: Include in session.save signal

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id
              }

    return result


@shared_task
def ml_supervised_class_features_selection(session_id, params):
    print ("SESSION_ID:", session_id)
    current_task.update_state(state='PROGRESS',meta={'process_percent': 0, 'estimated_time_finish': 0})
    print("params:", params)
    features_preprocessing = params["features_preprocessing"]
    datasets_class_names = params["datasets_class_names"]
    datasets_features_names = params["datasets_features_names"]
    dataframes_features = params["dataframes_features"]
    dataframes_features_classes = params["dataframes_features_classes"]

    session = SessionStore(session_key=session_id)

    data = {}
    try:
        if len(datasets_class_names) == 0 or len(datasets_features_names) == 0:
            data["error"] = True
            data["error_message"] = "Error: Select a dataset and features or upload a bayesian network file"
        else:
            datasets_user = session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

            # Ids
            id_column = datasets_user.datasets[datasets_features_names[0]].label_column_id

            # Predictor features
            columns_features = list(set(dataframes_features["values"]))
            columns_features.sort()

            if dataframes_features["selected_all"]:
                columns_features = None

            dataframe_features = dataset_helper.merge_session_dataframes(datasets_user, datasets_features_names,
                                                                         columns_features, include_ids=True)

            # Class features

            columns_classes = list(set(dataframes_features_classes["values"]))
            columns_classes.sort()

            if dataframes_features_classes["selected_all"]:
                columns_classes = None

            dataframe_classes = dataset_helper.merge_session_dataframes(datasets_user, datasets_class_names,
                                                                        columns_classes, include_ids=False)

            cols_features = list(set(dataframe_features) - set(dataframe_classes))
            cols_classes = list(set(dataframe_classes) - set(dataframe_features))
            dataframe = pd.merge(dataframe_features[cols_features], dataframe_classes[cols_classes], left_index=True, right_index=True,
                                 how='outer')

            dataset = dataset_helper.Dataset("ml_supervised_classification", session.session_key,
                                             app_name=settings.MORPHO_ANALYZER_APP_NAME, delete=True)

            # pre_process dataset
            dataframe, summary = preprocess.do_preprocess(dataframe, features_preprocessing, cols_classes, id_column)

            dataset.load(dataframe)

            supervised_classification = supervised_class_models.SupervisedClassification("example_dataset", dataframe,
                                                                                         id_column, cols_classes,
                                                                                         session.session_key)

            session["ml_supervised_classification"] = supervised_classification
            session.save()
            session.modified = True
            data["error"] = False
            data["summary"] = summary
    except Exception as e:
        data["error"] = True
        data["error_message"] = str(e)

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id,
              "data_processed": data,
              }

    return result


@shared_task
def ml_supervised_class_learn(session_id, params):
    print ("SESSION_ID:", session_id)
    current_task.update_state(state='PROGRESS',meta={'process_percent': 0, 'estimated_time_finish': 0})

    learning_algorithms = params["learning_algorithms"]
    learning_methods = params["learning_methods"]

    session = SessionStore(session_key=session_id)

    try:
        data = {}
        supervised_classification = session["ml_supervised_classification"]

        supervised_classification.models = {} # Clean old models
        x_train, y_train, x_test, y_test = supervised_classification.get_dataframes_x_y()

        metaclassifiers_keys = ["bagging", "boosting", "stacking"]
        metaclassifiers_algorithms = {}

        i = 1

        if len(learning_methods) > 0:
            # Multi-label flow
            for method_name, params_method in learning_methods.items():
                start_time = time.time()

                model = supervised_classification.get_multilabel_transformation_method(method_name, params_method, learning_algorithms)
                model.run(x_train, y_train, x_test, y_test, supervised_classification.k_folds)
                supervised_classification.add_model(model)

                if current_task:
                    end_time = time.time()
                    process_percent = int(100 * float(i) / float(len(learning_algorithms.keys())+2))
                    estimated_time_finish = round((end_time - start_time) * (len(learning_algorithms.keys())+2 - float(i)), 2)
                    current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                                      'estimated_time_finish': estimated_time_finish})
                i += 1
            supervised_classification.summary_evaluation = supervised_classification_metrics.create_all_df_metrics_multioutput(
                supervised_classification)
        else:
            # Normal classification flow
            for algorithm_name, params_algorithm in learning_algorithms.items():
                start_time = time.time()

                if algorithm_name in metaclassifiers_keys:
                    metaclassifiers_algorithms[algorithm_name] = params_algorithm
                else:
                    model = supervised_classification.get_learning_class(algorithm_name, params_algorithm)
                    model.run(x_train, y_train, x_test, y_test, supervised_classification.k_folds, params_algorithm["backend"])
                    supervised_classification.add_model(model)

                if current_task:
                    end_time = time.time()
                    process_percent = int(100 * float(i) / float(len(learning_algorithms.keys())+2))
                    estimated_time_finish = round((end_time - start_time) * (len(learning_algorithms.keys())+2 - float(i)), 2)
                    current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                                      'estimated_time_finish': estimated_time_finish})
                i += 1

            for algorithm_name, params_algorithm in metaclassifiers_algorithms.items():
                model = supervised_classification.get_learning_class(algorithm_name, params_algorithm)
                model.select_estimators(supervised_classification.models)
                model.run(x_train, y_train, x_test, y_test, supervised_classification.k_folds, params_algorithm["backend"])
                supervised_classification.add_model(model)

            supervised_classification.summary_evaluation = supervised_classification_metrics.create_all_df_metrics(supervised_classification)

        pd.set_option('display.max_colwidth', -1)
        data["models_evaluation_training_val_table"] = supervised_classification.summary_evaluation["training_val"].to_html(
            classes="datatable-new-ajax display no-border table-dataframe-html-with-stats table-with-stats", escape=False, border=0)
        data["models_evaluation_test_table"] = supervised_classification.summary_evaluation["test"].to_html(
            classes="datatable-new-ajax display no-border table-dataframe-html-with-stats table-with-stats", escape=False, border=0)

        data["error"] = False
    except Exception as e:
        data["error"] = True
        data["error_message"] = str(e)

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id,
              "data_processed": data
              }

    session.save()
    session.modified = True

    return result

@shared_task
def ml_non_probabilistic_clustering_features_selection(session_id, params):
    print ("SESSION_ID:", session_id)
    current_task.update_state(state='PROGRESS',meta={'process_percent': 0, 'estimated_time_finish': 0})
    print("params:", params)
    features_preprocessing = params["features_preprocessing"]
    datasets_features_names = params["datasets_features_names"]
    dataframes_features = params["dataframes_features"]

    session = SessionStore(session_key=session_id)

    data = {}
    try:
        if len(datasets_features_names) == 0:
            data["error"] = True
            data["error_message"] = "Error: Select a dataset and features or upload a bayesian network file"
        else:
            datasets_user = session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

            # Ids
            id_column = datasets_user.datasets[datasets_features_names[0]].label_column_id

            # Predictor features
            columns_features = list(set(dataframes_features["values"]))
            columns_features.sort()

            if dataframes_features["selected_all"]:
                columns_features = None

            dataframe_features = dataset_helper.merge_session_dataframes(datasets_user, datasets_features_names,
                                                                         columns_features, include_ids=True)

            cols_features = list(set(dataframe_features))

            dataframe = dataframe_features[cols_features]

            dataset = dataset_helper.Dataset("ml_supervised_classification", session.session_key,
                                             app_name=settings.MORPHO_ANALYZER_APP_NAME, delete=True)

            # pre_process dataset
            dataframe, summary = preprocess.do_preprocess(dataframe, features_preprocessing, None, id_column)

            dataset.load(dataframe)

            non_probabilistic_clustering_object = non_probabilistic_clustering.NonProbabilisticClustering("example_dataset", dataframe,
                                                                                         id_column,
                                                                                         session.session_key)

            session["non_probabilistic_clustering"] = non_probabilistic_clustering_object
            session.save()
            session.modified = True
            data["error"] = False
    except Exception as e:
        data["error"] = True
        data["error_message"] = str(e)

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id,
              "data_processed": data,
              }

    return result

@shared_task
def ml_non_probabilistic_class_learn(session_id, params):
    print ("SESSION_ID:", session_id)
    current_task.update_state(state='PROGRESS',meta={'process_percent': 0, 'estimated_time_finish': 0})

    learning_algorithm = params["learning_algorithm"]

    session = SessionStore(session_key=session_id)

    try:
        data = {}
        non_probabilistic_clustering = session["non_probabilistic_clustering"]

        non_probabilistic_clustering.model = {} # Clean old models
        dataframe = non_probabilistic_clustering.dataframe

        algorithm_name = learning_algorithm["algorithm_name"]
        params_algorithm = learning_algorithm

        model = non_probabilistic_clustering.get_learning_class(algorithm_name, params_algorithm)
        model.run(dataframe, params_algorithm["backend"])
        non_probabilistic_clustering.add_model(model)

        current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})

        data["summary"] = non_probabilistic_clustering.get_summary()
        data["summary"]["features"] = json.dumps(non_probabilistic_clustering.get_features().tolist())

        data["error"] = False
    except Exception as e:
        data["error"] = True
        data["error_message"] = str(e)

    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id,
              "data_processed": data
              }

    session.save()
    session.modified = True

    return result