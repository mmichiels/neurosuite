from django import template
register = template.Library()

@register.filter
def value_from_index(List, i):
    return List[int(i)]