from django.apps import AppConfig


class MorphoAnalyzerConfig(AppConfig):
    name = 'morpho_analyzer'
