import os
import io
import random
from base64 import b64encode
from io import BytesIO as IO

from django.core.files.uploadedfile import InMemoryUploadedFile
from django.shortcuts import render, redirect, render_to_response
from django.template.loader import render_to_string, get_template
from django.http import HttpResponse
from django.http import JsonResponse
from wsgiref.util import FileWrapper
from django.contrib.sessions.models import Session
from django.utils.encoding import smart_str
from django.conf import settings
from django.core.files.temp import NamedTemporaryFile
import mimetypes
import json
import zipfile
from io import BytesIO
import requests

import apps.morpho_analyzer.helpers.filter_data.check_neurons as check_neurons
from .helpers.get_data.get_data import *
from .helpers.get_data.get_features import *
from .helpers.get_data import neuromorpho_api_client
from .helpers import helpers
import apps.morpho_analyzer.helpers.neurostr.wrapper as neurostr
import apps.morpho_analyzer.helpers.gabaclassifier.wrapper_py as gabaclassifier
import neurosuite_dj.helpers as global_helpers
from django.contrib.sessions.models import Session
from django.contrib.sessions.backends.db import SessionStore

from celery.result import AsyncResult
import apps.morpho_analyzer.tasks as tasks
from celery.task.control import revoke
import celery.events.state
from htmlmin.decorators import not_minified_response
from datetime import datetime
import pandas as pd
import numpy as np
import shutil
import copy
import line_profiler
import networkx as nx
import neurosuite_dj.helpers_global.dataset as dataset_helper
import neurosuite_dj.helpers_global.datasets_user as datasets_user_helper
from chunked_upload.views import ChunkedUploadView, ChunkedUploadCompleteView
from chunked_upload.exceptions import ChunkedUploadError
from .models import ChunkedUploadCsv
import neurosuite_dj.helpers_global.dataframe_table_dynamic_api as dataframe_table_dynamic_api


from apps.morpho_analyzer.helpers.stats import plotly_helpers
from apps.morpho_analyzer.helpers.stats import r_plots_helpers
from apps.morpho_analyzer.helpers.stats import stats_helpers

from apps.morpho_analyzer.helpers.machine_learning import machine_learning_helpers as ml_helpers
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import io as bn_io
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import models as bn_models
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import learn_structure as bn_learn_structure
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import learn_parameters as bn_learn_parameters
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import plotting as bn_plotting
import apps.morpho_analyzer.helpers.machine_learning.bayesian_networks.utils.score_utils as bn_score_utils

from apps.morpho_analyzer.helpers.machine_learning.supervised_classification import models as supervised_class_models



# ----Select neurons----------
def index_select_neurons(request):
    data = {}
    clear_session(request.session)

    return render(request, 'index_select_neurons.html', data)


def select_without_neurons(request):
    data = {}

    clear_session(request.session)

    return render(request, 'select_without_neurons.html', data)


def select_demo_neurons(request):
    data = {}
    clear_session(request.session)

    return render(request, 'select_demo_neurons.html', data)


def select_neuromorpho_neurons(request):
    data = {}
    clear_session(request.session)

    neuromorpho_url = "http://neuromorpho.org"
    data["neuromorpho_down"] = False
    try:
        response = requests.get(neuromorpho_url, stream=True)
        if response.status_code > 400:
            data["neuromorpho_down"] = True
    except Exception as e:
        data["neuromorpho_down"] = True

    if not data["neuromorpho_down"]:
        data["fields_by_categories"] = neuromorpho_api_client.get_all_neuron_fields_by_categories()

    return render(request, 'select_neuromorpho_neurons.html', data)


def search_neuromorpho_query(request):
    context = {}

    if (not request.is_ajax()):  # 1st time running this method, it will enters here
        if (request.POST.get("task_done_frontend", "") == "1"):
            if 'task_id' in request.POST.keys() and request.POST['task_id']:
                context = {}
                helpers.get_and_clean_datasets_types(context, request.session)
                return render(request, 'index_morpho.html', context)

        query_data = request.POST.get("query-values-json", "{}")
        custom_options = request.POST.get("custom-options-json", "{}")

        create_session_if_not_exists(request)
        job = tasks.search_neuromorpho_query.delay(query_data, custom_options, request.session.session_key)
        # job = tasks.search_neuromorpho_query(query_data, custom_options, request.session.session_key)

        task = {}
        task["name"] = "Search neuromorpho query"
        task["description"] = "Loading neuromorpho neurons"
        task["url_on_loading"] = "/morpho/search_neuromorpho_query/"
        task["id"] = job.id
        context = {}
        context["task"] = task

        return return_loading_worker_task_html(request, context)
    else:  # Rest of time running this method, it will enters here
        return process_loading_worker(request)


def filter_session_neurons_query(request):
    data = {}

    query_data = json.loads(request.POST.get("query-values-json", "{}"))
    custom_options = json.loads(request.POST.get("custom-options-json", "{}"))
    half_filter = json.loads(request.POST.get("half-filter", "{}"))
    all_others = json.loads(request.POST.get("all-others", "{}"))
    subgroup_id = json.loads(request.POST.get("subgroup-id", "{}"))
    dataset_name = request.POST.get("dataset_name", "{}")
    data_type = request.POST.get("data-type", "continuous")
    label_column_id = request.POST.get("label-column-id", "Neuron name")
    has_input_dataset = request.POST.get("is-uploaded-dataset", None)
    datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
    session_id = request.session.session_key
    if datasets_user.has_input_dataset:
        dataset_name_global = datasets_user.input_dataset_name
    else:
        dataset_name_global = dataset_name

    result = helpers.filter_session_neurons_query(request.session, subgroup_id, query_data, custom_options, half_filter, all_others, dataset_name, dataset_name_global, data_type, label_column_id, has_input_dataset)

    if not result["error"]:
        dataset_subgroup = dataset_helper.Dataset("{0}_subgroup_{1}".format(dataset_name, subgroup_id), session_id, app_name=settings.MORPHO_ANALYZER_APP_NAME, delete=True)
        dataframe_subgroup = result["{0}_subgroup".format(dataset_name)]
        dataset_subgroup.load(dataframe_subgroup, datasets_user.global_label_column_id, datasets_user.global_label_instances)

        dataset_others = dataset_helper.Dataset("{0}_others".format(dataset_name_global), session_id, app_name=settings.MORPHO_ANALYZER_APP_NAME, delete=True)
        dataframe_others = result["{0}_others".format(dataset_name_global)]
        dataset_others.load(dataframe_others, datasets_user.global_label_column_id, datasets_user.global_label_instances)
        datasets_user.add([dataset_subgroup, dataset_others])

        request.session.save()
        request.session.modified = True
        datasets_user.save(request.session)  # TODO: Include in session.save signal

        data["number_neurons_new_sample"] = result["number_neurons_new_sample"]

    data["error"] = result["error"]
    data["error_message"] = result["error_message"]

    return JsonResponse(data)


def remove_datasets_subgroup(request):
    data = {}
    subgroup_id = json.loads(request.POST.get("subgroup_id", "{}"))
    dataset_name = request.POST.get("dataset_name", "")
    datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

    if datasets_user.has_input_dataset:
        dataframe_all = datasets_user.get_input_dataframe()
        dataset_others_name = "{0}_others".format(datasets_user.input_dataset_name)
    else:
        dataframe_all = datasets_user.datasets[dataset_name].get_dataframe()
        dataset_others_name = "{0}_others".format(dataset_name)

    if subgroup_id == 1:
        dataframe_others = dataframe_all
    else:
        dataframe_prev_subgroup = datasets_user.datasets["{0}_subgroup_{1}".format(dataset_name, subgroup_id-1)].get_dataframe()
        dataframe_others = helpers.get_dataframe_different_rows(dataframe_all, dataframe_prev_subgroup)

    datasets_user.datasets[dataset_others_name].update_and_save(dataframe_others)

    request.session.save()
    request.session.modified = True
    datasets_user.save(request.session)

    return JsonResponse(data)


def select_upload_neurons(request):
    data = {}
    clear_session(request.session)

    data["tus_upload_url_api_js"] = settings.TUS_UPLOAD_API_URL_JS
    return render(request, 'select_upload_neurons.html', data)


def process_demo_neurons(request):
    data = {}

    dataset_folder = get_data()

    neurons_user = []
    for neuron_name in os.listdir(dataset_folder):
        neuron_file = os.path.join(dataset_folder, neuron_name)
        with open(neuron_file, 'rb') as file:
            neuron_bytes_io = io.BytesIO(file.read())
            neuron_data_file = InMemoryUploadedFile(file=neuron_bytes_io, field_name=None, content_type='text', name=neuron_name, charset='utf8', size=os.stat(neuron_file).st_size)
            file_extension = os.path.splitext(neuron_name)[1]
            neuron_name = os.path.splitext(neuron_name)[0]
            neuron = {
                "name": neuron_name,
                "data_file": {
                    "file": neuron_data_file,
                    "extension": file_extension,
                    "lazy_load": False
                },
            }
            helpers.preprocess_neuron_data_file(neuron)

        neurons_user.append(neuron)

    datasets_user = datasets_user_helper.DatasetsUser(app_name=settings.MORPHO_ANALYZER_APP_NAME, has_input_dataset=False, has_morpho_reconstructions=True)
    request.session["neurons_user"] = neurons_user

    request.session.save()
    request.session.modified = True
    datasets_user.save(request.session)

    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'index_morpho.html', data)


def process_upload_neurons(request):
    data = {}
    response = {}
    print("uploading file in process ", os.getpid())
    uploaded_files = request.FILES
    uploaded_files_dict = dict(uploaded_files)
    uploaded_files = uploaded_files_dict["uploaded_files"]

    neurons_user = []
    for neuron in uploaded_files:
        neuron_name = os.path.splitext(neuron.name)[0]
        file_extension = os.path.splitext(neuron.name)[1]
        neuron = {
            "name": neuron_name,
            "data_file": {
                "file": neuron,
                "extension": file_extension,
                "lazy_load": False
            },
        }

        helpers.preprocess_neuron_data_file(neuron)
        neurons_user.append(neuron)

    datasets_user = datasets_user_helper.DatasetsUser(app_name=settings.MORPHO_ANALYZER_APP_NAME, has_input_dataset=False, has_morpho_reconstructions=True)
    request.session["neurons_user"] = neurons_user

    request.session.save()
    request.session.modified = True
    datasets_user.save(request.session)

    return JsonResponse(response)


def upload_dataset_done(request):
    response = {"error": False}

    try:
        if "tus_upload_filename" in request.session:
            upload_id = request.session["tus_upload_filename"]
        else:
            raise Exception("File not uploaded: upload file first")

        data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
        dataset_params = {
            "path": os.path.join(settings.TUS_UPLOAD_DIR, upload_id),
            "label_column_id": data_client_json["label-column-id"],
            "label_instances": data_client_json["label-instances"],
            "app": settings.MORPHO_ANALYZER_APP_NAME
        }
        create_session_if_not_exists(request)
        """
        dataset_params = {
            "path": os.path.join(settings.CHUNKED_UPLOAD_PATH, "clean_full_brain.csv"),
            "label_column_id": "areas",
            "label_instances": "Genes",
            "app": settings.MORPHO_ANALYZER_APP_NAME
        }
        """
        # job = tasks.read_dataset.delay(request.session.session_key, dataset_params) #This function is not prepared yet to do it with Cerlery
        job = tasks.read_dataset(request.session.session_key, dataset_params)
    except Exception as e:
        response["error"] = True
        response["error_message"] = "Error processing the dataset." + str(e)

    return JsonResponse(response)


def morpho_export_datasets(request):
    context = {}
    context["features"] = []
    context["dataset_name"] = "dataframe"

    datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
    context["datasets"] = datasets_user.datasets

    helpers.get_and_clean_datasets_types(context, request.session)
    return render(request, 'export_datasets.html', context)


def morpho_run_export_datasets(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    format_file = data_client_json["format_file"]
    dataset_name = data_client_json["dataset_name"]

    datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
    data["dataframe_exported_url"] = datasets_user.datasets[dataset_name].url
    data["dataframe_exported_url"] = datasets_user.datasets[dataset_name].url

    return JsonResponse(data)


# --NeuroMorpho API-----

def get_all_neuron_fields(request):
    data = neuromorpho_api_client.get_all_neuron_fields()

    return data


def get_values_neuron_field(request, neuron_field_id):
    data = {}
    custom_option = request.GET.get("custom_option")
    data["field"] = neuron_field_id
    if custom_option == "false":
        field_values = neuromorpho_api_client.get_values_neuron_field(neuron_field_id)
        data["values"] = field_values["values"]
        data["type"] = field_values["type"]
    else:
        if neuron_field_id == "sort_results":
            data["values"] = neuromorpho_api_client.get_all_neuron_fields()
            data["type"] = "text"
        else:
            data["values"] = []
            data["type"] = "text"

    return JsonResponse(data)


def get_local_session_values_neuron_field(request):
    data = {}
    custom_option = request.POST.get("custom_option")
    subgroups_selected = int(request.POST.get("subgroups_selected"))
    neuron_field_id = request.POST.get("neuron_field_id")  # neuron field frontend id
    field_name = request.POST.get("neuron_field_name")  # neuron field backend id
    dataset_name = request.POST.get("dataset_name")
    datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

    if subgroups_selected > 0:
        if datasets_user.has_input_dataset:
            dataset_name = "{0}_others".format(datasets_user.input_dataset_name)
        else:
            dataset_name = "{0}_others".format(dataset_name)

    if custom_option == "false":
        data = helpers.get_local_session_values_neuron_field(datasets_user, field_name, dataset_name)
    else:
        if field_name == "sort_results":
            data["values"] = neuromorpho_api_client.get_all_neuron_fields()
            data["type"] = "text"
        else:
            data["values"] = []
            data["type"] = "text"

    data["field"] = neuron_field_id

    return JsonResponse(data)


# ----Morpho analyzers-------------------------------------------
def index_morpho_analyzer(request):
    data = {}
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'index_morpho.html', data)


def morpho_neurostr(request):
    data = {}
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_neurostr.html', data)


def morpho_branch_features_neurostr(request):
    data = {}
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_branch_features_neurostr.html', data)


def morpho_analyzer_neurostr(request):
    data = {}
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_analyzer_neurostr.html', data)


def morpho_validator_neurostr(request):
    processed = False
    if (not request.is_ajax()):  # 1st time running this method, it will enters here
        if (request.POST.get("task_done_frontend", "") == "1"):
            if 'task_id' in request.POST.keys() and request.POST['task_id']:
                data = get_worker_task_result(request.POST['task_id'])
                processed = True
        else:
            if 'neurostr_validator_neurons' in request.session:  # i.e. it was already processed for this session
                processed = True
        if processed:
            context = {}
            neurostr_validator_neurons = request.session["neurostr_validator_neurons"]
            neurostr_validator_neurons["checks_neurons"] = neurostr_validator_neurons["checks_neurons"].to_html(
                classes='table-with-booleans')
            context['neurostr_validator_neurons'] = neurostr_validator_neurons
            context['neurons_user'] = request.session['neurons_user']
            helpers.get_and_clean_datasets_types(context, request.session)
            return render(request, 'morpho_validator_neurostr.html', context)

        create_session_if_not_exists(request)
        job = tasks.morpho_validator_neurostr.delay(request.session.session_key)
        # job = tasks.morpho_validator_neurostr(request.session.session_key)

        task = {}
        task["name"] = "NeuroSTR validator"
        task["description"] = "Analyzing all the neurons"
        task["url_on_loading"] = "/morpho/morpho_validator_neurostr/"
        task["id"] = job.id
        context = {}
        context["task"] = task

        return return_loading_worker_task_html(request, context)
    else:  # Rest of time running this method, it will enters here
        return process_loading_worker(request)


def morpho_validate_neuron_neurostr(request):
    data = {}
    neuron_selected = request.POST.get("neuron_selected")
    neurons_user = request.session['neurons_user']

    for neuron in neurons_user:
        if neuron["name"] == neuron_selected:
            data["neuron_name"] = neuron_selected
            data["neurostr_validator_output"] = check_neurons.check_neuron(neuron)
            break
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_validate_neuron_neurostr.html', data)


def morpho_format_converter_neurostr(request):
    data = {}

    context = {}
    context['neurons_user'] = request.session['neurons_user']
    helpers.get_and_clean_datasets_types(context, request.session)

    return render(request, 'morpho_format_converter_neurostr.html', context)


def morpho_format_converter_neuron_neurostr(request):
    neuron_name = request.POST.get("neuron_name", None)
    output_format = request.POST.get("output_format", None)
    correct = request.POST.get("correct", None)
    simplification_algorithm = request.POST.get("simplification_algorithm", None)

    neurons_user = request.session["neurons_user"]
    neuron_selected = {}

    for neuron in neurons_user:
        if neuron_name == neuron["name"]:
            neuron_selected = neuron
            break

    actual_format = neuron["data_file"]["extension"].split('.')[1]
    if actual_format == output_format:
        suffix_filename = "_original"
        neuron_file = helpers.get_file_neuron(neuron)
        result = neuron_file.file.getvalue()
    else:
        suffix_filename = "_converted"
        with tempfile.NamedTemporaryFile(suffix=neuron["data_file"]["extension"]) as tmp_file:
            neuron_file = helpers.neuron_in_memory_to_disk(neuron_selected, tmp_file)
            result = neurostr.format_converter(neuron_file, output_format, correct, simplification_algorithm)

    response = HttpResponse(content_type='application/force-download')
    response['Content-Disposition'] = 'attachment;filename="{0}"'.format(neuron_name + "{0}.".format(suffix_filename) + output_format)

    response.write(result)

    return response


def morpho_all_format_converter_neurostr(request):
    if (not request.is_ajax()):  # 1st time running this method, it will enters here
        if (request.POST.get("task_done_frontend", "") == "1"):
            if 'task_id' in request.POST.keys() and request.POST['task_id']:
                data = get_worker_task_result(request.POST['task_id'])
                context = {}
                converted_neurons = request.session["converted_neurons"]
                if "done" in converted_neurons:
                    del request.session["converted_neurons"]
                    context = {}
                    context['neurons_user'] = request.session['neurons_user']
                    helpers.get_and_clean_datasets_types(context, request.session)

                    return render(request, 'morpho_format_converter_neurostr.html', context)

                converted_neurons["done"] = True
                request.session.save()
                request.session.modified = True
                response = HttpResponse(converted_neurons["zip_file"].getvalue(), content_type='application/zip')
                response['Content-Disposition'] = 'attachment; filename={0}'.format(converted_neurons["zip_filename"])

                return response

        output_format = request.POST.get("output_format", None)
        correct = request.POST.get("correct", None)
        simplification_algorithm = request.POST.get("simplification_algorithm", None)

        create_session_if_not_exists(request)
        job = tasks.morpho_all_format_converter_neurostr.delay(request.session.session_key, output_format, correct, simplification_algorithm)
        # job = tasks.morpho_all_format_converter_neurostr(request.session.session_key, output_format, correct, simplification_algorithm)

        task = {}
        task["name"] = "NeuroSTR format converter"
        task["description"] = "Converting all the neurons"
        task["url_on_loading"] = "/morpho/morpho_all_format_converter_neurostr/"
        task["id"] = job.id
        context = {}
        context["task"] = task

        return return_loading_worker_task_html(request, context)
    else:  # Rest of time running this method, it will enters here
        return process_loading_worker(request)


def morpho_download_all_original_swc_neuromorpho(request):
    if (not request.is_ajax()):  # 1st time running this method, it will enters here
        if (request.POST.get("task_done_frontend", "") == "1"):
            if 'task_id' in request.POST.keys() and request.POST['task_id']:
                data = get_worker_task_result(request.POST['task_id'])
                context = {}
                original_neurons_swc = request.session["original_neurons_swc"]
                if "done" in original_neurons_swc:
                    del request.session["original_neurons_swc"]
                    context = {}
                    context['neurons_user'] = request.session['neurons_user']
                    helpers.get_and_clean_datasets_types(context, request.session)

                    neurons_user = request.session["neurons_user"]
                    data = {}
                    data["exists_neurons_user"] = len(neurons_user) > 0
                    data["exists_neurons_additional_data"] = helpers.neurons_get_additional_discrete_data(neurons_user)
                    helpers.get_and_clean_datasets_types(data, request.session)
                    return render(request, 'morpho_download_your_data.html', data)

                original_neurons_swc["done"] = True
                request.session.save()
                request.session.modified = True
                response = HttpResponse(original_neurons_swc["zip_file"].getvalue(), content_type='application/zip')
                response['Content-Disposition'] = 'attachment; filename={0}'.format(original_neurons_swc["zip_filename"])

                return response

        create_session_if_not_exists(request)
        job = tasks.morpho_download_all_original_swc_neuromorpho.delay(request.session.session_key)
        #job = tasks.morpho_download_all_original_swc_neuromorpho(request.session.session_key)

        """
        original_neurons_swc = request.session["original_neurons_swc"]
        if "done" in original_neurons_swc:
            del request.session["original_neurons_swc"]
            context = {}
            context['neurons_user'] = request.session['neurons_user']
            helpers.get_and_clean_datasets_types(context, request.session)

            return render(request, 'morpho_download_your_data.html', context)

        original_neurons_swc["done"] = True
        request.session.save()
        request.session.modified = True
        response = HttpResponse(original_neurons_swc["zip_file"].getvalue(), content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename={0}'.format(original_neurons_swc["zip_filename"])
        return response
        """

        task = {}
        task["name"] = "NeuroMorpho.org swc downloader"
        task["description"] = "Downloading all original SWC neurons"
        task["url_on_loading"] = "/morpho/morpho_download_all_original_swc_neuromorpho/"
        task["id"] = job.id
        context = {}
        context["task"] = task

        return return_loading_worker_task_html(request, context)
    else:  # Rest of time running this method, it will enters here
        return process_loading_worker(request)


def morpho_3DBasalRM(request):
    data = {}

    context = {}
    context['neurons_user'] = request.session['neurons_user']
    context["fields_to_display"] = ["species", "png_url"]
    helpers.get_and_clean_datasets_types(context, request.session)

    return render(request, 'morpho_3DBasalRM.html', context)


def morpho_repair_neuron_3DBasalRM(request):
    data = {}
    neuron_selected = request.POST.get("neuron_selected")
    download_neuron = request.POST.get("download_neuron")
    neurons_user = request.session['neurons_user']

    for neuron in neurons_user:
        if neuron["name"] == neuron_selected:
            result = helpers.morpho_repair_neuron_3DBasalRM(neuron, download_neuron)
            data["neuron_original"] = neuron
            data["neuron_original"]["data_file_b64"] = b64encode(helpers.get_file_neuron(neuron).file.getvalue())  # We send base64 object, javascript translate it into blob

            if "error" in result:
                data["error"] = True
                if download_neuron:
                    data["download_error"] = True
                helpers.get_and_clean_datasets_types(data, request.session)
                return render(request, 'morpho_view_repaired_3DBasalRM.html', data)
            neuron_repaired = result["neuron_repaired"]

            if download_neuron:
                output_format = request.POST.get("output_format")
                if output_format == "json":
                    result = result["neuron_repaired_json_bytes"]
                else:
                    with tempfile.NamedTemporaryFile(suffix=neuron_repaired["data_file"]["extension"]) as tmp_file:
                        neuron_file = helpers.neuron_in_memory_to_disk(neuron_repaired, tmp_file)
                        result = neurostr.format_converter(neuron_file, output_format, correct=None, simplification_algorithm=None)

                response = HttpResponse(content_type='application/force-download')
                response['Content-Disposition'] = 'attachment;filename="{0}"'.format(neuron["name"] + "_repaired" + "." + output_format)
                response.write(result)

                return response
            else:
                data["neuron_original"]["data_file_b64"] = b64encode(helpers.get_file_neuron(neuron).file.getvalue()) #We send base64 object, javascript translate it into blob
                data["neuron_repaired"] = neuron_repaired
                data["neuron_repaired"]["data_file_b64"] =  b64encode(helpers.get_file_neuron(neuron_repaired).file.getvalue()) #We send base64 object, javascript translate it into blob
                helpers.get_and_clean_datasets_types(data, request.session)
                return render(request, 'morpho_view_repaired_3DBasalRM.html', data)
            break

    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_view_repaired_3DBasalRM.html', data)


def morpho_repair_all_neurons_3DBasalRM(request):
    if (not request.is_ajax()):  # 1st time running this method, it will enters here
        if (request.POST.get("task_done_frontend", "") == "1"):
            if 'task_id' in request.POST.keys() and request.POST['task_id']:
                data = get_worker_task_result(request.POST['task_id'])
                context = {}
                neurons_repaired = request.session["neurons_repaired"]
                if "done" in neurons_repaired:
                    del request.session["neurons_repaired"]
                    context = {}
                    context['neurons_user'] = request.session['neurons_user']
                    helpers.get_and_clean_datasets_types(context, request.session)

                    return render(request, 'morpho_3DBasalRM.html', context)

                neurons_repaired["done"] = True
                request.session.save()
                request.session.modified = True
                response = HttpResponse(neurons_repaired["zip_file"].getvalue(), content_type='application/zip')
                response['Content-Disposition'] = 'attachment; filename={0}'.format(neurons_repaired["zip_filename"])

                return response

        output_format = request.POST.get("output_format", None)

        create_session_if_not_exists(request)
        job = tasks.morpho_repair_all_neurons_3DBasalRM.delay(request.session.session_key, output_format)
        # job = tasks.morpho_repair_all_neurons_3DBasalRM(request.session.session_key, output_format)

        task = {}
        task["name"] = "3DBasalRM repair all neurons"
        task["description"] = "Repairing all the neurons"
        task["url_on_loading"] = "/morpho/morpho_repair_all_neurons_3DBasalRM/"
        task["id"] = job.id
        context = {}
        context["task"] = task

        return return_loading_worker_task_html(request, context)
    else:  # Rest of time running this method, it will enters here
        return process_loading_worker(request)


def morpho_spineSimulation(request):
    data = {}
    data["url_shiny_app"] = settings.URL_SHINY_SERVER + "/" + settings.SHINY_APP_3DSPINES
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_spineSimulation.html', data)


def morpho_somaMS(request):
    data = {}
    data["url_shiny_app"] = settings.URL_SHINY_SERVER + "/" + settings.SHINY_APP_3DSOMAMS
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_somaMS.html', data)


def morpho_synapsesSA(request):
    data = {}
    data["url_shiny_app"] = settings.URL_SHINY_SERVER + "/" + settings.SHINY_APP_3DSYNAPSESSA
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_synapsesSA.html', data)


def morpho_hbp_dendrite_arborization_simulation(request):
    data = {}
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_hbp_dendrite_arborization_simulation.html', data)


def morpho_simulate_dendrite_arborization(request):
    data = {}
    create_session_if_not_exists(request)

    number_simulated_neurons = request.POST.get("input_number_simulated_neurons", None)
    output_folder_neurons = os.path.join(settings.HBP_SIMULATED_NEURONS_OUTPUT_DIR, request.session.session_key)
    data = helpers.morpho_simulate_dendrite_arborization(number_simulated_neurons, output_folder_neurons)

    if (not data["error"]):
        clear_session(request.session)

        for neuron_name in os.listdir(output_folder_neurons):
            neuron_file = os.path.join(output_folder_neurons, neuron_name)
            with open(neuron_file, 'rb') as file:
                file_extension = os.path.splitext(neuron_name)[1]
                neuron_name = os.path.splitext(neuron_name)[0]
                neuron_bytes_io = io.BytesIO(file.read())
                neuron_data_file = InMemoryUploadedFile(file=neuron_bytes_io, field_name=None, content_type='text', name=neuron_name, charset='utf8', size=os.stat(neuron_file).st_size)
                neuron = {
                    "name": neuron_name,
                    "data_file": {
                        "file": neuron_data_file,
                        "extension": file_extension,
                        "lazy_load": False,
                        "simulated_neuron": True,
                    },
                }
                helpers.preprocess_neuron_data_file(neuron)

            neurons_user = request.session['neurons_user']
            neurons_user.append(neuron)
            request.session["neurons_user"] = neurons_user
            request.session.save()
            request.session.modified = True
    else:
        raise Exception(data["exception"])

    shutil.rmtree(output_folder_neurons)

    return JsonResponse(data)


def morpho_neuroviewer(request):
    context = {}
    context['neurons_user'] = request.session['neurons_user']
    helpers.get_and_clean_datasets_types(context, request.session)

    return render(request, 'morpho_neuroviewer.html', context)


def morpho_neuron3D_neuroviewer(request):
    print ("Neuron3D")
    data = {}
    neuron_selected = request.POST.get("neuron_selected")
    neurons_user = request.session['neurons_user']

    for neuron in neurons_user:
        if neuron["name"] == neuron_selected:
            data["neuron"] = neuron
            data["neuron"]["data_file_b64"] = b64encode(helpers.get_file_neuron(neuron).file.getvalue()) #We send base64 object, javascript translate it into blob
            break

    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_neuron3D_neuroviewer.html', data)


def morpho_l_measure(request):
    if (not request.is_ajax()):  # 1st time running this method, it will enters here
        processed = False
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
        neurons_user = request.session["neurons_user"]

        if (request.POST.get("task_done_frontend", "") == "1"):
            if 'task_id' in request.POST.keys() and request.POST['task_id']:
                data = get_worker_task_result(request.POST['task_id'])
                processed = True
        else:
            if 'l_measure' in datasets_user.datasets:  # i.e. it was already processed for this session
                processed = True
        if processed:
            context = {}
            prepare_render_dataframe_l_measure(context, datasets_user, neurons_user)
            helpers.get_and_clean_datasets_types(context, request.session)

            return render(request, 'morpho_l_measure.html', context)

        valid_neurons = []

        create_session_if_not_exists(request)
        job = tasks.morpho_l_measure.delay(request.session.session_key, valid_neurons)
        # job = tasks.morpho_l_measure(request.session.session_key, valid_neurons)

        task = {}
        task["name"] = "L-Measure"
        task["description"] = "Analyzing all the neurons"
        task["url_on_loading"] = "/morpho/morpho_l_measure/"
        task["id"] = job.id
        context = {}
        context["task"] = task

        return return_loading_worker_task_html(request, context)
    else:  # Rest of time running this method, it will enters here
        return process_loading_worker(request)


def morpho_data_stats_discrete(request):
    data = {}
    datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
    data_type = "discrete"

    dataset_discrete_name = datasets_user.input_dataset_discrete_name
    dataset_continous_name = datasets_user.input_dataset_continuous_name
    label_column_id = datasets_user.global_label_column_id
    dataframe_data_columns, dataframe_others_columns = helpers.get_render_data_dataframe(data, data_type, datasets_user, dataset_discrete_name, dataset_continous_name, label_column_id)
    discrete_fields = list(dataframe_data_columns)
    continuous_fields = list(dataframe_others_columns)
    data["fields_by_categories"] = helpers.get_discr_cont_fields_by_categories(discrete_fields, continuous_fields)
    data["label_instances"] = datasets_user.global_label_instances

    data["data_have_discrete_fields"] = True
    data["discrete_fields"] = dataframe_data_columns
    data["columns_not_stats"] = [label_column_id]
    data["dataset_name"] = dataset_discrete_name
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_data_stats_discrete.html', data)


def morpho_data_stats_continuous(request):
    data = {}
    datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
    data_type = "continuous"

    dataset_discrete_name = datasets_user.input_dataset_discrete_name
    dataset_continous_name = datasets_user.input_dataset_continuous_name
    label_column_id = datasets_user.global_label_column_id
    dataframe_data_columns, dataframe_others_columns = helpers.get_render_data_dataframe(data, data_type, datasets_user, dataset_discrete_name, dataset_continous_name, label_column_id)
    if len(dataframe_others_columns) > 1: #discrete data with label_column_id + another cols
        data["data_have_discrete_fields"] = True
        data["discrete_fields"] = list(dataframe_others_columns)
        if label_column_id in data["discrete_fields"]:
            data["discrete_fields"].remove(label_column_id)

    discrete_fields = list(dataframe_others_columns)
    continuous_fields = list(dataframe_data_columns)
    data["fields_by_categories"] = helpers.get_discr_cont_fields_by_categories(discrete_fields, continuous_fields)
    data["label_instances"] = datasets_user.global_label_instances

    data["columns_not_stats"] = [label_column_id]
    data["dataset_name"] = dataset_continous_name
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_data_stats_continuous.html', data)


def morpho_gabaclassifier(request):
    if (not request.is_ajax()):  # 1st time running this method, it will enters here
        processed = False
        if (request.POST.get("task_done_frontend", "") == "1"):
            if 'task_id' in request.POST.keys() and request.POST['task_id']:
                data = get_worker_task_result(request.POST['task_id'])
                processed = True
        else:
            if 'gabaclassifier_results' in request.session:  # i.e. it was already processed for this session
                processed = True

        if processed:
            context = {}
            gabaclassifier_results = request.session["gabaclassifier_results"]

            context["neurons_columns"], context["neurons_list"] = global_helpers.pandas_dataframe_to_lists(
                dataframe=gabaclassifier_results["result_dataframe"], append_columns=["Analyze neuron"])

            context["possible_interneuron_classes"] = gabaclassifier_results["possible_interneuron_classes"]
            helpers.get_and_clean_datasets_types(context, request.session)

            return render(request, 'morpho_gabaclassifier.html', context)

        valid_neurons = []

        create_session_if_not_exists(request)
        job = tasks.morpho_gabaclassifier.delay(request.session.session_key, valid_neurons)
        # job = tasks.morpho_gabaclassifier(request.session.session_key, valid_neurons)

        task = {}
        task["name"] = "GabaClassifier"
        task["description"] = "Detecting class of interneurons"
        task["url_on_loading"] = "/morpho/morpho_gabaclassifier/"
        task["id"] = job.id
        context = {}
        context["task"] = task

        return return_loading_worker_task_html(request, context)
    else:  # Rest of time running this method, it will enters here
        return process_loading_worker(request)


def morpho_overview(request):
    data = {}

    print ("Morpho overview started! \n\n\n")
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_overview.html', data)


def morpho_download_your_data(request):
    data = {}
    neurons_user = request.session["neurons_user"]
    data["exists_neurons_user"] = len(neurons_user) > 0
    data["exists_neurons_additional_data"] = helpers.neurons_get_additional_discrete_data(neurons_user)
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_download_your_data.html', data)


def morpho_run_download_metadata(request):
    data = {}
    neurons_user = request.session["neurons_user"]
    all_metadata_jsons = []

    zip_subdir = "Neurons_metadata"
    zip_filename = "{0}.zip".format(zip_subdir)
    zip_file_memory = BytesIO()
    zf = zipfile.ZipFile(zip_file_memory, "w")

    for neuron in neurons_user:
        neuron_metadata_json = json.dumps(neuron["additional_data"])
        zf.writestr(neuron["name"] + ".json", neuron_metadata_json)

    zf.close()

    zip = {"zip_filename": zip_filename, "zip_file": zip_file_memory}

    response = HttpResponse(zip["zip_file"].getvalue(), content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename={0}'.format(zip["zip_filename"])

    return response


# --------------Stats---------------------
@not_minified_response
def univariate_descriptive_stats(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    dataset_name = input_data_json["dataset_name"]
    data_type = "continuous"
    if "data_type" in input_data_json:
        data_type = input_data_json["data_type"]
    columns_names = input_data_json["columns_names"]
    if columns_names is None:
        columns_names = []
    input_column_x = input_data_json["label_instances"]
    column_class = input_data_json["label_column_id"]
    result_stats = {}
    min_necessary_cols = 1
    num_cols = len(columns_names)

    result_stats = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols)
    if not result_stats["error"]:
        columns_get = columns_names + [column_class]
        dataframe = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME].datasets[dataset_name].get_dataframe(columns_get)
        result_stats["objects"] = {}
        result_stats["objects"]["descriptive_stats_table"] = stats_helpers.univariate_descriptive_stats(input_data=dataframe, input_column_x=input_column_x,
                                                                                                        input_columns_y=columns_names, data_type=data_type)

    return JsonResponse(result_stats)


@not_minified_response
def generate_multi_univariate_plots_html(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    dataset_name = input_data_json["dataset_name"]
    data_type = "continuous"
    if "data_type" in input_data_json:
        data_type = input_data_json["data_type"]
    columns_names = input_data_json["columns_names"]
    label_name = ""
    if "label_name" in input_data_json:
        if input_data_json["label_name"] is not None:
            label_name = input_data_json["label_name"]

    input_column_x = input_data_json["label_instances"]
    scatter_point_label = "Instance"
    column_class = input_data_json["label_column_id"]
    min_necessary_cols = 1
    num_cols = len(columns_names)

    result_plots = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols)
    if not result_plots["error"]:
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
        neurons_user = request.session["neurons_user"]
        columns_get = columns_names + [column_class]

        dataframe = datasets_user.datasets[dataset_name].get_dataframe(columns_get)

        if data_type == "discrete":
            result_plots["objects"] = {}
            result_plots["objects"]["bar_charts"] = plotly_helpers.bar_chart_univariate(input_data=dataframe, input_column_x=input_column_x,
                                                                                        input_columns_y=columns_names)
            result_plots["objects"]["pie_charts"] = plotly_helpers.pie_chart_univariate(input_data=dataframe, input_column_x=input_column_x,
                                                                                        input_columns_y=columns_names)
        else:

            if label_name != "":
                dataset_discrete_name = datasets_user.input_dataset_discrete_name
                if dataset_discrete_name in datasets_user.datasets:
                    column_class_discrete = datasets_user.datasets[dataset_discrete_name].label_column_id
                    columns_discrete = [column_class_discrete, label_name]
                    dataframe_discrete = datasets_user.datasets[dataset_discrete_name].get_dataframe(columns_discrete)
                    dataframe = stats_helpers.add_labels_to_dataframe(dataframe, dataframe_discrete, datasets_user, neurons_user, label_name, column_class_discrete)

            result_plots["objects"] = {}
            result_plots["objects"]["histograms"] = plotly_helpers.histogram_univariate(input_data=dataframe, input_column_x=input_column_x,
                                                                                        input_columns_y=columns_names)
            result_plots["objects"]["pdf_plots_univariate"] = plotly_helpers.density_functions(input_data=dataframe, input_column_x=input_column_x,
                                                                                               input_columns_y=columns_names, subplots=True, normalize=False)
            result_plots["objects"]["boxplots"] = plotly_helpers.boxplot_univariate(input_data=dataframe, input_column_x=input_column_x,
                                                                                    input_columns_y=columns_names, column_class=column_class, label_name=label_name)
            result_plots["objects"]["scatter_univariate"] = plotly_helpers.scatter_univariate(input_data=dataframe, input_column_x=input_column_x,
                                                                                              input_columns_y=columns_names, column_class=column_class,
                                                                                              scatter_point_label = scatter_point_label, label_name=label_name)

    return JsonResponse(result_plots)


@not_minified_response
def generate_bivariate_plots_html(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    dataset_name = input_data_json["dataset_name"]
    columns_names = input_data_json["columns_names"]
    data_type = "continuous"
    if "data_type" in input_data_json:
        data_type = input_data_json["data_type"]
    label_name = ""
    if "label_name" in input_data_json:
        if input_data_json["label_name"] is not None:
            label_name = input_data_json["label_name"]
    input_column_x = input_data_json["label_instances"]
    column_class = input_data_json["label_column_id"]
    scatter_point_label = "Instance"
    result_plots = {}
    num_cols = len(columns_names)
    if data_type == "discrete":
        min_necessary_cols = 2
        max_necessary_cols = 2
    else:
        min_necessary_cols = 2
        max_necessary_cols = 0  # i.e no max columns

    result_plots = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols, max_necessary_cols)
    if not result_plots["error"]:
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
        neurons_user = request.session["neurons_user"]
        columns_get = columns_names + [column_class]

        dataframe = datasets_user.datasets[dataset_name].get_dataframe(columns_get)

        if data_type == "discrete":
            result_plots["objects"] = {}
            result_plots["objects"]["bar_charts_stack"] = plotly_helpers.bar_chart_bivariate(input_data=dataframe, input_column_x=input_column_x,
                                                                                             input_columns_y=columns_names, pie_chart_type="stack")
            result_plots["objects"]["bar_charts_group"] = plotly_helpers.bar_chart_bivariate(input_data=dataframe, input_column_x=input_column_x,
                                                                                             input_columns_y=columns_names, pie_chart_type="group")
        else:
            if label_name != "":
                dataset_discrete_name = datasets_user.input_dataset_discrete_name
                if dataset_discrete_name in datasets_user.datasets:
                    column_class_discrete = datasets_user.datasets[dataset_discrete_name].label_column_id
                    columns_discrete = [column_class_discrete, label_name]
                    dataframe_discrete = datasets_user.datasets[dataset_discrete_name].get_dataframe(columns_discrete)
                    dataframe = stats_helpers.add_labels_to_dataframe(dataframe, dataframe_discrete, datasets_user, neurons_user, label_name, column_class_discrete)

            result_plots["objects"] = {}
            result_plots["objects"]["correlation_matrix_heatmap_pearson"] = plotly_helpers.correlation_matrix_heatmap(input_data=dataframe,
                                                                                                                      input_column_x=input_column_x, input_columns_y=columns_names,
                                                                                                                      column_class=column_class, correlation_method="pearson")
            result_plots["objects"]["correlation_matrix_heatmap_kendall"] = plotly_helpers.correlation_matrix_heatmap(input_data=dataframe,
                                                                                                                      input_column_x=input_column_x, input_columns_y=columns_names,
                                                                                                                      column_class=column_class, correlation_method="kendall")
            result_plots["objects"]["correlation_matrix_heatmap_spearman"] = plotly_helpers.correlation_matrix_heatmap(input_data=dataframe,
                                                                                                                       input_column_x=input_column_x, input_columns_y=columns_names,
                                                                                                                       column_class=column_class, correlation_method="spearman")
            result_plots["objects"]["scatterplot_matrix"] = plotly_helpers.scatterplot_matrix(input_data=dataframe, input_column_x=input_column_x,
                                                                                              input_columns_y=columns_names, column_class=column_class, label_name=label_name)

            result_plots["objects"]["histogram_2d_Contour_subplots"] = plotly_helpers.histogram_2d_Contour_subplots(input_data=dataframe, input_column_x=input_column_x,
                                                                                                                    input_columns_y=columns_names, column_class=column_class, scatter_point_label=scatter_point_label, label_name=label_name)

    return JsonResponse(result_plots)


@not_minified_response
def bivariate_stats(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    dataset_name = input_data_json["dataset_name"]
    columns_names = input_data_json["columns_names"]
    input_column_x = input_data_json["label_instances"]
    column_class = input_data_json["label_column_id"]
    result_stats = {}
    min_necessary_cols = 2
    num_cols = len(columns_names)

    result_stats = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols)
    if not result_stats["error"]:
        columns_get = columns_names + [column_class]
        dataframe = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME].datasets[dataset_name].get_dataframe(columns_get)

        result_stats["objects"] = {}

        result_stats["objects"]["correlation_matrix_pearson"] = stats_helpers.correlation_matrix(input_data=dataframe, input_column_x=input_column_x,
                                                                                                 input_columns_y=columns_names, correlation_method="pearson",
                                                                                                 section_title="Pearson correlation coefficient (Pearson's r)")
        result_stats["objects"]["correlation_matrix_kendall"] = stats_helpers.correlation_matrix(input_data=dataframe, input_column_x=input_column_x,
                                                                                                 input_columns_y=columns_names, correlation_method="kendall",
                                                                                                 section_title="Kendall correlation coefficient")
        result_stats["objects"]["correlation_matrix_spearman"] = stats_helpers.correlation_matrix(input_data=dataframe, input_column_x=input_column_x,
                                                                                                  input_columns_y=columns_names, correlation_method="spearman",
                                                                                                  section_title="Spearman correlation coefficient")
    return JsonResponse(result_stats)


@not_minified_response
def generate_trivariate_plots_html(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    dataset_name = input_data_json["dataset_name"]
    columns_names = input_data_json["columns_names"]
    label_name = ""
    if "label_name" in input_data_json:
        if input_data_json["label_name"] is not None:
            label_name = input_data_json["label_name"]
    input_column_x = input_data_json["label_instances"]
    column_class = input_data_json["label_column_id"]
    scatter_point_label = "Instance"
    result_plots = {}
    min_necessary_cols = 3
    max_necessary_cols = 3
    num_cols = len(columns_names)

    result_plots = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols, max_necessary_cols)
    if not result_plots["error"]:
        columns_get = columns_names + [column_class]
        dataframe = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME].datasets[dataset_name].get_dataframe(columns_get)
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
        neurons_user = request.session["neurons_user"]
        if label_name != "":
            dataset_discrete_name = datasets_user.input_dataset_discrete_name
            if dataset_discrete_name in datasets_user.datasets:
                column_class_discrete = datasets_user.datasets[dataset_discrete_name].label_column_id
                columns_discrete = [column_class_discrete, label_name]
                dataframe_discrete = datasets_user.datasets[dataset_discrete_name].get_dataframe(columns_discrete)
                dataframe = stats_helpers.add_labels_to_dataframe(dataframe, dataframe_discrete, datasets_user,
                                                                  neurons_user, label_name, column_class_discrete)

        result_plots["objects"] = {}
        result_plots["objects"]["scatterplot_bubble"] = plotly_helpers.scatterplot_bubble(input_data=dataframe, input_column_x=input_column_x,
                                                                                          input_columns_y=columns_names, column_class=column_class, scatter_point_label=scatter_point_label, label_name=label_name)
        result_plots["objects"]["scatterplot_3d"] = plotly_helpers.scatterplot_3d(input_data=dataframe, input_column_x=input_column_x,
                                                                                  input_columns_y=columns_names, column_class=column_class, scatter_point_label=scatter_point_label, label_name=label_name)
    return JsonResponse(result_plots)


@not_minified_response
def generate_multivariate_plots_html(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    dataset_name = input_data_json["dataset_name"]
    columns_names = input_data_json["columns_names"]
    label_name = ""
    if "label_name" in input_data_json:
        if input_data_json["label_name"] is not None:
            label_name = input_data_json["label_name"]
    input_column_x = input_data_json["label_instances"]
    column_class = input_data_json["label_column_id"]
    scatter_point_label = "Instance"
    result_plots = {}
    min_necessary_cols = 1
    num_cols = len(columns_names)

    result_plots = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols)
    if not result_plots["error"]:
        columns_get = columns_names + [column_class]
        dataframe = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME].datasets[dataset_name].get_dataframe(columns_get)
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
        neurons_user = request.session["neurons_user"]
        if label_name != "":
            dataset_discrete_name = datasets_user.input_dataset_discrete_name
            if dataset_discrete_name in datasets_user.datasets:
                column_class_discrete = datasets_user.datasets[dataset_discrete_name].label_column_id
                columns_discrete = [column_class_discrete, label_name]
                dataframe_discrete = datasets_user.datasets[dataset_discrete_name].get_dataframe(columns_discrete)
                dataframe = stats_helpers.add_labels_to_dataframe(dataframe, dataframe_discrete, datasets_user,
                                                                  neurons_user, label_name, column_class_discrete)

        result_plots["objects"] = {}
        result_plots["objects"]["density_functions"] = plotly_helpers.density_functions(input_data=dataframe, input_column_x=input_column_x,
                                                                                        input_columns_y=columns_names, column_class=column_class, normalize=True)
        result_plots["objects"]["parallel_coordinates"] = plotly_helpers.parallel_coordinates(input_data=dataframe, input_column_x=input_column_x,
                                                                                              input_columns_y=columns_names, column_class=column_class, label_name=label_name)
        result_plots["objects"]["andrew_curves"] = plotly_helpers.andrew_curves(input_data=dataframe, input_column_x=input_column_x,
                                                                                input_columns_y=columns_names, column_class=column_class, label_name=label_name)
        result_plots["objects"]["radviz"] = plotly_helpers.radviz(input_data=dataframe, input_column_x=input_column_x,
                                                                  input_columns_y=columns_names, column_class=column_class)
        result_plots["objects"]["chernoff_faces"] = r_plots_helpers.chernoff_faces(input_data=dataframe, input_column_x=input_column_x,
                                                                                   input_columns_y=columns_names, column_class=column_class,
                                                                                   session_id=request.session.session_key)
        result_plots["objects"]["radar_chart"] = plotly_helpers.radar_charts(input_data=dataframe, input_column_x=input_column_x,
                                                                             input_columns_y=columns_names, column_class=column_class, label_name=label_name)
        result_plots["objects"]["scatterplot_bubble_colorscale"] = plotly_helpers.scatterplot_bubble(input_data=dataframe, input_column_x=input_column_x,
                                                                                                     input_columns_y=columns_names, column_class=column_class,
                                                                                                     scatter_point_label=scatter_point_label, colorscale=True, label_name=label_name)
        result_plots["objects"]["scatterplot_3d_bubble_colorscale"] = plotly_helpers.scatterplot_3d(input_data=dataframe, input_column_x=input_column_x,
                                                                                                    input_columns_y=columns_names, column_class=column_class,
                                                                                                    scatter_point_label=scatter_point_label, bubbles_and_colorscale=True, label_name=label_name)

    return JsonResponse(result_plots)


@not_minified_response
def point_interval_estimates_stats(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    dataset_name = input_data_json["dataset_name"]
    columns_names = input_data_json["columns_names"]
    confidence_level = int(input_data_json["confidence_level"]) / 100
    data_type = "continuous"
    if "data_type" in input_data_json:
        data_type = input_data_json["data_type"]
    input_column_x = input_data_json["label_instances"]
    column_class = input_data_json["label_column_id"]
    scatter_point_label = "Instance"
    result_plots = {}
    min_necessary_cols = 1
    if data_type == "discrete":
        columns_names = [columns_names]
        values_field = input_data_json["values_field"]
        num_cols = len(values_field)
    else:
        num_cols = len(columns_names)

    result_stats = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols)
    if not result_stats["error"]:
        columns_get = columns_names + [column_class]
        dataframe = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME].datasets[dataset_name].get_dataframe(columns_get)

        if data_type == "discrete":
            result_stats["objects"] = {}
            result_stats["objects"]["confidence_intervals"] = stats_helpers.point_estimates_confidence_intervals(input_data=dataframe, input_column_x=input_column_x,
                                                                                                                 confidence_level=confidence_level, input_columns_y=columns_names,
                                                                                                                 column_class=column_class, data_type=data_type, values_field=values_field)
        else:
            result_stats["objects"] = {}
            result_stats["objects"]["confidence_intervals"] = stats_helpers.point_estimates_confidence_intervals(input_data=dataframe, input_column_x=input_column_x,
                                                                                                                 confidence_level=confidence_level, input_columns_y=columns_names, column_class=column_class)

    return JsonResponse(result_stats)


@not_minified_response
def point_interval_estimates_plots_html(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    dataset_name = input_data_json["dataset_name"]
    columns_names = input_data_json["columns_names"]
    confidence_level = int(input_data_json["confidence_level"]) / 100
    data_type = "continuous"
    if "data_type" in input_data_json:
        data_type = input_data_json["data_type"]
    input_column_x = input_data_json["label_instances"]
    column_class = input_data_json["label_column_id"]
    scatter_point_label = "Instance"
    column_y_name = "Probability density function"
    result_plots = {}
    min_necessary_cols = 1
    if data_type == "discrete":
        columns_names = [columns_names]
        values_field = input_data_json["values_field"]
        num_cols = len(values_field)
    else:
        num_cols = len(columns_names)

    result_plots = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols)
    if not result_plots["error"]:
        result_plots["objects"] = {}
        columns_get = columns_names + [column_class]
        dataframe = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME].datasets[dataset_name].get_dataframe(columns_get)

        if data_type == "discrete":
            result_plots["objects"]["pdf_confidence_intervals"] = plotly_helpers.point_estimates_confidence_intervals(input_data=dataframe, input_column_x=input_column_x,
                                                                                                                      confidence_level=confidence_level, input_columns_y=columns_names,
                                                                                                                      column_class=column_class, column_y_name=column_y_name,
                                                                                                                      data_type=data_type, values_field=values_field)
        else:
            result_plots["objects"]["pdf_confidence_intervals"] = plotly_helpers.point_estimates_confidence_intervals(input_data=dataframe, input_column_x=input_column_x,
                                                                                                                      confidence_level=confidence_level, input_columns_y=columns_names,
                                                                                                                      column_class=column_class, column_y_name=column_y_name)

    return JsonResponse(result_plots)


@not_minified_response
def hypothesis_testing_stats(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    columns_names = input_data_json["columns_names"]
    significance_level = int(input_data_json["significance_level"]) / 100
    hypothesis_test_type = input_data_json["hypothesis_test_type"]
    hypothesis_test_type_tails = input_data_json["hypothesis_test_type_tails"]

    if hypothesis_test_type == "one_sample":
        hypothesis_population_mean = float(input_data_json["hypothesis_population_mean"])
    data_type = "continuous"
    if "data_type" in input_data_json:
        data_type = input_data_json["data_type"]
    input_column_x = input_data_json["label_instances"]
    column_class = input_data_json["label_column_id"]
    scatter_point_label = "Instance"
    result_stats = {}

    dataset_name = input_data_json["dataset_name"]

    if data_type == "discrete":
        if hypothesis_test_type != "two_dependent_samples":
            columns_names = [columns_names]
            values_field = input_data_json["values_field"]
            num_cols = len(values_field)
    else:
        num_cols = len(columns_names)

    if hypothesis_test_type == "two_dependent_samples":
        min_necessary_cols = 2
        max_necessary_cols = 2
        if data_type == "discrete":
            values_field_1 = input_data_json["values_field_1"]
            values_field_2 = input_data_json["values_field_2"]
            values_field = [values_field_1, values_field_2]
            if len(values_field_1) > 0 and len(values_field_2):
                num_cols = 2
    else:
        min_necessary_cols = 1
        max_necessary_cols = 0  # i.e no max columns

    result_stats = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols, max_necessary_cols)
    if not result_stats["error"]:
        columns_get = columns_names + [column_class]
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
        result_stats["objects"] = {}

        if hypothesis_test_type == "one_sample":
            dataframe_all_neurons = datasets_user.datasets[dataset_name].get_dataframe(columns_get)

            if data_type == "discrete":
                result_stats["objects"]["hypothesis_testing"] = stats_helpers.hypothesis_testing_one_sample(
                    input_data=dataframe_all_neurons, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level, hypothesis_population_mean=hypothesis_population_mean,
                    input_columns_y=columns_names, column_class=column_class, data_type=data_type, values_field=values_field)
            else:
                result_stats["objects"]["hypothesis_testing"] = stats_helpers.hypothesis_testing_one_sample(
                    input_data=dataframe_all_neurons, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level, hypothesis_population_mean=hypothesis_population_mean,
                    input_columns_y=columns_names, column_class=column_class)
        elif hypothesis_test_type == "two_independent_samples":
            dataframes = []
            dataframe_subgroup_1 = datasets_user.datasets[dataset_name + "_subgroup_1"].get_dataframe(columns_get)
            dataframe_subgroup_2 = datasets_user.datasets[dataset_name + "_subgroup_2"].get_dataframe(columns_get)
            dataframes += [dataframe_subgroup_1, dataframe_subgroup_2]
            if data_type == "discrete":
                result_stats["objects"]["hypothesis_testing"] = stats_helpers.hypothesis_testing_two_independent_samples(
                    input_data=dataframes, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level,
                    input_columns_y=columns_names, column_class=column_class, data_type=data_type, values_field=values_field)
            else:
                result_stats["objects"]["hypothesis_testing"] = stats_helpers.hypothesis_testing_two_independent_samples(
                    input_data=dataframes, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level,
                    input_columns_y=columns_names, column_class=column_class)
        elif hypothesis_test_type == "two_dependent_samples":
            dataframe_all_neurons = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME].datasets[dataset_name].get_dataframe(columns_get)

            if data_type == "discrete":
                result_stats["objects"]["hypothesis_testing"] = stats_helpers.hypothesis_testing_two_dependent_samples(
                    input_data=dataframe_all_neurons, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level,
                    input_columns_y=columns_names, column_class=column_class, data_type=data_type, values_field=values_field)
            else:
                result_stats["objects"]["hypothesis_testing"] = stats_helpers.hypothesis_testing_two_dependent_samples(
                    input_data=dataframe_all_neurons, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level,
                    input_columns_y=columns_names, column_class=column_class)

    return JsonResponse(result_stats)


@not_minified_response
def hypothesis_testing_plots_html(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    dataset_name = input_data_json["dataset_name"]
    columns_names = input_data_json["columns_names"]
    significance_level = int(input_data_json["significance_level"]) / 100
    hypothesis_test_type = input_data_json["hypothesis_test_type"]
    hypothesis_test_type_tails = input_data_json["hypothesis_test_type_tails"]

    if hypothesis_test_type == "one_sample":
        hypothesis_population_mean = float(input_data_json["hypothesis_population_mean"])
    data_type = "continuous"
    if "data_type" in input_data_json:
        data_type = input_data_json["data_type"]
    input_column_x = input_data_json["label_instances"]
    column_class = input_data_json["label_column_id"]
    result_plots = {}
    if data_type == "discrete" and hypothesis_test_type != "two_dependent_samples":
        columns_names = [columns_names]
        values_field = input_data_json["values_field"]
        num_cols = len(values_field)
    else:
        dataset_name = input_data_json["dataset_name"]
        num_cols = len(columns_names)

    if hypothesis_test_type == "two_dependent_samples":
        min_necessary_cols = 2
        max_necessary_cols = 2
        if data_type == "discrete":
            values_field_1 = input_data_json["values_field_1"]
            values_field_2 = input_data_json["values_field_2"]
            values_field = [values_field_1, values_field_2]
            if len(values_field_1) > 0 and len(values_field_2):
                num_cols = 2
    else:
        min_necessary_cols = 1
        max_necessary_cols = 0  # i.e no max columns

    result_plots = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols, max_necessary_cols)
    if not result_plots["error"]:
        columns_get = columns_names + [column_class]
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
        result_plots["objects"] = {}

        if hypothesis_test_type == "one_sample":
            dataframe_all_neurons = datasets_user.datasets[dataset_name].get_dataframe(columns_get)

            if data_type == "discrete":
                result_plots["objects"]["hypothesis_testing"] = plotly_helpers.hypothesis_testing_one_sample(
                    input_data=dataframe_all_neurons, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level,
                    hypothesis_population_mean=hypothesis_population_mean, input_columns_y=columns_names, column_class=column_class, data_type=data_type, values_field=values_field)
            else:
                result_plots["objects"]["hypothesis_testing"] = plotly_helpers.hypothesis_testing_one_sample(
                    input_data=dataframe_all_neurons, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level, hypothesis_population_mean=hypothesis_population_mean,
                    input_columns_y=columns_names, column_class=column_class)
        elif hypothesis_test_type == "two_independent_samples":
            dataframes = []
            dataframe_subgroup_1 = datasets_user.datasets[dataset_name + "_subgroup_1"].get_dataframe(columns_get)
            dataframe_subgroup_2 = datasets_user.datasets[dataset_name + "_subgroup_2"].get_dataframe(columns_get)
            dataframes += [dataframe_subgroup_1, dataframe_subgroup_2]
            if data_type == "discrete":
                result_plots["objects"]["hypothesis_testing"] = plotly_helpers.hypothesis_testing_two_independent_samples(
                    input_data=dataframes, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level,
                    input_columns_y=columns_names, column_class=column_class, data_type=data_type, values_field=values_field)
            else:
                result_plots["objects"]["hypothesis_testing"] = plotly_helpers.hypothesis_testing_two_independent_samples(
                    input_data=dataframes, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level,
                    input_columns_y=columns_names, column_class=column_class)
        elif hypothesis_test_type == "two_dependent_samples":
            dataframe_all_neurons = datasets_user.datasets[dataset_name].get_dataframe(columns_get)

            if data_type == "discrete":
                result_plots["objects"]["hypothesis_testing"] = plotly_helpers.hypothesis_testing_two_dependent_samples(
                    input_data=dataframe_all_neurons, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level,
                    input_columns_y=columns_names, column_class=column_class, data_type=data_type, values_field=values_field)
            else:
                result_plots["objects"]["hypothesis_testing"] = plotly_helpers.hypothesis_testing_two_dependent_samples(
                    input_data=dataframe_all_neurons, hypothesis_test_type_tails=hypothesis_test_type_tails,
                    input_column_x=input_column_x, significance_level=significance_level,
                    input_columns_y=columns_names, column_class=column_class)

    return JsonResponse(result_plots)


@not_minified_response
def hypothesis_testing_distribution_stats(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    dataset_name = input_data_json["dataset_name"]
    columns_names = input_data_json["columns_names"]
    significance_level = float(input_data_json["significance_level"]) / 100
    hypothesis_test_distribution = input_data_json["hypothesis_test_distribution"]
    hypothesis_test_distribution_verbose = input_data_json["hypothesis_test_distribution_verbose"]
    data_type = "continuous"
    if "data_type" in input_data_json:
        data_type = input_data_json["data_type"]
    input_column_x = input_data_json["label_instances"]
    column_class = input_data_json["label_column_id"]
    min_necessary_cols = 1
    num_cols = len(columns_names)

    result_stats = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols)
    if not result_stats["error"]:
        result_stats["objects"] = {}
        columns_get = columns_names + [column_class]
        dataframe = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME].datasets[dataset_name].get_dataframe(columns_get)

        if hypothesis_test_distribution == "most_probable_dist":
            result_stats["objects"]["find_distribution"] = stats_helpers.find_distribution(input_data=dataframe,
                                                                                           section_title="Find distribution", significance_level=significance_level,
                                                                                           input_columns_y=columns_names, data_type=data_type)
        else:
            result_stats["objects"]["hypothesis_testing"] = stats_helpers.hypothesis_testing_distribution(
                input_data=dataframe, significance_level=significance_level,
                hypothesis_test_distribution=hypothesis_test_distribution, hypothesis_test_distribution_verbose=hypothesis_test_distribution_verbose,
                input_columns_y=columns_names, data_type=data_type)

    return JsonResponse(result_stats)


@not_minified_response
def hypothesis_testing_distribution_plots_html(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)

    dataset_name = input_data_json["dataset_name"]
    columns_names = input_data_json["columns_names"]
    significance_level = float(input_data_json["significance_level"]) / 100
    hypothesis_test_distribution = input_data_json["hypothesis_test_distribution"]
    hypothesis_test_distribution_verbose = input_data_json["hypothesis_test_distribution_verbose"]
    data_type = "continuous"
    if "data_type" in input_data_json:
        data_type = input_data_json["data_type"]
    input_column_x = input_data_json["label_instances"]
    column_class = input_data_json["label_column_id"]
    min_necessary_cols = 1
    num_cols = len(columns_names)

    result_plots = stats_helpers.check_min_max_columns_to_plot(num_cols, min_necessary_cols)
    if not result_plots["error"]:
        result_plots["objects"] = {}
        columns_get = columns_names + [column_class]
        dataframe = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME].datasets[dataset_name].get_dataframe(columns_get)

        result_plots["objects"]["find_distribution_pdfs"] = plotly_helpers.find_distribution_pdf(input_data=dataframe,
                                                                                                 section_title="Find distribution PDFs", significance_level=significance_level,
                                                                                                 input_columns_y=columns_names)
        result_plots["objects"]["find_distribution_cdfs"] = plotly_helpers.find_distribution_cdf(input_data=dataframe,
                                                                                                 section_title="Find distribution CDFs", significance_level=significance_level,
                                                                                                 input_columns_y=columns_names)
        result_plots["objects"]["find_distribution_q_q_plots"] = plotly_helpers.find_distribution_q_q_plots(input_data=dataframe,
                                                                                                            section_title="Find distribution CDFs", significance_level=significance_level,
                                                                                                            input_columns_y=columns_names, label_name=column_class)

    return JsonResponse(result_plots)


# ------------Machine learning--------------------
def ml_bayesian_networks(request):
    context = {}
    if "datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME in request.session:
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

        if "ml_bayesian_network" in request.session:
            del request.session['ml_bayesian_network']
            request.session.save()
            request.session.modified = True

        if datasets_user:
            context["datasets"] = datasets_user.datasets
            context["features"] = []

            helpers.get_and_clean_datasets_types(context, request.session)

    return render(request, 'ml_bayesian_networks.html', context)

def get_features_dataset(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    dataset_name = data_client_json["dataset_name"]
    datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
    include_ids = False
    if "include_ids" in data_client_json:
        include_ids = data_client_json["include_ids"]

    all_columns = list(datasets_user.datasets[dataset_name].columns)
    if not include_ids:
        label_column_id = datasets_user.datasets[dataset_name].label_column_id
        if label_column_id in all_columns:
            all_columns.remove(label_column_id)
    data["values"] = all_columns

    # data["values"] = np.arange(0, 20000).tolist() #Just to check that selectize allow so many values

    return JsonResponse(data)


def get_features_discrete_dataset(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    dataset_name = data_client_json["dataset_name"]
    datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
    include_ids = False
    if "include_ids" in data_client_json:
        include_ids = data_client_json["include_ids"]

    all_columns = list(datasets_user.datasets[datasets_user.input_dataset_discrete_name].columns)
    if not include_ids:
        label_column_id = datasets_user.datasets[dataset_name].label_column_id
        if label_column_id in all_columns:
            all_columns.remove(label_column_id)
    data["values"] = all_columns

    # data["values"] = np.arange(0, 20000).tolist() #Just to check that selectize allow so many values

    return JsonResponse(data)


def get_features_datasets(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    datasets_names = data_client_json["datasets_names"]
    datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
    include_ids = False
    if "include_ids" in data_client_json:
        include_ids = data_client_json["include_ids"]

    merged_dataframes_columns = dataset_helper.merge_session_dataframes_columns(datasets_user, datasets_names, include_ids)

    data["values"] = merged_dataframes_columns

    return JsonResponse(data)


def ml_bn_load_only_structure(request):
    response = {}
    uploaded_files = request.FILES
    uploaded_files_dict = dict(uploaded_files)
    uploaded_file = uploaded_files_dict["adj_matrix_file"][0]
    structure = json.loads(request.POST.get("structure", "{}"))

    try:
        bn_name = os.path.splitext(uploaded_file.name)[0]
        uploaded_file_extension = os.path.splitext(uploaded_file.name)[1]
        uploaded_file_format = uploaded_file_extension.split(".")[1]

        with tempfile.NamedTemporaryFile(suffix=uploaded_file_extension) as tmp_file:
            for chunk in uploaded_file.chunks():
                tmp_file.write(chunk)
            tmp_file.flush()  # To force the saving
            path_file = tmp_file.name

            bn_io_object = bn_io.IoBn.get_io_class(uploaded_file_format)
            bn_graph = bn_io_object.import_file(path_file)

        if structure == 2:
            if "ml_bayesian_network" not in request.session:
                raise Exception('You must upload the main structure before')
            bayesian_network = request.session['ml_bayesian_network']
            bayesian_network_metrics_1 = bayesian_network
            bayesian_network_metrics_2 = bn_models.BayesianNetwork(name=bn_name, is_uploaded_bn_file=True, graph=bn_graph, session_id=request.session.session_key)

            graphs = [bayesian_network_metrics_1.graph, bayesian_network_metrics_2.graph]
            response["score_results"] = bn_score_utils.run_scores_structures(graphs).to_html(
                classes="datatable-new-ajax display no-border", escape=False, border=0)

        elif structure == 1:
            bayesian_network = bn_models.BayesianNetwork(name=bn_name, is_uploaded_bn_file=True, graph=bn_graph, session_id=request.session.session_key)

        bayesian_network.add_new_graph_edges_net_attr(bn_graph, structure)
        request.session["ml_bayesian_network"] = bayesian_network
        request.session.save()
        request.session.modified = True
    except Exception as e:
        error_message = "Error importing the {0} file: " + str(e)
        response = {"error": "Error: " + error_message}

    return JsonResponse(response)

def ml_bn_load_only_params_continuous(request):
    response = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    uploaded_files = request.FILES
    uploaded_files_dict = dict(uploaded_files)
    params_continuous_file = uploaded_files_dict["params_continuous_file"][0]

    try:
        bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
        params_continuous = params_continuous_file.file.read()
        params_continuous = json.loads(params_continuous)
        bn_parameters = {}
        for node, params in params_continuous.items():
            bn_parameters[node] = bn_learn_parameters.GaussianNode(params["mean"], params["variance"],
                                                                   params["parents_names"], params["parents_coeffs"])

        bn.set_nodes_parameters_continuous(bn_parameters)

        request.session.save()
        request.session.modified = True
    except Exception as e:
        response = {"error": "Error: upload the structure or the bayesian network file before."}

    return JsonResponse(response)

def ml_bn_load_discrete_example(request):
    response = {}

    parent_folder = os.path.dirname(os.path.abspath(__file__))
    path_file = parent_folder + os.path.sep + "helpers" + os.path.sep + "machine_learning" + os.path.sep + "bayesian_networks" + \
                os.path.sep + "files_examples" + os.path.sep + "discrete" + os.path.sep + "1_small_size(less_20_nodes)" + os.path.sep + "asia.bif"
    uploaded_bn_extension = ".bif"
    uploaded_bn_format = "bif"
    bn_name = "Asia_example"
    classes_bn_file = {}

    try:
        bn_io_object = bn_io.IoBn.get_io_class(uploaded_bn_format)
        bn_graph, bn_parameters, bn_model_original_import = bn_io_object.import_file(path_file)
    except Exception as e:
        error_message = "Error importing the {0} file".format(uploaded_bn_extension)
        response = {"error": "Error: " + error_message}

        return JsonResponse(response)

    bayesian_network = bn_models.BayesianNetwork(name=bn_name, is_uploaded_bn_file=True, graph=bn_graph, parameters=bn_parameters,
                                                 model_original_import=bn_model_original_import, features_classes=classes_bn_file,
                                                 data_type="discrete", session_id=request.session.session_key)

    path_params_file = parent_folder + os.path.sep + "helpers" + os.path.sep + "machine_learning" + os.path.sep + "bayesian_networks" + \
                       os.path.sep + "files_examples" + os.path.sep + "discrete" + os.path.sep + "1_small_size(less_20_nodes)" + os.path.sep + "asia_params.json"
    with open(path_params_file, 'r') as params:
        bn_additional_parameters = params.read()
    bn_additional_parameters = json.loads(bn_additional_parameters)
    bayesian_network.set_additional_parameters(bn_additional_parameters)

    request.session["ml_bayesian_network"] = bayesian_network
    request.session.save()
    request.session.modified = True

    return JsonResponse(response)

def ml_bn_load_continuous_example(request):
    response = {}

    parent_folder = os.path.dirname(os.path.abspath(__file__))
    path_file = parent_folder + os.path.sep + "helpers" + os.path.sep + "machine_learning" + os.path.sep + "bayesian_networks" + \
                os.path.sep + "files_examples" + os.path.sep + "discrete" + os.path.sep + "1_small_size(less_20_nodes)" + os.path.sep + "asia.bif"
    uploaded_bn_extension = ".bif"
    uploaded_bn_format = "bif"
    bn_name = "Continuous example"

    try:
        bn_io_object = bn_io.IoBn.get_io_class(uploaded_bn_format)
        bn_graph, bn_parameters, bn_model_original_import = bn_io_object.import_file(path_file)
    except Exception as e:
        error_message = "Error importing the {0} file".format(uploaded_bn_extension)
        response = {"error": "Error: " + error_message}

        return JsonResponse(response)

    model_parameters = {}
    for node in bn_graph.nodes():
        parents_names = list(bn_graph.predecessors(node))

        if len(parents_names) == 0:
            mean = np.random.randint(-10, 10)
            variance = np.random.randint(1, 10)
            parents_coeffs = []
        else:
            mean = np.random.randint(-10, 10)
            variance = np.random.randint(1, 10)
            parents_coeffs = np.random.rand(len(parents_names))

        model_parameters[node] = bn_learn_parameters.GaussianNode(mean, variance, parents_names, parents_coeffs)

    bayesian_network = bn_models.BayesianNetwork(name=bn_name, is_uploaded_bn_file=True, graph=bn_graph, parameters=model_parameters,
                                                 data_type="continuous", session_id=request.session.session_key)

    request.session["ml_bayesian_network"] = bayesian_network
    request.session.save()
    request.session.modified = True

    return JsonResponse(response)


def ml_bn_load_dataset_example(request):
    input_data = request.POST.get("data_client_json", "{}")
    input_data_json = json.loads(input_data)
    dataset_type = input_data_json["dataset_type"]

    data = {}
    parent_folder = os.path.dirname(os.path.abspath(__file__))

    if dataset_type == "discrete":
        filename = "asia10k.csv"
    else:
        filename = "iris.csv"

    path_file = parent_folder + os.path.sep + "helpers" + os.path.sep + "machine_learning" + os.path.sep + "bayesian_networks" + \
                os.path.sep + "files_examples" + os.path.sep + "datasets" + os.path.sep + filename

    dataframe = pd.read_csv(path_file)
    dataframe = dataframe.drop(columns=[dataframe.columns[0]])

    if dataset_type == "discrete":
        dataframe = dataframe.iloc[0:2000, :]
    else:
        dataframe = dataframe.drop(columns=[dataframe.columns[-1]])


    dataset = dataset_helper.Dataset("ml_bayesian_network", request.session.session_key, app_name=settings.MORPHO_ANALYZER_APP_NAME)
    dataset.load(dataframe)

    bn = bn_models.BayesianNetwork(name="example_dataset", dataset=dataset, is_uploaded_bn_file=False,
                                   model_original_import={}, features_classes=[], session_id=request.session.session_key)

    request.session["ml_bayesian_network"] = bn
    request.session.save()
    request.session.modified = True

    return JsonResponse(data)


def ml_upload_bn_file(request):
    data = {}
    response = {}

    uploaded_files = request.FILES
    uploaded_files_dict = dict(uploaded_files)
    uploaded_bn = uploaded_files_dict["uploaded_bn"][0]
    classes_bn_file = request.POST.get("classes-bn-file", "Instances")
    classes_bn_file = classes_bn_file.split(",")
    bn_name = request.POST.get("bn-name", "")

    # Parse bn file
    if bn_name == "":
        bn_name = os.path.splitext(uploaded_bn.name)[0]
    uploaded_bn_extension = os.path.splitext(uploaded_bn.name)[1]
    uploaded_bn_format = copy.deepcopy(uploaded_bn_extension).split(".")[1]
    with tempfile.NamedTemporaryFile(suffix=uploaded_bn_extension) as tmp_file:
        # bn_in_memory_file = uploaded_bn.file
        for chunk in uploaded_bn.chunks():
            tmp_file.write(chunk)
        tmp_file.flush()  # To force the saving
        path_file = tmp_file.name

        bn_model = {}
        try:
            bn_io_object = bn_io.IoBn.get_io_class(uploaded_bn_format)
            bn_graph, bn_parameters, bn_model_original_import = bn_io_object.import_file(path_file)
        except Exception as e:
            error_message = "Error importing the {0} file".format(uploaded_bn_extension)
            response = {"error": "Error: " + error_message}

            return JsonResponse(response)

    bayesian_network = bn_models.BayesianNetwork(name=bn_name, is_uploaded_bn_file=True, graph=bn_graph, parameters=bn_parameters,
                                                 model_original_import=bn_model_original_import, features_classes=classes_bn_file
                                                 , session_id=request.session.session_key)

    bayesian_network.set_random_additional_parameters()
    request.session["ml_bayesian_network"] = bayesian_network
    request.session.save()
    request.session.modified = True

    return JsonResponse(response)


def ml_upload_bn_additional_parameters(request):
    response = {}
    model_name = request.POST.get("model_name", None)
    data_client_json = {"model_name": model_name}
    uploaded_files = request.FILES
    uploaded_files_dict = dict(uploaded_files)
    additional_params_file = uploaded_files_dict["uploaded_bn_additional_parameters"][0]

    try:
        bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
        bn_additional_parameters = additional_params_file.file.read()
        bn_additional_parameters = json.loads(bn_additional_parameters)
        bn.set_additional_parameters(bn_additional_parameters)

        request.session.save()
        request.session.modified = True
    except Exception as e:
        response = {"error": "Error: upload the dataset or the bayesian network before."}

    return JsonResponse(response)


def ml_bn_upload_dataset(request):
    if not 'task_id' in request.POST.keys():  # 1st time running this method, it will enters here
        create_session_if_not_exists(request)
        params = json.loads(request.POST.get("data_client_json", "{}"))

        job = tasks.ml_bn_upload_dataset.delay(request.session.session_key, params)
        # job = tasks.ml_bn_upload_dataset(request.session.session_key, params)

        data_task = {}
        data_task["name"] = "BN upload dataset"
        data_task["description"] = "Loading dataset"
        data_task["url_on_loading"] = "/morpho/ml_bn_upload_dataset/"
        data_task["id"] = job.id

        return JsonResponse(data_task)
    else:  # Rest of time running this method, it will enters here
        return return_worker_task_inline_result(request)

def ml_bn_upload_dataset_a(session_id, params):
    print ("SESSION_ID:", session_id)

    bn_name = params["bn_name"]
    datasets_names = params["datasets_names"]
    dataframes_features = params["dataframes_features"]
    dataframes_features_classes = params["dataframes_features_classes"]
    discretize = params["discretize"]

    session = SessionStore(session_key=session_id)

    data = {}
    try:
        if bn_name == "":
            bn_name = "BN"
        if discretize:
            discretize_method = params["discretize_method"]
        if len(datasets_names) == 0:
            data["error"] = True
            data["error_message"] = "Error: Select a dataset and features or upload a bayesian network file"
        elif not dataframes_features["selected_all"] and len(dataframes_features["values"]) < 2:
            data["error"] = True
            data["error_message"] = "Error: Select two or more features; Or upload a bayesian network file"
        else:
            datasets_user = session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

            if dataframes_features["selected_all"]:
                all_columns_dataframe = None
            else:
                all_columns_dataframe = list(set(dataframes_features["values"] + dataframes_features_classes))
                all_columns_dataframe.sort()

            dataframe = dataset_helper.merge_session_dataframes(datasets_user, datasets_names,
                                                                all_columns_dataframe, include_ids=False)
            if discretize:
                discretize_parameters = params["discretize_method_parameters"]
                dataframe = ml_helpers.discretize_data(dataframe, dataframes_features_classes, discretize_method,
                                                       discretize_parameters)

            dataset = dataset_helper.Dataset("ml_bayesian_network", session.session_key,
                                             app_name=settings.MORPHO_ANALYZER_APP_NAME, delete=True)
            dataset.load(dataframe)

            bn = bn_models.BayesianNetwork(name=bn_name, dataset=dataset, is_uploaded_bn_file=False,
                                           model_original_import={}, features_classes=dataframes_features_classes, session_id=request.session.session_key)

            session["ml_bayesian_network"] = bn
            session.save()
            session.modified = True
            data["error"] = False
    except Exception as e:
        data["error"] = True
        data["error_message"] = str(e)

    """
    current_task.update_state(state='SUCCESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
    result = {"done": True,
              "session_id": session_id,
              "data_processed": data
              }
    """
    return data

def ml_bn_upload_bn(request):
    data = {}

    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    if "model_name" in data_client_json:
        model_name = data_client_json["model_name"]
    else:
        model_name = "ml_bayesian_network"
    if model_name in request.session and request.session[model_name].is_uploaded_bn_file:
        bn = request.session[model_name]

        try:
            bn_plot = bn_plotting.BnPlot(bn)
            data = bn_plot.draw_networkx_sigmajs(layout_name="circular")
            data["structures_ids"] = bn.structures_ids
            data["error"] = False
        except Exception as e:
            data["error"] = True
            data["error_message"] = str(e)
    else:
        data["error"] = True
        data["error_message"] = "Error: Select a dataset and features or upload a bayesian network file"

    return JsonResponse(data)


def ml_bn_learn_structure(request):
    if not 'task_id' in request.POST.keys():  # 1st time running this method, it will enters here
        create_session_if_not_exists(request)
        params = json.loads(request.POST.get("data_client_json", "{}"))

        job = tasks.ml_bn_learn_structure.delay(request.session.session_key, params)
        #job = tasks.ml_bn_learn_structure(request.session.session_key, params)

        data_task = {}
        data_task["name"] = "BN learn structure"
        data_task["description"] = "Learning structure"
        data_task["url_on_loading"] = "/morpho/ml_bn_learn_structure/"
        data_task["id"] = job.id

        return JsonResponse(data_task)
    else:  # Rest of time running this method, it will enters here
        return return_worker_task_inline_result(request)


def ml_bn_learn_parameters(request):
    if not 'task_id' in request.POST.keys():  # 1st time running this method, it will enters here
        create_session_if_not_exists(request)
        params = json.loads(request.POST.get("data_client_json", "{}"))

        job = tasks.ml_bn_learn_parameters.delay(request.session.session_key, params)
        #job = tasks.ml_bn_learn_parameters(request.session.session_key, params)

        data_task = {}
        data_task["name"] = "BN learn parameters"
        data_task["description"] = "Learning parameters"
        data_task["url_on_loading"] = "/morpho/ml_bn_learn_parameters/"
        data_task["id"] = job.id

        return JsonResponse(data_task)
    else:  # Rest of time running this method, it will enters here
        return return_worker_task_inline_result(request)


def ml_bn_change_layout(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    layout_name = None
    if "layout_name" in data_client_json and data_client_json["layout_name"]:
        layout_name = data_client_json["layout_name"]
    additional_params = None
    if "additional_params" in data_client_json and data_client_json["additional_params"]:
        additional_params = data_client_json["additional_params"]

    try:
        data = {}
        bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
        bn_plot = bn_plotting.BnPlot(bn)
        data["nodes_pos"], data["edges"] = bn_plot.get_layouts_nodes_pos(layout_name, additional_params)
        #data = bn_plot.draw_networkx_sigmajs(layout_name=layout_name, additional_params=additional_params)
        data["error"] = False
    except Exception as e:
        data["error"] = True
        data["error_message"] = str(e)

    return JsonResponse(data)


def ml_bn_edges_size_dependent_weights(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    data["edges_sep_char"] = "$$"  # Character to separate source and target node in the edges_sizes dict keys
    data["edges_sizes"], data["min_edge_size"], data["max_edge_size"] = bn.get_edges_sizes_by_weights(data["edges_sep_char"])

    return JsonResponse(data)


def ml_bn_nodes_sizes_dependent_markov_blanket(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    data["nodes_sizes"], data["min_node_size"], data["max_node_size"] = bn.get_nodes_sizes_by_markov_blanket()

    return JsonResponse(data)


def ml_bn_nodes_sizes_dependent_neighbors(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    data["nodes_sizes"], data["min_node_size"], data["max_node_size"] = bn.get_nodes_sizes_by_neighbors()

    return JsonResponse(data)


def ml_bn_highlight_important_nodes(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
    params = json.loads(request.POST.get("data_client_json", "{}"))
    selection_option = params["selection_option"]
    max_node_size = float(params["max_node_size"])
    if selection_option == "degrees":
        num_neighbors = int(params["num_neighbors"])
        include_neighbors = params["include_neighbors"]
        data["important_nodes"] = bn.get_important_nodes(selection_option, num_neighbors, include_neighbors, max_node_size)
    elif selection_option == "betweenness-centrality":
        data["important_nodes"] = bn.get_important_nodes(selection_option, max_node_size=max_node_size)

    return JsonResponse(data)


def ml_bn_highlight_communities(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
    params = json.loads(request.POST.get("data_client_json", "{}"))
    selection_option = params["selection_option"]
    if selection_option == "louvain":
        data["communities"] = bn.get_communities(selection_option)
        additional_parameters = bn.add_new_additional_parameters(data["communities"], 'louvain')
        bn.set_additional_parameters(additional_parameters)
        data["additional_parameters"] = list(additional_parameters['discrete_features'].keys())
        request.session.save()
        request.session.modified = True

    return JsonResponse(data)


def ml_bn_restore_highlight_communities(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
    params = json.loads(request.POST.get("data_client_json", "{}"))
    selection_option = params["selection_option"]
    additional_parameters = bn.restore_additional_parameters(selection_option)
    bn.set_additional_parameters(additional_parameters)
    data["additional_parameters"] = list(additional_parameters['discrete_features'].keys())
    request.session.save()
    request.session.modified = True

    return JsonResponse(data)


def ml_bn_create_custom_group(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
    params = json.loads(request.POST.get("data_client_json", "{}"))
    custom_group = params["custom_group"]
    group_name = params["group_name"]
    group_color = params["group_color"]
    try:
        additional_parameters = bn.add_new_additional_parameters(custom_group, 'Custom groups', group_name, group_color)
        bn.set_additional_parameters(additional_parameters)
        data["additional_parameters"] = list(additional_parameters['discrete_features'].keys())
        request.session.save()
        request.session.modified = True
    except Exception as error:
        data["error"] = True
        data["error_message"] = "Error adding custom group"

    return JsonResponse(data)


def ml_bn_get_markov_blanket(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    node_id = data_client_json["node_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        data["nodes_selection"] = bn.get_markov_blanket(node_id)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error computing the markov blanket"

    return JsonResponse(data)

def ml_bn_get_neighbors(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    node_id = data_client_json["node_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        data["nodes_selection"] = bn.get_direct_neighbors(node_id)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error getting the neighbors"

    return JsonResponse(data)

def ml_bn_get_parents(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    node_id = data_client_json["node_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        data["nodes_selection"] = bn.get_parents(node_id)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error getting the parents"

    return JsonResponse(data)


def ml_bn_get_children(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    node_id = data_client_json["node_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        data["nodes_selection"] = bn.get_children(node_id)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error getting the children"

    return JsonResponse(data)


def ml_bn_get_groups(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    model = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        data["groups"] = model.get_groups()
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error taking groups of the model"

    return JsonResponse(data)

def ml_bn_get_info_nodes_by_group(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    group_id = data_client_json["group_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        data["info_nodes"] = bn.get_info_nodes_by_group(group_id)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error taking the colors of nodes in group {0}".format(group_id)

    return JsonResponse(data)


def ml_bn_get_nodes_in_category(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    group_id = data_client_json["group_id"]
    category_id = data_client_json["category_id"]
    show_neighbors = data_client_json["show_neighbors"]
    structure_id = "all"
    if "structure_id" in data_client_json and data_client_json["structure_id"] != "":
        structure_id = data_client_json["structure_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        data["nodes_in_category"], data["color_category"], data["neighbors"] = bn.get_nodes_in_category(group_id, category_id, structure_id, show_neighbors)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error taking the nodes of the same category: {}".format(category_id)

    return JsonResponse(data)

def ml_bn_get_nodes_in_categories(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    group_id = data_client_json["group_id"]
    categories_ids = data_client_json["categories_ids"]

    try:
        bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

        data["nodes_in_categories"] = bn.get_nodes_in_categories(group_id, categories_ids)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error taking the nodes of the same group"

    return JsonResponse(data)


def ml_bn_get_nodes_same_category(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    group_id = data_client_json["group_id"]
    node_id = data_client_json["node_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        category_id = bn.get_category_node(group_id, node_id)
        data["nodes_in_category"], data["color_category"] = bn.get_nodes_in_category(group_id, category_id)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error taking the nodes of the same group"

    return JsonResponse(data)


def ml_bn_category_values_in_group(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    group_id = data_client_json["group_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        data["categories_in_group"] = bn.get_categories_in_group(group_id)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error taking the nodes of the same group"

    return JsonResponse(data)


def ml_bn_get_node_parameters(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    node_id = data_client_json["node_id"]
    inference_mode = data_client_json["inference_mode"]
    model_name = data_client_json["model_name"]
    structure_id = None
    if "structure_id" in data_client_json:
        structure_id = data_client_json["structure_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        if bn.data_type == "discrete":
            data['data_type'] = 'discrete'
            node_parameters_states, node_parameters_values = bn.get_node_parameters_discrete(node_id)
            data["node_parameters_states"] = node_parameters_states
            data["node_parameters_values"] = node_parameters_values
        else:
            data['data_type'] = 'continuous'
            if model_name == "ml_bayesian_network":
                data["node_parameters_plot_html"], data["evidence_value"] = bn.get_node_parameters_continuous(node_id)
            else:
                data["node_parameters_plot_html"], data["evidence_value"] = bn.get_node_parameters_continuous(node_id, inference_mode, structure_id)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error getting node parameters"

    return JsonResponse(data)


def ml_bn_get_nodes_evidences(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    structure_id = None
    if "structure_id" in data_client_json:
        structure_id = data_client_json["structure_id"]
    model = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        data["nodes_evidences"] = model.get_evidences_names(structure_id)
    except Exception as e:
        data["nodes_evidences"] = []

    return JsonResponse(data)


def ml_bn_set_nodes_evidences(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    nodes_ids = data_client_json["nodes_ids"]
    evidence_value = float(data_client_json["evidence_value"])
    evidence_scale = data_client_json["evidence_scale"]
    structure_id = None
    if "structure_id" in data_client_json:
        structure_id = data_client_json["structure_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        bn.set_evidences(nodes_ids, evidence_value, evidence_scale, structure_id=structure_id)

        request.session.save()
        request.session.modified = True
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error setting nodes evidences"

    return JsonResponse(data)


def sort_alteration(val):
    return val[1]


def ml_bn_show_probabilities_effect(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
    graph_groups = {}
    additional_parameters = bn.get_additional_parameters()
    if additional_parameters:
        additional_parameters_nodes = additional_parameters["nodes"]
        for discrete_feature in additional_parameters["discrete_features"]:
            category = {}
            category_names = []
            for node in additional_parameters["nodes"]:
                category_name = additional_parameters_nodes[node]["discrete_features"][discrete_feature]
                for name in category_name:
                    if name not in category_names:
                        category_names.append(name)
                        category[name] = [node]
                    else:
                        category[name].append(node)
            graph_groups[discrete_feature] = category

    not_evidences_nodes_order = bn.get_evidences().get_not_evidences_nodes_order()
    start_means, start_std_devs, current_means, current_std_devs = bn.get_probabilities_effect()
    kl_divergences = []
    mean_alteration = []
    std_deviation_alteration = []

    for count, mean in enumerate(start_means):
        kl_divergences.append([not_evidences_nodes_order[count], bn_score_utils.KL_univariate_div(start_means[count],
                                                                                                  start_std_devs[count],
                                                                                                  current_means[count],
                                                                                                  current_std_devs[count])])
        mean_alteration.append([not_evidences_nodes_order[count], current_means[count] - start_means[count]])
        std_deviation_alteration.append([not_evidences_nodes_order[count], current_std_devs[count] - start_std_devs[count]])

    kl_divergences.sort(key=sort_alteration, reverse=True)
    mean_alteration.sort(key=sort_alteration, reverse=True)
    std_deviation_alteration.sort(key=sort_alteration, reverse=True)
    if kl_divergences and mean_alteration and std_deviation_alteration:
        data["kl_divergences"] = kl_divergences
        data["mean_alteration"] = mean_alteration
        data["std_deviation_alteration"] = std_deviation_alteration
        data["graph_groups"] = graph_groups
    else:
        data["error"] = True
        data["error_message"] = "Error getting probabilities effect"

    return JsonResponse(data)


def ml_bn_show_probabilities_effect_group(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    group_chosen_id = data_client_json["group_chosen_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
    additional_parameters = bn.get_additional_parameters()
    additional_parameters_nodes = additional_parameters["nodes"]
    groups = {}

    for discrete_feature in additional_parameters["discrete_features"]:
        category = {}
        category_names = []
        for node in additional_parameters["nodes"]:
            category_name = additional_parameters_nodes[node]["discrete_features"][discrete_feature]
            for name in category_name:
                if name not in category_names:
                    category_names.append(name)
                    category[name] = [node]
                else:
                    category[name].append(node)
        groups[discrete_feature] = category

    categories = groups[group_chosen_id]
    categories_no_evidences = {}
    for category_nodes in categories:
        evidence_nodes = list(bn.get_evidences().evidences.keys())
        found = False
        for evidence_node in evidence_nodes:
            if evidence_node in categories[category_nodes]:
                found = True
        if not found:
            categories_no_evidences[category_nodes] = categories[category_nodes]

    kl_divergences_group_categories = []
    if categories_no_evidences:
        start_means, start_std_devs, current_means, current_std_devs = bn.get_probabilities_effect(list(categories_no_evidences.values()))
        for category_count, category in enumerate(categories_no_evidences):
            kl_divergences_group_categories.append(
                [category, bn_score_utils.KL_div(start_means[category_count],
                                                 start_std_devs[category_count],
                                                 current_means[category_count],
                                                 current_std_devs[category_count])])

        kl_divergences_group_categories.sort(key=sort_alteration, reverse=True)
    if kl_divergences_group_categories:
        data["kl_divergences_group_categories"] = kl_divergences_group_categories
    else:
        data["error"] = True
        data["error_message"] = "Error getting probabilities effect"

    return JsonResponse(data)


def ml_bn_clear_nodes_evidences(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    nodes_ids = data_client_json["nodes_ids"]
    structure_id = None
    if "structure_id" in data_client_json:
        structure_id = data_client_json["structure_id"]
    model = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        model.clear_evidences(nodes_ids, structure_id=structure_id)

        request.session.save()
        request.session.modified = True
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error clearing nodes evidences"

    return JsonResponse(data)

def ml_bn_get_node_connections_info(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    node_id = data_client_json["node_id"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        data["num_parents"], data["num_children"], data["num_neighbors"], \
        data["top_parents"], data["top_children"] = bn.get_node_connections_info(node_id)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error getting the connections info"

    return JsonResponse(data)


def ml_bn_get_edge_info(request):
    data = {"error": False}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    source_node = data_client_json["source_node"]
    target_node = data_client_json["target_node"]
    bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    try:
        data["weight"] = bn.get_edge_info(source_node, target_node)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error getting the connections info"

    return JsonResponse(data)


def ml_export_bn_file(request):
    data = {}
    file_format = request.POST.get("file-format-export-bn", "")
    if file_format != "":
        data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
        bn = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
        date_now = datetime.now().strftime('%Y_%m_%d_%H_%M_%S_%f')

        try:
            bn_io_object = bn_io.IoBn.get_io_class(file_format)
            filename = request.session.session_key + "_" + date_now + "_" + bn.name
            file_exported = bn_io_object.export_file(filename, bn)

            date_now = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
            filename_export = bn.name + "_" + date_now + "." + file_format
            response = HttpResponse(content_type='application/force-download')
            response['Content-Disposition'] = 'attachment;filename="{0}"'.format(filename_export)
            response.write(file_exported)

            return response
        except Exception as e:
            response = HttpResponse()
            response.write("Error exporting the file: " + str(e))
    else:
        response = HttpResponse()
        response.write("Error exporting the file")

    return response


def ml_bn_get_edges_between_nodes(request):
    data = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    model = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

    structure_id = "all"
    if "structure_id" in data_client_json and data_client_json["structure_id"] != "":
        structure_id = data_client_json["structure_id"]

    try:
        nodes_ids = data_client_json["nodes_ids"]

        data["nodes_selection"] = model.get_edges_between_nodes(nodes_ids, structure_id)
    except Exception as e:
        data["error"] = True
        data["error_message"] = "Error getting edges between nodes"


    return JsonResponse(data)

def ml_bn_get_dseparated_nodes(request):
    response = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    start_nodes = data_client_json["start_nodes"]

    model = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
    try:
        response["reachable_nodes"] = model.get_dseparated_nodes(start_nodes)
    except Exception as e:
        error_message = "Error checking d-separation"
        response = {"error": "Error: " + error_message}

    return JsonResponse(response)

def ml_bn_is_dseparated(request):
    response = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    start_nodes = data_client_json["start_nodes"]
    end_nodes = data_client_json["end_nodes"]
    observed_nodes = data_client_json["observed_nodes"]
    model = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)
    try:
        response["is_dseparated"] = model.is_dseparated(start_nodes, observed_nodes, end_nodes)
    except Exception as e:
        error_message = "Error checking d-separation"
        response = {"error": "Error: " + error_message}

    return JsonResponse(response)

def ml_bn_filter_edges_by_weight(request):
    response = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    weights_range = data_client_json["weights_range"]

    try:
        model = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

        model.filter_edges_by_weight(weights_range)
        response["num_nodes"], response["num_edges"] = model.get_network_stats()
        response["error"] = False

        request.session.save()
        request.session.modified = True
    except Exception as e:
        response["error"] = True
        response["error_message"] = str(e)

    return JsonResponse(response)


#---------------------------------------------------------------------------------------------------------

def ml_probabilistic_clustering(request):
    context = {}
    if "datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME in request.session:
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

        if "ml_probabilistic_clustering" in request.session:
            del request.session['ml_probabilistic_clustering']
            request.session.save()
            request.session.modified = True

        if datasets_user:
            context["datasets"] = datasets_user.datasets
            context["features"] = []

            helpers.get_and_clean_datasets_types(context, request.session)

            dataset_continuous_name = datasets_user.input_dataset_continuous_name
            dataset_discrete_name = datasets_user.input_dataset_discrete_name
            dataset_continuous = datasets_user.datasets[dataset_continuous_name]
            dataset_discrete = datasets_user.datasets[dataset_discrete_name]
            dataframe_continuous = dataset_continuous.get_dataframe()
            dataframe_discrete = dataset_discrete.get_dataframe()

            dataframe_continuous.columns = dataframe_continuous.columns.str.replace('(','_').str.replace(')','').str.replace('=','_')
            dataset_continuous.update_and_save(dataframe_continuous)

            df_all = pd.concat([dataframe_continuous, dataframe_discrete.drop(dataset_discrete.label_column_id, 1)],axis=1, sort=False)
            df_cols = list(df_all.columns)
            max_cols = min(len(df_cols), 150)
            context["datatable_available"] = True
            context["datatable_col_id"] = df_cols[0]
            context["datatable_columns"] = df_cols[1:max_cols]
            context["datatable_max_cols"] = max_cols+1

            datasets_user.save(request.session)
        else:
            context["datatable_available"] = False

    return render(request, 'ml_probabilistic_clustering.html', context)

def ml_prob_clustering_table(request):
    received_info = dict(request.POST)

    try:
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

        dataset_continuous_name = datasets_user.input_dataset_continuous_name
        dataset_discrete_name = datasets_user.input_dataset_discrete_name
        dataset_continuous = datasets_user.datasets[dataset_continuous_name]
        dataset_discrete = datasets_user.datasets[dataset_discrete_name]
        dataframe_continuous = dataset_continuous.get_dataframe()
        dataframe_discrete = dataset_discrete.get_dataframe()

        df_all = pd.concat([dataframe_continuous, dataframe_discrete.drop(dataset_discrete.label_column_id, 1)], axis=1,
                           sort=False)

        cols_names = list(df_all.columns)
        cols_special = [col for col in cols_names if col.startswith("P_C_")]
        datatable_json = dataframe_table_dynamic_api.to_datatable_json(received_info, df_all, cols_special)
    except Exception as e:
        datatable_json = {"error": str(e)}

    return JsonResponse(datatable_json)


def ml_upload_probabilistic_clustering_model_graph(request):
    response = {}
    uploaded_files = request.FILES
    uploaded_files_dict = dict(uploaded_files)
    input_zip_file = uploaded_files_dict["model_zip_file"][0]

    try:
        bn_name = os.path.splitext(input_zip_file.name)[0]
        uploaded_file_extension = os.path.splitext(input_zip_file.name)[1]
        uploaded_file_format = uploaded_file_extension.split(".")[1]
        if uploaded_file_format != "zip":
            raise Exception("Only zip files are supported")

        clusters = {}
        nodes_names = []
        percentage_instances = []

        for filename, cluster_file_content in global_helpers.zip_inmemoryfiles_iterator(input_zip_file):
            if filename == "cluster_information.json":
                with tempfile.NamedTemporaryFile(suffix=".json") as tmp_file:
                    tmp_file.write(cluster_file_content)
                    tmp_file.flush()  # To force the saving
                    path_file = tmp_file.name

                    with open(path_file) as json_file:
                        clusters_info = json.load(json_file)
                nodes_names = clusters_info["attributes"]
                percentage_instances = clusters_info["mixture_weights"]
                continue

            cluster_filename = filename.split("__")[0]
            cluster_id = os.path.splitext(filename)[0].split("__")[1]
            cluster_file_extension = os.path.splitext(filename)[1]
            cluster_file_format = cluster_file_extension.split(".")[1]

            if cluster_id not in clusters:
                clusters[cluster_id] = {}
                clusters[cluster_id]["joint_dist"] = {}

            if cluster_filename == "mean_vector_cluster":
                with tempfile.NamedTemporaryFile(suffix=cluster_file_extension) as tmp_file:
                    tmp_file.write(cluster_file_content)
                    tmp_file.flush()  # To force the saving
                    path_file = tmp_file.name

                    bn_io_object = bn_io.IoBn.get_io_class(cluster_file_format, prob_clustering=True)
                    mean_vector = bn_io_object.import_file_numpy_matrix(path_file)

                clusters[cluster_id]["joint_dist"]["mu"] = np.squeeze(mean_vector)
            elif cluster_filename == "covariance_matrix_cluster":
                with tempfile.NamedTemporaryFile(suffix=cluster_file_extension) as tmp_file:
                    tmp_file.write(cluster_file_content)
                    tmp_file.flush()  # To force the saving
                    path_file = tmp_file.name

                    bn_io_object = bn_io.IoBn.get_io_class(cluster_file_format, prob_clustering=True)
                    cov_matrix = bn_io_object.import_file_numpy_matrix(path_file)

                clusters[cluster_id]["joint_dist"]["sigma"] = cov_matrix
            elif cluster_filename == "precision_matrix_cluster":
                with tempfile.NamedTemporaryFile(suffix=cluster_file_extension) as tmp_file:
                    tmp_file.write(cluster_file_content)
                    tmp_file.flush()  # To force the saving
                    path_file = tmp_file.name

                    bn_io_object = bn_io.IoBn.get_io_class(cluster_file_format, prob_clustering=True)
                    bn_graph = bn_io_object.import_file_precision_matrix(path_file)

                clusters[cluster_id]["graph"] = bn_graph
            else:
                raise Exception("File {} not supported".format(cluster_filename))

        graph_no_edges = nx.DiGraph()
        graph_no_edges.add_nodes_from(nodes_names)
        probabilistic_clustering_model = bn_models.ProbabilisticClustering(name=bn_name, is_uploaded_bn_file=True, graph=graph_no_edges,
                                                                           data_type="continuous", session_id=request.session.session_key,
                                                                           #ProbabilisticClustering params:
                                                                           structures_data=clusters)

        for i, cluster_id in enumerate(clusters.keys()):
            cluster_i_graph = clusters[cluster_id]["graph"]
            cluster_i_graph = bn_models.BayesianNetwork.relabel_nodes_names(cluster_i_graph, nodes_names)
            clusters[cluster_id]["percentage_instances"] = percentage_instances[i]
            probabilistic_clustering_model.add_new_multi_graph_edges_net_attr(cluster_i_graph, cluster_id)

        request.session["ml_probabilistic_clustering"] = probabilistic_clustering_model
        request.session.save()
        request.session.modified = True
    except Exception as e:
        error_message = "Error importing the {0} file: " + str(e)
        response = {"error": "Error: " + error_message}

    return JsonResponse(response)


def ml_upload_probabilistic_clustering_model(request):
    response = {}
    uploaded_files = request.FILES
    uploaded_files_dict = dict(uploaded_files)
    input_zip_file = uploaded_files_dict["model_zip_file"][0]

    try:
        bn_name = os.path.splitext(input_zip_file.name)[0]
        uploaded_file_extension = os.path.splitext(input_zip_file.name)[1]
        uploaded_file_format = uploaded_file_extension.split(".")[1]
        if uploaded_file_format != "zip":
            raise Exception("Only zip files are supported")

        clusters = {}
        nodes_names = []
        percentage_instances = []

        for filename, cluster_file_content in global_helpers.zip_inmemoryfiles_iterator(input_zip_file):
            if filename == "cluster_information.json":
                with tempfile.NamedTemporaryFile(suffix=".json") as tmp_file:
                    tmp_file.write(cluster_file_content)
                    tmp_file.flush()  # To force the saving
                    path_file = tmp_file.name

                    with open(path_file) as json_file:
                        clusters_info = json.load(json_file)
                nodes_names = clusters_info["attributes"]
                percentage_instances = clusters_info["mixture_weights"]
                continue

            cluster_filename = filename.split("__")[0]
            cluster_id = os.path.splitext(filename)[0].split("__")[1]
            cluster_file_extension = os.path.splitext(filename)[1]
            cluster_file_format = cluster_file_extension.split(".")[1]

            if cluster_id not in clusters:
                clusters[cluster_id] = {}
                clusters[cluster_id]["joint_dist"] = {}

            if cluster_filename == "mean_vector_cluster":
                with tempfile.NamedTemporaryFile(suffix=cluster_file_extension) as tmp_file:
                    tmp_file.write(cluster_file_content)
                    tmp_file.flush()  # To force the saving
                    path_file = tmp_file.name

                    bn_io_object = bn_io.IoBn.get_io_class(cluster_file_format, prob_clustering=True)
                    mean_vector = bn_io_object.import_file_numpy_matrix(path_file)

                clusters[cluster_id]["joint_dist"]["mu"] = np.squeeze(mean_vector)

                #-------------------------
                # Initial graphs:
                graph_complete = nx.complete_graph(len(nodes_names), create_using=nx.DiGraph)
                #graph_complete.add_nodes_from(nodes_names)
                clusters[cluster_id]["graph"] = graph_complete
            elif cluster_filename == "covariance_matrix_cluster":
                with tempfile.NamedTemporaryFile(suffix=cluster_file_extension) as tmp_file:
                    tmp_file.write(cluster_file_content)
                    tmp_file.flush()  # To force the saving
                    path_file = tmp_file.name

                    bn_io_object = bn_io.IoBn.get_io_class(cluster_file_format, prob_clustering=True)
                    cov_matrix = bn_io_object.import_file_numpy_matrix(path_file)

                clusters[cluster_id]["joint_dist"]["sigma"] = cov_matrix
            else:
                raise Exception("File {} not supported".format(cluster_filename))

        graph_no_edges = nx.DiGraph()
        graph_no_edges.add_nodes_from(nodes_names)
        probabilistic_clustering_model = bn_models.ProbabilisticClustering(name=bn_name, is_uploaded_bn_file=True, graph=graph_no_edges,
                                                                           data_type="continuous", session_id=request.session.session_key,
                                                                           #ProbabilisticClustering params:
                                                                           structures_data=clusters)

        for i, cluster_id in enumerate(clusters.keys()):
            cluster_i_graph = clusters[cluster_id]["graph"]
            cluster_i_graph = bn_models.BayesianNetwork.relabel_nodes_names(cluster_i_graph, nodes_names)
            clusters[cluster_id]["percentage_instances"] = percentage_instances[i]
            probabilistic_clustering_model.add_new_multi_graph_edges_net_attr(cluster_i_graph, cluster_id)

        request.session["ml_probabilistic_clustering"] = probabilistic_clustering_model
        request.session.save()
        request.session.modified = True
    except Exception as e:
        error_message = "Error importing the {0} file: " + str(e)
        response = {"error": "Error: " + error_message}

    return JsonResponse(response)


def ml_prob_clustering_load_example(request):
    response = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    model_example_name = data_client_json["model_example_name"]

    parent_folder = os.path.dirname(os.path.abspath(__file__))
    if model_example_name == "allen":
        file_name = "test_mario_hclust2000.zip"
    elif model_example_name == "mtcars":
        file_name = "mtcars_vbgmm_4.zip"
    else:
        raise Exception("Model file {} not found".format(model_example_name))

    path_file = parent_folder + os.path.sep + "helpers" + os.path.sep + "machine_learning" + os.path.sep + "bayesian_networks" + \
                os.path.sep + "files_examples" + os.path.sep + "prob_clustering" + os.path.sep + file_name

    try:
        bn_name = os.path.splitext(file_name)[0]
        uploaded_file_extension = os.path.splitext(file_name)[1]
        uploaded_file_format = uploaded_file_extension.split(".")[1]
        if uploaded_file_format != "zip":
            raise Exception("Only zip files are supported")

        clusters = {}
        nodes_names = []
        percentage_instances = []

        for filename, cluster_file_content in global_helpers.zip_filepath_iterator(path_file):
            if filename == "cluster_information.json":
                with tempfile.NamedTemporaryFile(suffix=".json") as tmp_file:
                    tmp_file.write(cluster_file_content)
                    tmp_file.flush()  # To force the saving
                    path_file = tmp_file.name

                    with open(path_file) as json_file:
                        clusters_info = json.load(json_file)
                nodes_names = clusters_info["attributes"]
                percentage_instances = clusters_info["mixture_weights"]
                continue

            cluster_filename = filename.split("__")[0]
            cluster_id = os.path.splitext(filename)[0].split("__")[1]
            cluster_file_extension = os.path.splitext(filename)[1]
            cluster_file_format = cluster_file_extension.split(".")[1]

            if cluster_id not in clusters:
                clusters[cluster_id] = {}
                clusters[cluster_id]["joint_dist"] = {}

            if cluster_filename == "mean_vector_cluster":
                with tempfile.NamedTemporaryFile(suffix=cluster_file_extension) as tmp_file:
                    tmp_file.write(cluster_file_content)
                    tmp_file.flush()  # To force the saving
                    path_file = tmp_file.name

                    bn_io_object = bn_io.IoBn.get_io_class(cluster_file_format, prob_clustering=True)
                    mean_vector = bn_io_object.import_file_numpy_matrix(path_file)

                clusters[cluster_id]["joint_dist"]["mu"] = np.squeeze(mean_vector)
            elif cluster_filename == "covariance_matrix_cluster":
                with tempfile.NamedTemporaryFile(suffix=cluster_file_extension) as tmp_file:
                    tmp_file.write(cluster_file_content)
                    tmp_file.flush()  # To force the saving
                    path_file = tmp_file.name

                    bn_io_object = bn_io.IoBn.get_io_class(cluster_file_format, prob_clustering=True)
                    cov_matrix = bn_io_object.import_file_numpy_matrix(path_file)

                clusters[cluster_id]["joint_dist"]["sigma"] = cov_matrix
            elif cluster_filename == "precision_matrix_cluster":
                with tempfile.NamedTemporaryFile(suffix=cluster_file_extension) as tmp_file:
                    tmp_file.write(cluster_file_content)
                    tmp_file.flush()  # To force the saving
                    path_file = tmp_file.name

                    bn_io_object = bn_io.IoBn.get_io_class(cluster_file_format, prob_clustering=True)
                    bn_graph = bn_io_object.import_file_precision_matrix(path_file)
                    bn_graph = bn_models.BayesianNetwork.relabel_nodes_names(bn_graph, nodes_names)

                clusters[cluster_id]["graph"] = bn_graph
            else:
                raise Exception("File {} not supported".format(cluster_filename))

        graph_no_edges = nx.DiGraph()
        graph_no_edges.add_nodes_from(nodes_names)
        probabilistic_clustering_model = bn_models.ProbabilisticClustering(name=bn_name, is_uploaded_bn_file=True,
                                                                           graph=graph_no_edges,
                                                                           data_type="continuous",
                                                                           session_id=request.session.session_key,
                                                                           # ProbabilisticClustering params:
                                                                           structures_data=clusters)

        for i, cluster_id in enumerate(clusters.keys()):
            cluster_i_graph = clusters[cluster_id]["graph"]
            clusters[cluster_id]["percentage_instances"] = percentage_instances[i]
            probabilistic_clustering_model.add_new_multi_graph_edges_net_attr(cluster_i_graph, cluster_id)


        if model_example_name == "allen":
            metadata_file = "most_relevant_features.json"
            path_metadata_file = parent_folder + os.path.sep + "helpers" + os.path.sep + "machine_learning" + os.path.sep + "bayesian_networks" + \
                                 os.path.sep + "files_examples" + os.path.sep + "prob_clustering" + os.path.sep + "test_mario_hclust2000" + os.path.sep + metadata_file

            with open(path_metadata_file, 'r') as params:
                metadata = params.read()
            metadata = json.loads(metadata)
            probabilistic_clustering_model.set_additional_parameters(metadata)

        request.session["ml_probabilistic_clustering"] = probabilistic_clustering_model
        request.session.save()
        request.session.modified = True
    except Exception as e:
        error_message = "Error importing the {0} file: " + str(e) + ". Make sure you have exactly the required files in your zip."
        response = {"error": "Error: " + error_message}

    return JsonResponse(response)


def ml_bn_select_structure_from_multi_structures(request):
    response = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    structure_id = data_client_json["structure_id"]

    try:
        model = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

        response["evidence_nodes"] = model.structures_data[structure_id]["evidences"].evidences.keys()
    except Exception as e:
        error_message = "Error importing the {0} file: " + str(e)
        response = {"error": "Error: " + error_message}

    return JsonResponse(response)

def ml_prob_clustering_update_glasso(request):
    response = {}
    data_client_json = json.loads(request.POST.get("data_client_json", "{}"))
    alpha = float(data_client_json["alpha"])
    tol = float(data_client_json["tol"])
    max_iter = int(data_client_json["max_iter"])

    try:
        model = bn_models.BayesianNetwork.get_saved_bn_by_model_name(data_client_json, request.session)

        model.update_glasso_paramters(alpha, tol, max_iter)

        # model_plot = bn_plotting.BnPlot(model)
        # response = model_plot.draw_networkx_sigmajs()
        response["num_nodes"], response["num_edges"] = model.get_network_stats()
        response["error"] = False

        request.session.save()
        request.session.modified = True
    except Exception as e:
        response["error"] = True
        response["error_message"] = str(e)

    return JsonResponse(response)

#==================================================================================

def ml_non_probabilistic_clustering(request):
    context = {}
    if "datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME in request.session:
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

        if "ml_bayesian_network" in request.session:
            del request.session['ml_bayesian_network']
            request.session.save()
            request.session.modified = True

        if datasets_user:
            context["datasets"] = datasets_user.datasets
            context["features"] = []

            helpers.get_and_clean_datasets_types(context, request.session)

    return render(request, 'ml_non_probabilistic_clustering.html', context)

def ml_non_probabilistic_features_selection(request):
    if not 'task_id' in request.POST.keys():  # 1st time running this method, it will enters here
        create_session_if_not_exists(request)
        params = json.loads(request.POST.get("data_client_json", "{}"))

        #job = tasks.ml_supervised_class_features_selection.delay(request.session.session_key, params)
        job = tasks.ml_non_probabilistic_clustering_features_selection(request.session.session_key, params)

        data_task = {}
        data_task["name"] = "Non probabilistic clustering features selection"
        data_task["description"] = "Selecting the features"
        data_task["url_on_loading"] = "/morpho/ml_non_probabilistic_features_selection/"

        #data_task["id"] = job.id

        return JsonResponse(data_task)
    else:  # Rest of time running this method, it will enters here
        return return_worker_task_inline_result(request)

def ml_non_probabilistic_class_learn(request):
    if not 'task_id' in request.POST.keys():  # 1st time running this method, it will enters here
        create_session_if_not_exists(request)
        params = json.loads(request.POST.get("data_client_json", "{}"))

        # job = tasks.ml_non_probabilistic_class_learn.delay(request.session.session_key, params)
        job = tasks.ml_non_probabilistic_class_learn(request.session.session_key, params)

        data_task = {}
        data_task["name"] = "Non probabilistic learning algorithms"
        data_task["description"] = "Running learning algorithms"
        data_task["url_on_loading"] = "/morpho/ml_non_probabilistic_class_learn/"
        #data_task["summary"] = job.result["data_processed"]["summary"]
        if (job["data_processed"]["error"]):
            data_task["error"] = job["data_processed"]["error"]
            data_task["error_message"] = job["data_processed"]["error_message"]
        else:
            data_task["summary"] = job["data_processed"]["summary"]
        #data_task["id"] = job.id

        return JsonResponse(data_task)
    else:  # Rest of time running this method, it will enters here
        return return_worker_task_inline_result(request)

#==================================================================================
def ml_supervised_classification(request):
    context = {}
    if "datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME in request.session:
        datasets_user = request.session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

        if "ml_bayesian_network" in request.session:
            del request.session['ml_bayesian_network']
            request.session.save()
            request.session.modified = True

        if datasets_user:
            context["datasets"] = datasets_user.datasets
            context["features"] = []

            helpers.get_and_clean_datasets_types(context, request.session)

    return render(request, 'ml_supervised_classification.html', context)

def ml_supervised_class_load_dataset_example(request):
    input_data = request.POST.get("data_client_json", "{}")
    input_data_json = json.loads(input_data)
    dataset_type = input_data_json["dataset_type"]

    data = {}
    parent_folder = os.path.dirname(os.path.abspath(__file__))

    if dataset_type == "discrete":
        filename = "asia10k.csv"
    else:
        filename = "iris_one_output.csv"

    path_file = parent_folder + os.path.sep + "helpers" + os.path.sep + "machine_learning" + os.path.sep + "supervised_classification" + \
                os.path.sep + "files_examples" + os.path.sep + "datasets" + os.path.sep + dataset_type + os.path.sep + filename

    dataframe = pd.read_csv(path_file)
    id_column = dataframe.columns[0]
    dataframe = dataframe.iloc[0:min(2000,dataframe.shape[0]) :]
    features_classes = [dataframe.columns[-1]]

    supervised_classification = supervised_class_models.SupervisedClassification("example_dataset", dataframe, id_column, features_classes, request.session.session_key)

    request.session["ml_supervised_classification"] = supervised_classification
    request.session.save()
    request.session.modified = True

    return JsonResponse(data)

def ml_supervised_class_features_selection(request):
    if not 'task_id' in request.POST.keys():  # 1st time running this method, it will enters here
        create_session_if_not_exists(request)
        params = json.loads(request.POST.get("data_client_json", "{}"))

        job = tasks.ml_supervised_class_features_selection(request.session.session_key, params)
        #job = tasks.ml_supervised_class_features_selection.delay(request.session.session_key,s params)

        data_task = {}
        data_task["name"] = "Supervised classification features selection"
        data_task["description"] = "Selecting the features"
        data_task["url_on_loading"] = "/morpho/ml_supervised_class_features_selection/"
#        if ("data_processed" in job.result):
#            if( job.result["data_processed"]["error"] ):
#                data_task["error"] = job.result["data_processed"]["error"]
#                data_task["error_message"] = job.result["data_processed"]["error_message"]
#            else:
#                data_task["summary"] = job.result["data_processed"]["summary"]
#        data_task["id"] = job.id


        if( job["data_processed"]["error"] ):
            data_task["error"] = job["data_processed"]["error"]
            data_task["error_message"] = job["data_processed"]["error_message"]
        else:
            data_task["summary"] = job["data_processed"]["summary"]


        return JsonResponse(data_task)
    else:  # Rest of time running this method, it will enters here
        return return_worker_task_inline_result(request)

def ml_non_probabilistic_class_download_clusters(request):

    non_probabilistic_clustering = request.session["non_probabilistic_clustering"]

    clusters = non_probabilistic_clustering.get_clusters()

    #if format == "csv":
    result = clusters.to_csv(index=False)


    response = HttpResponse(content_type='application/force-download')
    response['Content-Disposition'] = 'attachment;filename="dataset.csv"'
    response.write(result)

    return response

def ml_supervised_class_set_sets(request):
    response = {}
    input_data = request.POST.get("data_client_json", "{}")
    input_data_json = json.loads(input_data)
    k_folds = int(input_data_json["k_folds"])
    percentage_test_set = int(input_data_json["percentage_test_set"])
    shuffle_instances = bool(input_data_json["shuffle_instances"])

    try:
        supervised_classification = request.session["ml_supervised_classification"]
        supervised_classification.set_sets_config(k_folds, percentage_test_set, shuffle_instances)
        request.session.save()
        request.session.modified = True
        response["error"] = False
    except Exception as e:
        response["error"] = True
        response["error_message"] = str(e)

    return JsonResponse(response)


def ml_supervised_class_learn(request):
    if not 'task_id' in request.POST.keys():  # 1st time running this method, it will enters here
        create_session_if_not_exists(request)
        params = json.loads(request.POST.get("data_client_json", "{}"))

        job = tasks.ml_supervised_class_learn.delay(request.session.session_key, params)
        #job = tasks.ml_supervised_class_learn(request.session.session_key, params)

        data_task = {}
        data_task["name"] = "Supervised classification learning algorithms"
        data_task["description"] = "Running learning algorithms"
        data_task["url_on_loading"] = "/morpho/ml_supervised_class_learn/"
        data_task["id"] = job.id

        return JsonResponse(data_task)
    else:  # Rest of time running this method, it will enters here
        return return_worker_task_inline_result(request)


@not_minified_response
def ml_supervised_class_evaluation_stats_table(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)
    column_idx = int(input_data_json["columns_names"][0])
    set_name = input_data_json["set_name"]

    result_stats = {}

    supervised_classification = request.session["ml_supervised_classification"]
    dataframe = supervised_classification.summary_evaluation[set_name]
    columns_names = [dataframe.columns[column_idx]]
    input_column_x = "Model"

    result_stats["objects"] = {}
    result_stats["objects"]["descriptive_stats_table"] = stats_helpers.univariate_descriptive_stats(input_data=dataframe, input_column_x=input_column_x,
                                                                                                    input_columns_y=columns_names, data_type="continuous")

    return JsonResponse(result_stats)


@not_minified_response
def ml_supervised_class_evaluation_plots(request):
    input_data = request.POST.get("input_data_json", "{}")
    input_data_json = json.loads(input_data)
    column_idx = int(input_data_json["columns_names"][0])
    set_name = input_data_json["set_name"]

    result_plots = {}

    supervised_classification = request.session["ml_supervised_classification"]
    dataframe = supervised_classification.summary_evaluation[set_name]
    columns_names = [dataframe.columns[column_idx]]
    input_column_x = "Model"
    column_class = input_column_x
    label_name = input_column_x
    scatter_point_label = input_column_x

    result_plots["objects"] = {}
    result_plots["objects"]["bar_chart_univariate"] = plotly_helpers.bar_chart_univariate(input_data=dataframe, input_column_x=input_column_x,
                                                                                          input_columns_y=columns_names, column_class=column_class, group_categories=False)
    result_plots["objects"]["pdf_plots_univariate"] = plotly_helpers.density_functions(input_data=dataframe, input_column_x=input_column_x,
                                                                                       input_columns_y=columns_names, subplots=True, normalize=False)
    result_plots["objects"]["boxplots"] = plotly_helpers.boxplot_univariate(input_data=dataframe, input_column_x=input_column_x,
                                                                            input_columns_y=columns_names, column_class=column_class, label_name=label_name)
    result_plots["objects"]["scatter_univariate"] = plotly_helpers.scatter_univariate(input_data=dataframe, input_column_x=input_column_x,
                                                                                      input_columns_y=columns_names, column_class=column_class,
                                                                                      scatter_point_label = scatter_point_label, label_name=label_name)

    return JsonResponse(result_plots)

def ml_supervised_class_download_predictions(request):
    algorithm = request.POST.get("algorithm", None)
    set = request.POST.get("set", None)
    format = request.POST.get("format", None)

    supervised_classification = request.session["ml_supervised_classification"]
    x_train, y_train, x_test, y_test = supervised_classification.get_dataframes_x_y()
    model = supervised_classification.models[algorithm]
    if set == "training_val":
        predictions_array = model.predict(x_train)
        predictions_array_proba = model.predict_proba(x_train)
        ids = supervised_classification.ids[0:y_train.shape[0]]
        true_vals = y_train.values
    else:
        predictions_array = model.predict(x_test)
        predictions_array_proba = model.predict_proba(x_test)
        ids = supervised_classification.ids[y_train.shape[0]:]
        true_vals = y_test.values

    dataframe_predictions = pd.DataFrame(predictions_array, columns=supervised_classification.features_classes)
    dataframe_predictions[supervised_classification.id_column] = ids
    cols_sorted = [supervised_classification.id_column] + supervised_classification.features_classes
    dataframe_predictions = dataframe_predictions[cols_sorted]

    col_true_vals = supervised_classification.features_classes[0] + " (True value)"
    if len(supervised_classification.features_classes) > 1:
        col_true_vals = list(map(lambda x: x + " (True value)", supervised_classification.features_classes))
        cont = 0
        for col in col_true_vals:
            dataframe_predictions[col] = true_vals[:, cont]
            cont = cont + 1
    else:
        dataframe_predictions[col_true_vals] = true_vals

    col_prediction = supervised_classification.features_classes[0] + " (Prediction)"
    if len(supervised_classification.features_classes) > 1:
        col_prediction = list(map(lambda x: x + " (Prediction)", supervised_classification.features_classes))

    if predictions_array_proba is not None:
        dataframe_predictions = pd.concat([dataframe_predictions, predictions_array_proba], axis=1, sort=False)

        cols_classes = [supervised_classification.features_classes[0] + " (Prediction probability) - " + class_name for class_name in predictions_array_proba.columns]
        cols_renamed = [supervised_classification.id_column] + [col_prediction] + [col_true_vals] + cols_classes
        dataframe_predictions.columns = cols_renamed
    else:
        cols_renamed = [supervised_classification.id_column] + [col_prediction] + [col_true_vals]
        if len(supervised_classification.features_classes) > 1:
            cols_renamed = [supervised_classification.id_column , *col_prediction , *col_true_vals]
        dataframe_predictions.columns = cols_renamed

    dataframe_predictions.sort_values(by=[supervised_classification.id_column], inplace=True)

    if format == "csv":
        result = dataframe_predictions.to_csv(index=False)
    elif format == "parquet":
        format = "parquet.gzip"
        buffer = BytesIO()
        dataframe_predictions.to_parquet(buffer, engine="pyarrow", compression="gzip")
        result = buffer.getvalue()

    response = HttpResponse(content_type='application/force-download')
    response['Content-Disposition'] = 'attachment;filename="{0}"'.format(algorithm + "_" + set + ".{}".format(format))
    response.write(result)

    return response

def ml_supervised_class_download_wrong_predictions(request):
    algorithm = request.POST.get("algorithm", None)
    set = request.POST.get("set", None)
    format = request.POST.get("format", None)

    supervised_classification = request.session["ml_supervised_classification"]
    x_train, y_train, x_test, y_test = supervised_classification.get_dataframes_x_y()
    model = supervised_classification.models[algorithm]
    if set == "training_val":
        predictions_array = model.predict(x_train)
        predictions_array_proba = model.predict_proba(x_train)
        ids = supervised_classification.ids[0:y_train.shape[0]]
        true_vals = y_train.values
    else:
        predictions_array = model.predict(x_test)
        predictions_array_proba = model.predict_proba(x_test)
        ids = supervised_classification.ids[y_train.shape[0]:]
        true_vals = y_test.values

    dataframe_wrong_predictions = pd.DataFrame(predictions_array, columns=supervised_classification.features_classes)
    dataframe_wrong_predictions[supervised_classification.id_column] = ids
    cols_sorted = [supervised_classification.id_column] + supervised_classification.features_classes
    dataframe_wrong_predictions = dataframe_wrong_predictions[cols_sorted]

    col_true_vals = supervised_classification.features_classes[0] + " (True value)"
    dataframe_wrong_predictions[col_true_vals] = true_vals

    col_prediction = supervised_classification.features_classes[0] + " (Prediction)"

    if predictions_array_proba is not None:
        dataframe_wrong_predictions = pd.concat([dataframe_wrong_predictions, predictions_array_proba], axis=1, sort=False)

        cols_classes = [supervised_classification.features_classes[0] + " (Prediction probability) - " + class_name for class_name in predictions_array_proba.columns]
        cols_renamed = [supervised_classification.id_column] + [col_prediction] + [col_true_vals] + cols_classes
        dataframe_wrong_predictions.columns = cols_renamed
    else:
        cols_renamed = [supervised_classification.id_column] + [col_prediction] + [col_true_vals]
        dataframe_wrong_predictions.columns = cols_renamed

    dataframe_wrong_predictions.sort_values(by=[supervised_classification.id_column], inplace=True)
    mask_corect_predictions = dataframe_wrong_predictions[col_prediction] == dataframe_wrong_predictions[col_true_vals]
    dataframe_wrong_predictions = dataframe_wrong_predictions[~mask_corect_predictions]
    dataframe_wrong_predictions = dataframe_wrong_predictions.reset_index(drop=True)

    if format == "csv":
        result = dataframe_wrong_predictions.to_csv(index=False)
    elif format == "parquet":
        format = "parquet.gzip"
        buffer = BytesIO()
        dataframe_wrong_predictions.to_parquet(buffer, engine="pyarrow", compression="gzip")
        result = buffer.getvalue()

    response = HttpResponse(content_type='application/force-download')
    response['Content-Disposition'] = 'attachment;filename="{0}"'.format(algorithm + "_" + set + ".{}".format(format))
    response.write(result)

    return response

def ml_supervised_class_model_explanation(request):
    response = {"error": False}
    input_data = request.POST.get("data_client_json", "{}")
    input_data_json = json.loads(input_data)
    algorithm = input_data_json["algorithm"]

    supervised_classification = request.session["ml_supervised_classification"]
    x_train, y_train, x_test, y_test = supervised_classification.get_dataframes_x_y()
    model = supervised_classification.models[algorithm]

    try:
        features_names = list(x_train.columns)
        classes_names = list(y_train.columns)
        response["text"], response["plot"] = model.show_explanation(features_names, classes_names)
    except Exception as e:
        response["error"] = True
        response["error_message"] = str(e)

    return JsonResponse(response)

def ml_supervised_class_upload_prediction_new_dataset(request):
    response = {}
    uploaded_files = request.FILES
    uploaded_files_dict = dict(uploaded_files)
    uploaded_file = uploaded_files_dict["new_prediction_dataset"][0]
    col_id = json.loads(request.POST.get("col_id", "{}"))

    try:
        filename = os.path.splitext(uploaded_file.name)[0]
        uploaded_file_extension = os.path.splitext(uploaded_file.name)[1]
        uploaded_file_format = uploaded_file_extension.split(".")[1]

        dataframe_file_str = io.StringIO(uploaded_file.file.getvalue().decode("utf-8"))
        if uploaded_file_format == "csv":
            new_prediction_dataset_df = pd.read_csv(dataframe_file_str)
        elif uploaded_file_format == "parquet.gzip":
            new_prediction_dataset_df = pd.read_parquet(dataframe_file_str, engine="fastparquet")
        else:
            error_message = "Format {} is not allowed. Only csv and parquet.gzip are accepted"
            response = {"error": "Error: " + error_message}
            return JsonResponse(response)

        supervised_classification = request.session["ml_supervised_classification"]
        supervised_classification.load_new_prediction_dataset(new_prediction_dataset_df)

        request.session.save()
        request.session.modified = True
    except Exception as e:
        error_message = "Error importing the {0} file: " + str(e)
        response = {"error": "Error: " + error_message}

    return JsonResponse(response)

def ml_supervised_class_prediction_new_dataset(request):
    input_data = request.POST.get("data_client_json", "{}")
    input_data_json = json.loads(input_data)
    algorithms = input_data_json["algorithms"]
    format = input_data_json["format"]
    format_file = format

    if format == "parquet":
        format_file = "parquet.gzip"

    try:
        dataframes_predictions = {}
        supervised_classification = request.session["ml_supervised_classification"]
        new_prediction_dataframe, new_prediction_dataset_ids = supervised_classification.get_new_prediction_dataframe()

        for algorithm in algorithms:
            model = supervised_classification.models[algorithm]
            model_prediction = model.predict(new_prediction_dataframe)
            model_prediction_proba = model.predict_proba(new_prediction_dataframe)

            model_prediction_df = pd.DataFrame(model_prediction, columns=supervised_classification.features_classes)
            model_prediction_df[supervised_classification.id_column] = new_prediction_dataset_ids
            cols_sorted = [supervised_classification.id_column] + supervised_classification.features_classes
            model_prediction_df = model_prediction_df[cols_sorted]
            col_prediction = supervised_classification.features_classes[0] + " (Prediction)"

            if model_prediction_proba is not None:
                model_prediction_df = pd.concat([model_prediction_df, model_prediction_proba], axis=1, sort=False)

                cols_classes = [
                    supervised_classification.features_classes[0] + " (Prediction probability) - " + class_name
                    for class_name in model_prediction_proba.columns]
                cols_renamed = [supervised_classification.id_column] + [col_prediction] + cols_classes
            else:
                cols_renamed = [supervised_classification.id_column] + [col_prediction]

            model_prediction_df.columns = cols_renamed

            if format == "csv":
                dataframes_predictions[algorithm] = model_prediction_df.to_csv(index=False)
            elif format == "parquet":
                buffer = BytesIO()
                model_prediction_df.to_parquet(buffer, engine="pyarrow", compression="gzip")
                dataframes_predictions[algorithm] = buffer.getvalue()

        if len(algorithms) > 1:
            zip_subdir = "New_data_set_predictions"
            zip_filename = "{0}.zip".format(zip_subdir)
            zip_file_memory = BytesIO()
            zf = zipfile.ZipFile(zip_file_memory, "w")

            for algorithm in algorithms:
                zf.writestr(algorithm + "." + format_file, dataframes_predictions[algorithm])

            zf.close()

            zip = {"zip_filename": zip_filename, "zip_file": zip_file_memory}

            response = HttpResponse(zip["zip_file"].getvalue(), content_type='application/zip')
            response['Content-Disposition'] = 'attachment; filename={0}'.format(zip["zip_filename"])
        else:
            response = HttpResponse(content_type='application/force-download')
            response['Content-Disposition'] = 'attachment;filename="new_dataset_predictions_{0}"'.format(
                algorithms[0] + ".{}".format(format_file))
            response.write(dataframes_predictions[algorithms[0]])
    except Exception as e:
        error_message = "Error importing the {0} file: " + str(e)
        response = JsonResponse({"error": "Error: " + error_message})

    return response

#==================================================================================

@not_minified_response
def load_data_morpho_overview(request):
    data = {}

    print ("Morpho overview started! \n\n\n")

    neurons_user = request.session['neurons_user']
    data["neurons_user"] = neurons_user
    data["fields_to_display"] = ["species", "archive", "age_classification", "domain", "brain_region", "cell_type", "png_url"]
    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'load_data_morpho_overview.html', data)


def morpho_neuron_overview(request):
    data = {}

    print ("Morpho morpho_neuron_overview started! \n\n\n")

    neuron_selected = request.POST.get("neuron_selected")
    neurons_user = request.session['neurons_user']

    for neuron in neurons_user:
        if neuron["name"] == neuron_selected:
            data["neuron"] = neuron
            features_l_measure = get_features([neuron])["dataframe"].to_dict(orient='records')
            if (len(features_l_measure) > 0):
                data["features_l_measure"] = features_l_measure[0]
            data["neurostr_validator_output"] = check_neurons.check_neuron(neuron)
            data["neuron"]["data_file_b64"] = b64encode(helpers.get_file_neuron(neuron).file.getvalue()) #We send base64 object, javascript translate it into blob
            data["gabaclassifier_output"] = {}
            if not "error" in data["neurostr_validator_output"]:
                data["gabaclassifier_output"]["result"] = helpers.morpho_classify_interneuron_gabaclassifier(neuron, verbose_output=True)
            data["gabaclassifier_output"]["possible_interneuron_classes"] = gabaclassifier.get_possible_interneuron_classes()
            break

    helpers.get_and_clean_datasets_types(data, request.session)

    return render(request, 'morpho_neuron_overview.html', data)


# Deprecated. Don't use
def export_dataframe(request):
    app = request.POST.get("app")
    section = request.POST.get("section")
    format = request.POST.get("format")

    response = {}
    if app in request.session:
        dataframe = request.session[app][section]

        if format == "csv":
            result = dataframe.to_csv()
            response = HttpResponse(content_type='application/force-download')
            response['Content-Disposition'] = 'attachment;filename="{0}"'.format(section + ".csv")
            response.write(result)
        elif format == "excel":
            excel_file = IO()

            xlwriter = pd.ExcelWriter(excel_file, engine='xlsxwriter')

            dataframe.to_excel(xlwriter, 'sheetname')

            xlwriter.save()
            xlwriter.close()

            # important step, rewind the buffer or when it is read() you'll get nothing
            # but an error message when you try to open your zero length file in Excel
            excel_file.seek(0)

            # set the mime type so that the browser knows what to do with the file
            response = HttpResponse(excel_file.read(),
                                    content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            response['Content-Disposition'] = 'attachment;filename="{0}"'.format(section + ".xlsx")
    else:
        response = "Error"

    return response


# --------------------------------------------------------------------
# ---------------------General methods-----------------------------------------------
def clear_session(session):
    session.flush()
    session['neurons_user'] = []
    session['datasets_user_' + settings.MORPHO_ANALYZER_APP_NAME] = {}

    return 0


# ---------------------Worker task general methods-----------------------------------------------
def return_loading_worker_task_html(request, context):
    context["sidebar"] = "sidebar_morpho_analyzer.html"
    context["send_to_background_url"] = "/morpho/index_morpho_analyzer"
    if not "cancel_possible" in context:
        context["cancel_possible"] = 1

    helpers.get_and_clean_datasets_types(context, request.session)

    return render(request, "loading_worker_task.html", context)


def process_loading_worker(request):
    if 'task_id' in request.POST.keys() and request.POST['task_id']:
        try:
            data = get_worker_task_result(request.POST['task_id'])
            if data and "done" in data and data["done"]:
                data["processed_django"] = True

            json_data = json.dumps(data)
            return HttpResponse(json_data, content_type='application/json')
        except ValueError:  # includes simplejson.decoder.JSONDecodeError
            print ('Decoding JSON has failed')
            return return_loading_worker_task_html(request, context)
    else:
        data = 'No task_id in the request'
        context = {}
        return return_loading_worker_task_html(request, context)


def get_worker_task_result(task_id):
    task = AsyncResult(task_id)
    data = task.result or task.state

    return data


def revoke_worker_task(request):
    task_id = request.POST.get("task_id", "")
    result = revoke(task_id, terminate=True, signal='SIGKILL')

    data = {"result": result}
    json_data = json.dumps(data)
    return HttpResponse(json_data, content_type='application/json')


def on_revoked(self):
    pid = self.process.pid
    print("RUning")
    os.kill(pid, signal.SIGTERM)


def return_worker_task_inline_result(request):
    data = get_worker_task_result(request.POST['task_id'])
    if isinstance(data, Exception):
        data = {"done": True}  # Force to terminate
        data["data_processed"] = {"error": True, "error_message": "Fatal error running the algorithm"}
    return JsonResponse(data, safe=False)


celery.events.state.Task.on_revoked = on_revoked


# -----------------------
def create_session_if_not_exists(request):
    if request.session.session_key is None:
        request.session.save()
        request.session.modified = True
