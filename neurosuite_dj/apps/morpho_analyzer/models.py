from django.db import models

from chunked_upload.models import ChunkedUpload


from django.contrib.sessions.models import Session
from django.db.models.signals import post_save
from django.db.models.signals import pre_delete
from django.db.models.signals import pre_save
from django.dispatch import receiver


# Create your models here.
class ChunkedUploadCsv(ChunkedUpload):
    user = models.ForeignKey("self", related_name='+')
    pass

# Override the default ChunkedUpload to make the `user` field nullable
ChunkedUploadCsv._meta.get_field('user').null = True