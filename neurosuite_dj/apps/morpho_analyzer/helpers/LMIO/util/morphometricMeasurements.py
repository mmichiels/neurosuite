#Author: Ajayrama Kumaraswamy(ajayramak@bio.lmu.de)
#Date: 2 May 2014
#Place: Dept. of Biology II, LMU, Munich

#********************************************List of Dependencies*******************************************************
#The following code has been tested with the indicated versions on 64bit Linux and PYTHON 2.7.3

#os: Use standard library with comes with python.
#pint: 0.5.1

#***********************************************************************************************************************


from ..wrapper import *
import os
from pint import UnitRegistry
ureg = UnitRegistry()

measureNames = [
    'Soma_Surface',
    'N_stems',
    'N_bifs',
    'N_branch',
    'N_tips',
    'Width',
    'Height',
    'Depth',
    'Type',
    'Diameter',
    'Diameter_pow',
    'Length',
    'Surface',
    'SectionArea',
    'Volume',
    'EucDistance',
    'PathDistance',
    'Branch_Order',
    'Terminal_degree',
    'TerminalSegment',
    'Taper_1',
    'Taper_2',
    'Branch_pathlength',
    'Contraction',
    'Fragmentation',
    'Daughter_Ratio',
    'Parent_Daughter_Ratio',
    'Partition_asymmetry',
    'Rall_Power',
    'Pk',
    'Pk_classic',
    'Pk_2',
    'Bif_ampl_local',
    'Bif_ampl_remote',
    'Bif_tilt_local',
    'Bif_tilt_remote',
    'Bif_torque_local',
    'Bif_torque_remote',
    'Last_parent_diam',
    'Diam_threshold',
    'HillmanThreshold',
    'Helix',
    'Fractal_Dim'
]

#Documentation obtained from http://cng.gmu.edu:8080/Lm/help/index.htm
values_to_consider = [
    4,
    0,
    0,
    0,
    0,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    0,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4
]


def getAllMorphMeasures(swcfName):
    """

    :param swcfName: relative/absolute path of the swc file.
    :return: a dictionary of scalar measurements as key value pairs. The name of the key is the name of the measurement. The values are pint quantities.
    """

    #swcfName = os.path.abspath(swcfName)



    LMOutputSimple = getMeasure(measureNames, swcfName)

    #measures = {}
    measures = []
    for i, measure in enumerate(LMOutputSimple):
        """
        Wrapped by measure
        measure_dict = {}
        measure_dict["total"] = measure['WholeCellMeasures'][0][0]
        measure_dict["avg"] = measure['WholeCellMeasures'][0][4]
        measure_dict["max"] = measure['WholeCellMeasures'][0][5]
        measure_dict["min"] = measure['WholeCellMeasures'][0][3]
        measure_dict["sd"] = measure['WholeCellMeasures'][0][6]
        """
        #Fully unwrapped
        """
        me = {}
        me[measureNames[i] + "_total"] =  measure['WholeCellMeasures'][0][0]
        measures.append(me)
        me = {}
        me[measureNames[i] + "_avg"] =  measure['WholeCellMeasures'][0][4]
        measures.append(me)
        me = {}
        me[measureNames[i] + "_max"] =  measure['WholeCellMeasures'][0][5]
        measures.append(me)
        me = {}
        me[measureNames[i] + "_min"] =  measure['WholeCellMeasures'][0][3]
        measures.append(me)
        me = {}
        me[measureNames[i] + "_sd"] =  measure['WholeCellMeasures'][0][6]
        measures.append(me) 
        """
        value_to_consider = values_to_consider[i]
        value_measurement = measure['WholeCellMeasures'][0][value_to_consider]
        value_measurement = round(value_measurement, 3)
        measures.append(value_measurement)



        #measures[measureNames[i]] = measure_dict

    """
    scalarDict = dict(
                    Width=width * ureg.um,
                    Height=height * ureg.um,
                    Depth=depth * ureg.um,
                    Length=length * ureg.um,
                    Volume=volume * (ureg.um) ** 3,
                    Surface=surface * (ureg.um) ** 2,
                    NumberofBifurcations=ureg.Quantity(nbifs, None) ,
                    )

    retDict = dict(scalarMeasurements=scalarDict)

    """


    return measures
    #*******************************************************************************************************************

def getAllMorphNames():
    all_morph_names = []
    for measure in measureNames:
        all_morph_names.append(measure + "_total")
        all_morph_names.append(measure + "_avg")
        all_morph_names.append(measure + "_max")
        all_morph_names.append(measure + "_min")
        all_morph_names.append(measure + "_sd")

    return all_morph_names

def getAvgMorphNames():
    all_morph_names = []
    for measure in measureNames:
        all_morph_names.append(measure + "_avg")

    return all_morph_names

def getRegularMorphNames():

    return measureNames

def round_int(x):
    if x == float("inf") or x == float("-inf"):
        return float('nan') # or x or return whatever makes sense
    return int(round(x))