import rpy2
from rpy2.rinterface import R_VERSION_BUILD
import rpy2.rinterface as rinterface
from rpy2.robjects import r
from rpy2.robjects.packages import importr
import datetime
import re
from rpy2.rinterface import RRuntimeWarning
from apps.morpho_analyzer.helpers import helpers

INTERNEURON_CLASSES =  {
    "BTC": "Bitufted cell",
    "ChC": "Chandelier cell",
    "DBC": "Double bouquet cell",
    "LBC": "Large basket cell",
    "MC": "Martinotti cell",
    "NBC": "Nest basket cell",
    "SBC": "Small basket cell",
}

def classify_interneuron(neuron_file_path_input, soma_layer=23, verbose_output=True):
    result = {
        "error": False,
        "class_interneuron": None,
        "class_verbose": None,
        "test_errors": None,
        "exception": None,
    }

    try:
        gabaclassifier_R = importr("gabaclassifier")

        std = helpers.supress_stout_stderr()
        output_raw_R = gabaclassifier_R.main_classify_interneuron(neuron_file_path_input=neuron_file_path_input, soma_layer=soma_layer)
        helpers.enable_stdout_stderr(std)

        output_R = {}

        for key in output_raw_R.names:
            output_R[key] = output_raw_R.rx2(key)
            if output_R[key] == rinterface.NULL:
                result["error"] = True
            elif output_R[key].names == rinterface.NULL and not str(output_R[key]) == "character(0)\n":
                result["error"] = True

        if result["error"] and verbose_output:
            error_message = str(output_R["error"])
            error_message = re.sub('\\[[^\\]]*\\]', '', error_message)
            error_message = error_message.replace('" "', "<br>")
            error_message = error_message.replace('\n', "<br>")
            error_message = error_message.replace('"', "")

            errors = []
            lines = error_message.split("<br>")
            del lines[0]
            if len(lines) > 0:
                del lines[-1]
                for line in lines:
                    test_description = line.rsplit(' ', 1)[0]
                    if test_description.split(' ', 1)[0] == "":
                        test_description = test_description.split(' ', 1)[1]
                    test_description_split = test_description.split(' ', 1)
                    test_number = test_description_split[0]
                    test_description = test_description_split[1]
                    test_result = line.split(' ')[-1]
                    if test_result == "FALSE":
                        test_result = False
                    else:
                        test_result = True
                    test = {
                        "number": test_number,
                        "description": test_description,
                        "result": test_result,
                    }
                    errors.append(test)

                result["test_errors"] = errors
            else:
                result["error"] = True
                if verbose_output:
                    result["exception"] = "Unknown error"
        else:
            output_R_string = str(output_R["class_interneuron"])
            output_R_split = output_R_string.split('\n')
            if len(output_R_split) > 1:
                result["class_interneuron"] = output_R_split[1].replace(" ", "")
                result["class_verbose"] = INTERNEURON_CLASSES[result["class_interneuron"]]
            else:
                result["error"] = True
                if verbose_output:
                    result["exception"] = "Unknown error"

    except Exception as e:
        result["error"] = True
        if verbose_output:
            result["exception"] = str(e)
            result["exception"] = result["exception"].replace('\n', "<br>")

    return result

def get_possible_interneuron_classes():
    return INTERNEURON_CLASSES



