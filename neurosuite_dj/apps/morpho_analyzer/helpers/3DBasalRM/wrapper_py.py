import rpy2
from rpy2.rinterface import R_VERSION_BUILD
from rpy2.robjects import r
from rpy2.robjects.packages import importr
import datetime
from apps.morpho_analyzer.helpers import helpers


def repair_neuron(neuron_name, neuron_file_path, seed=1):
    result = {}
    result["ok"] = False
    basalrm_3d = importr("basalrm")
    try:
        now = datetime.datetime.now()
        datetime_now = now.strftime("%Y_%m_%d_%H_%M_%S")
        file_path_output = "/neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/3DBasalRM/output/" + neuron_name + "_" + datetime_now + ".json"

        std = helpers.supress_stout_stderr()
        result_R = basalrm_3d.main_repair_neuron_to_json(file_path_input=neuron_file_path, file_path_output=file_path_output, seed=seed)
        helpers.enable_stdout_stderr(std)

        if result_R[0] == 0:
            result["ok"] = True
            result["path"] = file_path_output
    except Exception as e:
        result["ok"] = False

    return result


