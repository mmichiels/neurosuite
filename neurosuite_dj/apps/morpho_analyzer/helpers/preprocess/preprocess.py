#import Orange
import numpy as np
from fitter import Fitter
from sklearn import preprocessing
import matplotlib.pyplot as plt
import pandas as pd
from apps.morpho_analyzer.helpers.stats import plotly_helpers

#from orange3_utils.utils2 import df2table
from apps.morpho_analyzer.helpers.machine_learning import machine_learning_helpers


def do_preprocess(X, params, class_features, id_column):
    summary = {}
    id = X[[id_column]]
    X = X.loc[:, X.columns != id_column]
    X = fill_na(X, params["filling_na_algorithm"])
    # Remove invariant features
    if(params["remove_non_variant_features"]):
        X = delete_invariant_features(X)
    if(params["normalize_values"]):
        X = normalize_data(X, params["normalization_method"])
    if (params["dataset_discretize"]):
        X = machine_learning_helpers.discretize_data(X, class_features, params["discretize_method"], params)
    if( "apply_dimentionality_reduction" in params):
        if(params["apply_dimentionality_reduction"]):
             if(params["when_apply_pca"] == "before" or params["when_apply_pca"] == "both"):
                X, summary = machine_learning_helpers.apply_dimentionality_reduction(X, class_features, summary, params)
             if (params["normalize_values"]):
                X = normalize_data(X, params["normalization_method"])
    if("apply_feature_subset_selection" in params):
        if(params["apply_feature_subset_selection"]):
            X, summary = machine_learning_helpers.apply_feature_subset_selection(X, class_features, summary, params)
    if ("apply_dimentionality_reduction" in params):
        if(params["apply_dimentionality_reduction"]):
             if(params["when_apply_pca"] == "after" or params["when_apply_pca"] == "both"):
                X, summary = machine_learning_helpers.apply_dimentionality_reduction(X, class_features, summary, params)
    X[id_column] = id
    return X, summary

def fill_na(X, method):
    if(method == "mean"):
        X.fillna(X.mean(), inplace=True)
    if(method == "rows_deletion"):
        X = X.replace(r'\s+( +\.)|#',np.nan,regex=True).replace('',np.nan)
        X.dropna(inplace=True)
    return X

def delete_invariant_features(X):
    # Delete invariant features(columns)
    X = X.loc[:, (X != X.iloc[0]).any()]

    return X

def normalize_data(X, normalization_method):
    if(normalization_method == "max_min"):
        scaler = preprocessing.MinMaxScaler()
        X_norm = scaler.fit_transform(X)
        X = pd.DataFrame(data=X_norm, columns=X.columns)
    if(normalization_method == "l2"):
        X_norm = preprocessing.normalize(X, norm='l2')
        X = pd.DataFrame(data=X_norm, columns=X.columns)
    return X

def reassign_bins_indices(X):
    """
    Example:
    input:
    0
    0
    0
    5
    2

    Output:
    0
    0
    0
    2
    1

    :param X:
    :return:
    """


    for colname, col in X.iteritems():
        n_bins = X.loc[:, colname].nunique()
        unique_values = np.sort(X.loc[:, colname].unique())

        for i, unique_value in enumerate(unique_values):
            values_to_translate = X.groupby(colname).groups[unique_value].values.tolist()
            X.loc[values_to_translate, colname] = i

    return X


def fit_distributions(X):
    common_distributions = ['uniform', 'bernoulli', 'hypergeom', 'binom', 'geom',
                            'nbinom', 'poisson', 'expon', 'lognorm', 'norm', 't', 'chi2', 'gamma', 'beta']
    fitted_distributions = []
    for colname, col in X.iteritems():
        # plt.figure(X.columns.get_loc(colname)+2)
        plt.figure(2)
        f = Fitter(X.loc[:, colname],
                   distributions=common_distributions)  # If no distributions parameter then all distributions are computed
        f.fit()
        # may take some time since by default, all distributions are tried
        # but you call manually provide a smaller set of distributions
        print("----------Best fitted distribution------------")
        best_fit_function = f.summary(Nbest=1)
        print(best_fit_function)
        name_fit_function = list(best_fit_function.index)[0]
        squared_error = best_fit_function.iloc[0, 0]
        new_fitted_distribution = [colname, name_fit_function, squared_error]
        fitted_distributions.append(new_fitted_distribution)

    print("====Alll)")
    print(fitted_distributions)
    fitted_dists = pd.DataFrame(data=fitted_distributions)
    fitted_dists.columns = ['Measure', 'Fit_function', 'squared_error']
    fitted_distributions = fitted_dists.set_index("Measure")

    print(fitted_distributions)

    return fitted_distributions


def discretize(X):
    X_table = df2table(X)

    new_features = []
    for feature in X_table.domain.attributes:
        n_bins = X.loc[:, feature.name].nunique()
        if (n_bins > 10):
            n_bins = 6
        disc = Orange.preprocess.discretize.EqualWidth(n=n_bins)
    """
    TODO: Increase the accuracy of the discretization by using some well-known algorithms like PKID or WPKID
    Step 1: Compute number of bins(n_bins) by resolving the equation of PKID or WPKID:
    PKID equation: t²=N => t=raíz_cuadrada_de(N)
    WPKID equation: (m+t)*t=N => t²+t*n-N=0
    s = size of bin
    t = number of bins (n_bins)
    N = number of instances of the dataset
    m = 30

    PKID: Ying Yang and Geoffrey Webb. Proportional k-interval discretization for naive-bayes classifiers.Machine Learning: ECML 2001, pages 564–575, 2001.
    WPKID: Ying Yang and Geofrey Webb. Weighted proportional k-interval discretization for naive-bayes classifiers.Advances in Knowledge Discovery and Data Mining, pages 		565-565, 2003.

    Step 2:
    disc = Orange.preprocess.discretize.EqualWidth(n=n_bins)
    """
    disc(X_table, feature)
    new_features.append(disc(X_table, feature))

    domain = Orange.data.Domain(new_features)
    X_disc = Orange.data.Table(domain, X_table)

    """
    print("Discretized data set:")
    for e in X_disc[:,0]:
        print (str(e) + " ==== " + str(round_int(e.x[0])))
    """

    X = pd.DataFrame(data=X_disc.X, columns=X.columns).astype(int)

    """
    Reassign bins if there are empty bins.

    """
    X = reassign_bins_indices(X)

    print("Discretized data set:")
    print(X)

    plt.figure(2)
    X_hist = X.hist(bins='auto')
    for ax in X_hist.flatten():
        ax.set_xlabel("Units of measure")
        ax.set_ylabel("# of neurons with that measure")

    return X
