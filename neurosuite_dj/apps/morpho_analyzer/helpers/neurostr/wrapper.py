import os
import subprocess
import platform
import pandas as pd

import numpy as np

def validator(neuron_file, arguments=["-e"]): #Default argument is -e, i.e. "All tests"
    osName = platform.system()
    if osName == 'Linux':
        (bit, linkage) = platform.architecture()
        neurostr_path = os.path.join(os.path.dirname(__file__), "build")
        neurostr_path = os.path.join(neurostr_path, "bin")
        neurostr_exec = 'neurostr_validator'

    elif osName == 'Darwin':
        neurostr_path = 'binMac'
        neurostr_exec = 'neurostr_validator'

    elif osName == 'Windows':
        neurostr_path = 'binWindows'
        neurostr_exec = 'neurostr_validator.exe'

    else:
        raise (NotImplementedError('Currently, this wrapper is supported only on Linux. \
        Sorry for the inconvenience.'))

    neurostr_path_exec = os.path.join(neurostr_path, neurostr_exec)
    neurostr_full_command = [neurostr_path_exec, '--input', neuron_file, ' '.join(arguments)]
    neurostr_cmd = ' '.join(neurostr_full_command)

    print("Processing neuron {0} with neurostr_validator...".format(neuron_file))
    output = subprocess.Popen(neurostr_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output_out, output_err  = output.communicate()

    if len(output_err) == 0:
        neurostr_val_json_output = pd.io.json.loads(output_out)
    else:
        neurostr_val_json_output = {
            "error": str(output_err),
        }

    return neurostr_val_json_output

def format_converter(neuron_file, output_format, correct=None, simplification_algorithm=None):
    osName = platform.system()
    if osName == 'Linux':
        (bit, linkage) = platform.architecture()
        neurostr_path = os.path.join(os.path.dirname(__file__), "build")
        neurostr_path = os.path.join(neurostr_path, "bin")
        neurostr_exec = 'neurostr_converter'

    elif osName == 'Darwin':
        neurostr_path = 'binMac'
        neurostr_exec = 'neurostr_converter'

    elif osName == 'Windows':
        neurostr_path = 'binWindows'
        neurostr_exec = 'neurostr_converter.exe'

    else:
        raise (NotImplementedError('Currently, this wrapper is supported only on Linux. \
        Sorry for the inconvenience.'))

    neurostr_path_exec = os.path.join(neurostr_path, neurostr_exec)
    output_file_name = neuron_file + "_converted." + output_format
    neurostr_full_command = [neurostr_path_exec, '--input', neuron_file, '-o', output_file_name, '-f', output_format]
    if correct == "" or correct == True:
        neurostr_full_command.append("-c")
    if simplification_algorithm == "":
        neurostr_full_command.append("-e")
        neurostr_full_command.append("0.0")

    neurostr_cmd = ' '.join(neurostr_full_command)

    output = subprocess.Popen(neurostr_cmd, shell=True, stdout=subprocess.PIPE)
    neurostr_converter_output = (output.communicate()[0])
    bytes = b''
    with open(output_file_name, 'rb') as converted_file:
        bytes = converted_file.read()

    return bytes



