import os
import subprocess
import tempfile

import pandas as pd

import apps.morpho_analyzer.helpers.neurostr.wrapper as neurostr #  #helpers.neurostr.wrapper import get_val_neurostr
import apps.morpho_analyzer.helpers.helpers as helpers
import json
import re
import time
from celery import shared_task, current_task

def check_neurons(neurons_user):
    #neuronm_valid_neurons = get_neuronm_valid_neurons(dataset_folder)
    neurostr_validator_neurons = check_neurostr_neurons(neurons_user)

    """----------------------------Get only valid neurons---------------------------"""
    #valid_neurons = list(set(neuronm_valid_neurons + valid_neurons))
    #valid_neurons = list(set(valid_neurons))

    #print("--------Neuronm Validator-------" + '\n' + str(neuronm_valid_neurons) + '\n\n')
    print("--------Neurostr Validator-------" + '\n' + str(neurostr_validator_neurons) + '\n\n')

    return neurostr_validator_neurons


def check_neuron(neuron):
    neurostr_validator_output = ""
    with tempfile.NamedTemporaryFile(suffix=neuron["data_file"]["extension"]) as tmp_file:
        neuron_file = helpers.neuron_in_memory_to_disk(neuron,tmp_file)
        neurostr_validator_output = neurostr.validator(neuron_file)
        if not "error" in neurostr_validator_output:
            parse_neuron_check_output(neurostr_validator_output)

    return neurostr_validator_output

def get_neuronm_valid_neurons(dataset_folder):
    """------------------------------Neurom-------------------------------------"""
    """
    #-------Tests---------------
    structural_checks:
        - is_single_tree
        - has_soma_points
        - has_valid_soma
        - has_valid_neurites
    neuron_checks:
        - has_basal_dendrite
        - has_axon
        - has_all_nonzero_segment_lengths
        - has_all_nonzero_section_lengths
        - has_all_nonzero_neurite_radii
        - has_nonzero_soma_radius
    """
    neuronm_valid_neurons = []
    for i, neuron_name in enumerate(os.listdir(dataset_folder)):
        neuron_file = os.path.join(dataset_folder, neuron_name)
        """Plot neuron"""
        # neuron = nm.load_neuron(neuron_file)
        # fig, ax = viewer.draw(neuron, mode='2d') # valid modes '2d', '3d', 'dendrogram'
        # f = plt.figure(1)
        # fig.show()

        neurom_cmd = 'morph_check ' + neuron_file
        # output = subprocess.Popen(neurom_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        subprocess.Popen(neurom_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # neuronm_validator_output = output.communicate()[1].decode('utf_8').split('\n')
        with open('./summary.json') as file_output:
            neuronm_validator_output = pd.io.json.loads(file_output.read())
            if ([elem for elem in neuronm_validator_output["files"].values()][0]["ALL"] == True):
                neuronm_valid_neurons.append(neuron_name)
                # neurostr_val_json_output = json.loads(output.communicate()[0])

    return neuronm_valid_neurons

def check_neurostr_neurons(neurons_user):
    """------------------------------Neurostr-------------------------------------"""
    checks_neurons = []
    valid_neurons = []
    """
        #-------Tests---------------
        
        Neurite is attached to soma
        Neuron has soma
        Planar neurite reconstruction
        Abnormal dendrite count
        Abnormal apical count
        Abnormal axon count
        Trifrucation nodes detection
        Linear branches
        Zero-length compartments
        Intersecting node spheres
        Increasing diameters
        Branch collision
        Extreme bifurcation/elongation angles
    """
    # neurostr_validator --input neuron_test.swc -e
    columns = []

    for i, neuron in enumerate(neurons_user):
        start_time = time.time()
        with tempfile.NamedTemporaryFile(suffix=neuron["data_file"]["extension"]) as tmp_file:
            neuron_file = helpers.neuron_in_memory_to_disk(neuron, tmp_file)
            neurostr_validator_output = neurostr.validator(neuron_file)
            parse_neuron_check_output(neurostr_validator_output)
            if i == 0:
                columns = get_test_names(neurostr_validator_output)

            if (not any(d['pass'] == False for d in neurostr_validator_output)):
                valid_neurons.append(neuron.name)
            neurostr_validator_output = parse_neuron_check_output(neurostr_validator_output)
            neurostr_validator_output.insert(0, neuron["name"])
            checks_neurons.append(neurostr_validator_output)

        end_time = time.time()
        if current_task:
            process_percent = int(100 * float(i) / float(len(neurons_user)))
            estimated_time_finish = round((end_time - start_time) * (float(len(neurons_user)) - float(i)), 2)
            print ("process_percent: ", process_percent, "Task: ", current_task)
            current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                              'estimated_time_finish': estimated_time_finish})

    """-------------------------Create dataset matrix---------------------------"""
    print("Creating dataset matrix... \n")
    columns.insert(0, "Neuron name")
    checks_neurons = pd.DataFrame(data=checks_neurons, columns=columns)

    print("--------Neurostr-validator-matrix-------" + '\n')
    data = {"checks_neurons": checks_neurons,
            "valid_neurons": valid_neurons}

    return data


def parse_neuron_check_output(neurostr_validator_output):
    result = []
    for test in neurostr_validator_output:
        result.append(test["pass"])
        if 'neuron_id' in test:
            del test['neuron_id']
        if 'results' in test and len(test["results"]) > 0:
            if test["pass"]:
                test['results'] = []
            else:
                test['results'] = json.dumps({"data": test["results"]})

    return result

def get_test_names(neurostr_validator_output):
    test_names = []
    for test in neurostr_validator_output:
        test_names.append(test["name"])

    return test_names