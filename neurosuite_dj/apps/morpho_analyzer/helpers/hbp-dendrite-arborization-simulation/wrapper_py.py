import rpy2
from rpy2.rinterface import R_VERSION_BUILD
import rpy2.rinterface as rinterface
from rpy2.robjects import r
from rpy2.robjects.packages import importr
import datetime
import re

from apps.morpho_analyzer.helpers import helpers

def simulate_dendrite_arborization(number_simulated_neurons, output_folder_neurons):

    result =  {
        "error": False,
        "exception": None,
    }

    try:
        hbp_dendrite_arborization_simulation_R = importr("dendrsim")

        std = helpers.supress_stout_stderr()
        output_raw_R = hbp_dendrite_arborization_simulation_R.main_console(number_simulated_neurons=number_simulated_neurons,
                                                                           output_folder_neurons=output_folder_neurons)
        helpers.enable_stdout_stderr(std)

    except Exception as e:
        result["error"] = True
        result["exception"] = str(e)
        result["exception"] = result["exception"].replace('\n', "<br>")


    return result
