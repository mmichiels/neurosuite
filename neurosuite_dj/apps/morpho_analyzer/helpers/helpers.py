import apps.morpho_analyzer.helpers.gabaclassifier.wrapper_py as gabaclassifier
import apps.morpho_analyzer.helpers.neurostr.wrapper as neurostr
import copy
import io
import neurosuite_dj.helpers as global_helpers
import numpy as np
import os
import pandas as pd
import sys
import tempfile
import time
from datetime import datetime, timezone
import zipfile
from apps.morpho_analyzer.helpers import helpers
from apps.morpho_analyzer.helpers.get_data import neuromorpho_api_client
from celery import shared_task, current_task
from django.conf import settings
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.sessions.models import Session
from django.core.files.uploadedfile import InMemoryUploadedFile
from io import BytesIO
import neurosuite_dj.helpers_global.dataset as dataset_helper
import neurosuite_dj.helpers_global.datasets_user as datasets_user_helper


def neuron_in_memory_to_disk(neuron, tmp_file):
    neuron_in_memory_file = get_file_neuron(neuron)
    for chunk in neuron_in_memory_file.chunks():
        tmp_file.write(chunk)
        neuron_file = tmp_file.name

    tmp_file.flush() #To force the saving

    return neuron_file

def get_file_neuron(neuron, l_measure_and_simulated=False):
    if neuron["data_file"]["lazy_load"]:
        print("Downloading swc file", neuron["name"])
        file = neuromorpho_api_client.get_swc_neuron(neuron)
        neuron["data_file"]["original_file_url"] = neuron["data_file"]["file"]
        neuron["data_file"]["lazy_load"] = False
        neuron["data_file"]["file"] = file
        print("Prepare to preprocess neuron {0}".format(neuron["name"]))
        helpers.preprocess_neuron_data_file(neuron)
    else:
        if l_measure_and_simulated:
            file = fix_simulated_neurons_swc_for_l_measure(neuron)
        else:
            file = neuron["data_file"]["file"]

    return file

def CRLF_TO_LF_bytes(content_bytes):
    windows_line_ending = b'\r\n'
    linux_line_ending = b'\n'

    content_bytes = content_bytes.replace(windows_line_ending, linux_line_ending)

    return content_bytes


def preprocess_neuron_data_file(neuron):
    print("Preprocessing neuron {0}".format(neuron["name"]))
    neuron["name"] = neuron["name"].replace(".", "_")
    data_file_bytes = neuron["data_file"]["file"].file.getvalue()
    data_file_bytes = helpers.CRLF_TO_LF_bytes(data_file_bytes)
    data_file_bytes_io = io.BytesIO(data_file_bytes)
    neuron["data_file"]["file"].file = data_file_bytes_io
    print("Preprocessing neuron data_file_bytes_io {0}".format(neuron["name"]))

    #Automatically convert to swc
    with tempfile.NamedTemporaryFile(suffix=neuron["data_file"]["extension"]) as tmp_file:
        data_file_bytes_io = helpers.file_convert_to_swc(neuron)
        neuron["data_file"]["file"].file = data_file_bytes_io
        neuron["data_file"]["extension"] = ".swc"
        neuron["data_file"]["file"].content_type = "application/octet-stream"
        neuron["data_file"]["file"].size = data_file_bytes_io.getbuffer().nbytes

    return 0


def file_convert_to_swc(neuron):
    #1st iteration (to convert to swc)
    data_file_bytes_io = helpers.file_iteration_convert_to_swc(neuron, neuron["data_file"]["file"], iteration=1)



    if not ("simulated_neuron" in neuron["data_file"] and neuron["data_file"]["simulated_neuron"]):
        #----2nd iteration(to correct the swc created)
        intermediate_inmemory_data_file = InMemoryUploadedFile(file=data_file_bytes_io, field_name=None,
                                                               content_type='application/octet-stream',
                                                               name=neuron["name"], charset='utf8', size=data_file_bytes_io.getbuffer().nbytes)

        data_file_bytes_io = helpers.file_iteration_convert_to_swc(neuron, intermediate_inmemory_data_file, iteration=2)


    return data_file_bytes_io


def file_iteration_convert_to_swc(neuron, neuron_in_memory, iteration=1, simulated_neuron=False):
    if iteration == 1:
        suffix = neuron["data_file"]["extension"]
    else:
        suffix = ".swc"
    with tempfile.NamedTemporaryFile(suffix=suffix) as tmp_file:
        for chunk in neuron_in_memory.chunks():
            tmp_file.write(chunk)
            neuron_file = tmp_file.name

        tmp_file.flush()  # To force the saving

        data_file_bytes = neurostr.format_converter(neuron_file, "swc")
        data_file_bytes_io = io.BytesIO(data_file_bytes)


        return data_file_bytes_io



def fix_simulated_neurons_swc_for_l_measure(neuron):
    data_file_bytes = neuron["data_file"]["file"].file.getvalue()
    data_file_bytes = swc_fix_order_nodes(data_file_bytes)

    data_file_bytes_io = io.BytesIO(data_file_bytes)

    inmemory_data_file = InMemoryUploadedFile(file=data_file_bytes_io, field_name=None,
                                              content_type='application/octet-stream',
                                              name=neuron["name"], charset='utf8',
                                              size=data_file_bytes_io.getbuffer().nbytes)

    return inmemory_data_file

def swc_fix_order_nodes(swc_bytes):
    swc_fixed_order = ""
    swc_str = swc_bytes.decode("utf-8")
    swc_lines = swc_str.split("\n")
    node_counter = 1
    for i, line in enumerate(swc_lines):
        node = line.split(" ")
        if node[0] != "#" and (node[0].isdigit() or node[0] == "-1"):
            node[0] = str(node_counter)
            node_counter += 1
        if node[0] != "-1":
            node_str = ' '.join(node)
            if i != len(swc_lines):
                swc_fixed_order += node_str + "\n"
            else:
                swc_fixed_order += node_str

    data_file_bytes = swc_fixed_order.encode("utf-8")

    return data_file_bytes

def get_session_object_from_db(session_id, object):
    session = SessionStore(session_key=session_id)
    object = session[object]

    return object

def store_object_to_session_db(session_id, request, object_name):
    session = SessionStore(session_key=session_id)
    object = session[object_name]
    request.session[object_name] = object

    return None

def supress_stout_stderr():
    std = {
        "_err": sys.stderr,
        "_out": sys.stdout
    }
    null = open(os.devnull, 'wb')
    sys.stdout = sys.stderr = null

    return std

def enable_stdout_stderr(std):
    sys.stderr = std["_err"]
    sys.stdout = std["_out"]

    return 0

def get_and_clean_datasets_types(data, session):

    #Get datasets types:
    if "datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME in session:
        datasets_user = session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]
        if not datasets_user:
            data["empty_input_data"] = True  # i.e. no access to tools that need morpho reconstructions or input dataset
        else:
            data["has_input_dataset"] = datasets_user.has_input_dataset
            data["has_morpho_reconstructions"] = datasets_user.has_morpho_reconstructions

            # Clean datasets marked as "delete" and save:
            datasets_user.clean_old_datasets(session)
    else:
        data["empty_input_data"] = True

    return 0

def neurons_get_additional_discrete_data(neurons_user):
    neurons_have_additional_data = False
    neurons_additional_discrete_fields = []

    for neuron in neurons_user:
        if "additional_data" in neuron:
            neurons_have_additional_data = True
            """
            for field_id in neuron["additional_data"]:
                field_type = get_local_session_values_neuron_field(datasets_user={},
                                                                   field_id=field_id,
                                                                   dataset_name="")["type"]
                if field_type == "text":
                    neurons_additional_discrete_fields.append(field_id)
            """
            break

    return neurons_have_additional_data

def neurons_additional_data_to_dataframe(neurons_user, label_column_id="neuron_name"):
    all_aditional_data = []

    for neuron in neurons_user:
        if "additional_data" in neuron:
            """
            neuron_additional_data = {}
            for field_id in neuron["additional_data"]:
                field_values = neuron["additional_data"][field_id]
                if type(field_values) is not list:
                    field_values_list = [field_values]
                else:
                    field_values_list = field_values

                neuron_additional_data[field_id] = field_values
                
                field_type = global_helpers.get_type_values(field_values_list)
                if field_type == "text":
                    neuron_additional_data[field_id] = field_values
                if field_id == label_column_id:
                    neuron_additional_data[field_id] = field_values
                elif field_type == "numeric" and type(field_values) is not list:
                    if field_values is None:
                        field_values = np.nan
                    neuron_additional_data[field_id] = float(field_values)
                """
            all_aditional_data.append(neuron["additional_data"])

    dataframe = pd.DataFrame(data=all_aditional_data)
    dataframe.replace([None, 'None', 'nan', 'Not Reported', 'Not reported'], np.NaN, inplace=True)
    dataframe = dataframe.apply(pd.to_numeric, errors='ignore')

    try:
        columns = dataframe.columns.tolist()

        columns.remove(label_column_id)
        columns.insert(0, label_column_id)

        column_links = "_links"
        columns.remove(column_links)
        columns.insert(-1, column_links)

        dataframe = dataframe[columns]
    except ValueError:
        pass

    return dataframe

def get_render_data_dataframe(data, data_type, datasets_user, dataset_discrete_name="", dataset_continuous_name="", label_column_id="", neurons_user=[]):
    data["data_type"] = data_type

    if datasets_user.has_input_dataset:
        if data_type == "discrete":
            dataset_data = datasets_user.datasets[dataset_discrete_name]
            dataframe_others_columns = datasets_user.datasets[dataset_continuous_name].columns
        else:
            dataset_data = datasets_user.datasets[dataset_continuous_name]
            dataframe_others_columns = datasets_user.datasets[dataset_discrete_name].columns
    else:
        data["fields_by_categories"] = helpers.get_session_all_neuron_fields_by_categories(neurons_user, additional_fields=[])

    dataframe_data_columns = dataset_data.columns
    num_rows_dataframe, num_cols_dataframe = dataset_data.shape
    data["dataframe_shape"] = dataset_data.shape
    if num_rows_dataframe > 100:
        data["features_columns"] = dataframe_data_columns
        data["features_data"] = []
        data["too_much_tabular_data"] = True
    else:
        dataframe_data = dataset_data.get_dataframe()
        if data_type == "discrete":
            # To shorten up long strings:
            dataframe_strings = dataframe_data.select_dtypes(['object'])
            dataframe_data[dataframe_strings.columns] = dataframe_strings.apply(lambda x: x.str.slice(0, 40))
        if datasets_user.has_morpho_reconstructions:
            columns_to_append = ["Analyze neuron"]
            data["features_columns"], data["features_data"] = global_helpers.pandas_dataframe_to_lists(dataframe=dataframe_data, index_column=False, append_columns=columns_to_append)
            data["columns_not_stats"] = [label_column_id] + columns_to_append
        else:
            data["features_columns"], data["features_data"] = global_helpers.pandas_dataframe_to_lists(dataframe=dataframe_data, index_column=False)
            data["columns_not_stats"] = [label_column_id]

        data["too_much_tabular_data"] = False

    data["label_column_id"] = label_column_id

    return dataframe_data_columns, dataframe_others_columns

def get_session_all_neuron_fields_by_categories(neurons_user, additional_fields):
    fields = []
    neuron_additional_data = False

    for neuron in neurons_user:
        if "additional_data" in neuron:
            fields = neuromorpho_api_client.get_all_neuron_fields_by_categories(sort_field=False)
            fields = copy.deepcopy(fields)
            neuron_additional_data = True
            break

    if not neuron_additional_data:
        fields = neuromorpho_api_client.get_only_custom_options(sort_field=False)

    fields += additional_fields

    return fields

def get_discr_cont_fields_by_categories(values_discrete_fields, values_continuous_fields):
    discrete_fields = []
    continuous_fields = []
    if len(values_discrete_fields) > 100:
        values_discrete_fields = values_discrete_fields[0:100]
    if len(values_continuous_fields) > 100:
        values_continuous_fields = values_continuous_fields[0:100]

    for field in values_discrete_fields:
        discrete_field = {
            "name": field,
            "verbose_name": global_helpers.verbosize_string(field)
        }
        discrete_fields.append(discrete_field)

    for field in values_continuous_fields:
        continuous_field = {
            "name": field,
            "verbose_name": global_helpers.verbosize_string(field)
        }
        continuous_fields.append(continuous_field)

    discrete_category = {
        "name": "discrete",
        "verbose_name": "Discrete fields",
        "fields": discrete_fields,
    }
    continuous_category = {
        "name": "continuous",
        "verbose_name": "Continuous fields",
        "fields": continuous_fields,
    }

    custom_options_category = helpers.get_filter_custom_options_category()

    fields_by_categories = [discrete_category, continuous_category, custom_options_category]

    return fields_by_categories

def get_filter_custom_options_category():
    custom_option_fields = [
        {
            "name": "limit_number_results",
            "verbose_name": "Limit the number of results",
            "custom_option": True
        },
    ]
    custom_options_category = {
        "name": "custom_options",
        "verbose_name": "Custom options",
        "fields": custom_option_fields,
    }

    return custom_options_category

def get_local_session_values_neuron_field(datasets_user, field_id, dataset_name):
    data = {}

    data["values"] = []
    if field_id in datasets_user.datasets[dataset_name].columns:
        dataframe = datasets_user.datasets[dataset_name].get_dataframe([field_id])
    else:
        dataframe = datasets_user.get_input_dataframe([field_id])
    all_values_field = dataframe.loc[:, field_id].astype('str')
    categories = all_values_field.astype('str').value_counts().index.values
    data["values"] = list(categories)

    data["type"] = global_helpers.get_type_values(data["values"])

    return data

def get_category_l_measure(features_l_measure):
    l_measure_columns = features_l_measure.columns.get_values().tolist()
    del l_measure_columns[0]

    fields_l_measure = []
    for field in l_measure_columns:
        fields_l_measure.append({
            "name": field,
            "verbose_name": field,
        })
    category_l_measure = {
        "name": "l-measure",
        "verbose_name": "L-Measure",
        "fields": fields_l_measure
    }

    return category_l_measure


def morpho_all_format_converter_neurostr(neurons_user, output_format, correct=None, simplification_algorithm=None):
    neurons_converted = []
    for i, neuron in enumerate(neurons_user):
        start_time = time.time()
        with tempfile.NamedTemporaryFile(suffix=neuron["data_file"]["extension"]) as tmp_file:
            neuron_file = helpers.neuron_in_memory_to_disk(neuron, tmp_file)
            result = neurostr.format_converter(neuron_file, output_format, correct, simplification_algorithm)
            neuron_converted = {"name": neuron["name"] + "_converted." + output_format, "file": result}
            neurons_converted.append(neuron_converted)

        end_time = time.time()
        if current_task:
            process_percent = int(100 * float(i) / float(len(neurons_user)))
            estimated_time_finish = round((end_time - start_time) * (float(len(neurons_user)) - float(i)), 2)
            print ("process_percent: ", process_percent, "Task: ", current_task)
            current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                              'estimated_time_finish': estimated_time_finish})

    zip_subdir = "neurons_converted_neurosuite"
    zip_filename = "{0}.zip".format(zip_subdir)

    zip_file_memory = BytesIO()
    zf = zipfile.ZipFile(zip_file_memory, "w")

    for neuron in neurons_converted:
        string_file = neuron["file"].decode("utf-8")
        zf.writestr(neuron["name"], string_file)

    zf.close()

    zip = {"zip_filename": zip_filename, "zip_file": zip_file_memory}

    return zip

def morpho_download_all_original_swc_neuromorpho(neurons_user):
    original_neurons_swc = []
    for i, neuron in enumerate(neurons_user):
        start_time = time.time()
        with tempfile.NamedTemporaryFile(suffix=neuron["data_file"]["extension"]) as tmp_file:
            neuron_in_memory_file = neuromorpho_api_client.get_swc_neuron(neuron)
            neuron_original = {"name": neuron["name"] + ".swc", "file": neuron_in_memory_file}
            original_neurons_swc.append(neuron_original)

        end_time = time.time()
        if current_task:
            process_percent = int(100 * float(i) / float(len(neurons_user)))
            estimated_time_finish = round((end_time - start_time) * (float(len(neurons_user)) - float(i)), 2)
            print ("process_percent: ", process_percent, "Task: ", current_task)
            current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                              'estimated_time_finish': estimated_time_finish})

    zip_subdir = "neurons_neuromorpho_swc_neurosuite"
    zip_filename = "{0}.zip".format(zip_subdir)

    zip_file_memory = BytesIO()
    zf = zipfile.ZipFile(zip_file_memory, "w")

    for neuron in original_neurons_swc:
        string_file = neuron["file"].file.getvalue().decode("utf-8")
        zf.writestr(neuron["name"], string_file)

    zf.close()

    zip = {"zip_filename": zip_filename, "zip_file": zip_file_memory}

    return zip


def morpho_repair_neuron_3DBasalRM(neuron, download_neuron):
    result = {}
    neuron["name"] = neuron["name"].replace(".", "_")
    with tempfile.NamedTemporaryFile(suffix=neuron["data_file"]["extension"]) as tmp_file:
        neuron_file_path = helpers.neuron_in_memory_to_disk(neuron, tmp_file)
        importlib = __import__('importlib')
        BasalRM_3D = importlib.import_module('apps.morpho_analyzer.helpers.3DBasalRM.wrapper_py')

        neuron_repaired_result = BasalRM_3D.repair_neuron(neuron["name"], neuron_file_path, seed=1)
        if not neuron_repaired_result["ok"]:
            result["error"] = True
            if download_neuron:
                result["download_error"] = True
        else:
            neuron_name = neuron["name"] + "_repaired" + ".json"
            with open(neuron_repaired_result["path"], 'rb') as file:
                neuron_bytes = file.read()
                neuron_bytes_io = io.BytesIO(neuron_bytes)
                neuron_data_file = InMemoryUploadedFile(file=neuron_bytes_io, field_name=None, content_type='text',
                                                        name=neuron_name, charset='utf8',
                                                        size=os.stat(neuron_repaired_result["path"]).st_size)
                file_extension = os.path.splitext(neuron_name)[1]
                neuron_name = os.path.splitext(neuron_name)[0]
                neuron_repaired = {
                    "name": neuron_name,
                    "data_file": {
                        "file": neuron_data_file,
                        "extension": file_extension,
                        "lazy_load": False
                    },
                }
                result["neuron_repaired"] = neuron_repaired
                result["neuron_repaired_json_bytes"] = neuron_bytes
                try:
                    os.remove(neuron_repaired_result["path"])  # Remove output file because now is in memory
                except OSError:
                    return False

    return result

def morpho_repair_all_neurons_3DBasalRM(neurons_user, output_format):
    neurons_repaired = []
    for i, neuron in enumerate(neurons_user):
        start_time = time.time()

        result = helpers.morpho_repair_neuron_3DBasalRM(neuron, download_neuron=True)
        if not "error" in result:
            neuron_repaired = result["neuron_repaired"]
            with tempfile.NamedTemporaryFile(suffix=neuron_repaired["data_file"]["extension"]) as tmp_file:
                neuron_file = helpers.neuron_in_memory_to_disk(neuron_repaired, tmp_file)
                result = neurostr.format_converter(neuron_file, output_format)
                neuron_repaired = {"name": neuron_repaired["name"] + "." + output_format, "file": result}
            neurons_repaired.append(neuron_repaired)

        end_time = time.time()
        if current_task:
            process_percent = int(100 * float(i) / float(len(neurons_user)))
            estimated_time_finish = round((end_time - start_time) * (float(len(neurons_user)) - float(i)), 2)
            print ("process_percent: ", process_percent, "Task: ", current_task)
            current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                              'estimated_time_finish': estimated_time_finish})


    zip_subdir = "neurons_repaired_neurosuite"
    zip_filename = "{0}.zip".format(zip_subdir)

    zip_file_memory = BytesIO()
    zf = zipfile.ZipFile(zip_file_memory, "w")

    for neuron in neurons_repaired:
        string_file = neuron["file"].decode("utf-8")
        zf.writestr(neuron["name"], string_file)

    zf.close()

    zip = {"zip_filename": zip_filename, "zip_file": zip_file_memory}

    return zip

def filter_session_neurons_query(session, subgroup_id, query_data, custom_options, half_filter, all_others, dataset_name, dataset_name_global, data_type="continuous", label_column_id="Neuron name", has_input_dataset=None):
    datasets_user = session["datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME]

    if subgroup_id == 1:
        features =   datasets_user.datasets[dataset_name].get_dataframe()
        if dataset_name_global == dataset_name:
            features_all_global = datasets_user.datasets[dataset_name_global].get_dataframe()
        else:
            features_all_global =  datasets_user.get_input_dataframe()

        num_total_instances = len(features)
    elif subgroup_id == 2:
        features_all =  datasets_user.datasets[dataset_name].get_dataframe()
        features_all_global =  datasets_user.datasets["{0}_others".format(dataset_name_global)].get_dataframe()

        features_subgroup_1 = datasets_user.datasets["{0}_subgroup_1".format(dataset_name)].get_dataframe()
        features = helpers.get_dataframe_different_rows(features_all, features_subgroup_1)

        num_total_instances = len(features_all)

    result = {}
    result["{0}_subgroup".format(dataset_name)] = []
    result["error"] = False
    result["error_message"] = ""

    if len(features) < 2:
        result["error"] = True
        result["error_message"] = "There must be a total of at least 2 neurons to create the samples"
        return result

    features_subgroup = []

    if half_filter:
        features_subgroup = copy.deepcopy(features)
        half_point = int(len(features_subgroup) / 2)
        features_subgroup = features_subgroup[0:half_point]

        features_others = helpers.get_dataframe_different_rows(features_all_global, features_subgroup)
    elif all_others:
        features_subgroup = features
        features_others = helpers.get_dataframe_different_rows(features_all_global, features_subgroup_1)
    else:
        features_all_global_mod = copy.deepcopy(features_all_global)
        features_mod = copy.deepcopy(features)
        for filter_query in query_data:
            filter_id = filter_query["id"]
            if filter_id in features.columns.values:
                features_filter_dataset = features_mod
            else:
                features_filter_dataset = features_all_global_mod

            filter_values = filter_query["values"]
            if filter_query["data_type_values"] == "discrete":
                features_subgroup = features_filter_dataset[features_filter_dataset.astype(str)[filter_id].isin(filter_values)]
            else:
                min_value_filter = float(filter_values[0])
                max_value_filter = float(filter_values[1])
                features_subgroup = features_filter_dataset[(features_filter_dataset[filter_id] >= min_value_filter) &
                                                               (features_filter_dataset[filter_id] <= max_value_filter)]

            if filter_id in features_mod.columns.values:
                features_mod = features_subgroup
                features_all_global_mod = dataset_helper.get_dataframe_same_rows(features_filter_dataset, features_all_global_mod)
            else:
                features_mod = dataset_helper.get_dataframe_same_rows(features_subgroup, features_mod)
                features_all_global_mod = features_subgroup

        for custom_option in custom_options:
            if custom_option["id"] == "limit_number_results":
                cut_point = int(custom_option["values"])
                if cut_point >= len(features_subgroup):
                    cut_point = len(features_subgroup)-1
                features_subgroup = features_subgroup.iloc[0:cut_point]

        features_others = helpers.get_dataframe_different_rows(features_all_global, features_subgroup)

    if len(features_subgroup) < 1:
        result["error"] = True
        result["error_message"] = "Empty neurons set. There must be at least 1 instance to create the sample"
        return result
    elif len(features_subgroup) >= num_total_instances:
        result["error"] = True
        result["error_message"] = "All instances taken. You can not select all the instances in a single sample"
        return result

    result["{0}_subgroup".format(dataset_name)] = features_subgroup
    result["{0}_others".format(dataset_name_global)] = features_others
    result["number_neurons_new_sample"] = len(features_subgroup)

    return result


def get_dataframe_different_rows(dataframe_1, dataframe_2):
    indices_dataframe_1 = list(dataframe_1.index)
    indices_dataframe_2 = list(dataframe_2.index)
    indices_remaining = []
    for index in indices_dataframe_1:
        if index not in indices_dataframe_2:
            indices_remaining.append(index)

    dataframe_others = dataframe_1.loc[indices_remaining, :]

    return dataframe_others




def morpho_classify_interneuron_gabaclassifier(neuron, verbose_output):
    result = {}
    with tempfile.NamedTemporaryFile(suffix=neuron["data_file"]["extension"]) as tmp_file:
        neuron_file_path_input = helpers.neuron_in_memory_to_disk(neuron, tmp_file)

        result = gabaclassifier.classify_interneuron(neuron_file_path_input, soma_layer=23, verbose_output=verbose_output)

    return result



def morpho_classify_all_interneuron_gabaclassifier(neurons_user, valid_neurons):
    data_processed = []
    data_errors = []
    # for neuron_name in valid_neurons:
    image_in_neurons = False
    for i, neuron in enumerate(neurons_user):  # All neurons
        start_time = time.time()
        result = morpho_classify_interneuron_gabaclassifier(neuron, verbose_output=True)

        result = [neuron["name"], not result["error"], result["class_interneuron"], result["class_verbose"], result["test_errors"], str(result["exception"])] #Not result["error"] because we'll display class found or class not found
        if "additional_data" in neuron and "png_url" in neuron["additional_data"]:
            result.append((neuron["additional_data"]["png_url"]))
            image_in_neurons = True
        data_processed.append(result)

        end_time = time.time()
        if current_task:
            process_percent = int(100 * float(i) / float(len(neurons_user)))
            estimated_time_finish = round((end_time - start_time) * (float(len(neurons_user)) - float(i)), 2)
            print ("process_percent: ", process_percent, "Task: ", current_task)
            current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                              'estimated_time_finish': estimated_time_finish})

    """-------------------------Create dataset matrix---------------------------"""
    print("Creating dataset matrix... \n")
    columns = ["Neuron name", "Class found", "Class (short name)", "Class (long name)", "Test errors", "Another errors"] #, "Tests fails"]
    if image_in_neurons:
        columns.append("Image")
    result_dataframe = pd.DataFrame(data=data_processed, columns=columns)

    print("--------GabaClassifier all-------" + '\n')

    results = {
        "result_dataframe": result_dataframe,
        "possible_interneuron_classes": gabaclassifier.get_possible_interneuron_classes()
    }


    return results

def morpho_simulate_dendrite_arborization(number_simulated_neurons, output_folder_neurons):
    importlib = __import__('importlib')
    hbp_dendrite_arborization_simulation = importlib.import_module('apps.morpho_analyzer.helpers.hbp-dendrite-arborization-simulation.wrapper_py')

    #Create output_folder_neurons if not exists:
    if not os.path.exists(output_folder_neurons):
        os.makedirs(output_folder_neurons)

    result = hbp_dendrite_arborization_simulation.simulate_dendrite_arborization(number_simulated_neurons, output_folder_neurons)

    return result
