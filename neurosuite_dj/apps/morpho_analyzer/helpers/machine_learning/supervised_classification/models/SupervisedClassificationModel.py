import secrets

class SupervisedClassificationModel:

    def __init__(self, name="BN", dataframe=[], data_type=None, features_classes=[]):
        self.dataframe = dataframe
        self.features_classes = features_classes
        self.data_type = data_type