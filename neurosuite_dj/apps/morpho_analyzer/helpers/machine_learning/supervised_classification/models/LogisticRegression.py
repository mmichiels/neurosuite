from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from sklearn.linear_model import LogisticRegression as sklearn_logisticRegression

class LogisticRegression():
    def __init__(self, algorithm_parameters):
        self.name = "logisticRegression"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["cross_validation"] = algorithm_parameters["logisticRegression_cross_validation"]
        self.params["penalty"] = algorithm_parameters["logisticRegression_penalty"] #For non-linear SMVs (SVC and NuSVC)

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "scikit-learn":
            self.train_logisticRegression_scikit_learn(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def get_not_trained_model(self, k_folds=5):
        if self.params["cross_validation"]:
            param_grid = [{'penalty': ['none'], 'solver': ['saga']},
                          {'penalty': ['l1', 'l2'], 'solver': ['liblinear']},
                          {'penalty': ['elasticnet'], 'solver': ['saga'], 'l1_ratio': np.arange(0, 1.1, 0.1)},
                          ]
            self.not_trained_model = sklearn_GridSearchCV(sklearn_logisticRegression(), param_grid, cv=k_folds,
                                              scoring='f1_macro')
        else:
            solver = "liblinear"
            if self.params["penalty"] == "none" or self.params["penalty"] == "elasticnet":
                solver = "saga"

            if self.params["penalty"] == "elasticnet":
                self.not_trained_model = sklearn_logisticRegression(penalty=self.params["penalty"], solver=solver, l1_ratio=0.5)
            else:
                self.not_trained_model = sklearn_logisticRegression(penalty=self.params["penalty"], solver=solver)

        return self.not_trained_model


    def train_logisticRegression_scikit_learn(self, x_train, y_train, k_folds):
        if self.params["cross_validation"]:
            param_grid = [{'penalty': ['none'], 'solver': ['saga']},
                          {'penalty': ['l1', 'l2'], 'solver': ['liblinear']},
                          {'penalty': ['elasticnet'], 'solver': ['saga'], 'l1_ratio': np.arange(0, 1.1, 0.1)},
                          ]
            self.model = sklearn_GridSearchCV(sklearn_logisticRegression(), param_grid, cv=k_folds,
                                              scoring=supervised_classification_metrics.get_scorer_f_score_multiclass())
        else:
            solver = "liblinear"
            if self.params["penalty"] == "none" or self.params["penalty"] == "elasticnet":
                solver = "saga"

            if self.params["penalty"] == "elasticnet":
                self.model = sklearn_logisticRegression(penalty=self.params["penalty"], solver=solver, l1_ratio=0.5)
            else:
                self.model = sklearn_logisticRegression(penalty=self.params["penalty"], solver=solver)

        self.model.fit(x_train, y_train)
        if self.params["cross_validation"]:
            self.params["penalty"] = self.model.best_params_["penalty"]

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                if hasattr(self.model, "classes_"):
                    classes = self.model.classes_
                else:
                    classes = self.model.best_estimator_.estimators_[0].classes_

                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=classes)
        except Exception as e:
            pass

        return y_pred_proba_df

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      self.y_training_val_pred_proba,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_metrics(y_test, self.y_test_pred,
                                                                                              self.y_test_pred_proba,
                                                                                              training_time)
