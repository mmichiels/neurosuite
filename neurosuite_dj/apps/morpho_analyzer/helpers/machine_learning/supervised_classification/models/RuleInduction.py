from .SupervisedClassification import *
from .SupervisedClassificationModel import *
import Orange

class RuleInduction():
    def __init__(self, algorithm_parameters):
        self.name = "ruleInduction"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["beam_width"] = int(algorithm_parameters["ruleInduction_beam_width"])
        self.params["constrain_continuous"] = algorithm_parameters["ruleInduction_constrain_continuous"]
        self.params["min_covered_examples"] = int(algorithm_parameters["ruleInduction_min_covered_examples"])
        self.params["max_rule_length"] = int(algorithm_parameters["ruleInduction_max_rule_length"])
        self.params["cross_validation"] = algorithm_parameters["ruleInduction_cross_validation"]

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "orange":
            self.train_ruleInduction_orange(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_ruleInduction_orange(self, x_train, y_train, k_folds):
        orange_table = pandas_df_with_class_to_orange(x_train, y_train)

        cn2_learner = Orange.classification.CN2UnorderedLearner()

        if self.params["cross_validation"]:
            pass
        else:
            # consider up to x solution streams at one time
            cn2_learner.rule_finder.search_algorithm.beam_width = self.params["beam_width"]
            # continuous value space is constrained to reduce computation time
            cn2_learner.rule_finder.search_strategy.constrain_continuous = self.params["constrain_continuous"]
            # found rules must cover at least x examples
            cn2_learner.rule_finder.general_validator.min_covered_examples = self.params["min_covered_examples"]
            # found rules may combine at most x selectors (conditions)
            cn2_learner.rule_finder.general_validator.max_rule_length = self.params["max_rule_length"]

        self.model = cn2_learner(orange_table)

        if self.params["cross_validation"]:
            pass

    def show_explanation(self, features_names, classes_names):
        # print_ruleInduction_orange
        text = ""
        plot = None
        classes = self.model.domain.class_vars[0].values

        for rule in self.model.rule_list:
            text += "{} <br> Classes distribution ({}): {} -- Rule quality: {} <br><br>".format(str(rule), classes, str(rule.curr_class_dist.tolist()), round(rule.quality, 2))

        return text, plot

    def predict(self, x_test):
        # Predict and evaluate metrics in the test set
        classes = self.model.domain.class_vars[0].values
        y_pred_probs = self.model.predict(x_test.as_matrix())

        y_pred = []
        for row in y_pred_probs:
            max_prob_class_idx = np.argmax(row)
            max_prob_class = classes[max_prob_class_idx]
            y_pred.append(max_prob_class)

        y_pred = np.array(y_pred)

        return y_pred

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None
        classes = self.model.domain.class_vars[0].values

        try:
            if x_test.shape[0] > 0:
                y_pred_probs = self.model.predict(x_test.as_matrix())

                y_pred_proba_vals = []
                for row in y_pred_probs:
                    y_pred_proba_vals.append(row)

                y_pred_proba_vals = np.array(y_pred_proba_vals)
                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=classes)
        except Exception as e:
            pass

        return y_pred_proba_df

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      self.y_training_val_pred_proba,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_metrics(y_test, self.y_test_pred,
                                                                                              self.y_test_pred_proba,
                                                                                              training_time)
