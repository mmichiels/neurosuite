from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from sklearn import neighbors as sklearn_neighbors
from skmultilearn.adapt import MLkNN as skmultilearn_MLkNN

class MLKnn():
    def __init__(self, algorithm_parameters):
        self.name = "MLknn"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["cross_validation"] = algorithm_parameters["mlknn_cross_validation"]
        self.params["k"] = int(algorithm_parameters["mlknn_k"])

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "scikit-learn":
            self.train_knn_scikit_learn(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_knn_scikit_learn(self, x_train, y_train, k_folds):
        if self.params["cross_validation"]:
            param_grid = {'k': np.arange(1, 8)}
            self.model = sklearn_GridSearchCV(skmultilearn_MLkNN(), param_grid, cv=k_folds,
                                              scoring='f1_macro')
        else:
            self.model = skmultilearn_MLkNN(self.params["k"])

        y_train_ = y_train.astype('int')
        self.model.fit(x_train.values, y_train_.values)
        if self.params["cross_validation"]:
            self.params["k"] = self.model.best_params_["k"]

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred.toarray()

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=self.model.classes_)
        except Exception as e:
            pass

        return y_pred_proba_df

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_multioutput_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_multioutput_metrics(y_test, self.y_test_pred,
                                                                                              training_time)