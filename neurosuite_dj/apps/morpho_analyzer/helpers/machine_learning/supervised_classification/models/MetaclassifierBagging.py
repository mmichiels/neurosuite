from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from sklearn.ensemble import BaggingClassifier as sklearn_BaggingClassifier
# https://scikit-learn.org/stable/modules/ensemble.html#bagging-meta-estimator

class MetaclassifierBagging():
    def __init__(self, algorithm_parameters):
        self.name = "bagging"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["cross_validation"] = algorithm_parameters["bagging_cross_validation"]
        self.params["base_estimator"] = algorithm_parameters["bagging_base_estimator"]
        if self.params["base_estimator"] == "":
            self.params["base_estimator"] = None
        self.params["n_estimators"] = int(algorithm_parameters["bagging_n_estimators"])
        self.params["max_samples"] = int(algorithm_parameters["bagging_max_samples"])
        self.params["max_features"] = int(algorithm_parameters["bagging_max_features"])
        self.params["bootstrap_samples"] = algorithm_parameters["bagging_bootstrap_samples"]
        self.params["bootstrap_features"] = algorithm_parameters["bagging_bootstrap_features"]

    def select_estimators(self, models_learned):
        if self.params["base_estimator"] is not None:
            self.params["base_estimator"] = models_learned[self.params["base_estimator"]].model
            if hasattr(self.params["base_estimator"], "estimator"):
                self.params["base_estimator"] = self.params["base_estimator"].estimator

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "scikit-learn":
            self.train_metaclassifierBagging_scikit_learn(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_metaclassifierBagging_scikit_learn(self, x_train, y_train, k_folds):
        self.model = sklearn_BaggingClassifier(base_estimator=self.params["base_estimator"],n_estimators=self.params["n_estimators"],
                                               max_samples=self.params["max_samples"], max_features=self.params["max_features"],
                                               bootstrap=self.params["bootstrap_samples"], bootstrap_features=self.params["bootstrap_features"])

        # Cross val (if not input parameters are received)
        self.model.fit(x_train, y_train)
        if self.params["cross_validation"]:
            self.params = SupervisedClassification.parse_best_params_cv(self.model.best_params_)

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=self.model.classes_)
        except Exception as e:
            pass

        return y_pred_proba_df

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      self.y_training_val_pred_proba,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_metrics(y_test, self.y_test_pred,
                                                                                              self.y_test_pred_proba,
                                                                                              training_time)
