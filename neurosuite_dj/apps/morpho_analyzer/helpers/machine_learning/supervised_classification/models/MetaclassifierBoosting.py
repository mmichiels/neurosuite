from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from sklearn.ensemble import AdaBoostClassifier as sklearn_AdaBoostClassifier
# https://scikit-learn.org/stable/modules/ensemble.html#adaboost

class MetaclassifierBoosting():
    def __init__(self, algorithm_parameters):
        self.name = "boosting"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["cross_validation"] = algorithm_parameters["boosting_cross_validation"]
        self.params["base_estimator"] = algorithm_parameters["boosting_base_estimator"]
        if self.params["base_estimator"] == "":
            self.params["base_estimator"] = None
        self.params["n_estimators"] = int(algorithm_parameters["boosting_n_estimators"])
        self.params["learning_rate"] = float(algorithm_parameters["boosting_learning_rate"])
        self.params["algorithm"] = algorithm_parameters["boosting_algorithm"]


    def select_estimators(self, models_learned):
        if self.params["base_estimator"] is not None:
            self.params["base_estimator"] = models_learned[self.params["base_estimator"]].model
            if hasattr(self.params["base_estimator"], "estimator"):
                self.params["base_estimator"] = self.params["base_estimator"].estimator

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "scikit-learn":
            self.train_metaclassifierBoosting_scikit_learn(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_metaclassifierBoosting_scikit_learn(self, x_train, y_train, k_folds):
        self.model = sklearn_AdaBoostClassifier(base_estimator=self.params["base_estimator"], n_estimators=self.params["n_estimators"],
                                            learning_rate=self.params["learning_rate"], algorithm=self.params["algorithm"])


        self.model.fit(x_train, y_train)
        if self.params["cross_validation"]:
            self.params = SupervisedClassification.parse_best_params_cv(self.model.best_params_)

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=self.model.classes_)
        except Exception as e:
            pass

        return y_pred_proba_df

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      self.y_training_val_pred_proba,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_metrics(y_test, self.y_test_pred,
                                                                                              self.y_test_pred_proba,
                                                                                              training_time)
