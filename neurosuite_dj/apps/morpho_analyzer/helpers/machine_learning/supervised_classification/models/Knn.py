from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from sklearn import neighbors as sklearn_neighbors

class Knn():
    def __init__(self, algorithm_parameters):
        self.name = "knn"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["cross_validation"] = algorithm_parameters["knn_cross_validation"]
        self.params["k"] = int(algorithm_parameters["knn_k"])

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "scikit-learn":
            self.train_knn_scikit_learn(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def get_not_trained_model(self, k_folds=5):
        if self.params["cross_validation"]:
            param_grid = [{'n_neighbors': np.arange(1, 21)}]
            self.not_trained_model = sklearn_GridSearchCV(sklearn_neighbors.KNeighborsClassifier(), param_grid, cv=k_folds,
                                              scoring='f1_macro')
        else:
            self.not_trained_model = sklearn_neighbors.KNeighborsClassifier(self.params["k"], weights="uniform")
        return self.not_trained_model

    def train_knn_scikit_learn(self, x_train, y_train, k_folds):
        if self.params["cross_validation"]:
            param_grid = [{'n_neighbors': np.arange(1, 21)}]
            self.model = sklearn_GridSearchCV(sklearn_neighbors.KNeighborsClassifier(), param_grid, cv=k_folds,
                                              scoring=supervised_classification_metrics.get_scorer_f_score_multiclass())
        else:
            self.model = sklearn_neighbors.KNeighborsClassifier(self.params["k"], weights="uniform")

        self.model.fit(x_train, y_train)
        if self.params["cross_validation"]:
            self.params["k"] = self.model.best_params_["n_neighbors"]

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=self.model.classes_)
        except Exception as e:
            pass

        return y_pred_proba_df

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      self.y_training_val_pred_proba,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_metrics(y_test, self.y_test_pred,
                                                                                              self.y_test_pred_proba,
                                                                                              training_time)