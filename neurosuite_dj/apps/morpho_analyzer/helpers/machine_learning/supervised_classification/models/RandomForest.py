from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from sklearn.ensemble import RandomForestClassifier as sklearn_RandomForestClassifier

class RandomForest():
    def __init__(self, algorithm_parameters):
        self.name = "randomForest"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["cross_validation"] = algorithm_parameters["randomForest_cross_validation"]
        self.params["bootstrap"] = algorithm_parameters["randomForest_bootstrap"]
        self.params["max_depth"] = int(algorithm_parameters["randomForest_max_depth"])
        self.params["max_features"] = algorithm_parameters["randomForest_max_features"]
        self.params["criterion"] = algorithm_parameters["randomForest_criterion"]
        self.params["min_samples_leaf"] = int(algorithm_parameters["randomForest_min_samples_leaf"])
        self.params["min_samples_split"] = int(algorithm_parameters["randomForest_min_samples_split"])
        self.params["n_estimators"] = int(algorithm_parameters["randomForest_n_estimators"])

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "scikit-learn":
            self.train_randomForest_scikit_learn(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def get_not_trained_model(self, k_folds=5):
        if self.params["max_depth"] == 0:
            self.params["max_depth"] = None

        if self.params["cross_validation"]:
            param_grid = {
                'bootstrap': [self.params["bootstrap"]],
                "criterion": ["gini", "entropy"],
                'max_depth': [self.params["max_depth"]],
                'max_features': ["sqrt",  "log2"],
                'min_samples_leaf': [1, 5],
                'min_samples_split': [2, 6],
                'n_estimators': [self.params["n_estimators"]]
            }
            self.not_trained_model = sklearn_GridSearchCV(sklearn_RandomForestClassifier(), param_grid, cv=k_folds,
                                              scoring='f1_macro')
        else:
            self.not_trained_model = sklearn_RandomForestClassifier(bootstrap=self.params["bootstrap"], criterion=self.params["criterion"],
                                                        max_depth=self.params["max_depth"], max_features=self.params["max_features"],
                                                        min_samples_leaf=self.params["min_samples_leaf"], min_samples_split=self.params["min_samples_split"],
                                                        n_estimators=self.params["n_estimators"])
        self.not_trained_model

    def train_randomForest_scikit_learn(self, x_train, y_train, k_folds):
        if self.params["max_depth"] == 0:
            self.params["max_depth"] = None

        if self.params["cross_validation"]:
            param_grid = {
                'bootstrap': [self.params["bootstrap"]],
                "criterion": ["gini", "entropy"],
                'max_depth': [self.params["max_depth"]],
                'max_features': ["sqrt",  "log2"],
                'min_samples_leaf': [1, 5],
                'min_samples_split': [2, 6],
                'n_estimators': [self.params["n_estimators"]]
            }
            self.model = sklearn_GridSearchCV(sklearn_RandomForestClassifier(), param_grid, cv=k_folds,
                                              scoring=supervised_classification_metrics.get_scorer_f_score_multiclass())
        else:
            self.model = sklearn_RandomForestClassifier(bootstrap=self.params["bootstrap"], criterion=self.params["criterion"],
                                                        max_depth=self.params["max_depth"], max_features=self.params["max_features"],
                                                        min_samples_leaf=self.params["min_samples_leaf"], min_samples_split=self.params["min_samples_split"],
                                                        n_estimators=self.params["n_estimators"])

        self.model.fit(x_train, y_train)
        if self.params["cross_validation"]:
            self.params = SupervisedClassification.parse_best_params_cv(self.model.best_params_, split_string=False)

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=self.model.classes_)
        except Exception as e:
            pass

        return y_pred_proba_df


    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      self.y_training_val_pred_proba,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_metrics(y_test, self.y_test_pred,
                                                                                              self.y_test_pred_proba,
                                                                                              training_time)