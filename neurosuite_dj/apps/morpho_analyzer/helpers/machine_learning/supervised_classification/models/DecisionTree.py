import base64

from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from sklearn import tree as sklearn_tree
#from sklearn.externals.six import StringIO
import io

import matplotlib.pyplot as plt
import plotly
import plotly.plotly as py
import plotly.tools as tls
import pydotplus

class DecisionTree():
    def __init__(self, algorithm_parameters):
        self.name = "decisionTree"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["cross_validation"] = algorithm_parameters["decisionTree_cross_validation"]
        self.params["max_depth"] = int(algorithm_parameters["decisionTree_max_depth"])
        self.params["max_features"] = algorithm_parameters["decisionTree_max_features"]
        self.params["criterion"] = algorithm_parameters["decisionTree_criterion"]
        self.params["min_samples_leaf"] = int(algorithm_parameters["decisionTree_min_samples_leaf"])

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "scikit-learn":
            self.train_decisionTree_scikit_learn(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def get_not_trained_model(self, k_folds=5):
        if self.params["max_depth"] == 0:
            self.params["max_depth"] = None

        if self.params["cross_validation"]:
            param_grid = {
                "criterion": ["gini", "entropy"],
                'max_depth': [None, 10, 100, 200],#self.params["max_depth"]],
                'max_features': ["sqrt",  "log2"],
                'min_samples_leaf': [1, 5],
            }
            self.not_trained_model = sklearn_GridSearchCV(sklearn_tree.DecisionTreeClassifier(), param_grid, cv=k_folds,
                                              scoring='f1_macro')
        else:
            self.not_trained_model = sklearn_tree.DecisionTreeClassifier(criterion=self.params["criterion"], max_features=self.params["max_features"],
                                                             max_depth=self.params["max_depth"], min_samples_leaf=self.params["min_samples_leaf"])
        return self.not_trained_model

    def train_decisionTree_scikit_learn(self, x_train, y_train, k_folds):
        if self.params["max_depth"] == 0:
            self.params["max_depth"] = None

        if self.params["cross_validation"]:
            param_grid = {
                "criterion": ["gini", "entropy"],
                'max_depth': [None, 10, 100, 200],#self.params["max_depth"]],
                'max_features': ["sqrt",  "log2"],
                'min_samples_leaf': [1, 5],
            }
            self.model = sklearn_GridSearchCV(sklearn_tree.DecisionTreeClassifier(), param_grid, cv=k_folds,
                                              scoring=supervised_classification_metrics.get_scorer_f_score_multiclass())
        else:
            self.model = sklearn_tree.DecisionTreeClassifier(criterion=self.params["criterion"], max_features=self.params["max_features"],
                                                             max_depth=self.params["max_depth"], min_samples_leaf=self.params["min_samples_leaf"])

        # Cross val (if not input parameters are received)
        self.model.fit(x_train, y_train)
        if self.params["cross_validation"]:
            self.params = SupervisedClassification.parse_best_params_cv(self.model.best_params_, split_string=False)

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=self.model.classes_)
        except Exception as e:
            pass

        return y_pred_proba_df

    def show_explanation(self, features_names, classes_names):
        text = ""

        model = self.model
        if self.params["cross_validation"]:
            model = self.model.best_estimator_

        dot_data = io.StringIO()
        sklearn_tree.export_graphviz(model, out_file=dot_data, rounded=True, proportion=False, precision=2,
                                     filled=True, special_characters=True,
                                     feature_names=features_names,class_names=model.classes_
                                     )
        graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
        image_png = graph.create_png()
        plot = base64.b64encode(image_png).decode('utf-8')

        return text, plot

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      self.y_training_val_pred_proba,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_metrics(y_test, self.y_test_pred,
                                                                                              self.y_test_pred_proba,
                                                                                              training_time)