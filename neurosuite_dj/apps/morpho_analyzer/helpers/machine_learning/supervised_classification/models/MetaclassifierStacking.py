from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from mlxtend.classifier import StackingClassifier as mlxtend_StackingClassifier
from sklearn import tree as sklearn_tree
from sklearn.linear_model import LogisticRegression as sklearn_logisticRegression
from sklearn import preprocessing as sklearn_preprocessing

# http://rasbt.github.io/mlxtend/user_guide/classifier/StackingClassifier/
# https://scikit-learn.org/dev/modules/generated/sklearn.ensemble.StackingClassifier.html
# https://scikit-learn.org/stable/developers/advanced_installation.html#installing-nightly-builds

class MetaclassifierStacking():
    def __init__(self, algorithm_parameters):
        self.name = "stacking"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["cross_validation"] = algorithm_parameters["stacking_cross_validation"]
        self.params["classifiers"] = algorithm_parameters["stacking_classifiers"]
        self.params["meta_classifier"] = algorithm_parameters["stacking_meta_classifier"]

    def select_estimators(self, models_learned):
        model_classifiers = []
        if self.params["classifiers"]:
            for classifier in self.params["classifiers"]:
                model_classifier = models_learned[classifier].model
                if hasattr(model_classifier, "estimator"):
                    model_classifier = model_classifier.estimator
                model_classifiers.append(model_classifier)
            self.params["classifiers"] = model_classifiers
        else:
            self.params["classifiers"] = [sklearn_tree.DecisionTreeClassifier()]

        if self.params["meta_classifier"]:
            self.params["meta_classifier"] = models_learned[self.params["meta_classifier"]].model
            if hasattr(classifier, "estimator"):
                self.params["meta_classifier"] = classifier.estimator
        else:
            self.params["meta_classifier"] = sklearn_logisticRegression()

    def transform_pd_strings_to_ints(self, y):
        y_ints_vals = self.label_encoder.transform(y)
        y_ints = pd.DataFrame(y_ints_vals, columns=[self.class_col_name])

        return y_ints

    def transform_pd_ints_to_strings(self, y):
        y_strings_vals = self.label_encoder.inverse_transform(y)
        y_strings = pd.DataFrame(y_strings_vals, columns=[self.class_col_name])

        return y_strings

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "mlxtend":
            self.train_metaclassifierStacking_mlxtend(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_metaclassifierStacking_mlxtend(self, x_train, y_train, k_folds):
        self.model = mlxtend_StackingClassifier(classifiers=self.params["classifiers"],
                                  meta_classifier=self.params["meta_classifier"], use_probas=True)

        self.class_col_name = y_train.columns[0]
        self.label_encoder = sklearn_preprocessing.LabelEncoder()
        self.label_encoder.fit(y_train)
        y_train_ints = self.transform_pd_strings_to_ints(y_train)

        self.model.fit(x_train, y_train_ints)
        if self.params["cross_validation"]:
            self.params = SupervisedClassification.parse_best_params_cv(self.model.best_params_)

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        y_pred = self.transform_pd_ints_to_strings(y_pred)

        return y_pred

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=self.model.meta_classifier.classes_)
        except Exception as e:
            pass

        return y_pred_proba_df

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      self.y_training_val_pred_proba,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_metrics(y_test, self.y_test_pred,
                                                                                              self.y_test_pred_proba,
                                                                                              training_time)
