from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from skmultilearn.problem_transform import BinaryRelevance as skmllearn_binary_relevance

class BinaryRelevance():
    def __init__(self, method_parameters, algorithm_parameters, supervised_classification):
        self.name = "Binary relevance"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["classification_algorithm_name"] = method_parameters["binary-relevance-select-learning-algorithm"]
        self.classification_algorithm = supervised_classification.get_learning_class(
            algorithm_name=self.params["classification_algorithm_name"],
            algorithm_parameters=algorithm_parameters[self.params["classification_algorithm_name"]]
        ).get_not_trained_model(supervised_classification.k_folds)

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "scikit-learn":
            self.train_binary_relevance(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_binary_relevance(self, x_train, y_train, k_folds):
        self.model = skmllearn_binary_relevance(classifier = self.classification_algorithm)

        y_train_ = y_train.astype('int')

        self.model.fit(x_train, y_train_)

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred.toarray()

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=self.model.classes_)
        except Exception as e:
            pass

        return y_pred_proba_df

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_multioutput_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_multioutput_metrics(y_test, self.y_test_pred,
                                                                                              training_time)