from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from sklearn.naive_bayes import GaussianNB as sklearn_gaussianNB
from pytan._clg import CLGBayesNetClassifier
from pytan._discrete import DiscreteBayesNetClassifier

class TAN():
    def __init__(self, algorithm_parameters):
        self.name = "tan"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["cross_validation"] = algorithm_parameters["tan_cross_validation"]

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "pytan":
            self.train_tan_pytan(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

        features_names = list(x_train.columns)
        classes_names = list(y_train.columns)
        explanation = self.show_explanation(features_names, classes_names)

    def get_not_trained_model(self, k_folds=5):
        self.not_trained_model = CLGBayesNetClassifier()
        return self.not_trained_model

    def train_tan_pytan(self, x_train, y_train, k_folds):
        self.model = CLGBayesNetClassifier()

        # Cross val (if not input parameters are received)
        self.model.fit(x_train, y_train)
        if self.params["cross_validation"]:
            self.params = SupervisedClassification.parse_best_params_cv(self.model.best_params_)

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                if hasattr(self.model, "classes_"):
                    classes = self.model.classes_
                else:
                    classes = self.model.estimators_[0].classes_

                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=classes)
        except Exception as e:
            pass

        return y_pred_proba_df

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      self.y_training_val_pred_proba,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_metrics(y_test, self.y_test_pred,
                                                                                              self.y_test_pred_proba,
                                                                                              training_time)
    def show_explanation(self, features_names, classes_names):
        # print_ruleInduction_orange
        text = ""
        plot = None

        if hasattr(self.model, "classes_"):
            model = self.model
        else:
            model = self.model.estimators_[0]

        classes = model.classes_
        graph_pd = pd.DataFrame(model.graph_, columns=features_names)
        graph_pd.insert(loc=0, column=classes_names[0], value=np.array(classes))
        graph_pd_html = graph_pd.to_html(classes="datatable-new-ajax display no-border table-dataframe-html-with-stats table-with-stats",
                         escape=False, border=0)

        text = "Graph matrix (Each feature per class): <br><br> {}".format(graph_pd_html)

        return text, plot