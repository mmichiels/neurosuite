import secrets
import neurosuite_dj.helpers_global.dataset as dataset_helper
import neurosuite_dj.helpers_global.datasets_user as datasets_user_helper
from django.conf import settings
import time
import numpy as np
import pandas as pd
import csv
from io import StringIO
from collections import OrderedDict
import Orange
from Orange.data.pandas_compat import table_from_frame as orange_table_from_frame
from sklearn.multioutput import MultiOutputClassifier as sklearn_MultiOutputClassifier
from sklearn.model_selection import GridSearchCV as sklearn_GridSearchCV
from apps.morpho_analyzer.helpers.machine_learning.supervised_classification.evaluation import supervised_classification_metrics

class SupervisedClassification:

    def __init__(self, name="supervised_classification", dataframe=[], id_column="", features_classes=[], session_id=None):
        self.name = name
        self.set_session_id(session_id)
        self.features_classes = features_classes
        self.id_column = id_column
        self.load_datasets_x_y(dataframe)
        self.k_folds = None
        self.percentage_test_instances = None
        self.models = {}
        self.summary_evaluation = {}
        self.new_prediction_dataset = None

    def set_session_id(self, session_id):
        if session_id is None:
            session_id = secrets.token_hex(16)
        self.session_id = session_id

    def load_datasets_x_y(self, dataframe):
        features_x = list(set(dataframe.columns) - set(self.features_classes) - set([self.id_column]))
        dataframe_x_train = dataframe.loc[:, features_x]
        dataframe_y_train = dataframe.loc[:, self.features_classes]
        self.ids = list(dataframe.loc[:, self.id_column])

        self.x_train = dataset_helper.Dataset("ml_supervised_classification_x_train", self.session_id,
                                              app_name=settings.MORPHO_ANALYZER_APP_NAME)
        self.x_train.load(dataframe_x_train)

        self.data_type = dataset_helper.dataframe_get_type(dataframe_x_train.dtypes)

        self.y_train = dataset_helper.Dataset("ml_supervised_classification_y_train", self.session_id,
                                              app_name=settings.MORPHO_ANALYZER_APP_NAME)
        self.y_train.load(dataframe_y_train)

        self.x_test = self.x_train
        self.y_test = self.y_train

    def set_sets_config(self, k_folds, percentage_test_instances, shuffle_instances):
        self.k_folds = k_folds
        self.percentage_test_instances = percentage_test_instances / 100
        percentage_train_instances = 1 - self.percentage_test_instances
        x_train, y_train, _, _ = self.get_dataframes_x_y()

        if shuffle_instances:
            x_train = x_train.sample(frac=1, random_state=1)  # random_state is the seed
            indices_order = list(x_train.index)
            y_train = y_train.reindex(indices_order)
            x_train = x_train.reset_index(drop=True)
            y_train = y_train.reset_index(drop=True)
            self.ids = [self.ids[i] for i in indices_order]


        num_train_instances = int(x_train.shape[0] * percentage_train_instances)
        dataframe_x_train = x_train.iloc[:num_train_instances, :]
        dataframe_y_train = y_train.iloc[:num_train_instances, :]
        dataframe_x_test = x_train.iloc[num_train_instances:, :]
        dataframe_y_test = y_train.iloc[num_train_instances:, :]

        self.x_train.update_and_save(dataframe_x_train)
        self.y_train.update_and_save(dataframe_y_train)

        self.x_test = dataset_helper.Dataset("ml_supervised_classification_x_test", self.session_id,
                                             app_name=settings.MORPHO_ANALYZER_APP_NAME)
        self.x_test.load(dataframe_x_test)
        self.y_test = dataset_helper.Dataset("ml_supervised_classification_y_test", self.session_id,
                                             app_name=settings.MORPHO_ANALYZER_APP_NAME)
        self.y_test.load(dataframe_y_test)

    def get_dataframes_x_y(self):
        x_train = self.x_train.get_dataframe()
        y_train = self.y_train.get_dataframe()

        x_test = self.x_test.get_dataframe()
        y_test = self.y_test.get_dataframe()

        return x_train, y_train, x_test, y_test

    def add_model(self, model):
        self.models[model.name] = model

    def load_new_prediction_dataset(self, dataframe):
        features_x = list(set(dataframe.columns) - set(self.features_classes) - set([self.id_column]))
        self.new_prediction_dataset_ids = dataframe.loc[:, self.id_column]
        dataframe_new_prediction = dataframe.loc[:, features_x]
        self.new_prediction_dataset = dataset_helper.Dataset("ml_supervised_classification_new_prediction_dataset", self.session_id,
                                              app_name=settings.MORPHO_ANALYZER_APP_NAME)
        self.new_prediction_dataset.load(dataframe_new_prediction)

    def get_new_prediction_dataframe(self):
        new_prediction_dataframe = self.new_prediction_dataset.get_dataframe()

        return new_prediction_dataframe, self.new_prediction_dataset_ids

    @staticmethod
    def parse_best_params_cv(best_params_cv, split_string=False):
        best_params = {"cross_validation": True}

        for param, val in best_params_cv.items():
            if split_string:
                param = param.split("__")[1]
            best_params[param] = val

        return best_params

    def get_multilabel_transformation_method(self, method_name, method_parameters, learning_algorithms):
        from .MLKnn import MLKnn
        from .MLTSvm import MLTSvm
        from .BinaryRelevance import BinaryRelevance
        from .ClassifierChains import ClassifierChains
        from .LabelPowerset import LabelPowerset
        from .RAKELd import Rakeld

        method_class = {}
        if method_name == "mlknn":
            method_class = MLKnn(method_parameters)
        elif method_name == "mltsvm":
            method_class = MLTSvm(method_parameters)
        elif method_name == "classifier-chains":
            method_class = ClassifierChains(method_parameters, learning_algorithms, self)
        elif method_name == "binary-relevance":
            method_class = BinaryRelevance(method_parameters, learning_algorithms, self)
        elif method_name == "label-powerset":
            method_class = LabelPowerset(method_parameters, learning_algorithms, self)
        elif method_name == "rakeld":
            method_class = Rakeld(method_parameters, learning_algorithms, self)
        return method_class


    def get_learning_class(self, algorithm_name, algorithm_parameters):
        from .Knn import Knn
        from .DecisionTree import DecisionTree
        from .SVM import SVM
        from .LogisticRegression import LogisticRegression
        from .NaiveBayes import NaiveBayes
        from .TAN import TAN
        from .LDA import LDA
        from .QDA import QDA
        from .NeuralNetwork import NeuralNetwork
        from .RandomForest import RandomForest
        from .RuleInduction import RuleInduction
        from .MetaclassifierBagging import MetaclassifierBagging
        from .MetaclassifierBoosting import MetaclassifierBoosting
        from .MetaclassifierStacking import MetaclassifierStacking

        algorithm_class = {}

        if algorithm_name == "knn":
            algorithm_class = Knn(algorithm_parameters)
        elif algorithm_name == "decisionTree":
            algorithm_class = DecisionTree(algorithm_parameters)
        elif algorithm_name == "svm":
            algorithm_class = SVM(algorithm_parameters)
        elif algorithm_name == "logisticRegression":
            algorithm_class = LogisticRegression(algorithm_parameters)
        elif algorithm_name == "naiveBayes":
            algorithm_class = NaiveBayes(algorithm_parameters)
        elif algorithm_name == "tan":
            algorithm_class = TAN(algorithm_parameters)
        elif algorithm_name == "lda":
            algorithm_class = LDA(algorithm_parameters)
        elif algorithm_name == "qda":
            algorithm_class = QDA(algorithm_parameters)
        elif algorithm_name == "neuralNetwork":
            algorithm_class = NeuralNetwork(algorithm_parameters)
        elif algorithm_name == "randomForest":
            algorithm_class = RandomForest(algorithm_parameters)
        elif algorithm_name == "ruleInduction":
            algorithm_class = RuleInduction(algorithm_parameters)
        elif algorithm_name == "bagging":
            algorithm_class = MetaclassifierBagging(algorithm_parameters)
        elif algorithm_name == "boosting":
            algorithm_class = MetaclassifierBoosting(algorithm_parameters)
        elif algorithm_name == "stacking":
            algorithm_class = MetaclassifierStacking(algorithm_parameters)

        return algorithm_class

def pandas_df_with_class_to_orange(x, y):
    df = pd.merge(x, y, left_index=True, right_index=True, how='outer')
    df.iloc[:, -1] = df.iloc[:, -1].astype('category')
    data = orange_table_from_frame(df)
    domain = data.domain
    new_domain = Orange.data.Domain(attributes=domain.attributes[:-1], metas=domain.metas,
                                    class_vars=data.domain.attributes[-1])

    orange_table = Orange.data.Table(new_domain, data)

    return orange_table

def pandas_df_to_orange(x):
    data = orange_table_from_frame(x)
    domain = data.domain
    new_domain = Orange.data.Domain(attributes=domain.attributes, metas=domain.metas)
    orange_table = Orange.data.Table(new_domain, data)

    return orange_table
