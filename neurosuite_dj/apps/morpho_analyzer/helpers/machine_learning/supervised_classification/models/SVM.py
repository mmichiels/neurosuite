from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from sklearn import svm as sklearn_svm

class SVM():
    def __init__(self, algorithm_parameters):
        self.name = "svm"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.params["cross_validation"] = algorithm_parameters["svm_cross_validation"] #For non-linear SMVs (SVC and NuSVC)
        self.params["kernel"] = algorithm_parameters["svm_kernel"] #For non-linear SMVs (SVC and NuSVC)
        self.params["c"] = float(algorithm_parameters["svm_c"])

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "scikit-learn":
            self.train_svm_scikit_learn(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def get_not_trained_model(self, k_folds=5):
        if self.params["cross_validation"]:
            param_grid = [{'kernel': ['linear', 'poly', 'rbf', 'sigmoid'], 'C': [0.000000001, 1, 5, 10]}]
            self.not_trained_model = sklearn_GridSearchCV(sklearn_svm.SVC(probability=True), param_grid, cv=k_folds,
                                              scoring='f1_macro')
        else:
            if self.params["kernel"] == "linear":
                self.not_trained_model = sklearn_svm.LinearSVC(C=self.params["c"])
            else:
                self.not_trained_model = sklearn_svm.SVC(kernel=self.params["kernel"], C=self.params["c"], probability=True)
        return self.not_trained_model

    def train_svm_scikit_learn(self, x_train, y_train, k_folds):
        if self.params["cross_validation"]:
            param_grid = [{'kernel': ['linear', 'poly', 'rbf', 'sigmoid'], 'C': [0.000000001, 1, 5, 10]}]
            self.model = sklearn_GridSearchCV(sklearn_svm.SVC(probability=True), param_grid, cv=k_folds,
                                              scoring=supervised_classification_metrics.get_scorer_f_score_multiclass())
        else:
            if self.params["kernel"] == "linear":
                self.model = sklearn_svm.LinearSVC(C=self.params["c"])
            else:
                self.model = sklearn_svm.SVC(kernel=self.params["kernel"], C=self.params["c"], probability=True)

        self.model.fit(x_train, y_train)
        if self.params["cross_validation"]:
            self.params = SupervisedClassification.parse_best_params_cv(self.model.best_params_, split_string=False)

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                if hasattr(self.model, "classes_"):
                    classes = self.model.classes_
                else:
                    classes = self.model.best_estimator_.estimators_[0].classes_

                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=classes)
        except Exception as e:
            pass

        return y_pred_proba_df

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      self.y_training_val_pred_proba,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_metrics(y_test, self.y_test_pred,
                                                                                              self.y_test_pred_proba,
                                                                                              training_time)
