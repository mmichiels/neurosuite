from .SupervisedClassification import *
from .SupervisedClassificationModel import *
from sklearn.neural_network import MLPClassifier as sklearn_MLPClassifier

class NeuralNetwork():
    def __init__(self, algorithm_parameters):
        self.name = "neuralNetwork"
        self.model = None
        self.params = {}
        self.params["cross_validation"] = algorithm_parameters["neuralNetwork_cross_validation"]
        self.params["num_hidden_layers"] = int(algorithm_parameters["neuralNetwork_num_hidden_layers"])
        self.params["size_hidden_layers"] = int(algorithm_parameters["neuralNetwork_size_hidden_layers"])
        self.params["activation_function"] = algorithm_parameters["neuralNetwork_activation_function"]
        self.params["learning_rate"] = float(algorithm_parameters["neuralNetwork_learning_rate"])
        self.params["l2_penalty"] = float(algorithm_parameters["neuralNetwork_l2_penalty"])
        self.params["max_epochs"] = int(algorithm_parameters["neuralNetwork_max_epochs"])
        self.evaluation_metrics = {}

    def run(self, x_train, y_train, x_test, y_test, k_folds=5, backend="scikit-learn"):
        start_time = time.time()

        if backend == "scikit-learn":
            self.train_neuralNetwork_scikit_learn(x_train, y_train, k_folds)
            training_time = time.time() - start_time
        else:
            raise Exception("Backend {} is not supported".format(backend))

        self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_neuralNetwork_scikit_learn(self, x_train, y_train, k_folds):
        if self.params["cross_validation"]:
            hidden_layer_sizes = []
            num_hidden_layers = [3, 10] #np.arange(0, 5, 1)
            neurons_per_h_layer = [x_train.shape[1]+15, x_train.shape[1]+30] #np.arange(x_train.shape[1], (x_train.shape[1]*2)+1, 2)

            for num_layers in num_hidden_layers:
                for neurons_per_layer in neurons_per_h_layer:
                    hidden_layer_size = (neurons_per_layer,) * num_layers
                    hidden_layer_sizes.append(hidden_layer_size)

            param_grid = [{'hidden_layer_sizes': hidden_layer_sizes,
                           'activation': [self.params["activation_function"]], #['relu', 'logistic', 'tanh'],
                           'learning_rate_init': [self.params["learning_rate"]], #np.arange(0.001, 0.006, 0.003),
                           'alpha': [self.params["l2_penalty"]], #np.arange(0.0001, 0.0011, 0.0005),
                           'max_iter': [self.params["max_epochs"]], #np.arange(50, 2001, 500),
                           'solver': ['adam'],
                           }]
            self.model = sklearn_GridSearchCV(sklearn_MLPClassifier(), param_grid, cv=k_folds,
                                              scoring=supervised_classification_metrics.get_scorer_f_score_multiclass())
        else:
            self.params["size_hidden_layers"] += x_train.shape[1]
            hidden_layer_sizes = (self.params["size_hidden_layers"],) * self.params["num_hidden_layers"]
            self.model = sklearn_MLPClassifier(hidden_layer_sizes=hidden_layer_sizes, activation=self.params["activation_function"],
                                               solver="adam", alpha=self.params["l2_penalty"], learning_rate_init=self.params["learning_rate"],
                                               max_iter=self.params["max_epochs"])

        # Cross val (if not input parameters are received)
        self.model.fit(x_train, y_train)
        if self.params["cross_validation"]:
            self.params["num_hidden_layers"] = len(self.model.best_params_["hidden_layer_sizes"])
            self.params["size_hidden_layers"] = self.model.best_params_["hidden_layer_sizes"][0]
            self.params["activation_function"] = self.model.best_params_["activation"]
            self.params["learning_rate"] = self.model.best_params_["learning_rate_init"]
            self.params["l2_penalty"] = self.model.best_params_["alpha"]
            self.params["max_epochs"] = self.model.best_params_["max_iter"]

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def predict_proba(self, x_test):
        # Predict probabilities
        y_pred_proba_df = None

        try:
            if x_test.shape[0] > 0:
                y_pred_proba_vals = self.model.predict_proba(x_test)
                if hasattr(self.model, "classes_"):
                    classes = self.model.classes_
                else:
                    classes = self.model.best_estimator_.estimators_[0].classes_

                y_pred_proba_df = pd.DataFrame(y_pred_proba_vals, columns=classes)
        except Exception as e:
            pass

        return y_pred_proba_df

    def evaluate(self, training_time, x_test, x_train, y_test, y_train):
        self.y_training_val_pred = self.predict(x_train)
        self.y_training_val_pred_proba = self.predict_proba(x_train)
        self.y_test_pred = self.predict(x_test)
        self.y_test_pred_proba = self.predict_proba(x_test)
        self.evaluation_metrics["training_val"] = supervised_classification_metrics.calculate_metrics(y_train,
                                                                                                      self.y_training_val_pred,
                                                                                                      self.y_training_val_pred_proba,
                                                                                                      training_time)
        self.evaluation_metrics["test"] = supervised_classification_metrics.calculate_metrics(y_test, self.y_test_pred,
                                                                                              self.y_test_pred_proba,
                                                                                              training_time)
