import django
import sklearn.metrics as sklearn_metrics
import numpy as np
import pandas as pd
import math

# https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html#sphx-glr-auto-examples-classification-plot-classifier-comparison-py
# Add also time comparisons


def f_score_multioutput_multiclass(y_true, y_pred):
    num_classes = y_true.shape[1]
    f_score_vals = []

    for i in range(num_classes):
        y_true_i = np.array(y_true.iloc[:, i])
        y_pred_i = y_pred[:, i]
        precision_recall_fscore_support = sklearn_metrics.precision_recall_fscore_support(y_true_i, y_pred_i)
        fscore = precision_recall_fscore_support[2]*100
        f_score_vals.append(fscore)

    f_score_mean_in_class_feature = [np.mean(f_score_vals[i]) for i in range(num_classes)]
    f_score_mean = np.mean(f_score_mean_in_class_feature)

    return f_score_mean

def f_score_multiclass(y_true, y_pred):
    y_true = np.array(y_true.iloc[:, 0])

    precision_recall_fscore_support = sklearn_metrics.precision_recall_fscore_support(y_true, y_pred)
    fscore = precision_recall_fscore_support[2]*100
    fscore_in_class_mean = np.mean(fscore)

    return fscore_in_class_mean

def get_scorer_f_score_multiclass():
    scorer_f_score_multioutput_multiclass = sklearn_metrics.make_scorer(f_score_multiclass)

    return scorer_f_score_multioutput_multiclass

def get_scorer_f_score_multioutput_multiclass():
    scorer_f_score_multioutput_multiclass = sklearn_metrics.make_scorer(f_score_multioutput_multiclass)

    return scorer_f_score_multioutput_multiclass


def brier_score_multiclass(true, predictions_probs):
    if predictions_probs is None:
        return np.nan

    classes = list(predictions_probs.columns)
    num_classes = len(classes)
    predictions_probs_vals = predictions_probs.values

    true_probs = []
    for instance_num in range(predictions_probs_vals.shape[0]):
        true_class_instance = true[instance_num]
        true_class_idx = classes.index(true_class_instance)

        true_instance_probs = np.zeros(num_classes)
        true_instance_probs[true_class_idx] = 1
        true_probs.append(true_instance_probs)

    true_probs = np.array(true_probs)

    # Code from https://stats.stackexchange.com/questions/403544/how-to-compute-the-brier-score-for-more-than-two-classes
    return np.mean(np.sum((predictions_probs - true_probs)**2, axis=1))

def calculate_metrics(y_true, y_pred, y_pred_proba, training_time):
    # Every metric is multiclass (1 class with multiple possible values)
    # The for loops just applies the multioutput meaning (Many classes)

    metrics = {}
    metrics["training_time"] = {"mean": training_time}
    metrics["confusion_matrix"] = {"values": []}
    metrics["accuracy_score"] = {"mean_in_class_feature": []}
    metrics["precision"] = {"values": []}
    metrics["recall"] = {"values": []}
    metrics["fscore"] = {"values": []}
    metrics["fscore_weighted"] = {"mean_in_class_feature": []}
    metrics["brier_score"] = {"mean_in_class_feature": []}
    num_classes = y_true.shape[1]

    y_true = np.array(y_true.iloc[:, 0])
    confusion_matrix = sklearn_metrics.confusion_matrix(y_true, y_pred)
    accuracy = sklearn_metrics.accuracy_score(y_true, y_pred)*100
    precision_recall_fscore_support = sklearn_metrics.precision_recall_fscore_support(y_true, y_pred)
    precision = precision_recall_fscore_support[0]*100
    recall = precision_recall_fscore_support[1]*100
    fscore = precision_recall_fscore_support[2]*100
    fscore_weighted = sklearn_metrics.f1_score(y_true, y_pred, average="weighted")*100
    brier_score = brier_score_multiclass(y_true, y_pred_proba)

    metrics["confusion_matrix"]["values"].append(confusion_matrix)
    metrics["accuracy_score"]["mean_in_class_feature"].append(accuracy)
    metrics["precision"]["values"].append(precision)
    metrics["recall"]["values"].append(recall)
    metrics["fscore"]["values"].append(fscore)
    metrics["fscore_weighted"]["mean_in_class_feature"].append(fscore_weighted)
    metrics["brier_score"]["mean_in_class_feature"].append(brier_score)

    metrics["fscore"]["mean_in_class_feature"] = [np.mean(metrics["fscore"]["values"][i]) for i in range(num_classes)]
    metrics["precision"]["mean_in_class_feature"] = [np.mean(metrics["precision"]["values"][i]) for i in range(num_classes)]
    metrics["recall"]["mean_in_class_feature"] = [np.mean(metrics["recall"]["values"][i]) for i in range(num_classes)]

    return metrics

def calculate_multioutput_metrics(y_true, y_pred, training_time):
    # Every metric is multiclass (1 class with multiple possible values)

    metrics = {}
    metrics["training_time"] = {"mean": training_time}
    metrics["hamming_loss"] = {"values": []}
    metrics["accuracy_score"] = {"mean_in_class_feature": []}
    metrics["precision"] = {"values": []}
    metrics["recall"] = {"values": []}
    metrics["fscore"] = {"values": []}
    metrics["jaccard_similarity"] = {"values": []}


    y_true = y_true.astype('int')
    y_pred = y_pred.astype('int')

    hammning_loss = sklearn_metrics.hamming_loss(y_true, y_pred)
    accuracy_score = sklearn_metrics.accuracy_score(y_true, y_pred)
    precision = sklearn_metrics.precision_score(y_true, y_pred, average="weighted")
    recall = sklearn_metrics.recall_score(y_true, y_pred, average="weighted")
    fscore = sklearn_metrics.f1_score(y_true, y_pred, average="weighted")
    jaccard_similarity = sklearn_metrics.jaccard_similarity_score(y_true, y_pred)

    metrics["accuracy_score"]["mean"] = accuracy_score
    metrics["hamming_loss"]["mean"] = hammning_loss
    metrics["accuracy_score"]["mean"] = accuracy_score
    metrics["fscore"]["mean"] = fscore
    metrics["precision"]["mean"] = precision
    metrics["recall"]["mean"] = recall
    metrics["jaccard_similarity"]["mean"] = jaccard_similarity
    return metrics

def metric_str_to_verbose(metric_name):
    metric_to_verbose = {
        "training_time": "Training time (s)",
        "accuracy_score": "Accuracy",
        "fscore": "F-score",
        "fscore_weighted": "F-score weighted",
        "precision": "Precision",
        "recall": "Recall",
        "brier_score": "Brier score",
        "jaccard_similarity": "Jaccard_similarity",
        "hamming_loss": "Hamming loss"
    }
    metric_verbose_name = metric_to_verbose[metric_name]

    return metric_verbose_name

def create_all_df_metrics(supervised_classification):
    summary_evaluation = {}
    summary_evaluation["training_val"] = create_df_metrics(supervised_classification, set_name="training_val")
    summary_evaluation["test"] = create_df_metrics(supervised_classification, set_name="test")

    return summary_evaluation

def create_all_df_metrics_multioutput(supervised_classification):
    summary_evaluation = {}
    summary_evaluation["training_val"] = create_df_metrics_multioutput(supervised_classification, set_name="training_val")
    summary_evaluation["test"] = create_df_metrics_multioutput(supervised_classification, set_name="test")

    return summary_evaluation

def create_df_metrics(supervised_classification, set_name="test"):
    metrics = {}
    columns = ["Model", "Hyperparameters", "Download predictions (CSV)", "Download predictions (Parquet gzip)", "Download wrong predictions (CSV)", "Download wrong predictions (Parquet gzip)", "Model explanation", "Training time (s)"]

    for feature_class in supervised_classification.features_classes:
        columns_class = ["Accuracy ({})".format(feature_class), "F-score ({})".format(feature_class),
                         "Precision ({})".format(feature_class), "Recall ({})".format(feature_class),
                         "Brier score ({})".format(feature_class)]
        columns += columns_class

    for model_name, model in supervised_classification.models.items():
        if np.isnan(model.evaluation_metrics[set_name]["accuracy_score"]["mean_in_class_feature"][0]):
            continue

        metrics[model_name] = {}
        metrics[model_name]["Hyperparameters"] = model.params
        metrics[model_name]["Download predictions (CSV)"] = '<form action="/morpho/ml_supervised_class_download_predictions" method="post">' \
                                                                      '<input type="hidden" class="form-empty-csrfmiddlewaretoken" name="csrfmiddlewaretoken" value="">' \
                                                                      '<input type="hidden" name="algorithm" value="' + model_name + '">' \
                                                                      '<input type="hidden" name="set" value="' + set_name + '">' \
                                                                      '<input type="hidden" name="format" value="csv">' \
                                                                      '<button type="submit" class="btn btn-primary">CSV</button>' \
                                                                      '</form>'
        metrics[model_name]["Download predictions (Parquet gzip)"] = '<form action="/morpho/ml_supervised_class_download_predictions" method="post">' \
                                                                      '<input type="hidden" class="form-empty-csrfmiddlewaretoken" name="csrfmiddlewaretoken" value="">' \
                                                                      '<input type="hidden" name="algorithm" value="' + model_name + '">' \
                                                                      '<input type="hidden" name="set" value="' + set_name + '">' \
                                                                      '<input type="hidden" name="format" value="parquet">' \
                                                                      '<button type="submit" class="btn btn-primary">Parquet gzip</button>' \
                                                                      '</form>'
        metrics[model_name]["Download wrong predictions (CSV)"] = '<form action="/morpho/ml_supervised_class_download_wrong_predictions" method="post">' \
                                                                      '<input type="hidden" class="form-empty-csrfmiddlewaretoken" name="csrfmiddlewaretoken" value="">' \
                                                                      '<input type="hidden" name="algorithm" value="' + model_name + '">' \
                                                                      '<input type="hidden" name="set" value="' + set_name + '">' \
                                                                      '<input type="hidden" name="format" value="csv">' \
                                                                      '<button type="submit" class="btn btn-primary">CSV</button>' \
                                                                      '</form>'
        metrics[model_name]["Download wrong predictions (Parquet gzip)"] = '<form action="/morpho/ml_supervised_class_download_wrong_predictions" method="post">' \
                                                                      '<input type="hidden" class="form-empty-csrfmiddlewaretoken" name="csrfmiddlewaretoken" value="">' \
                                                                      '<input type="hidden" name="algorithm" value="' + model_name + '">' \
                                                                      '<input type="hidden" name="set" value="' + set_name + '">' \
                                                                      '<input type="hidden" name="format" value="parquet">' \
                                                                      '<button type="submit" class="btn btn-primary">Parquet gzip</button>' \
                                                                      '</form>'

        metrics[model_name]["Model explanation"] = '<button type="submit" class="btn btn-primary model-explanation-button" algorithm="' + model_name + '" set="' + set_name + '">View</button>'


        model_metrics = model.evaluation_metrics[set_name]
        metrics[model_name]["Training time (s)"] = model_metrics["training_time"]["mean"]
        for metric, vals in model_metrics.items():
            if "mean_in_class_feature" in vals:
                for j, feature_class in enumerate(supervised_classification.features_classes):
                    metric_class_name = metric_str_to_verbose(metric) + " ({})".format(feature_class)
                    metrics[model_name][metric_class_name] = vals["mean_in_class_feature"][j]

    df = pd.DataFrame.from_dict(metrics, orient="index")#.T
    if df.shape[0] > 0:
        df[columns[0]] = df.index # To put the index as a column
        df = df[columns] # To reorder the columns
        df.reset_index(drop=True, inplace=True)
        #pd.options.display.precision = 3
        #pd.set_option('precision', 3)
        #pd.set_option('display.float_format', lambda x: '%.3f' % x)
        pd.options.display.float_format = '{:.3f}'.format
        #df.round(3)

    return df

def create_df_metrics_multioutput(supervised_classification, set_name="test"):
    metrics = {}
    columns = ["Model",
               "Hyperparameters",
               "Download predictions (CSV)",
               "Download predictions (Parquet gzip)",
               "Model explanation",
               "Training time (s) (mean)",
               "Accuracy (mean)",
               "Hamming loss (mean)",
               "F-score (mean)",
               "Precision (mean)",
               "Recall (mean)",
               "Jaccard_similarity (mean)"]

    #for feature_class in supervised_classification.features_classes:
    #    columns_class = ["Accuracy ({})".format(feature_class), "F-score ({})".format(feature_class),
    #                     "Precision ({})".format(feature_class), "Recall ({})".format(feature_class)]
    #    columns += columns_class

    for model_name, model in supervised_classification.models.items():
        metrics[model_name] = {}
        metrics[model_name]["Hyperparameters"] = model.params
        metrics[model_name]["Download predictions (CSV)"] = '<form action="/morpho/ml_supervised_class_download_predictions" method="post">' \
                                                                      '<input type="hidden" class="form-empty-csrfmiddlewaretoken" name="csrfmiddlewaretoken" value="">' \
                                                                      '<input type="hidden" name="algorithm" value="' + model_name + '">' \
                                                                      '<input type="hidden" name="set" value="' + set_name + '">' \
                                                                      '<input type="hidden" name="format" value="csv">' \
                                                                      '<button type="submit" class="btn btn-primary">CSV</button>' \
                                                                      '</form>'
        metrics[model_name]["Download predictions (Parquet gzip)"] = '<form action="/morpho/ml_supervised_class_download_predictions" method="post">' \
                                                                      '<input type="hidden" class="form-empty-csrfmiddlewaretoken" name="csrfmiddlewaretoken" value="">' \
                                                                      '<input type="hidden" name="algorithm" value="' + model_name + '">' \
                                                                      '<input type="hidden" name="set" value="' + set_name + '">' \
                                                                      '<input type="hidden" name="format" value="parquet">' \
                                                                      '<button type="submit" class="btn btn-primary">Parquet gzip</button>' \
                                                                      '</form>'

        metrics[model_name]["Model explanation"] = '<button type="submit" class="btn btn-primary model-explanation-button" algorithm="' + model_name + '" set="' + set_name + '">View</button>'

        model_metrics = model.evaluation_metrics[set_name]
        for metric, vals in model_metrics.items():
            if "mean" in vals:
                metric_mean_name = metric_str_to_verbose(metric) + " (mean)"
                metrics[model_name][metric_mean_name] = vals["mean"]

    df = pd.DataFrame.from_dict(metrics, orient="index")#.T
    df[columns[0]] = df.index # To put the index as a column
    df = df[columns] # To reorder the columns
    df.reset_index(drop=True, inplace=True)
    #pd.options.display.precision = 3
    #pd.set_option('precision', 3)
    #pd.set_option('display.float_format', lambda x: '%.3f' % x)
    pd.options.display.float_format = '{:.3f}'.format
    #df.round(3)

    return df