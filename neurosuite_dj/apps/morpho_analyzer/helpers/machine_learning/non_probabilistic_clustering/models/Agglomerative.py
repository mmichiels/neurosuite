from .NonProbabilisticClustering import *
from sklearn.cluster import AgglomerativeClustering as sklearn_agglomerative
import numpy as np
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
from collections import Counter
import json
from io import BytesIO
import base64

class Agglomerative():
    def __init__(self, algorithm_parameters):
        self.name = "agglomerative"
        self.model = None
        self.dendrogram = ""
        self.clusters = ""
        self.max_d = 0
        self.evaluation_metrics = {}
        self.params = {}
        self.params["k"] = int(algorithm_parameters["agglomerative_k"])
        self.params["linkage"] = algorithm_parameters["agglomerative_linkage"]
        self.params["distance"] = algorithm_parameters["agglomerative_distance"]
        self.params["backend"] = algorithm_parameters["backend"]
        self.params["criteria"] = algorithm_parameters["agglomerative_criteria"]
        if(self.params["criteria"]) == "distance":
            self.params["max_d"] = float(algorithm_parameters["agglomerative_max_d"])

    def run(self, dataframe, backend="scikit-learn"):
        if backend == "scikit-learn":
            self.train_agglomerative_scikit_learn(dataframe)
        elif backend == "scipy":
            self.train_agglomerative_scipy(dataframe)
        else:
            raise Exception("Backend {} is not supported".format(backend))

        #self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_agglomerative_scikit_learn(self, dataframe):
        self.model = sklearn_agglomerative(n_clusters=self.params["k"], affinity=self.params["distance"], linkage=self.params["linkage"])
        self.model.fit(dataframe)
        self.dendrogram = self.plot_sklearn_dendrogram(self.model, dataframe)


    def train_agglomerative_scipy(self, dataframe):
        if self.params["distance"] == "manhattan":
            self.params["distance"] = "cityblock"
        self.model = linkage(y = dataframe, metric=self.params["distance"], method=self.params["linkage"])

        if self.params["criteria"] == "distance":
            self.dendrogram = self.plot_scipy_dendrogram( self.model , self.params["max_d"])
            self.clusters = fcluster(self.model, self.params["max_d"], criterion='distance')
        else:
            self.dendrogram = self.plot_scipy_dendrogram( self.model )
            if self.params["criteria"] == "k":
                self.clusters = fcluster(self.model, self.params["k"], criterion='maxclust')
            elif self.params["criteria"] == "auto":
                self.clusters = fcluster(self.model, t = 1.1)

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def get_clusters(self):
        return self.clusters

    def summary(self):
        summary = {}
        max_d = 50
        #clusters = fcluster(self.model, max_d, criterion='distance')
        if self.params["backend"] == "scikit-learn":
            summary["clusters"] = json.dumps({str(k): int(v) for k, v in Counter(self.model.labels_).items()})
        elif self.params["backend"] == "scipy":
            labels = self.clusters
            summary["clusters"] = json.dumps({str(k): int(v) for k, v in Counter(labels).items()})
            clusters_dict = {}
            size = len(labels)
            clusters = {int(k): int(v) for k, v in Counter(labels).items()}
            for key in clusters:
                clusters_dict[str(key)] = [clusters[key], round(float(clusters[key]) * 100 / size, 1)]
            summary["clusters"] = json.dumps(clusters_dict)

        summary["elbow_method_plot"] = self.elbow_method()
        summary["dendrogram"] = self.dendrogram
        return summary

    def plot_sklearn_dendrogram(self, model, dataframe, **kwargs):
        # Children of hierarchical clustering
        children = model.children_

        # Distances between each pair of children
        # Since we don't have this information, we can use a uniform one for plotting
        distance = np.arange(children.shape[0])

        # The number of observations contained in each cluster level
        no_of_observations = np.arange(2, children.shape[0] + 2)

        # Create linkage matrix and then plot the dendrogram
        linkage_matrix = np.column_stack([children, distance, no_of_observations]).astype(float)

        # Plot the corresponding dendrogram
        fig = plt.figure(figsize=(16, 8))
        plt.title('Hierarchical Clustering Dendrogram')
        plt.xlabel('sample index')
        plt.ylabel('distance')
        self.fancy_dendrogram(linkage_matrix,
                              p=16,
                              leaf_rotation=90.,
                              leaf_font_size=12.,
                              show_contracted=True,
                              annotate_above=10
                              )
        figdata = BytesIO()
        fig.savefig(figdata, format='png')
        figdata.seek(0)
        plot = base64.b64encode(figdata.read()).decode("ascii")
        return plot

    def plot_scipy_dendrogram(self, model, max_d = 0, **kwargs):
        fig = plt.figure(figsize=(16, 8))
        plt.title('Hierarchical Clustering Dendrogram')
        plt.xlabel('sample index')
        plt.ylabel('distance')

        self.fancy_dendrogram(
            model,
            truncate_mode='lastp',
            p=16,
            leaf_rotation=90.,
            leaf_font_size=12.,
            show_contracted=True,
            annotate_above=10,
            max_d=max_d
        )
        figdata = BytesIO()
        fig.savefig(figdata, format='png')
        figdata.seek(0)
        plot = base64.b64encode(figdata.read()).decode("ascii")
        return plot

    def fancy_dendrogram(self, *args, **kwargs):
        max_d = kwargs.pop('max_d', None)
        if max_d and 'color_threshold' not in kwargs:
            kwargs['color_threshold'] = max_d
        annotate_above = kwargs.pop('annotate_above', 0)

        ddata = dendrogram(*args, **kwargs)

        if not kwargs.get('no_plot', False):
            plt.title('Hierarchical clustering dendrogram (truncated)')
            plt.xlabel('Sample index or (cluster size)')
            plt.ylabel('distance')
            for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
                x = 0.5 * sum(i[1:3])
                y = d[1]
                if y > annotate_above:
                    plt.plot(x, y, 'o', c=c)
                    plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
                                 textcoords='offset points',
                                 va='top', ha='center')
            if max_d:
                plt.axhline(y=max_d, c='k')
        return ddata

    def elbow_method(self):
        fig = plt.figure(figsize=(8, 4))
        last = self.model[-10:, 2]
        last_rev = last[::-1]
        idxs = np.arange(1, len(last) + 1)
        plt.plot(idxs, last_rev)
        plt.xlabel('k')
        plt.ylabel('Acceleration of distance growth')
        plt.title('Elbow method')

        acceleration = np.diff(last, 2)  # 2nd derivative of the distances
        acceleration_rev = acceleration[::-1]
        plt.plot(idxs[:-2] + 1, acceleration_rev)
        figdata = BytesIO()
        fig.savefig(figdata, format='png')
        figdata.seek(0)
        plot = base64.b64encode(figdata.read()).decode("ascii")
        k = acceleration_rev.argmax() + 2  # if idx 0 is the max of this we want 2 clusters
        elbow_method = {}
        elbow_method['plot'] = plot
        elbow_method['k'] = k
        return plot

    def get_fitted_dataframe(self):
        temp_X = self.X
        temp_X['cluster'] = self.model.labels_
        return temp_X
