from .NonProbabilisticClustering import *
from sklearn.cluster import SpectralClustering as sklearn_spectral
import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial.distance import cdist
from collections import Counter
import json
from io import BytesIO
import base64

class Spectral():
    def __init__(self, algorithm_parameters):
        self.name = "Spectral"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.X = {}
        self.params["clusters_selection"] = algorithm_parameters["spectral_clusters_selection"]
        self.params["k"] = int(algorithm_parameters["spectral_k"])

    def run(self, dataframe, backend="scikit-learn"):
        self.X = dataframe
        if backend == "scikit-learn":
            self.train_spectral_scikit_learn(dataframe)
        else:
            raise Exception("Backend {} is not supported".format(backend))
        #self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_spectral_scikit_learn(self, dataframe):
        if self.params["clusters_selection"] == "manual":
            self.model = sklearn_spectral(n_clusters= self.params["k"], affinity="nearest_neighbors", random_state=0)
        else:
            self.model = sklearn_spectral( affinity="nearest_neighbors", random_state=0)
        self.model.fit(dataframe)
        self.model

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def summary(self):
        summary = {}
        clusters_dict = {}
        size = len(self.model.labels_)
        clusters = {int(k):int(v) for k,v in Counter(self.model.labels_).items()}
        for key in clusters:
            clusters_dict[str(key + 1)] = [clusters[key], round(float(clusters[key]) * 100 / size, 1)]

        summary["clusters"] = json.dumps( clusters_dict )

        #summary["elbow_method_plot"] = self.elbow_method(self.X)
        return summary

    def get_clusters(self):
        return [*map( lambda x: x + 1 , self.model.labels_)]





