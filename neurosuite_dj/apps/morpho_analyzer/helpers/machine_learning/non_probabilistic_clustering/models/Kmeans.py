from .NonProbabilisticClustering import *
from sklearn.cluster import KMeans as sklearn_kmeans
import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial.distance import cdist
from collections import Counter
import json
from io import BytesIO
import base64

class Kmeans():
    def __init__(self, algorithm_parameters):
        self.name = "knn"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.X = {}
        self.params["k"] = int(algorithm_parameters["kmeans_k"])

    def run(self, dataframe, backend="scikit-learn"):
        self.X = dataframe
        if backend == "scikit-learn":
            self.train_kmeans_scikit_learn(dataframe)
        else:
            raise Exception("Backend {} is not supported".format(backend))

        #self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_kmeans_scikit_learn(self, dataframe):
        self.model = sklearn_kmeans(n_clusters=self.params["k"], random_state=0)
        self.model.fit(dataframe)
        self.model

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def summary(self):
        summary = {}
        centroids = []
        for centroid in self.model.cluster_centers_.tolist():
            centroids.append( [ round(item, 4) for item in centroid ])

        summary["centroids"] = json.dumps( centroids )
        clusters_dict = {}
        size = len(self.model.labels_)
        clusters = {int(k): int(v) for k, v in Counter(self.model.labels_).items()}
        for key in clusters:
            clusters_dict[str(key + 1)] = [clusters[key], round(float(clusters[key]) * 100 / size, 1)]

        summary["clusters"] = json.dumps(clusters_dict)
        summary["elbow_method_plot"] = self.elbow_method(self.X)
        return summary

    def get_clusters(self):
        return [*map( lambda x: x + 1 , self.model.labels_)]

    def elbow_method(self, X):
        fig = plt.figure(figsize=(8, 4))

        distortions = []
        K = range(1, 10)
        for k in K:
            kmeanModel = sklearn_kmeans(n_clusters=k).fit(self.X)
            kmeanModel.fit(X)
            distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])

        # Plot the elbow
        plt.plot(K, distortions, 'bx-')
        plt.xlabel('k')
        plt.ylabel('Acceleration of distance growth')
        plt.title('Elbow method')

        figdata = BytesIO()
        fig.savefig(figdata, format='png')
        figdata.seek(0)
        plot = base64.b64encode(figdata.read()).decode("ascii")

        elbow_method = {}
        elbow_method['plot'] = plot

        return plot




