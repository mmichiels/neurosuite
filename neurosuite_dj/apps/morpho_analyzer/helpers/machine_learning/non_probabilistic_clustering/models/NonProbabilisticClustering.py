import secrets
import neurosuite_dj.helpers_global.dataset as dataset_helper
import neurosuite_dj.helpers_global.datasets_user as datasets_user_helper
from django.conf import settings
from sklearn.decomposition import PCA
from apps.morpho_analyzer.helpers.machine_learning import machine_learning_helpers
import pandas as pd
import numpy as np

class NonProbabilisticClustering:

    def __init__(self, name="non_probabilistic_clustering", dataframe=[], id_column="", features_classes=[], session_id=None):
        self.name = name
        self.set_session_id(session_id)
        self.features_classes = features_classes
        self.id_column = id_column
        self.dataframe = dataframe.loc[:, dataframe.columns != id_column]
        self.model = {}
        self.new_prediction_dataset = None

    def set_session_id(self, session_id):
        if session_id is None:
            session_id = secrets.token_hex(16)
        self.session_id = session_id

    def add_model(self, model):
        self.model = model

    def get_features(self):
        return self.dataframe.columns.values

    def load_new_prediction_dataset(self, dataframe):
        features_x = list(set(dataframe.columns) - set([self.id_column]))
        self.new_prediction_dataset_ids = dataframe.loc[:, self.id_column]
        dataframe_new_prediction = dataframe.loc[:, features_x]
        self.new_prediction_dataset = dataset_helper.Dataset("ml_non_probabilistic_clustering_new_prediction_dataset", self.session_id,
                                              app_name=settings.MORPHO_ANALYZER_APP_NAME)
        self.new_prediction_dataset.load(dataframe_new_prediction)

    def get_new_prediction_dataframe(self):
        new_prediction_dataframe = self.new_prediction_dataset.get_dataframe()

        return new_prediction_dataframe, self.new_prediction_dataset_ids

    def get_learning_class(self, algorithm_name, algorithm_parameters):
        from .Kmeans import Kmeans
        from .Agglomerative import Agglomerative
        from .Dbscan import Dbscan
        from .Affinity import Affinity
        from .Spectral import Spectral
        from .Birch import Birch
        from .Optics import Optics

        algorithm_class = {}

        if algorithm_name == "k-means":
            algorithm_class = Kmeans(algorithm_parameters)
        elif algorithm_name == "agglomerative":
            algorithm_class = Agglomerative(algorithm_parameters)
        elif algorithm_name == "dbscan":
            algorithm_class = Dbscan(algorithm_parameters)
        elif algorithm_name == "affinity":
            algorithm_class = Affinity(algorithm_parameters)
        elif algorithm_name == "spectral":
            algorithm_class = Spectral(algorithm_parameters)
        elif algorithm_name == "birch":
            algorithm_class = Birch(algorithm_parameters)
        elif algorithm_name == "optics":
            algorithm_class = Optics(algorithm_parameters)


        return algorithm_class

    def get_summary(self):
        summary =  self.model.summary()
        summary['plot'] = self.get_dimensionality_reduction_plot()
        return summary

    def get_dimensionality_reduction_plot(self):
        clusters = self.model.get_clusters()
        transformer = PCA(n_components=2, random_state=0)
        x = transformer.fit_transform(self.dataframe)
        X = pd.DataFrame(x)
        X.rename(columns=lambda x: str(x), inplace=True)
        X['cluster'] = clusters
        plot = machine_learning_helpers.plot_dimentionality_reduction_data(X,'cluster',16,8)
        return plot

    def get_clusters(self):
        temp_dataframe = self.dataframe
        temp_dataframe['cluster'] = self.model.get_clusters()
        return temp_dataframe





