from .NonProbabilisticClustering import *
from sklearn.cluster import Birch as sklearn_birch
import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial.distance import cdist
from collections import Counter
import json
from io import BytesIO
import base64

class Birch():
    def __init__(self, algorithm_parameters):
        self.name = "Birch"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.X = {}
        self.params["n_clusters"] = int(algorithm_parameters["birch_k"])
        self.params["branching_factor"] = int(algorithm_parameters["birch_branching_factor"])
        self.params["threshold"] = float(algorithm_parameters["birch_threshold"])

    def run(self, dataframe, backend="scikit-learn"):
        self.X = dataframe
        if backend == "scikit-learn":
            self.train_birch_scikit_learn(dataframe)
        else:
            raise Exception("Backend {} is not supported".format(backend))
        #self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_birch_scikit_learn(self, dataframe):
        self.model = sklearn_birch(threshold=self.params["threshold"], n_clusters=self.params["n_clusters"], branching_factor=self.params["branching_factor"])
        self.model.fit(dataframe)
        self.model

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def summary(self):
        summary = {}
        clusters_dict = {}
        size = len(self.model.labels_)
        clusters = {int(k): int(v) for k, v in Counter(self.model.labels_).items()}
        for key in clusters:
            clusters_dict[str(key + 1)] = [clusters[key], round(float(clusters[key]) * 100 / size, 1)]

        summary["clusters"] = json.dumps(clusters_dict)
        return summary

    def get_clusters(self):
        return [*map( lambda x: x + 1 , self.model.labels_)]





