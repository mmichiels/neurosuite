from .NonProbabilisticClustering import *
from sklearn.cluster import OPTICS as sklearn_optics
import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial.distance import cdist
from collections import Counter
import json
from io import BytesIO
import base64

class Optics():
    def __init__(self, algorithm_parameters):
        self.name = "Optics"
        self.model = None
        self.evaluation_metrics = {}
        self.params = {}
        self.X = {}
        self.params["min_samples"] = int(algorithm_parameters["optics_min_samples"])
        self.params["metric"] = algorithm_parameters["optics_metric"]

    def run(self, dataframe, backend="scikit-learn"):
        self.X = dataframe
        if backend == "scikit-learn":
            self.train_optics_scikit_learn(dataframe)
        else:
            raise Exception("Backend {} is not supported".format(backend))
        #self.evaluate(training_time, x_test, x_train, y_test, y_train)

    def train_optics_scikit_learn(self, dataframe):
        self.model = sklearn_optics(metric=self.params["metric"], min_samples=self.params["min_samples"])
        self.model.fit(dataframe)
        self.model

    def predict(self, x_test):
        # Predict
        y_pred = []

        if x_test.shape[0] > 0:
            y_pred = self.model.predict(x_test)

        return y_pred

    def summary(self):
        summary = {}
        clusters_dict = {}
        size = len(self.model.labels_)
        clusters = {int(k): int(v) for k, v in Counter(self.model.labels_).items()}
        for key in clusters:
            clusters_dict[str(key + 2)] = [clusters[key], round(float(clusters[key]) * 100 / size, 1)]

        summary["clusters"] = json.dumps(clusters_dict)
        return summary

    def get_clusters(self):
        return [*map( lambda x: x + 1 , self.model.labels_)]





