from .base import *

class Tan(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, features_classes, states_names=None):
        super(Tan, self).__init__(data, data_type, states_names)
        self.features_classes = features_classes
        if len(self.features_classes) == 0:
            raise Exception("To run this classifier, you must supply one class feature in the previous section.")

    def run(self, backend="bnlearn"):
        nodes = list(self.data.columns.values)

        if backend == "neurosuites":
            model = self.run_tan_neurosuites(nodes)
        elif backend == "bnlearn":
            model = self.run_tan_bnlearn(nodes)

        return model

    def run_tan_bnlearn(self, nodes):
        dataframe, nodes = self.prepare_input_structure_bnlearn(nodes)

        bnlearn = importr("bnlearn")
        try:
            nodes.remove(self.features_classes[0])
        except Exception as e:
            pass #Class feature is not in the set of predictor features so no need to remove it from the predictor features set
        nodes = np.array(nodes)
        output_raw_R = bnlearn.tree_bayes(x = dataframe, training=self.features_classes[0], explanatory=nodes)

        graph = self.parse_output_structure_bnlearn(nodes, output_raw_R)

        return graph



    def run_tan_neurosuites(self, nodes):

        return 0
