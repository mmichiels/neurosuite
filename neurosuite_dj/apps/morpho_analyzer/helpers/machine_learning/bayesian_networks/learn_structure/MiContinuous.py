from .base import *
from sklearn.feature_selection import mutual_info_regression as sklearn_mi

class MiContinuous(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(MiContinuous, self).__init__(data, data_type, states_names)

    def run(self, backend="neurosuites"):
        if self.data_type != "continuous":
            raise Exception("Algorithm only supported for continuous datasets ")

        nodes_names = list(self.data.columns.values)

        if backend == "scikit-learn":
            graph = self.run_mi_continuous_scikit_learn(nodes_names)
        else:
            raise Exception("Backend {} is not supported.".format(backend))

        return graph



    def run_mi_continuous_scikit_learn(self, nodes_names):
        mi_matrix = []
        data_np = np.array(self.data)

        for i in range(self.data.shape[1]):
            y = data_np[:, i]
            mi_y = sklearn_mi(data_np, y)
            mi_matrix.append(mi_y)

        mi_matrix = np.array(mi_matrix)
        adj_matrix = np.array(mi_matrix)
        np.fill_diagonal(adj_matrix, 0)
        adj_matrix = np.triu(adj_matrix)

        graph = self.nx_graph_from_adj_matrix(adj_matrix, nodes_names)

        return graph
