from .base import *


class PC(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(PC, self).__init__(data, data_type, states_names)
        self.max_number_parents = int(algorithm_parameters["pc_max_number_parents"])
        self.max_adjacency_size = int(algorithm_parameters["pc_max_adjacency_size"])
        self.significance_level = float(algorithm_parameters["pc_significance_level"]) / 100

    def run(self, backend="bnlearn"):
        nodes = list(self.data.columns.values)

        if backend == "neurosuites":
            model = self.run_pc_neurosuites(nodes)
        elif backend == "bnlearn":
            model = self.run_pc_bnlearn(nodes)

        return model

    def run_pc_neurosuites(self, nodes):
        # 1st part PC (Create undirected Graph)
        skeleton, separating_sets = self.estimate_eskeleton(nodes)
        # 2nd part PC (Direct all graph edges to create a DAG)
        pdag = pgmpy_estimators.ConstraintBasedEstimator.skeleton_to_pdag(skeleton, separating_sets)
        dag = pgmpy_estimators.ConstraintBasedEstimator.pdag_to_dag(pdag)

        return dag

    def run_pc_bnlearn(self, nodes):
        dataframe, nodes = self.prepare_input_structure_bnlearn(nodes)

        bnlearn = importr("bnlearn")
        output_raw_R = bnlearn.pc_stable(x = dataframe, alpha = self.significance_level)

        graph = self.parse_output_structure_bnlearn(nodes, output_raw_R)
        #dag = pgmpy_estimators.ConstraintBasedEstimator.pdag_to_dag(graph)

        return graph

    def estimate_eskeleton(self, nodes):
        #Start with a complete undirected graph G on the set V of all vertices
        graph = nx.Graph(combinations(nodes, 2))
        #return graph, []

        lim_neighbors = 0
        separating_sets = dict()
        while not all([len(list(graph.neighbors(node))) < lim_neighbors for node in nodes]):
            for node in nodes:
                for neighbor in list(graph.neighbors(node)):
                    # search if there is a set of neighbors (of size lim_neighbors)
                    # that makes X and Y independent:
                    for separating_set in combinations(set(list(graph.neighbors(node))) - set([neighbor]), lim_neighbors):
                        if bn_utils.is_independent_chi_square_test(self, node, neighbor, separating_set):
                            separating_sets[frozenset((node, neighbor))] = separating_set
                            graph.remove_edge(node, neighbor)
                            break
            lim_neighbors += 1

        return graph, separating_sets

    def direct_edges(self, nodes):
        graph = DirectedGraph(nodes)

        return graph
