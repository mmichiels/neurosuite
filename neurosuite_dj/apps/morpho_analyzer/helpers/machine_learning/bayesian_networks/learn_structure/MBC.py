from .base import *

class MBC(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, features_classes, states_names=None):
        super(MBC, self).__init__(data, data_type, states_names)
        self.features_classes = features_classes
        if len(self.features_classes) == 0:
            raise Exception("To run this classifier, you must supply at least one class feature in the previous section.")

    def run(self, backend="bnlearn"):
        nodes = list(self.data.columns.values)

        if backend == "neurosuites":
            model = self.run_mbc_neurosuites(nodes)
        elif backend == "bnlearn":
            model = self.run_mbc_bnlearn(nodes)

        return model

    def run_mbc_bnlearn(self, nodes):
        dataframe, nodes = self.prepare_input_structure_bnlearn(nodes)

        bnlearn = importr("bnlearn")
        features = list(set(nodes) - set(self.features_classes))

        # Black list of arcs from features to classes
        blacklist = pd.DataFrame(columns=["from", "to"]) #Shape = (len(self.features_classes) * len(features), 2)
        blacklist["from"] = features * len(self.features_classes)
        blacklist["to"] = np.repeat(self.features_classes, [len(features)], axis=0)
        """
        bl < - matrix(nrow=length(classes) * length(features), ncol=2, dimnames=list(NULL, c("from", "to")))
        bl[, "from"] < - rep(features, each=length(classes))
        bl[, "to"] < - rep(classes, length(features))
        """
        # Learn MBC structure
        output_raw_R = bnlearn.hc(x = dataframe, blacklist=blacklist)

        graph = self.parse_output_structure_bnlearn(nodes, output_raw_R)

        return graph

    def run_mbc_neurosuites(self, nodes):

        return 0
