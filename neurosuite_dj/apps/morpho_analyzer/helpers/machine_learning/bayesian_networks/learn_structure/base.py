
import networkx as nx
import networkx.readwrite as networkx_io
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks.pgmpy import estimators as pgmpy_estimators

from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks.utils import bn_utils


from apps.morpho_analyzer.helpers import helpers
import neurosuite_dj.helpers_global.workers as helper_workers
import pyBN
import numpy as np
from itertools import combinations
import time
import pandas as pd
from celery import shared_task, current_task

import rpy2
from rpy2.rinterface import R_VERSION_BUILD
import rpy2.rinterface as rinterface
from rpy2.robjects import r
from rpy2.robjects.packages import importr
import datetime
import re
from rpy2.rinterface import RRuntimeWarning
from rpy2.robjects import pandas2ri
from rpy2.robjects import conversion as rpy2_conversion


class LearnStructure(object):
    """
    Base class for all LearningStructure classes.
    """
    def __init__(self, data, data_type, states_names=None):
        self.data = data
        self.data_type = data_type
        variables = list(data.columns.values)

        if not isinstance(states_names, dict):
            self.states_names = {var: self._collect_states_names(var) for var in variables}
        else:
            self.states_names = dict()
            for var in variables:
                if var in states_names:
                    if not set(self._collect_states_names(var)) <= set(states_names[var]):
                        raise ValueError("Data contains unexpected states for variable '{0}'.".format(str(var)))
                    self.states_names[var] = sorted(states_names[var])
                else:
                    self.states_names[var] = self._collect_states_names(var)

    def _collect_states_names(self, variable):
        "Return a list of states that the variable takes in the data"
        states = sorted(list(self.data.ix[:, variable].dropna().unique()))
        return states

    def parse_output_structure_bnlearn(self, nodes, output_raw_R):
        arcs_R = output_raw_R.rx2("arcs")
        edges_from = np.array(arcs_R.rx(True, 1))
        edges_to = np.array(arcs_R.rx(True, 2))
        edges = zip(edges_from, edges_to)
        graph = nx.DiGraph()
        graph.add_nodes_from(nodes)
        graph.add_edges_from(edges)
        return graph

    def prepare_input_structure_bnlearn(self, nodes):
        pandas2ri.activate()
        dataframe = self.data
        if self.data_type == "hybrid":
            raise Exception("This algorithm still does not support hybrid bayesian networks")

        return dataframe, nodes

    def nx_graph_from_adj_matrix(self, adj_matrix, nodes_names):
        nx_graph = nx.from_numpy_matrix(adj_matrix, create_using=nx.DiGraph)

        mapping = {}
        for i, node in enumerate(nx_graph.nodes()):
            mapping[node] = nodes_names[i]

        nx_graph = nx.relabel_nodes(nx_graph, mapping)

        return nx_graph


    @staticmethod
    def get_structure_learning_class(algorithm_name, algorithm_parameters, dataframe, data_type, features_classes, states_names=None, session_id=None):
        from .Pearson import Pearson
        from .MiContinuous import MiContinuous
        from .Lr import Lr
        from .Glasso import Glasso
        from .Genie import Genie
        from .PC import PC
        from .Gs import Gs
        from .Iamb import Iamb
        from .FastIamb import FastIamb
        from .InterIamb import InterIamb

        from .Hc import Hc
        from .HcTabu import HcTabu
        from .CL import CL

        from .MMHC import MMHC
        from .MMPC import MMPC
        from .HitonPC import HitonPC
        from .SparseBn import SparseBn
        from .FGES import FGES

        from .NB import NB
        from .Tan import Tan
        from .MBC import MBC


        algorithm_class = {}

        if algorithm_name == "pearson":
            algorithm_class = Pearson(dataframe, algorithm_parameters, data_type, states_names)
        if algorithm_name == "miContinuous":
            algorithm_class = MiContinuous(dataframe, algorithm_parameters, data_type, states_names)
        if algorithm_name == "lr":
            algorithm_class = Lr(dataframe, algorithm_parameters, data_type, states_names)
        if algorithm_name == "glasso":
            algorithm_class = Glasso(dataframe, algorithm_parameters, data_type, states_names)
        if algorithm_name == "genie3":
            algorithm_class = Genie(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "pc":
            algorithm_class = PC(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "gs":
            algorithm_class = Gs(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "iamb":
            algorithm_class = Iamb(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "fastIamb":
            algorithm_class = FastIamb(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "interIamb":
            algorithm_class = InterIamb(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "hc":
            algorithm_class = Hc(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "hcTabu":
            algorithm_class = HcTabu(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "cl":
            algorithm_class = CL(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "mmhc":
            algorithm_class = MMHC(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "mmpc":
            algorithm_class = MMPC(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "hitonPC":
            algorithm_class = HitonPC(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "sparseBn":
            algorithm_class = SparseBn(dataframe, algorithm_parameters, data_type, states_names)
        elif algorithm_name == "fges":
            algorithm_class = FGES(dataframe, algorithm_parameters, data_type, states_names, session_id)
        elif algorithm_name == "nb":
            algorithm_class = NB(dataframe, algorithm_parameters, data_type, features_classes, states_names)
        elif algorithm_name == "tan":
            algorithm_class = Tan(dataframe, algorithm_parameters, data_type, features_classes, states_names)
        elif algorithm_name == "mbc":
            algorithm_class = MBC(dataframe, algorithm_parameters, data_type, features_classes, states_names)

        return algorithm_class
