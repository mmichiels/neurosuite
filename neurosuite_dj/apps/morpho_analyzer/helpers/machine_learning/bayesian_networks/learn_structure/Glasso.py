from .base import *
from sklearn import covariance as sk_learn_cov

class Glasso(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(Glasso, self).__init__(data, data_type, states_names)
        self.alpha = float(algorithm_parameters["glasso_alpha"])
        self.tol = float(algorithm_parameters["glasso_tol"])
        self.max_iter = int(algorithm_parameters["glasso_max_iter"])

    def run(self, backend="scikit-learn"):
        if self.data_type != "continuous":
            raise Exception("Algorithm only supported for continuous datasets ")

        nodes_names = list(self.data.columns.values)

        if backend == "scikit-learn":
            graph = self.run_glasso_scikit_learn(nodes_names)
        else:
            raise Exception("Backend {} is not supported.".format(backend))

        return graph



    def run_glasso_scikit_learn(self, nodes_names):
        data_np = np.array(self.data)

        cov_matrix = np.cov(data_np.T)
        _, precision_matrix = sk_learn_cov.graphical_lasso(cov_matrix, alpha=self.alpha, tol=self.tol, max_iter=self.max_iter)
        adj_matrix = np.array(precision_matrix)
        np.fill_diagonal(adj_matrix, 0)
        adj_matrix = np.triu(adj_matrix)
        adj_matrix = np.square(adj_matrix)

        graph = self.nx_graph_from_adj_matrix(adj_matrix, nodes_names)

        return graph
