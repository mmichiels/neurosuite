from .base import *

class Pearson(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(Pearson, self).__init__(data, data_type, states_names)

    def run(self, backend="neurosuites"):
        if self.data_type != "continuous":
            raise Exception("Algorithm only supported for continuous datasets ")

        nodes_names = list(self.data.columns.values)

        if backend == "neurosuites":
            graph = self.run_pearson_neurosuites(nodes_names)
        else:
            raise Exception("Backend {} is not supported.".format(backend))

        return graph



    def run_pearson_neurosuites(self, nodes_names):
        corr_matrix = self.data.corr()

        adj_matrix = np.array(corr_matrix)
        np.fill_diagonal(adj_matrix, 0)
        adj_matrix = np.triu(adj_matrix)
        adj_matrix = np.square(adj_matrix)

        graph = self.nx_graph_from_adj_matrix(adj_matrix, nodes_names)

        return graph
