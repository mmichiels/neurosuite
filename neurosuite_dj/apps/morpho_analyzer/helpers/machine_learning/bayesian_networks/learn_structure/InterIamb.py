from .base import *

class InterIamb(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(InterIamb, self).__init__(data, data_type, states_names)
        self.significance_level = float(algorithm_parameters["interIamb_significance_level"]) / 100

    def run(self, backend="bnlearn"):
        nodes = list(self.data.columns.values)

        if backend == "neurosuites":
            model = self.run_interIamb_neurosuites(nodes)
        elif backend == "bnlearn":
            model = self.run_interIamb_bnlearn(nodes)

        return model

    def run_interIamb_bnlearn(self, nodes):
        dataframe, nodes = self.prepare_input_structure_bnlearn(nodes)

        bnlearn = importr("bnlearn")
        output_raw_R = bnlearn.inter_iamb(x = dataframe, alpha = self.significance_level)

        graph = self.parse_output_structure_bnlearn(nodes, output_raw_R)

        return graph



    def run_interIamb_neurosuites(self, nodes):

        return 0
