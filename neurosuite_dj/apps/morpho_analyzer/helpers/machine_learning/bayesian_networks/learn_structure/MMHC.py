from .base import *

class MMHC(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(MMHC, self).__init__(data, data_type, states_names)

    def run(self, backend="bnlearn"):
        nodes = list(self.data.columns.values)

        if backend == "neurosuites":
            model = self.run_mmhc_neurosuites(nodes)
        elif backend == "bnlearn":
            model = self.run_mmhc_bnlearn(nodes)

        return model

    def run_mmhc_bnlearn(self, nodes):
        dataframe, nodes = self.prepare_input_structure_bnlearn(nodes)

        bnlearn = importr("bnlearn")
        output_raw_R = bnlearn.mmhc(x = dataframe)

        graph = self.parse_output_structure_bnlearn(nodes, output_raw_R)

        return graph



    def run_mmhc_neurosuites(self, nodes):

        return 0
