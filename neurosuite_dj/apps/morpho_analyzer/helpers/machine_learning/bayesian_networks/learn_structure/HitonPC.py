from .base import *

class HitonPC(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(HitonPC, self).__init__(data, data_type, states_names)

    def run(self, backend="bnlearn"):
        nodes = list(self.data.columns.values)

        if backend == "neurosuites":
            model = self.run_hitonPC_neurosuites(nodes)
        elif backend == "bnlearn":
            model = self.run_hitonPC_bnlearn(nodes)

        return model

    def run_hitonPC_bnlearn(self, nodes):
        dataframe, nodes = self.prepare_input_structure_bnlearn(nodes)

        bnlearn = importr("bnlearn")
        output_raw_R = bnlearn.si_hiton_pc(x = dataframe)

        graph = self.parse_output_structure_bnlearn(nodes, output_raw_R)

        return graph



    def run_hitonPC_neurosuites(self, nodes):

        return 0
