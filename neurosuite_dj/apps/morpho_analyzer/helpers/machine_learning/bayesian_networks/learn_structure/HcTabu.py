from .base import *

class HcTabu(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(HcTabu, self).__init__(data, data_type, states_names)
        self.max_number_parents = algorithm_parameters["hcTabu_max_number_parents"]
        self.iterations = algorithm_parameters["hcTabu_iterations"]
        self.sample_size = algorithm_parameters["hcTabu_sample_size"]

    def run(self, backend="bnlearn"):
        nodes = list(self.data.columns.values)

        if backend == "neurosuites":
            model = self.run_hcTabu_neurosuites(nodes)
        elif backend == "bnlearn":
            model = self.run_hcTabu_bnlearn(nodes)

        return model

    def run_hcTabu_bnlearn(self, nodes):
        dataframe, nodes = self.prepare_input_structure_bnlearn(nodes)

        bnlearn = importr("bnlearn")
        output_raw_R = bnlearn.tabu(x = dataframe)

        graph = self.parse_output_structure_bnlearn(nodes, output_raw_R)

        return graph



    def run_hcTabu_neurosuites(self, nodes):

        return 0
