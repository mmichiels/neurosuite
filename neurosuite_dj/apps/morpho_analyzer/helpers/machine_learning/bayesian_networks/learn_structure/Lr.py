from .base import *
from sklearn.linear_model import LinearRegression

class Lr(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(Lr, self).__init__(data, data_type, states_names)

    def run(self, backend="scikit-learn"):
        if self.data_type != "continuous":
            raise Exception("Algorithm only supported for continuous datasets ")

        nodes_names = list(self.data.columns.values)

        if backend == "scikit-learn":
            graph = self.run_lr_scikit_learn(nodes_names)
        else:
            raise Exception("Backend {} is not supported.".format(backend))

        return graph

    def run_lr_scikit_learn(self, nodes_names):
        data_np = np.array(self.data)

        N, G = data_np.shape
        adj_matrix = np.zeros((G, G))
        for i in range(G):
            X = data_np[:, np.array([j != i for j in range(G)])]
            y = data_np[:, i]
            regression = LinearRegression().fit(X, y)
            adj_matrix[i] = np.concatenate((regression.coef_[:i], [0], regression.coef_[i:]))

        np.fill_diagonal(adj_matrix, 0)
        adj_matrix = np.triu(adj_matrix)
        adj_matrix = np.square(adj_matrix)

        graph = self.nx_graph_from_adj_matrix(adj_matrix, nodes_names)

        return graph
