from .base import *

class CL(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(CL, self).__init__(data, data_type, states_names)
        self.max_number_parents = algorithm_parameters["cl_max_number_parents"]

    def run(self, backend="bnlearn"):
        nodes = list(self.data.columns.values)

        if backend == "neurosuites":
            model = self.run_cl_neurosuites(nodes)
        elif backend == "bnlearn":
            model = self.run_cl_bnlearn(nodes)

        return model

    def run_cl_bnlearn(self, nodes):
        dataframe, nodes = self.prepare_input_structure_bnlearn(nodes)

        bnlearn = importr("bnlearn")
        output_raw_R = bnlearn.chow_liu(x = dataframe)

        graph = self.parse_output_structure_bnlearn(nodes, output_raw_R)

        return graph



    def run_cl_neurosuites(self, nodes):

        return 0
