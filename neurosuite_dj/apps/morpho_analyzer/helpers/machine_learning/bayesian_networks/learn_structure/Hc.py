from .base import *

class Hc(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(Hc, self).__init__(data, data_type, states_names)
        self.max_number_parents = algorithm_parameters["hc_max_number_parents"]
        self.iterations = algorithm_parameters["hc_iterations"]
        self.sample_size = algorithm_parameters["hc_sample_size"]

    def run(self, backend="bnlearn"):
        nodes = list(self.data.columns.values)

        if backend == "neurosuites":
            model = self.run_hc_neurosuites(nodes)
        elif backend == "bnlearn":
            model = self.run_hc_bnlearn(nodes)

        return model

    def run_hc_bnlearn(self, nodes):
        dataframe, nodes = self.prepare_input_structure_bnlearn(nodes)

        bnlearn = importr("bnlearn")
        output_raw_R = bnlearn.hc(x = dataframe)

        graph = self.parse_output_structure_bnlearn(nodes, output_raw_R)

        return graph



    def run_hc_neurosuites(self, nodes):

        return 0
