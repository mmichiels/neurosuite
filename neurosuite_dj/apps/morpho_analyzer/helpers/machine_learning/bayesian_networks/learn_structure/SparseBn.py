from .base import *

class SparseBn(LearnStructure):
    def __init__(self, data, algorithm_parameters, data_type, states_names=None):
        super(SparseBn, self).__init__(data, data_type, states_names)

    def run(self, backend="sparsebn"):
        nodes = list(self.data.columns.values)

        if backend == "neurosuites":
            model = self.run_sparseBn_neurosuites(nodes)
        elif backend == "sparsebn":
            model = self.run_sparseBn_sparsebn(nodes)

        return model

    def run_sparseBn_sparsebn(self, nodes):
        helper_workers.update_progress_worker(current_task, 5) #----

        dataframe, nodes = self.prepare_input_structure_bnlearn(nodes)

        start_time = time.time()
        sparsebn = importr("sparsebn")
        sparsebn_utils = importr("sparsebnUtils")
        data_R = sparsebn_utils.sparsebnData(dataframe, type = self.data_type)
        dags_R = sparsebn.estimate_dag(data_R)
        solution_idx_R = sparsebn_utils.select_parameter(dags_R, data_R)
        output_raw_R = sparsebn_utils.select(dags_R, index = solution_idx_R)
        output_raw_R = dict(output_raw_R.items())

        helper_workers.update_progress_worker(current_task, 60) #----

        edges_r = np.array(output_raw_R["edges"])
        nodes = output_raw_R["edges"].names
        edges_python = []
        for i, parents in enumerate(edges_r):
            parents_node_i = list(parents)
            for parent in parents_node_i:
                edge = (nodes[parent], nodes[i])
                edges_python.append(edge)

        end_time = time.time()
        total_time = round(end_time - start_time, 2)
        print("-------TOTAL TIME sparsebn:--------", total_time)

        helper_workers.update_progress_worker(current_task, 95) #----

        graph = nx.DiGraph()
        graph.add_nodes_from(nodes)
        graph.add_edges_from(edges_python)

        return graph


    def r_vector_to_list(self, r_vector):
        return list(r_vector)

    def run_sparseBn_neurosuites(self, nodes):

        return 0
