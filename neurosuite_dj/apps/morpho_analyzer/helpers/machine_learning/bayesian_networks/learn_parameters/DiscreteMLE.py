from .base import *


class DiscreteMLE(LearnParameters):
    def __init__(self, data, data_type, graph, algorithm_parameters):
        super(DiscreteMLE, self).__init__(data, data_type, graph)
        self.algorithm_parameters = algorithm_parameters

    def run(self, backend="bnlearn"):
        if backend == "pgmpy":
            model_parameters = self.run_mle_pgmpy()

        return model_parameters

    def run_mle_pgmpy(self):
        bn_model_pgmpy = bn_io.IoBn.nx_graph_parameters_to_pgmpy_model(graph=self.graph, parameters={})

        mle_pgmpy = pgmpy_estimators.MaximumLikelihoodEstimator(bn_model_pgmpy, self.data)
        model_parameters = mle_pgmpy.get_parameters()

        return model_parameters

