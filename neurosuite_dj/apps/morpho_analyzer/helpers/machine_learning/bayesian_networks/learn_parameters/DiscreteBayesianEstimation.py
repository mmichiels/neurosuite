from .base import *


class DiscreteBayesianEstimation(LearnParameters):
    def __init__(self, data, data_type, graph, algorithm_parameters):
        super(DiscreteBayesianEstimation, self).__init__(data, data_type, graph)
        self.prior = algorithm_parameters["bayesianEstimation_prior"]
        if self.prior == "BDeu":
            self.equivalent_size = algorithm_parameters["bayesianEstimation_equivalent_size"]

    def run(self, backend="bnlearn"):
        nodes = list(self.data.columns.values)

        if backend == "pgmpy":
            model = self.run_bayesianEstimation_pgmpy()

        return model

    def run_bayesianEstimation_pgmpy(self):
        bn_model_pgmpy = bn_io.IoBn.nx_graph_parameters_to_pgmpy_model(graph=self.graph, parameters={})

        bayesian_estimator_pgmpy = pgmpy_estimators.BayesianEstimator(bn_model_pgmpy, self.data)
        if self.prior == "K2":
            model_parameters = bayesian_estimator_pgmpy.get_parameters(prior_type='K2')
        elif self.prior == "BDeu":
            model_parameters = bayesian_estimator_pgmpy.get_parameters(prior_type='BDeu',
                                                                       equivalent_sample_size=self.equivalent_size)

        return model_parameters



