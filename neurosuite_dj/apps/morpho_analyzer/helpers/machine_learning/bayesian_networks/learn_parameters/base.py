
import networkx as nx
import networkx.readwrite as networkx_io
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks.pgmpy import estimators as pgmpy_estimators
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks.utils import bn_utils
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import io as bn_io
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks.utils.graph_utils import *

from apps.morpho_analyzer.helpers import helpers
import pyBN
import numpy as np
from itertools import combinations

import rpy2
from rpy2.rinterface import R_VERSION_BUILD
import rpy2.rinterface as rinterface
from rpy2.robjects import r
from rpy2.robjects.packages import importr
import datetime
import re
from rpy2.rinterface import RRuntimeWarning
from rpy2.robjects import pandas2ri
from rpy2.robjects import conversion as rpy2_conversion


class LearnParameters(object):
    """
    Base class for all LearningStructure classes.
    """
    def __init__(self, data, data_type, graph):
        self.data = data
        self.data_type = data_type
        self.graph = graph

    @staticmethod
    def get_parameters_learning_class(algorithm_name, dataframe, data_type, graph, algorithm_parameters):
        from .DiscreteMLE import DiscreteMLE
        from .GaussianMLE import GaussianMLE
        from .DiscreteBayesianEstimation import DiscreteBayesianEstimation

        algorithm_class = {}

        if algorithm_name == "discreteMle":
            algorithm_class = DiscreteMLE(dataframe, data_type, graph, algorithm_parameters)
        elif algorithm_name == "gaussianMle":
            algorithm_class = GaussianMLE(dataframe, data_type, graph, algorithm_parameters)
        elif algorithm_name == "discreteBayesianEstimation":
            algorithm_class = DiscreteBayesianEstimation(dataframe, data_type, graph, algorithm_parameters)


        return algorithm_class

    def prepare_input_parameters_bnlearn(self):
        pandas2ri.activate()
        dataframe = self.data
        if self.data_type == "hybrid":
            raise Exception("This algorithm still does not support hybrid bayesian networks")
        dataframe.columns = dataframe.columns.str.replace(".", "")
        dataframe.columns = dataframe.columns.str.replace("-", "")

        return dataframe