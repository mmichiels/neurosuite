import os
import sys
import importlib


actual_folder = os.path.dirname(os.path.realpath(__file__))
actual_folder = os.path.dirname(actual_folder)
pgmpy_dependencies_folder = os.path.join(actual_folder, "pgmpy_dependencies")
sys.path.insert(0, pgmpy_dependencies_folder)
path_sys = sys.path

#This module initializes flags for optional dependencies
try:
    a = 1

    """
    importlib.invalidate_caches()
    #setattr(pandas, "empty", None)
    #pandas = importlib.reload(pandas)
    del pandas
    del sys.modules["pandas"]
    pd = importlib.import_module("pandas")
    pd_version = pd.__version__
    """
    import pandas
    pd_version = pandas.__version__

    HAS_PANDAS = True
except ImportError:
    HAS_PANDAS = False
