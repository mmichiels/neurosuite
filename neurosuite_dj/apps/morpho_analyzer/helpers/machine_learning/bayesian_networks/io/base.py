from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks.pgmpy import readwrite as pgmpy_io
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks.pgmpy import models as pgmpy_models
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import models as bn_models
import tempfile
import os
import networkx
import pyBN
import numpy as np
import pandas as pd
#import pgmpy.readwrite as pgmpy_io

class IoBn(object):
    """
    Base class for all IoBn classes.
    """
    def __init__(self):
        pass

    @staticmethod
    def pgmpy_model_to_nx_graph_parameters(bn_model_pgmpy):
        graph = networkx.DiGraph()
        graph.add_nodes_from(bn_model_pgmpy.nodes())
        graph.add_edges_from(bn_model_pgmpy.edges())
        parameters = bn_model_pgmpy.cpds

        return graph, parameters

    @staticmethod
    def nx_graph_parameters_to_pgmpy_model(graph, parameters):
        pgmpy_model = pgmpy_models.BayesianModel()
        pgmpy_model.add_nodes_from(graph.nodes())
        pgmpy_model.add_edges_from(graph.edges())
        if parameters:
            pgmpy_model.add_cpds(*parameters)

        return pgmpy_model

    @staticmethod
    def get_io_class(file_format, prob_clustering=False):
        from .BIF import BIF
        from .AdjMatrix import AdjMatrix
        from .ProbabilisticClustering import ProbabilisticClustering

        io_class = {}

        if file_format == "bif":
            io_class = BIF()
        elif (file_format == "csv" or file_format == "gzip") and not prob_clustering:
            io_class = AdjMatrix(file_format)
        elif (file_format == "csv" or file_format == "gzip") and prob_clustering:
            io_class = ProbabilisticClustering(file_format)
        else:
            raise Exception("File extension is not supported yet")

        return io_class

