import numpy as np
import matplotlib
from scipy.stats import multivariate_normal as mvn
from scipy.integrate import nquad
import matplotlib.pyplot as plt
import networkx as nx

def joint(order, nodes_names, parameters):
    # Takes the factorized distribution implied by the BN and creates the join gaussian.

    n = len(nodes_names)
    Mu = np.zeros((n,))
    Sigma = np.zeros((n, n))

    for node in order:
        node_name = nodes_names[node]
        params = parameters[node_name]
        mean, var, parents_coeffs, parents = params.mean, params.var, params.parents_coeffs, params.parents_names
        parents = [nodes_names.index(i) for i in parents]
        Mu[node] = mean + sum([Mu[i]*j for i, j in zip(parents, parents_coeffs)])
        if parents:
            cov_parents_involved = Sigma[:, parents]
            cov_parents = cov_parents_involved[parents, :]
            Sigma[:, node] = np.dot(cov_parents_involved, np.array(parents_coeffs))
            Sigma[node, node] = var + np.dot(np.array(parents_coeffs), np.dot(cov_parents, np.array(parents_coeffs)))
            Sigma[node, :] = Sigma[:, node]
        else:
            Sigma[node, node] = var

    joint_dist = {"mu": Mu, "sigma": Sigma}

    return joint_dist


def condition_on_evidence(joint_dist, nodes_names, evidences):
    Mu = joint_dist["mu"]
    Sigma = joint_dist["sigma"]

    # Conditions a multivariate gaussian on some evidences
    evidences_nodes = evidences.get_names()
    evidences_vals = [evidences.get(node) for node in evidences_nodes]
    not_evidences_nodes_order = [x for x in nodes_names if x not in evidences_nodes]

    """Divide the covariance matrix into blocks xx, xy, yy for xx: nodes with evidences, yy:nodes wo evidences"""
    indices = list(range(Sigma.shape[0]))
    idx_sigma = np.array([nodes_names[i] in evidences_nodes for i in indices])

    sigma_xy = Sigma[idx_sigma, :][:, ~idx_sigma]
    sigma_xx = Sigma[idx_sigma, :][:, idx_sigma]
    sigma_inv = np.linalg.solve(sigma_xx, sigma_xy)
    sigma_yy = Sigma[~idx_sigma, :][:, ~idx_sigma]

    # Compute conditional distribution
    mu_y = Mu[~idx_sigma] + np.dot(sigma_inv.T, (evidences_vals - Mu[idx_sigma]))
    sigma_y = sigma_yy - np.dot(sigma_xy.T, sigma_inv)

    joint_dist_cond = {"mu": mu_y, "sigma": sigma_y}

    return joint_dist_cond, not_evidences_nodes_order


def marginal(joint_dist, all_nodes_names, marginal_nodes_names=None, only_one_marginal=False, multivariate=False):
    mu_joint = joint_dist["mu"]
    sigma_joint = joint_dist["sigma"]

    if multivariate:
        idx_mu = [all_nodes_names.index(node_name) for node_name in marginal_nodes_names]
        indices = list(range(sigma_joint.shape[0]))
        idx_sigma = np.array([all_nodes_names[i] in marginal_nodes_names for i in indices])

        mu_marginal = mu_joint[idx_mu]
        sigma_marginal = sigma_joint[idx_sigma, :][:, idx_sigma]

        if len(marginal_nodes_names) == 1:
            mu_marginal = mu_marginal
            sigma_marginal = sigma_marginal.item()
    else:
        if only_one_marginal:
            mu_marginal = mu_joint[all_nodes_names.index(marginal_nodes_names[0])].item()
            sigma_marginal = sigma_joint[all_nodes_names.index(marginal_nodes_names[0])].item()
            return mu_marginal, sigma_marginal
        else:
            indices_marginals = [all_nodes_names.index(node_name) for node_name in marginal_nodes_names]
            mu_marginal = mu_joint[indices_marginals]
            sigma_marginal = sigma_joint[indices_marginals]

    return mu_marginal, sigma_marginal


def condition_on_ineq(Mu, Sigma, evidences, mode="greater"):
    # Takes greater than or lower than as modes. Conditions a joint distribution on inequality evidence and returns
    # the posterior. P(Y|X>x) = P(X>x|Y)P(Y)/P(X>x)
    if mode not in ["greater", "lower"]:
        raise ValueError("Conditioning mode can only be greater or lower")

    evidences_nodes = evidences.get_names()
    evidences_vals = [evidences[node] for node in evidences_nodes]
    not_evidences_nodes_order = [x for x in nodes_names if x not in evidences.get_names()]

    # Get the marginals for X and Y
    mu_x, sigma_x = marginal(Mu, Sigma, nodes_names, evidences_nodes)
    p_X = mvn(mean=mu_x, cov=sigma_x)
    cdf_X = p_X.cdf(np.array(evidences_vals))
    if mode is "greater":
        cdf_X = 1 - cdf_X

    mu_y, sigma_y = marginal(Mu, Sigma, nodes_names, not_evidences_nodes_order)
    p_Y = mvn(mean=mu_y, cov=sigma_y)

    def posterior(y, post_mode="equal"):
        if post_mode not in ["map", "equal", "greater", "lower"]:
            raise ValueError("Mode can only be map, equal (pdf), lower or greater (cdf)")

        if post_mode is "map":
            # I think this is just mu_y, but need to check
            return mu_y

        elif post_mode is "equal":
            # Get inverse conditional P(X|Y)
            evidence_inverse = {name:y[i] for i, name in enumerate(not_evidences_nodes_order)}
            mu_xy, sigma_xy, _ = condition_on_evidence(Mu, Sigma, evidence_inverse)
            cdf_X_if_Y = mvn(mean=mu_xy, cov=sigma_xy).cdf(np.array(evidences_vals))
            if mode is "greater":
                cdf_X_if_Y = 1 - cdf_X_if_Y

            # Return pdf for P(Y=y|X>x)
            return p_Y.pdf(np.array(y))*cdf_X_if_Y/cdf_X

        else:
            def integrand(*args):
                value_y = [arg for arg in args]
                return posterior(value_y)

            # Use pdf mode and integrate to obtain cdf
            if post_mode is "lower":
                ranges = [(mu_y[i] - 10*sigma_y[i, i], y[i]) for i in range(len(y))]
                cdf = nquad(integrand, ranges)

            else:
                ranges = [(y[i], mu_y[i] + 10*sigma_y[i, i]) for i in range(len(y))]
                cdf = nquad(integrand, ranges)

            return cdf

    # Returns a function from where you can query the pdf, cdf and map of the posterior.
    return posterior



