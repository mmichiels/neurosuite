
class Evidences():
    def __init__(self):
        self.evidences = {}
        self.not_evidences_nodes_order = None

    def set(self, nodes_ids, evidence_value):
        for node in nodes_ids:
            self.evidences[node] = evidence_value

    def clear(self, nodes_ids):
        for node in nodes_ids:
            del self.evidences[node]

    def clear_all(self):
        self.evidences = {}

    def get_names(self):
        return list(self.evidences.keys())

    def get(self, node_id):
        try:
            return self.evidences[node_id]
        except KeyError as e:
            return None

    def get_not_evidences_nodes_order(self):
        return self.not_evidences_nodes_order

    def set_not_evidences_nodes_order(self, not_evidences_nodes_order):
        self.not_evidences_nodes_order = not_evidences_nodes_order

    def is_empty(self):
        return not bool(self.evidences)




