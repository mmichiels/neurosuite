import networkx
import networkx.readwrite as networkx_io
import igraph
from fa2 import ForceAtlas2
import pygraphviz

class BnLayout(object):
    """
    Base class for all Layout classes.
    """
    def __init__(self, graph, graph_initial_width=None, graph_initial_height=None):
        self.graph = graph
        self.layout = {}
        self.graph_initial_width = graph_initial_width
        self.graph_initial_height = graph_initial_height

    @staticmethod
    def get_layout_class(layout_name, graph, graph_initial_width=None, graph_initial_height=None, additional_params=None):
        from .LayoutDot import LayoutDot
        from .LayoutForceatlas2 import LayoutForceatlas2
        from .LayoutFruchtermanReingold import LayoutFruchtermanReingold
        from .LayoutSugiyama import LayoutSugiyama
        from .LayoutCircular import LayoutCircular
        from .LayoutGrid import LayoutGrid
        from .LayoutImage import LayoutImage

        switcher = {
            "dot": LayoutDot(graph, graph_initial_width, graph_initial_height),
            "forceatlas2": LayoutForceatlas2(graph, graph_initial_width, graph_initial_height),
            "fruchterman_reingold": LayoutFruchtermanReingold(graph, graph_initial_width, graph_initial_height),
            "sugiyama": LayoutSugiyama(graph, graph_initial_width, graph_initial_height),
            "circular": LayoutCircular(graph, graph_initial_width, graph_initial_height),
            "grid": LayoutGrid(graph, graph_initial_width, graph_initial_height),
            "image": LayoutImage(graph, graph_initial_width, graph_initial_height, additional_params),
        }

        return switcher[layout_name]

    @staticmethod
    def networkx_to_igraph(graph_networkx):
        graph_igraph = igraph.Graph(directed=True)
        nodes = list(graph_networkx.nodes())
        edges = list(graph_networkx.edges())
        graph_igraph.add_vertices(nodes)
        graph_igraph.add_edges(edges)

        return graph_igraph