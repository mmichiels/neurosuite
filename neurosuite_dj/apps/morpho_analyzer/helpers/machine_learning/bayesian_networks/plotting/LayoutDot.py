from .BnLayout import *

class LayoutDot(BnLayout):
    def __init__(self, graph, graph_initial_width=None, graph_initial_height=None):
        super(LayoutDot, self).__init__(graph, graph_initial_width, graph_initial_height)

    def run(self):
        layout_reversed = networkx.drawing.nx_agraph.graphviz_layout(self.graph, prog='dot')

        for node_reversed in layout_reversed:
            self.layout[node_reversed] = (
                (layout_reversed[node_reversed][0]) / self.graph_initial_width,
                (layout_reversed[node_reversed][1] * -1) / self.graph_initial_height
            )

        return self.layout
