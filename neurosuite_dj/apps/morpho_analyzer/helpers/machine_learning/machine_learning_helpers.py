import json
import tempfile
import sys
import os
import io
import timeit

import time
from django.conf import settings
from apps.morpho_analyzer.helpers import helpers
import neurosuite_dj.helpers as global_helpers
from celery import shared_task, current_task
from io import BytesIO
import zipfile
import pandas as pd
import numpy as np
import math
from sklearn import preprocessing as sklearn_preprocessing
import random
import math
import datetime
import scipy.stats as scipy_stats
from mdlp.discretization import MDLP
import copy
import itertools
import neurosuite_dj.helpers_global.dataset as dataset_helper
from sklearn.feature_selection import SelectKBest, SelectPercentile, SelectFwe, SelectFpr, SelectFdr, chi2, mutual_info_classif, f_classif
from sklearn.decomposition import PCA, KernelPCA, FastICA
from sklearn.manifold import TSNE, MDS
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import base64
import colorlover as cl


def discretize_data(dataframe, class_features, discretize_method, discretize_parameters):
    dataframe_continuous = dataframe.select_dtypes(include=[np.number])
    cols_discrete = list(set(dataframe.columns) - set(dataframe_continuous.columns))
    dataframe_discrete = dataframe.loc[:, cols_discrete]

    if not dataframe_continuous.empty:
        if discretize_method == "equal_width":
            bins = int(discretize_parameters["n_bins"])

            dataframe_cont_discretized = dataframe_continuous.apply(discretize_equal_width, axis=0, bins=bins)
        elif discretize_method == "equal_frequency":
            bins = int(discretize_parameters["n_bins"])

            dataframe_cont_discretized = dataframe_continuous.apply(discretize_equal_freq, axis=0, bins=bins)
        elif discretize_method == "fayyad_irani":
            if len(class_features) == 0:
                raise Exception("Fayyad & Irani's MDL discretization method requires a class feature.")
            column_class = dataframe.loc[:, class_features[0]]
            column_class_dtype = pd.Series([column_class.dtypes])
            class_data_type = dataset_helper.dataframe_get_type(column_class_dtype)
            if class_data_type is "continuous":
                raise Exception("Fayyad & Irani's MDL discretization method requires a discrete class feature. You selected a continuous class feature.")

            #Replace categories string values by numbers to fit with the algorithm input:
            column_class = column_class.astype('category')
            column_class = column_class.cat.codes

            mdlp = MDLP()
            discretized_data = mdlp.fit_transform(dataframe_continuous, column_class).astype(int)
            dataframe_cont_discretized = pd.DataFrame(discretized_data, columns=dataframe_continuous.columns, dtype="str").astype("category")
            dataframe_cont_discretized = dataframe_cont_discretized.apply(transform_state_names, axis=0)

        dataframe_cont_discretized = dataframe_cont_discretized.astype("str")
        if not dataframe_discrete.empty:
            # Merge continuous (discretized data) with data already discrete:
            dataframe = pd.concat([dataframe_discrete, dataframe_cont_discretized], axis=1)
        else:
            dataframe = dataframe_cont_discretized

    return dataframe

def discretize_equal_width(col, **kwargs):
    bins, labels = get_n_bins_and_labels(column=col, n_bins=kwargs["bins"])

    return pd.cut(col.values, bins=bins, labels=labels)

def discretize_equal_freq(col, **kwargs):
    bins, labels = get_n_bins_and_labels(column=col, n_bins=kwargs["bins"])

    return pd.qcut(col.rank(method='first'), q=bins, labels=labels)

def get_n_bins_and_labels(column, n_bins):
    labels = []
    bins = n_bins
    if len(column) > 100:
        num_categories = bins
    else:
        num_categories = len(column.value_counts().values)

    if (num_categories < bins):
        bins = num_categories
        labels = []
    for i in range(bins):
        labels.append("state_{}".format(i + 1))

    return bins, labels

def transform_state_names(col, **kwargs):
    bins = len(col.value_counts().values)
    labels = []
    for i in range(bins):
        labels.append("state_{}".format(i + 1))

    col = col.cat.rename_categories(labels)
    return col


def apply_feature_subset_selection(X, class_features, summary, params):
    X_features = X.loc[:, X.columns != class_features[0]]
    alpha_value = float(params["alpha"])
    y = X[class_features]
    selector = None
    score = None
    values = []
    if(params["feature_selection_algorithm"] == "chi2"):
        score = chi2
    elif (params["feature_selection_algorithm"] == "m_i"):
        score = mutual_info_classif
    elif (params["feature_selection_algorithm"] == "f-score"):
        score = f_classif

    if (params["filter"] == "kbest"):
        selector = SelectKBest(score, k=int(params["k_best"]))
    elif (params["filter"] == "percentile"):
        selector = SelectPercentile(score, percentile=float(params["percentile"]))
    elif (params["filter"] == "fscore"):
        selector = SelectFwe(score_func=score, alpha=alpha_value)

    fit = selector.fit(X_features, y)
    X = X_features[X_features.columns[fit.get_support(indices=True)]]

    if (params["feature_selection_algorithm"] == "m_i"):
        values = [ round(score, 4) for score in fit.scores_]
    else:
        values = [ round(pvalue, 4) for pvalue in fit.pvalues_]

    stacked_values = np.column_stack((np.array(values), np.array(X_features.columns.values)))
    stacked_values = stacked_values[stacked_values[:, 0].argsort()]

    X[class_features] = y
    X = pd.DataFrame(data=X, columns=X.columns)
    summary["features_values"] = json.dumps(stacked_values.tolist())
    return X, summary

def apply_dimentionality_reduction(X, class_features, summary, params):
    X_features = X.loc[:, X.columns != class_features[0]]
    y = X[class_features]
    transformer = None

    if (params["pca_algorithm"] == "pca"):
        transformer = PCA(n_components=int(params["n_components"]))
    if (params["pca_algorithm"] == "kpca"):
        transformer = KernelPCA(kernel="rbf", n_components=int(params["n_components"]), fit_inverse_transform=True, gamma=10)
    if (params["pca_algorithm"] == "ica"):
        transformer = FastICA(n_components=int(params["n_components"]), random_state = 0)
    if (params["pca_algorithm"] == "t-sne"):
        transformer = TSNE(n_components=int(params["n_components"]), random_state=0)
    if (params["pca_algorithm"] == "mds"):
        transformer = MDS(n_components=int(params["n_components"]), random_state=0)
    x = transformer.fit_transform(X_features)
    if (params["pca_algorithm"] == "pca"):
        explained_variance_ratio = transformer.explained_variance_ratio_
        summary["explained_variance_ratio"] = explained_variance_ratio[0]
    X = pd.DataFrame(x)
    X.rename(columns=lambda x: str(x), inplace=True)
    X[class_features] = y
    if (int(params["n_components"]) > 1 and int(params["n_components"]) <= 3) :
        dimentionality_reduction_plot = plot_dimentionality_reduction_data(X, class_features, 8, 6)
        summary["dimentionality_reduction_plot"] = dimentionality_reduction_plot
    return X, summary

def plot_dimentionality_reduction_data(X, class_features, width=0, height=0):
    fig = plt.figure()
    if width > 0 and height > 0:
        fig = plt.figure(figsize=(width, height))
    i = 0
    colors = [(166,206,227), (31,120,180), (178,223,138), (178,223,138), (51,160,44), (251,154,153), (227,26,28),
              (253,191,111), (255,127,0), (202,178,214), (106,61,154), (255,255,153)]
    colors = list(map(lambda x: (x[0]/255, x[1]/255 ,x[2]/255 ), colors))
    if X.columns.values.size == 3:
        groups = X.groupby(class_features)
        ax = fig.add_subplot(1,1,1)
        ax.set_xlabel("component 1")
        ax.set_ylabel("component 2")
        for name, group in groups:
            gColor = colors[(i * 2 + 1) % 11]
            ax.plot(group['0'], group['1'], marker='o', linestyle='', color=gColor, ms=2, label=name)
            i += 1
        ax.legend()
    if X.columns.values.size == 4:
        groups = X.groupby(class_features)
        ax = fig.add_subplot(1,1,1, projection='3d')
        ax.set_xlabel("component 1")
        ax.set_ylabel("component 2")
        ax.set_xlabel("component 3")
        for name, group in groups:
            gColor = colors[(i * 2 + 1) % 11]
            ax.plot(group['0'], group['1'], group['2'], marker='o', linestyle='', color=gColor, ms=1, label=name)
            i += 1
        ax.legend()
    figdata = BytesIO()
    fig.savefig(figdata, format='png')
    figdata.seek(0)
    plot = base64.b64encode(figdata.read()).decode("ascii")
    return plot


