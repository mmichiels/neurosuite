import os


def get_data():
    # Get data
    dataset_folder = get_dataset_folder()


    return dataset_folder

def get_dataset_folder():
    dataset_folder = "./demo_neurons_samples"
    dataset_folder = os.path.join(os.path.dirname(__file__), os.pardir)
    dataset_folder = os.path.join(dataset_folder, os.pardir)
    dataset_folder = os.path.join(dataset_folder, 'demo_neurons_samples')

    return dataset_folder
