import os
import tempfile
import pandas as pd
import time
import neurosuite_dj.helpers as global_helpers
from apps.morpho_analyzer.helpers import helpers
import neurosuite_dj.helpers_global.dataset as dataset_helper
import neurosuite_dj.helpers_global.datasets_user as datasets_user_helper
from celery import shared_task, current_task

#from apps.morpho_analyzer.helpers.LMIO import *

from apps.morpho_analyzer.helpers.LMIO.util.morphometricMeasurements import getAllMorphMeasures
from apps.morpho_analyzer.helpers.LMIO.util.morphometricMeasurements import getRegularMorphNames
from apps.morpho_analyzer.helpers import helpers


#L-Measure wrapper
def get_features(neurons_user, valid_neurons=None):
    """------------------------------L-Measure-------------------------------------"""
    data_processed = []
    data_errors = []
    num_variables_selected = 43 #All variables are 43
    convert_to_swc = False
    # for neuron_name in valid_neurons:
    for i, neuron in enumerate(neurons_user):  # All neurons (i.e. including valid and invalid neurons) Only for tests
        start_time = time.time()
        with tempfile.NamedTemporaryFile(suffix=neuron["data_file"]["extension"]) as tmp_file:
            if ("simulated_neuron" in neuron["data_file"] and neuron["data_file"]["simulated_neuron"]):
                neuron_in_memory_file = helpers.get_file_neuron(neuron, l_measure_and_simulated=True)
            else:
                neuron_in_memory_file = helpers.get_file_neuron(neuron, l_measure_and_simulated=False)

            for chunk in neuron_in_memory_file.chunks():
                tmp_file.write(chunk)
            tmp_file.flush()  # To force the saving

            neuron_file = [tmp_file.name]
            try:
                LMOutput = getAllMorphMeasures(neuron_file)
                LMOutput.insert(0, neuron["name"])
                data_processed.append(LMOutput[0:num_variables_selected + 1])  # +1 because of the neuron name column
            except Exception as e:
                data_errors.append(neuron["name"])

        end_time = time.time()
        if current_task:
            process_percent = int(100 * float(i) / float(len(neurons_user)))
            estimated_time_finish = round((end_time - start_time) * (float(len(neurons_user)) - float(i)), 2)
            print ("process_percent: ", process_percent, "Task: ", current_task)
            current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                              'estimated_time_finish': estimated_time_finish})

    """-------------------------Create dataset matrix---------------------------"""
    print("Creating dataset matrix... \n")
    columns = getRegularMorphNames()[0:num_variables_selected]
    columns.insert(0, "Neuron name")
    X = pd.DataFrame(data=data_processed, columns=columns)

    print("--------L-Measure-------" + '\n')
    data = {"dataframe": X,
            "data_errors": data_errors}

    return data

def prepare_render_dataframe_l_measure(context, datasets_user, neurons_user):
    dataset_name = "l_measure"
    dataset_l_measure = datasets_user.datasets[dataset_name]
    dataframe_l_measure = dataset_l_measure.get_dataframe()
    context["features_columns"], context["features_data"] = global_helpers.pandas_dataframe_to_lists(
        dataframe=dataframe_l_measure, append_columns=["Analyze neuron"], index_column=False)
    context["features_error"] = dataset_l_measure.instances_error
    context["columns_not_stats"] = ["Analyze neuron"] + [dataset_l_measure.label_column_id]
    context["dataset_name"] = dataset_name
    # ----Filter session neurons-----
    additional_fields = []
    category_l_measure = helpers.get_category_l_measure(dataframe_l_measure)
    additional_fields.append(category_l_measure)
    context["fields_by_categories"] = helpers.get_session_all_neuron_fields_by_categories(neurons_user,
                                                                                          additional_fields)
    # --------------------------------
    # ----Get neurons additional data-----
    if datasets_user.input_dataset_discrete_name in datasets_user.datasets:
        dataset_discrete_data = datasets_user.datasets[datasets_user.input_dataset_discrete_name]
        context["data_have_discrete_fields"] = True
        context["discrete_fields"] = list(dataset_discrete_data.get_dataframe().columns.values)
        context["discrete_fields"].remove(dataset_discrete_data.label_column_id)
    else:
        context["data_have_discrete_fields"] = False

    context["label_column_id"] = dataset_l_measure.label_column_id
    context["label_instances"] = dataset_l_measure.label_instances

    return 0