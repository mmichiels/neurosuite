import requests
import json
import io
import os
import sys
from django.core.files.uploadedfile import InMemoryUploadedFile
from dateutil.parser import parse
import time
import copy
from apps.morpho_analyzer.helpers import helpers
import neurosuite_dj.helpers as global_helpers
from celery import shared_task, current_task

BASE_URL_API = "http://neuromorpho.org/api"
BASE_URL_WEB = "http://neuromorpho.org"

#My default data parameters
class DataParams():
    def __init__(self, page=0, size=500, sort="neuron_id,asc", custom_options=None):
        self.page = page
        self.size = size
        self.sort = sort
        if custom_options is not None:
            if hasattr(custom_options, 'limit_number_results') and custom_options.limit_number_results < self.size:
                self.size = custom_options.limit_number_results
            if hasattr(custom_options, 'sort_results') and custom_options.sort_results is not None:
                self.sort = custom_options.sort_results

class CustomOptions():
    def __init__(self, limit_number_results=None, sort_results=None):
        self.limit_number_results = limit_number_results
        self.sort_results = sort_results

    def __init__(self, custom_options=None):
        for key, value in custom_options.items():
            if key == "limit_number_results":
                self.limit_number_results = int(value)
            if key == "sort_results":
                order = "asc"
                if value["order"] == "Ascending":
                    order = "asc"
                else:
                    order = "desc"
                self.sort_results = "";
                self.sort_results += "neuron_name,archive," + order

ENDPOINTS_URLS =  {
    "neuron_fields": "/neuron/fields",
    "values_neuron_field": "/neuron/fields/{field_name}",
    "search_all_neurons": "/neuron",
    "search_neurons_query": "/neuron/select",
    "morphometry_query": "/morphometry/id/{neuron_id}",
    "get_swc_neuron": "/dableFiles/{archive}/CNG version/{neuron_name}.CNG.swc"
}



NEURON_FIELD_CATEGORIES_FRONTEND = [
    {
        "name":"animal",
        "verbose_name": "Animal",
        "fields":
            [
                {
                    "name": "species",
                    "verbose_name": "Species"
                },
                {
                    "name": "min_weight",
                    "verbose_name": "Minimum weight"
                },
                {
                    "name": "max_weight",
                    "verbose_name": "Maximum weight"
                },
                {
                    "name": "age_classification",
                    "verbose_name": "Development"
                },
                {
                    "name": "min_age",
                    "verbose_name": "Minimum age"
                },
                {
                    "name": "max_age",
                    "verbose_name": "Maximum age"
                },
            ]
    },
    {
        "name": "anatomy",
        "verbose_name": "Anatomy",
        "fields":
            [
                {
                    "name": "brain_region",
                    "verbose_name": "Brain region"
                },
                {
                    "name": "cell_type",
                    "verbose_name": "Cell type"
                },
            ]
    },
    {
        "name": "completeness",
        "verbose_name": "Completeness",
        "fields":
            [
                {
                    "name": "domain",
                    "verbose_name": "Structural domain"
                },
                {
                    "name": "Physical_Integrity",
                    "verbose_name": "Physical Integrity"
                },
                {
                    "name": "attributes",
                    "verbose_name": "Morphological attributes"
                },
            ]
    },
    {
        "name": "experiment",
        "verbose_name": "Experiment",
        "fields":
            [
                {
                    "name": "protocol",
                    "verbose_name": "Protocol"
                },
                {
                    "name": "experiment_condition",
                    "verbose_name": "Experimental condition"
                },
                {
                    "name": "stain",
                    "verbose_name": "Stain"
                },
                {
                    "name": "slice_thickness",
                    "verbose_name": "Slicing thickness"
                },
                {
                    "name": "slicing_direction",
                    "verbose_name": "Slicing direction"
                },
                {
                    "name": "shrinkage_reported",
                    "verbose_name": "Tissue Shrinkage"
                },
                {
                    "name": "reconstruction_software",
                    "verbose_name": "Reconstruction software"
                },
                {
                    "name": "objective_type",
                    "verbose_name": "Objective type"
                },
                {
                    "name": "magnification",
                    "verbose_name": "Object magnification"
                },
            ]
    },

    {
        "name": "source",
        "verbose_name": "Source",
        "fields":
            [
                {
                    "name": "archive",
                    "verbose_name": "Archive"
                },
                {
                    "name": "reference_pmid",
                    "verbose_name": "PMID"
                },
                {
                    "name": "original_format",
                    "verbose_name": "Original format"
                },
                {
                    "name": "deposition_date",
                    "verbose_name": "Date of deposition"
                },
                {
                    "name": "upload_date",
                    "verbose_name": "Date of upload"
                },
            ]
    },
    {
        "name": "morphometry",
        "verbose_name": "Morphometry",
        "fields":
            [
                {
                    "name": "surface",
                    "verbose_name": "Surface"
                },
                {
                    "name": "shrinkage_corrected",
                    "verbose_name": "Shrinkage corrected"
                },
                {
                    "name": "reported_value",
                    "verbose_name": "Reported value"
                },
                {
                    "name": "reported_xy",
                    "verbose_name": "Reported xy"
                },
                {
                    "name": "reported_z",
                    "verbose_name": "Reported z"
                },
                {
                    "name": "corrected_value",
                    "verbose_name": "Corrected value"
                },
                {
                    "name": "corrected_xy",
                    "verbose_name": "Corrected xy"
                },
                {
                    "name": "corrected_z",
                    "verbose_name": "Corrected z"
                },
                {
                    "name": "soma_surface",
                    "verbose_name": "Soma surface"
                },
                {
                    "name": "volume",
                    "verbose_name": "Volume"
                },
            ]
    },
    {
        "name": "other_fields",
        "verbose_name": "Other fields",
        "fields":
            [

            ]
    },
    {
        "name": "custom_options",
        "verbose_name": "Custom options",
        "fields":
            [
                {
                    "name": "limit_number_results",
                    "verbose_name": "Limit the number of results",
                    "custom_option": True
                },
                {
                    "name": "sort_results",
                    "verbose_name": "Sort results by",
                    "custom_option": True
                },

            ]
    },
]

def get_all_neuron_fields():
    url_endpoint = ENDPOINTS_URLS["neuron_fields"]
    url_api = BASE_URL_API + url_endpoint
    response = requests.get(url_api)
    if response.status_code == 200:
        data = response.json()["Neuron Fields"]
    else:
        data = "ERROR {0} GET {1}".format(response.status_code, url_api)

    return data


def get_values_neuron_field(neuron_field):
    data = {}
    url_endpoint = ENDPOINTS_URLS["values_neuron_field"]
    url_endpoint = url_endpoint.replace("{field_name}", neuron_field)
    url_api = BASE_URL_API + url_endpoint
    response = requests.get(url_api)
    if response.status_code == 200:
        data["values"] = response.json()["fields"]
        data["type"] = global_helpers.get_type_values(data["values"])
        if data["type"] == "text" or data["type"] == "date":
            data["values"].sort(key=lambda x: x.lower())
        elif data["type"] == "numeric":
            data["values"].sort(key=float)
    else:
        data["values"] = "ERROR {0} GET {1}".format(response.status_code, url_api)

    return data


def get_all_neuron_fields_by_categories(sort_field=True):
    neuron_fields = get_all_neuron_fields()

    neuron_fields_by_category = copy.deepcopy(NEURON_FIELD_CATEGORIES_FRONTEND)

    known_fields = []
    unknown_fields = []


    #-----Get fields in no category (i.e. unknown fields, i.e. other fields)----
    for category in neuron_fields_by_category:
        if (category["name"] != "other_fields" and category["name"] != "custom_options"):
            known_fields.extend(category["fields"])
        elif category["name"] == "custom_options":
            for field in category["fields"]:
                if not sort_field and field["name"] == "sort_results":
                    category["fields"].remove(field)

    known_fields_names = []
    for field in known_fields:
        known_fields_names.append(field["name"])

    for neuron_field in neuron_fields:
        if (neuron_field not in known_fields_names):
            unknown_fields.append(neuron_field)

    #Create pseudo verbose names for the "Other fields--
    other_fields = []
    for field in unknown_fields:
        verbose_name = global_helpers.verbosize_string(field)
        new_field = {
            "name": field,
            "verbose_name": verbose_name,
        }
        other_fields.append(new_field)

    #----Check that the fields in a category are in fact returned by the api-------
    for category in neuron_fields_by_category:
        if category["name"] == "other_fields":
            category["fields"] = other_fields
        elif category["name"] != "custom_options":
            if len(category["fields"]) == 0:
                neuron_fields_by_category.remove(category)
            else:
                for field in category["fields"]:
                    if field["name"] not in neuron_fields:
                        category["fields"].remove(field)
                        if len(category["fields"]) == 0:
                            neuron_fields_by_category.remove(category)

    return neuron_fields_by_category

def get_only_custom_options(sort_field=True):
    custom_options = []
    for category in NEURON_FIELD_CATEGORIES_FRONTEND:
        if category["name"] == "custom_options":
            category_to_insert = copy.deepcopy(category)
            for field in category_to_insert["fields"]:
                if not sort_field and field["name"] == "sort_results":
                    category_to_insert["fields"].remove(field)
            custom_options.append(category_to_insert)

    return custom_options

def parse_json_to_neuromorpho_query(query_data_json):
    """
    The data passed is of type {"key1":"value1", "key2":"value2"} with header "Content-Type: application/json".
    Key : Neuron field
    Value : String value elements

    Examples:
            curl -d '{"species": ["cat","rat","monkey"],
             "stain":["lucifer yellow"],"brain_region":["layer 3"],
             "strain":["Macaque"]}'
             -H "Content-Type: application/json"
             -X POST http://neuromorpho.org/api/neuron/select
    """
    query_params = query_data_json

    """
    query_params = ""
    for i, field in enumerate(query_data_json):
        if i == 0:
              query_params += q=
        else:
            query_params += &fq=
        query_params +=  field["name"] + ":"
        values = field["value"]
        for value in values[:-1]:
            query_params += value + ","
        if isinstance(values, list) and len(values) > 0:
            query_params += values[-1]
    """

    return query_params


def search_neurons_query(query_params, custom_options):
    if not query_params or query_params == "" or query_params is None:
        data = search_all_neurons(custom_options)
    else:
        url_endpoint = ENDPOINTS_URLS["search_neurons_query"]
        url_api = BASE_URL_API + url_endpoint
        custom_options = CustomOptions(custom_options)
        params = vars(DataParams(custom_options=custom_options))  # Var is used to cast to dict

        headers = {'Content-Type': 'application/json'}
        response = requests.post(url_api, data=json.dumps(query_params), params=params, headers=headers)
        status_code = response.status_code
        response = response.json()
        if status_code == 200 and (not ("status" in response) or (response["status"] == 200)):
            data = iterate_neuron_pages_api(url_api, response, params, custom_options, query_params)
        else:
            data = "ERROR {0} GET {1}".format(status_code, url_api)

    return data


def search_all_neurons(custom_options):
    url_endpoint = ENDPOINTS_URLS["search_all_neurons"]
    url_api = BASE_URL_API + url_endpoint
    custom_options = CustomOptions(custom_options)
    params = vars(DataParams(custom_options=custom_options)) #Var is used to cast to dict

    response = requests.get(url_api, params=params)
    status_code = response.status_code
    response = response.json()
    if status_code == 200 and (not ("status" in response) or (response["status"] == 200)):
        data = iterate_neuron_pages_api(url_api, response, params, custom_options)
    else:
        data = "ERROR {0} GET {1}".format(status_code, url_api)

    return data

def iterate_neuron_pages_api(url_api, response, params, custom_options, query_params={}):
    data = []
    totalPages = response["page"]["totalPages"]
    totalElements = response["page"]["totalElements"]
    size = params["size"]
    params["page"] += 1
    if totalElements <= 0:
        return data
    elif totalElements < size:
        size = totalElements
    data = response["_embedded"]["neuronResources"]
    # Delay to not overload the NeuroMorpho server
    # time.sleep(50.0 / 1000.0);
    while params["page"] < totalPages:
        start_time = time.time()
        if hasattr(custom_options, 'limit_number_results'):
            if (size + params["size"]) > custom_options.limit_number_results:
                params["size"] -= abs(custom_options.limit_number_results - (size + params["size"]))
            if size >= custom_options.limit_number_results:
                current_task.update_state(state='PROGRESS', meta={'process_percent': 100, 'estimated_time_finish': 0})
                break
        url_search_all_neurons = BASE_URL_API +  ENDPOINTS_URLS["search_all_neurons"]
        if url_api == url_search_all_neurons:
            response = requests.get(url_api, params=params)
        else:
            headers = {'Content-Type': 'application/json'}
            response = requests.post(url_api, data=json.dumps(query_params), params=params, headers=headers)
        if response.status_code == 200:
            response = response.json()
            size += params["size"]
            params["page"] += 1

            data.extend(response["_embedded"]["neuronResources"])

            end_time = time.time()
            if hasattr(custom_options, 'limit_number_results'):
                print("Downloading neurons from neuromorpho. Size {0} of {1}".format(size, custom_options.limit_number_results))
                process_percent = int(100 * float(size) / float(custom_options.limit_number_results))
                estimated_time_finish = round((end_time - start_time) * (float(custom_options.limit_number_results) - float(size))/params["size"], 2)
                print ("process_percent: ", process_percent, "Task: ", current_task)
                current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent, 'estimated_time_finish': estimated_time_finish})
            else:
                print("Downloading neurons from neuromorpho. Page {0} of {1}".format(params["page"], totalPages))
                process_percent = int(100 * float(params["page"]) / float(totalPages))
                estimated_time_finish = round((end_time - start_time) * (float(totalPages) - float(params["page"])), 2)
                print ("process_percent: ", process_percent, "Task: ", current_task)
                current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent, 'estimated_time_finish': estimated_time_finish})
        else:
            data = "ERROR {0} GET {1}".format(response.status_code, url_api)
            break

    for neuron in data:
        url_morphometry_query = BASE_URL_API + ENDPOINTS_URLS["morphometry_query"]
        url_morphometry_query = url_morphometry_query.replace("{neuron_id}", str(neuron["neuron_id"]))
        headers = {'Content-Type': 'application/json'}
        response_morphometry = requests.get(url_morphometry_query, headers=headers)
        if response_morphometry.status_code == 200:
            morphometry_json = response_morphometry.json()
            neuron.update(morphometry_json)

    return data

def get_swc_neuron(neuron):
    if "original_file_url" in neuron["data_file"]:
        url = neuron["data_file"]["original_file_url"]
    else:
        url = neuron["data_file"]["file"]

    response = requests.get(url, timeout=15)

    if response.status_code == 200:
        neuron_bytes_io = io.BytesIO(response.content)
        swc = InMemoryUploadedFile(file=neuron_bytes_io, field_name=None, content_type='text',
                                   name=neuron["name"], charset='utf8', size=sys.getsizeof(response.content))
        print("swc neuron got OK")
    else:
        swc = "ERROR {0} GET {1}".format(response.status_code, url_endpoint)
        print("swc neuron got ERROR")
        print("ERROR {0} GET {1}".format(response.status_code, url_endpoint))


    return swc

def get_url_swc_neuron(neuron):
    url_endpoint = ENDPOINTS_URLS["get_swc_neuron"]
    url_endpoint = url_endpoint.replace("{archive}", neuron["archive"].lower())
    url_endpoint = url_endpoint.replace("{neuron_name}", neuron["neuron_name"])
    url = BASE_URL_WEB + url_endpoint

    return url


