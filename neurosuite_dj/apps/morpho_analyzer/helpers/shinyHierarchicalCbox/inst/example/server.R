shinyServer(function(input, output, session) {
  
  # Output info
  output$textoutput <- renderText({
    boxes <- input$hcbox
    sprintf("Selected items are: %s", paste(boxes,collapse=", "))
  })

  # Update hcbox
  observeEvent(input$run,{
                updateHCBox(session, "hcbox",
                            theme = input$theme, 
                            animation = input$animation,
                            skipTopLevels = input$skipTopLevels,
                            multiple  = input$multiple,
                            expand.selected = input$expandSelected,
                            escape = input$escape,
                            icons = input$icons,
                            stripedBg = input$strippedBg,
                            withDots = input$withDots,
                            forgetStatus= input$forgetStatus)
  })
  
  
})