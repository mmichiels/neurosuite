import tempfile
import sys
import os
import io

import time
from django.conf import settings
from apps.morpho_analyzer.helpers import helpers
import neurosuite_dj.helpers as global_helpers
from celery import shared_task, current_task
from io import BytesIO
import zipfile
import pandas as pd
import numpy as np
import math
from sklearn import preprocessing as sklearn_preprocessing
import random
import math
import datetime
import scipy.stats as scipy_stats
import copy
import itertools
import statsmodels
from statsmodels.base.model import LikelihoodModel

import rpy2
from rpy2.rinterface import R_VERSION_BUILD
import rpy2.rinterface as rinterface
from rpy2.robjects import r
from rpy2.robjects.packages import importr
import datetime
import re
from rpy2.rinterface import RRuntimeWarning
from apps.morpho_analyzer.helpers import helpers

def add_labels_to_dataframe(dataframe=[], dataframe_discrete=[], datasets_user={}, neurons_user=[], label_name="", label_column_id="Neuron name"):
    if label_name != "":
        if datasets_user.has_input_dataset:
            dataframe[label_name] = dataframe_discrete[label_name].astype(str)
        else:
            for neuron in neurons_user:
                if neuron["name"] in dataframe[label_column_id].values:
                    if "additional_data" in neuron:
                        label = neuron["additional_data"][label_name]
                        dataframe.loc[dataframe[label_column_id] == neuron["name"], label_name] = str(label)

    return dataframe

def univariate_descriptive_stats(input_data=[], input_column_x=0, input_columns_y=[], section_title="Descriptive stats table", data_type="continuous"):
    result = {}
    pd.set_option('display.max_colwidth', -1)

    try:
        input_data = input_data.loc[:, input_columns_y]
        if data_type == "discrete":
            columns = ["Count", "Categories", "Frequencies", "Proportions"]
            rows = []
            for i, input_column_y in enumerate(input_columns_y):
                input_data_y = input_data[input_column_y]
                #input_data_y = pd.Series(np.array(list(itertools.chain.from_iterable(input_data_y)))) #To show all values instead of list values as only 1 value
                input_data_y = input_data_y.astype('str')

                categories_counts = input_data_y.value_counts()
                categories_values_html = '<br>'.join(str(e) for e in categories_counts.index.values)
                categories_counts_html = '<br>'.join(str(e) for e in categories_counts.values)
                proportions = ((categories_counts / len(input_data_y)) * 100).round(decimals=2)
                proportions = (str(e) + " %" for e in proportions.values)
                proportions_html = '<br>'.join(proportions)
                row = [len(input_data_y), categories_values_html, categories_counts_html, proportions_html]
                rows.append(row)

            descriptive_stats_dataframe = pd.DataFrame(rows, columns=columns, index=input_columns_y)
        else:
            descriptive_stats_dataframe = input_data.describe().round(decimals=2)
            descriptive_stats_dataframe = descriptive_stats_dataframe.T

        result["object_result_html"] = "<h4>{0}</h4>".format(section_title)
        result["object_result_html"] += descriptive_stats_dataframe.to_html(classes="datatable-new-ajax display no-border", escape=False, border=0)
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def correlation_matrix(input_data=[], input_column_x=0, input_columns_y=[], correlation_method="pearson", section_title="Correlation coefficient"):
    result = {}
    result["object_result_html"] = "<h4>{0}</h4>".format(section_title)

    try:
        input_data = input_data.loc[:, input_columns_y]

        #----Correlation matrices----
        correlation_matrix_dataframe = input_data.corr(method=correlation_method)
        correlation_matrix_dataframe = correlation_matrix_dataframe.round(decimals=2)
        if correlation_method == "pearson":
            min_correlation_value = -1
            max_correlation_value = 1
        elif  correlation_method == "kendall":
            min_correlation_value = 0
            max_correlation_value = 1
        elif  correlation_method == "spearman":
            min_correlation_value = -1
            max_correlation_value = 1
        result["object_result_html"] += "<p>Range values: [{0}, {1}]</p>".format(min_correlation_value, max_correlation_value)

        result["object_result_html"] += correlation_matrix_dataframe.to_html(classes="datatable-new-ajax display no-border", border=0)
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result

def compute_point_estimates_confidence_intervals(data_var=[], confidence_level=0.95, data_type="continuous", values_field=[]):
    #Observation: t-statistic means z statistics for discrete variables, the variable names has not been changed

    """
    #To visualize the CLT (Central Limit Theorem)
    data_var = []
    for x in range(2000):  # Generate 200 samples
        sample = np.random.choice(a=data_var_original, size=20)
        data_var.append(sample.mean())
    data_var = np.array(data_var)
    """
    sample_size = len(data_var)

    if data_type == "discrete":
        categories_counts = data_var.astype(str).value_counts()
        values_field = np.array(values_field).astype(str)
        categories_counts_index = categories_counts.index.values.astype(str)
        indices_values_selected = np.where(np.in1d(categories_counts_index, values_field))[0]
        categories_counts = categories_counts[indices_values_selected]
        sum_all_values = sum(categories_counts)
        estimate_mean = sum_all_values / sample_size #Mean = proportion
        estimate_variance = "" #unknown
        estimate_std_dev = "" #unknown
    else:
        estimate_mean = data_var.mean()
        estimate_variance = data_var.var(ddof=1)  # ddof=0 -> raw variance- ddof=1 -> bessel corrected (unbiased variance)
        estimate_std_dev = math.sqrt(estimate_variance)
        sample_std_dev = math.sqrt(estimate_variance)

    # --confidence_intervals--
    confidence_level_two_sides = (1 + confidence_level) / 2
    t_degrees_of_freedom = sample_size - 1
    if data_type == "discrete":
        t_statistic = scipy_stats.norm.ppf(q=confidence_level_two_sides)  # Get the t-critical value*
        std_error = math.sqrt((estimate_mean * (1 - estimate_mean)) / sample_size)
    else:
        t_statistic = scipy_stats.t.ppf(q=confidence_level_two_sides, df=t_degrees_of_freedom)  # Get the t-critical value*
        std_error = sample_std_dev / math.sqrt(sample_size)
    margin_error = t_statistic * std_error

    confidence_interval = (estimate_mean - margin_error, estimate_mean + margin_error)
    confidence_interval_low = estimate_mean - margin_error
    confidence_interval_high = estimate_mean + margin_error
    t_critical_values = []
    critical_values = []
    if data_type == "discrete":
        t_critical_value_low = scipy_stats.norm.ppf(q=1 - confidence_level_two_sides)  # Upper tail
        t_critical_value_high = scipy_stats.norm.ppf(q=confidence_level_two_sides)  # Lower tail
        t_ppf_all_values = scipy_stats.norm.ppf(q=0.999)
    else:
        t_critical_value_low = scipy_stats.t.ppf(q=1 - confidence_level_two_sides, df=t_degrees_of_freedom)  # Upper tail
        t_critical_value_high = scipy_stats.t.ppf(q=confidence_level_two_sides, df=t_degrees_of_freedom)  # Lower tail
        t_ppf_all_values = scipy_stats.t.ppf(q=0.999, df=t_degrees_of_freedom)

    t_critical_values += [t_critical_value_low, t_critical_value_high]
    critical_value_low = estimate_mean + (t_critical_value_low * std_error)
    critical_value_high = estimate_mean + (t_critical_value_high * std_error)
    critical_values += [critical_value_low, critical_value_high]

    # ------------------------

    if data_type == "discrete":
        estimate_mean = np.float64(estimate_mean)
        if confidence_interval_low < 0:
            confidence_interval_low = np.float64(0.0)
        elif confidence_interval_high > 1:
            confidence_interval_high = np.float64(1.0)

    confidence_interval_str = "{0} \u00B1 {1} <br> ({2}, {3})".format(estimate_mean.round(decimals=2),
                                                                      margin_error.round(decimals=2),
                                                                      confidence_interval_low.round(decimals=2),
                                                                      confidence_interval_high.round(decimals=2))

    return sample_size, estimate_mean, estimate_variance, estimate_std_dev, t_statistic, t_critical_values, critical_values, std_error, margin_error, t_degrees_of_freedom, t_ppf_all_values, confidence_interval_str, confidence_interval_low, confidence_interval_high


def point_estimates_confidence_intervals(input_data=[], column_class="", input_column_x=0, input_columns_y=[], confidence_level=0.95,
                                         section_title="Point estimates and confidence intervals", data_type="continuous", values_field=[]):
    result = {}

    try:
        result_data = []
        if data_type == "discrete":
            result_columns = ["Feature", "Feature values", "Confidence level", "Confidence interval (proportion mean estimate)",
                              "Sample size", "Proportion mean estimate", "Standard error (proportion mean estimate)", "Margin of error (proportion mean estimate)",
                              "Z critical values", "Critical values", "Z statistic", ]
            variance_observation = ""
            distribution_observation = "Normal (Gaussian) distribution is used to compute the confidence interval"
            documentation_observation = '<a href="http://sphweb.bumc.bu.edu/otlt/MPH-Modules/QuantCore/PH717_ConfidenceIntervals-OneSample/PH717_ConfidenceIntervals-OneSample5.html" target="_blank" >Check the documentation for more info</a>'
        else:
            result_columns = ["Feature", "Confidence level", "Confidence interval (mean estimate)", "Sample size", "Mean estimate",
                              "Variance estimate", "Standard deviation estimate", "Standard error (mean estimate)", "Margin of error (mean estimate)",
                              "Degrees of freedom", "t critical values", "Critical values", "t-statistic",]
            variance_observation = "Variance estimate is Bessel's corrected (i.e. unbiased) <br>"
            distribution_observation = "Student's t-distribution is used to compute the confidence interval (since population variance is unknown)"
            documentation_observation = '<a href="http://www.statisticshowto.com/probability-and-statistics/confidence-interval/" target="_blank" >Check the documentation for more info</a>'

        dataframe_input = input_data.loc[:, input_columns_y]
        for i, input_column_y in enumerate(input_columns_y):
            if data_type == "discrete":
                data_var = dataframe_input[input_column_y]
            else:
                data_var = dataframe_input[input_column_y].values

            sample_size, estimate_mean, estimate_variance, estimate_std_dev, t_statistic, t_critical_values, critical_values, \
            std_error, margin_error, t_degrees_of_freedom, t_ppf_all_values, confidence_interval_str, confidence_interval_low, \
            confidence_interval_high = compute_point_estimates_confidence_intervals(data_var, confidence_level, data_type=data_type, values_field=values_field)

            t_statistic = np.round(t_statistic, decimals=3)
            t_critical_values = np.round(t_critical_values, decimals=3)
            critical_values = np.round(critical_values, decimals=3)
            t_degrees_of_freedom = np.round(t_degrees_of_freedom, decimals=3)
            if data_type == "discrete":
                result_data.append([input_column_y, values_field, confidence_level, confidence_interval_str, sample_size, estimate_mean, std_error, margin_error, t_critical_values, critical_values, t_statistic])
            else:
                result_data.append([input_column_y, confidence_level, confidence_interval_str, sample_size, estimate_mean, estimate_variance, estimate_std_dev, std_error, margin_error, t_degrees_of_freedom, t_critical_values, critical_values, t_statistic])

        result_dataframe = pd.DataFrame(result_data, columns=result_columns).round(decimals=2)

        result["object_result_html"] = "<h4>{0}</h4>".format(section_title)
        result["object_result_html"] = """
        <p><strong>Observations</strong><br>
        {0}
        {1} <br>
        {2}
        </p>
        """.format(variance_observation, distribution_observation, documentation_observation)
        result["object_result_html"] += result_dataframe.to_html(classes="datatable-new-ajax display no-border", escape=False, border=0)
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result

def compute_hypothesis_testing_one_sample(data_var=[], hypothesis_test_type_tails="different", hypothesis_test_type="one_sample", significance_level=0.05, hypothesis_population_mean=0, data_type="continuous", values_field=[]):
    sample_size = len(data_var)
    hypothesis_population_mean_str = str(round(hypothesis_population_mean, 3))

    # Scipy stats method (it matchs exactly with the manual method)
    """
    if data_type == "discrete":
        statsmodels.stats.proportion.proportions_ztest(....)
    else:
        t_statistic_scipy, p_value_scipy = scipy_stats.ttest_1samp(a=data_var, popmean=hypothesis_population_mean)
    """

    # Manual method
    if data_type == "discrete":
        if hypothesis_test_type == "one_sample":
            categories_counts = data_var.value_counts()
            values_field = np.array(values_field).astype(str)
            categories_counts_index = categories_counts.index.values.astype(str)
            indices_values_selected = np.where(np.in1d(categories_counts_index, values_field))[0]
            categories_counts = categories_counts[indices_values_selected]
            sum_all_values = sum(categories_counts)
            sample_mean = np.float64(sum_all_values / sample_size) #Mean = proportion
            sample_mean_str = str(round(sample_mean, 3))
        elif hypothesis_test_type == "two_dependent_samples":
            sample_size = len(data_var[0])
            categories_counts = [data_var[0].value_counts(), data_var[1].value_counts()]
            values_field_1 = np.array(values_field[0]).astype(str)
            values_field_2 = np.array(values_field[1]).astype(str)
            categories_counts_index = [categories_counts[0].index.values.astype(str),
                                       categories_counts[1].index.values.astype(str)]
            indices_values_selected = [np.where(np.in1d(categories_counts_index[0], values_field_1))[0],
                                       np.where(np.in1d(categories_counts_index[1], values_field_2))[0]]
            categories_counts = [categories_counts[0][indices_values_selected[0]],
                                 categories_counts[1][indices_values_selected[1]]]
            sum_all_values = [sum(categories_counts[0]), sum(categories_counts[1])]
            sample_mean = abs(np.float64(sum_all_values[0] / sample_size) - np.float64(sum_all_values[1] / sample_size))  # Mean = proportion
            sample_mean_str = str(round(sample_mean, 3))

        sample_variance = "" #unknown
        sample_std_dev = "" #unknown
    else:
        sample_mean = data_var.mean()
        sample_mean_str = str(round(sample_mean, 3))
        sample_variance = data_var.var(ddof=1)  # ddof=0 -> raw variance- ddof=1 -> bessel corrected (unbiased variance)
        sample_std_dev = math.sqrt(sample_variance)

    t_degrees_of_freedom = sample_size - 1
    significance_level_one_tail = significance_level / 2
    t_critical_values = []
    critical_values = []
    alternative_hypothesis_mean_differences = sample_mean - hypothesis_population_mean
    null_hypothesis_mean_differences = 0 #Sample 1 population mean - hypothesis population mean = 0
    mean_hypothesis_differences = alternative_hypothesis_mean_differences - null_hypothesis_mean_differences
    if data_type == "discrete":
        std_error = math.sqrt((sample_mean * (1 - sample_mean)) / sample_size)
    else:
        std_error = sample_std_dev / math.sqrt(sample_size)
    t_statistic = mean_hypothesis_differences / std_error

    if t_statistic <= 0:
        if data_type == "discrete":
            p_value = scipy_stats.norm.cdf(x=t_statistic)
        else:
            p_value = scipy_stats.t.cdf(x=t_statistic, df=t_degrees_of_freedom)
    else:
        if data_type == "discrete":
            p_value = 1 - scipy_stats.norm.cdf(x=t_statistic)
        else:
            p_value = 1 - scipy_stats.t.cdf(x=t_statistic, df=t_degrees_of_freedom)

    symbol_null_hypothesis = "="
    if hypothesis_test_type_tails == "different":
        if data_type == "discrete":
            t_critical_value_low = scipy_stats.norm.ppf(q=significance_level_one_tail)  # Lower tail
            t_critical_value_high = scipy_stats.norm.ppf(q=1 - significance_level_one_tail)  # Upper tail
        else:
            t_critical_value_low = scipy_stats.t.ppf(q=significance_level_one_tail, df=t_degrees_of_freedom)  # Lower tail
            t_critical_value_high = scipy_stats.t.ppf(q=1 - significance_level_one_tail,df=t_degrees_of_freedom)  # Upper tail
        t_critical_values += [t_critical_value_low, t_critical_value_high]
        critical_value_low = hypothesis_population_mean + (t_critical_value_low*std_error)
        critical_value_high = hypothesis_population_mean + (t_critical_value_high*std_error)
        critical_values += [critical_value_low, critical_value_high]
        symbol_alternative_hypothesis = "!="
        p_value = p_value * 2 #Because there are two tails
    elif hypothesis_test_type_tails == "greater":
        if data_type == "discrete":
            t_critical_value_high = scipy_stats.norm.ppf(q=1 - significance_level)  # Upper tail
        else:
            t_critical_value_high = scipy_stats.t.ppf(q=1 - significance_level,df=t_degrees_of_freedom)  # Upper tail
        t_critical_values += [t_critical_value_high]
        critical_value_high = hypothesis_population_mean + (t_critical_value_high*std_error)
        critical_values += [critical_value_high]
        symbol_alternative_hypothesis = ">"
        if t_statistic <= 0:
            p_value = 1 - p_value
        #p_value_scipy = p_value_scipy / 2
    elif hypothesis_test_type_tails == "lesser":
        if data_type == "discrete":
            t_critical_value_low = scipy_stats.norm.ppf(q=significance_level)  # Upper tail
        else:
            t_critical_value_low = scipy_stats.t.ppf(q=significance_level, df=t_degrees_of_freedom)  # Upper tail
        t_critical_values += [t_critical_value_low]
        critical_value_low = hypothesis_population_mean + (t_critical_value_low*std_error)
        critical_values += [critical_value_low]
        symbol_alternative_hypothesis = "<"
        if t_statistic >= 0:
            p_value = 1 - p_value
        #p_value_scipy = p_value_scipy / 2

    if hypothesis_test_type == "two_dependent_samples":
        if data_type == "discrete":
            population_mean_label = "Population proportion diff mean "
            sample_mean_label = " Sample proportion diff mean "
        else:
            population_mean_label = "Population diff mean "
            sample_mean_label = " Sample diff mean "
    else:
        if data_type == "discrete":
            population_mean_label = "Population proportion mean "
            sample_mean_label = " Sample proportion mean "
        else:
            population_mean_label = "Population mean "
            sample_mean_label = " Sample mean "

    null_hypothesis_str = population_mean_label + symbol_null_hypothesis + sample_mean_label + "<br><br> " + \
                          hypothesis_population_mean_str + " " + symbol_null_hypothesis + " " + sample_mean_str
    alternative_hypothesis_str = population_mean_label + " " + symbol_alternative_hypothesis + " " + sample_mean_label + "<br><br> " + \
                                 hypothesis_population_mean_str + " " + symbol_alternative_hypothesis + " " + sample_mean_str

    if p_value < significance_level:
        result_hypothesis_str = "Reject null hypothesis in favor of alternative hypothesis <br><br> " +  alternative_hypothesis_str
    else:
        result_hypothesis_str = "Do not reject null hypothesis. We stick with the null hypothesis <br><br> " +  null_hypothesis_str

    if data_type == "discrete":
        t_ppf_all_values = scipy_stats.norm.ppf(q=0.999)
    else:
        t_ppf_all_values = scipy_stats.t.ppf(q=0.999, df=t_degrees_of_freedom)

    return null_hypothesis_str, alternative_hypothesis_str, sample_size, sample_mean, sample_variance, sample_std_dev, std_error, t_ppf_all_values, t_degrees_of_freedom, t_critical_values, critical_values, t_statistic, p_value, result_hypothesis_str

def hypothesis_testing_one_sample(input_data=[], section_title="Hypothesis test one sample", hypothesis_test_type_tails="different", input_column_x=0, significance_level=0.05, hypothesis_population_mean=0, input_columns_y=[], column_class="", data_type="continuous", values_field=[]):
    result = {}

    try:
        result_data = []
        if data_type == "discrete":
            result_columns = ["Feature", "Feature values", "Significance level", "p-value", "Hypothesis result", "Null hypothesis",
                              "Alternative hypothesis", "Sample size",
                              "Null hypothesis population proportion mean", "Sample proportion mean", "Standard error (mean estimate)",
                              "Z critical values", "Critical values", "Z statistic", ]
            variance_observation = ""
            distribution_observation = "Normal (Gaussian) distribution is used to compute the p-values"
            documentation_observation = '<a href="https://stattrek.com/hypothesis-test/proportion.aspx" target="_blank" >Check the documentation for more info</a>'
        else:
            result_columns = ["Feature", "Significance level", "p-value", "Hypothesis result", "Null hypothesis", "Alternative hypothesis", "Sample size",
                              "Null hypothesis population mean", "Sample mean", "Sample variance", "Sample standard deviation", "Standard error (mean estimate)",
                              "Degrees of freedom", "t critical values", "Critical values", "t-statistic",]
            variance_observation = "Variance estimate is Bessel's corrected (i.e. unbiased) <br>"
            distribution_observation = "Student's t-distribution is used to compute the confidence p-values (since population variance is unknown)"
            documentation_observation = '<a href="https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ttest_1samp.html" target="_blank" >Check the documentation for more info</a>'

        dataframe_input = input_data.loc[:, input_columns_y]
        for i, input_column_y in enumerate(input_columns_y):
            if data_type == "discrete":
                data_var = dataframe_input[input_column_y]
            else:
                data_var = dataframe_input[input_column_y].values

            null_hypothesis_str, alternative_hypothesis_str, sample_size, sample_mean, sample_variance, sample_std_dev, \
            std_error, t_ppf_all_values, t_degrees_of_freedom, t_critical_values, critical_values, t_statistic, p_value, \
            result_hypothesis_str =  compute_hypothesis_testing_one_sample(data_var=data_var, hypothesis_test_type_tails=hypothesis_test_type_tails, hypothesis_test_type="one_sample",
                                                                           significance_level=significance_level, hypothesis_population_mean=hypothesis_population_mean, data_type=data_type, values_field=values_field)

            t_statistic = np.round(t_statistic, decimals=3)
            t_critical_values = np.round(t_critical_values, decimals=3)
            critical_values = np.round(critical_values, decimals=3)
            t_degrees_of_freedom = np.round(t_degrees_of_freedom, decimals=3)
            #------------------------
            if data_type == "discrete":
                result_data.append([input_column_y, values_field, significance_level, p_value, result_hypothesis_str, null_hypothesis_str, alternative_hypothesis_str,
                                    sample_size, hypothesis_population_mean, sample_mean, std_error,
                                    t_critical_values, critical_values, t_statistic])
            else:
                result_data.append([input_column_y, significance_level, p_value, result_hypothesis_str, null_hypothesis_str, alternative_hypothesis_str,
                                    sample_size, hypothesis_population_mean, sample_mean, sample_variance, sample_std_dev, std_error, t_degrees_of_freedom,
                                    t_critical_values, critical_values, t_statistic])

        pd.option_context('display.max_colwidth', -1)
        result_dataframe = pd.DataFrame(result_data, columns=result_columns).round(decimals=3)

        result["object_result_html"] = "<h4>{0}</h4>".format(section_title)
        result["object_result_html"] += """
        <p><strong>Observations</strong><br>
        p-value has been rounded to 3 decimals<br>
        {0}
        {1} <br>
        {2}
        </p>
        """.format(variance_observation, distribution_observation, documentation_observation)

        pd.set_option('display.max_colwidth', -1)
        result["object_result_html"] += result_dataframe.to_html(classes="datatable-new-ajax display no-border", escape=False, border=0)
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result

def compute_hypothesis_testing_two_independent_samples(data_vars=[], hypothesis_test_type_tails="different", significance_level=0.05, data_type="continuous", values_field=[]):
    # Scipy stats method (it matchs exactly with the manual method)
    #t_statistic_scipy, p_value_scipy = scipy_stats.ttest_ind(a=data_vars[0], b=data_vars[1], equal_var=equal_population_variances)

    # Manual method
    samples_sizes = [len(data_vars[0]), len(data_vars[1])]
    if data_type == "discrete":
        categories_counts = [data_vars[0].value_counts(), data_vars[1].value_counts()]
        values_field = np.array(values_field).astype(str)
        categories_counts_index = [categories_counts[0].index.values.astype(str), categories_counts[1].index.values.astype(str)]
        indices_values_selected = [np.where(np.in1d(categories_counts_index[0], values_field))[0], np.where(np.in1d(categories_counts_index[1], values_field))[0]]
        categories_counts = [categories_counts[0][indices_values_selected[0]], categories_counts[1][indices_values_selected[1]]]
        sum_all_values = [sum(categories_counts[0]), sum(categories_counts[1])]
        samples_means = [np.float64(sum_all_values[0] / samples_sizes[0]), np.float64(sum_all_values[1] / samples_sizes[1])]  #Mean = proportion
        samples_means_str = [str(round(samples_means[0], 3)), str(round(samples_means[1], 3))]

        samples_variances = [] #unknown
        samples_stds_devs = [] #unknown
        t_degrees_of_freedom = np.float64(0) #unknown
    else:
        #Test equal population variances
        significance_level_levene = 0.05
        w_statistic, p_value_levene = scipy_stats.levene(data_vars[0], data_vars[1])
        if p_value_levene < significance_level_levene:
            result_hypothesis_levene_str = "Reject null hypothesis in favor of alternative hypothesis"
            equal_population_variances = False
        else:
            result_hypothesis_levene_str = "Do not reject null hypothesis. We stick with the null hypothesis"
            equal_population_variances = True

        samples_means = [data_vars[0].mean(), data_vars[1].mean()]
        samples_means_str = [str(round(samples_means[0], 3)), str(round(samples_means[1], 3))]
        samples_variances = [data_vars[0].var(ddof=1), data_vars[1].var(ddof=1)]  # ddof=0 -> raw variance- ddof=1 -> bessel corrected (unbiased variance)
        samples_stds_devs = [math.sqrt(samples_variances[0]), math.sqrt(samples_variances[1])]

        if equal_population_variances:
            t_degrees_of_freedom = samples_sizes[0] + samples_sizes[1] - 2
        else:
            t_degrees_of_freedom = (((samples_variances[0] / samples_sizes[0]) + (samples_variances[1] / samples_sizes[1]))**2) / \
                                   ((samples_variances[0]**2 / ((samples_sizes[0]**2) * (samples_sizes[0] - 1))) +
                                    (samples_variances[1]**2 / ((samples_sizes[1]**2) * (samples_sizes[1] - 1)))) #Welch–Satterthwaite equation

    significance_level_one_tail = significance_level / 2
    t_critical_values = []
    critical_values = []
    alternative_hypothesis_mean_differences = samples_means[0] - samples_means[1]
    null_hypothesis_mean_differences = 0 #samples 1 population mean - hypothesis population mean = 0
    mean_hypothesis_differences = alternative_hypothesis_mean_differences - null_hypothesis_mean_differences
    if data_type == "discrete":
        std_error = math.sqrt(
            ((samples_means[0] * (1 - samples_means[0])) / samples_sizes[0])
            + ((samples_means[1] * (1 - samples_means[1])) / samples_sizes[1])
        )
    else:
        std_error = math.sqrt(
            (samples_variances[0] / samples_sizes[0])
            + (samples_variances[1] / samples_sizes[1])
        )
    t_statistic = mean_hypothesis_differences / std_error

    if data_type == "discrete":
        if t_statistic <= 0:
            p_value = scipy_stats.norm.cdf(x=t_statistic)
        else:
            p_value = 1 - scipy_stats.norm.cdf(x=t_statistic)
    else:
        if t_statistic <= 0:
            p_value = scipy_stats.t.cdf(x=t_statistic, df=t_degrees_of_freedom)
        else:
            p_value = 1 - scipy_stats.t.cdf(x=t_statistic, df=t_degrees_of_freedom)

    symbol_null_hypothesis = "="
    if hypothesis_test_type_tails == "different":
        if data_type == "discrete":
            t_critical_value_low = scipy_stats.norm.ppf(q=significance_level_one_tail)  # Lower tail
            t_critical_value_high = scipy_stats.norm.ppf(q=1 - significance_level_one_tail)  # Upper tail
        else:
            t_critical_value_low = scipy_stats.t.ppf(q=significance_level_one_tail, df=t_degrees_of_freedom)  # Lower tail
            t_critical_value_high = scipy_stats.t.ppf(q=1 - significance_level_one_tail,df=t_degrees_of_freedom)  # Upper tail
        t_critical_values += [t_critical_value_low, t_critical_value_high]
        critical_value_low = null_hypothesis_mean_differences + (t_critical_value_low*std_error)
        critical_value_high = null_hypothesis_mean_differences + (t_critical_value_high*std_error)
        critical_values += [critical_value_low, critical_value_high]
        symbol_alternative_hypothesis = "!="
        p_value = p_value * 2 #Because there are two tails
    elif hypothesis_test_type_tails == "greater":
        if data_type == "discrete":
            t_critical_value_high = scipy_stats.norm.ppf(q=1 - significance_level)  # Upper tail
        else:
            t_critical_value_high = scipy_stats.t.ppf(q=1 - significance_level,df=t_degrees_of_freedom)  # Upper tail
        t_critical_values += [t_critical_value_high]
        critical_value_high = null_hypothesis_mean_differences + (t_critical_value_high*std_error)
        critical_values += [critical_value_high]
        symbol_alternative_hypothesis = ">"
        if t_statistic <= 0:
            p_value = 1 - p_value
        #p_value_scipy = p_value_scipy / 2
    elif hypothesis_test_type_tails == "lesser":
        if data_type == "discrete":
            t_critical_value_low = scipy_stats.norm.ppf(q=significance_level)  # Upper tail
        else:
            t_critical_value_low = scipy_stats.t.ppf(q=significance_level,df=t_degrees_of_freedom)  # Upper tail
        t_critical_values += [t_critical_value_low]
        critical_value_low = null_hypothesis_mean_differences + (t_critical_value_low*std_error)
        critical_values += [critical_value_low]
        symbol_alternative_hypothesis = "<"
        if t_statistic >= 0:
            p_value = 1 - p_value
        #p_value_scipy = p_value_scipy / 2

    null_hypothesis_str = "Sample 1 population mean " + symbol_null_hypothesis + " Sample 2 population mean <br><br> " + \
                          samples_means_str[0] + " " + symbol_null_hypothesis + " " + samples_means_str[1]
    alternative_hypothesis_str = "Sample 1 population mean" + " " + symbol_alternative_hypothesis + " " + "Sample 2 population mean <br><br> " + \
                                 samples_means_str[0] + " " + symbol_alternative_hypothesis + " " + samples_means_str[1]

    if p_value < significance_level:
        result_hypothesis_str = "Reject null hypothesis in favor of alternative hypothesis <br><br> " +  alternative_hypothesis_str
    else:
        result_hypothesis_str = "Do not reject null hypothesis. We stick with the null hypothesis <br><br> " + null_hypothesis_str

    if data_type == "discrete":
        t_ppf_all_values = scipy_stats.norm.ppf(q=0.999)
    else:
        t_ppf_all_values = scipy_stats.t.ppf(q=0.999, df=t_degrees_of_freedom)

    return null_hypothesis_str, null_hypothesis_mean_differences, alternative_hypothesis_str, mean_hypothesis_differences, samples_sizes, samples_means, samples_variances, samples_stds_devs, std_error, t_ppf_all_values, t_degrees_of_freedom, t_critical_values, critical_values, t_statistic, p_value, result_hypothesis_str


def hypothesis_testing_two_independent_samples(input_data=[], section_title="Hypothesis test two independent samples", hypothesis_test_type_tails="different", input_column_x=0, significance_level=0.05, input_columns_y=[], column_class="", data_type="continuous", values_field=[]):
    result = {}

    try:
        result_data = []
        if data_type == "discrete":
            result_columns = ["Feature", "Values feature", "Significance level", "p-value", "Hypothesis result", "Null hypothesis", "alternative hypothesis", "Samples sizes",
                              "Samples proportion means", "Standard error (proportion mean estimate)",
                              "Z critical values", "Critical values", "Z statistic",]
            variance_observation = ""
            distribution_observation = "Normal (Gaussian) distribution is used to compute the p-values"
            documentation_observation = '<a href="https://onlinecourses.science.psu.edu/stat500/node/55/" target="_blank" >Check the documentation for more info</a>'
        else:
            result_columns = ["Feature", "Significance level", "p-value", "Hypothesis result", "Null hypothesis", "alternative hypothesis", "Samples sizes",
                              "Samples means", "Samples variances", "Samples standard deviations", "Standard error (mean estimate)",
                              "Degrees of freedom", "t critical values", "Critical values", "t-statistic",]
            variance_observation = "Variance estimate is Bessel's corrected (i.e. unbiased) <br>"
            distribution_observation = "Student's t-distribution is used to compute the p-values (since population variance is unknown)"
            documentation_observation = """
            <a href="https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.stats.levene.html" target="_blank">Levene test</a> is used to estimate whether population variances are equal or not <br>
            If equal population variances: perform Welch’s t-test. If different population variances: perform a standard independent 2 sample test. <br>
            <a href="https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ttest_ind.html#r3566833beaa2-2" target="_blank" >Check the documentation for more info</a>
            """

        input_data[0] = input_data[0].loc[:, input_columns_y]
        input_data[1] = input_data[1].loc[:, input_columns_y]
        for i, input_column_y in enumerate(input_columns_y):
            data_vars = []
            if data_type == "discrete":
                data_vars.append(input_data[0][input_column_y])
                data_vars.append(input_data[1][input_column_y])
            else:
                data_vars.append(input_data[0][input_column_y].values)
                data_vars.append(input_data[1][input_column_y].values)

            null_hypothesis_str, null_hypothesis_mean_differences, alternative_hypothesis_str, mean_hypothesis_differences, \
            samples_sizes, samples_means, samples_variances, samples_stds_devs, std_error, t_ppf_all_values, t_degrees_of_freedom, \
            t_critical_values, critical_values, t_statistic, p_value, result_hypothesis_str =  compute_hypothesis_testing_two_independent_samples(data_vars=data_vars, hypothesis_test_type_tails=hypothesis_test_type_tails,
                                                                                                                                                  significance_level=significance_level, data_type=data_type, values_field=values_field)

            samples_means = np.round(samples_means, decimals=3)
            samples_variances = np.round(samples_variances, decimals=3)
            samples_stds_devs = np.round(samples_stds_devs, decimals=3)
            t_statistic = np.round(t_statistic, decimals=3)
            t_critical_values = np.round(t_critical_values, decimals=3)
            critical_values = np.round(critical_values, decimals=3)
            t_degrees_of_freedom = np.round(t_degrees_of_freedom, decimals=3)
            #------------------------
            if data_type == "discrete":
                result_data.append([input_column_y, values_field, significance_level, p_value, result_hypothesis_str, null_hypothesis_str, alternative_hypothesis_str,
                                    samples_sizes, samples_means, std_error,
                                    t_critical_values, critical_values, t_statistic,])
            else:
                result_data.append([input_column_y, significance_level, p_value, result_hypothesis_str, null_hypothesis_str, alternative_hypothesis_str,
                                    samples_sizes, samples_means, samples_variances, samples_stds_devs, std_error,
                                    t_degrees_of_freedom, t_critical_values, critical_values, t_statistic,])

        result_dataframe = pd.DataFrame(result_data, columns=result_columns).round(decimals=3)

        result["object_result_html"] = "<h4>{0}</h4>".format(section_title)
        result["object_result_html"] += """
        <p><strong>Observations</strong><br>
        p-value has been rounded to 3 decimals<br>
        {0}
        {1} <br>
        {2}
        </p>
        """.format(variance_observation, distribution_observation, documentation_observation)

        pd.set_option('display.max_colwidth', -1)
        result["object_result_html"] += result_dataframe.to_html(classes="datatable-new-ajax display no-border", escape=False, border=0)
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result

def compute_diff_features(dataframe_input, data_type="continuous", values_field=[]):
    if data_type == "continuous":
        data_var_diff = dataframe_input.iloc[:, 0] - dataframe_input.iloc[:, 1]

    return data_var_diff

def hypothesis_testing_two_dependent_samples(input_data=[], section_title="Hypothesis test two dependent samples", hypothesis_test_type_tails="different", input_column_x=0, significance_level=0.05, input_columns_y=[], column_class="", data_type="continuous", values_field=[]):
    result = {}

    try:
        result_data = []
        if data_type == "discrete":
            result_columns = ["Features", "Significance level", "p-value", "Hypothesis result", "Null hypothesis", "alternative hypothesis", "Sample size",
                              "Null hypothesis population diff mean", "Sample diff mean",
                              "Sample proportion mean feature {0}".format(input_columns_y[0]), "Sample mean feature {0}".format(input_columns_y[1]),
                              "Standard error (diff mean estimate)", "Degrees of freedom", "t critical values", "Critical values", "t-statistic",]
        else:
            result_columns = ["Features", "Significance level", "p-value", "Hypothesis result", "Null hypothesis", "alternative hypothesis", "Sample size",
                              "Null hypothesis population diff mean", "Sample diff mean",
                              "Sample mean feature {0}".format(input_columns_y[0]), "Sample mean feature {0}".format(input_columns_y[1]), "Sample diff variance",
                              "Sample diff standard deviation", "Standard error (diff mean estimate)",
                              "Degrees of freedom", "t critical values", "Critical values", "t-statistic",]

        if data_type == "discrete":
            dataframe_input = [input_data.loc[:, input_columns_y[0]], input_data.loc[:, input_columns_y[1]]]
            data_var_diff = dataframe_input #we compute the diff in the compute_hypothesis_testing_one_sample method
            sample_size = len(dataframe_input[0])
            categories_counts = [dataframe_input[0].value_counts(), dataframe_input[1].value_counts()]
            values_field_1 = np.array(values_field[0]).astype(str)
            values_field_2 = np.array(values_field[1]).astype(str)
            categories_counts_index = [categories_counts[0].index.values.astype(str),
                                       categories_counts[1].index.values.astype(str)]
            indices_values_selected = [np.where(np.in1d(categories_counts_index[0], values_field_1))[0],
                                       np.where(np.in1d(categories_counts_index[1], values_field_2))[0]]
            categories_counts = [categories_counts[0][indices_values_selected[0]],
                                 categories_counts[1][indices_values_selected[1]]]
            sum_all_values = [sum(categories_counts[0]), sum(categories_counts[1])]
            samples_means = [np.float64(sum_all_values[0] / sample_size), np.float64(sum_all_values[1] / sample_size)]
        else:
            dataframe_input = input_data.loc[:, input_columns_y]
            data_var_diff = compute_diff_features(dataframe_input)

        hypothesis_population_diff_mean = 0 #Diff means data var = 0

        null_hypothesis_str, alternative_hypothesis_str, sample_size, sample_mean, sample_variance, sample_std_dev, \
        std_error, t_ppf_all_values, t_degrees_of_freedom, t_critical_values, critical_values, t_statistic, p_value, \
        result_hypothesis_str =  compute_hypothesis_testing_one_sample(data_var=data_var_diff, hypothesis_test_type_tails=hypothesis_test_type_tails, hypothesis_test_type="two_dependent_samples",
                                                                       significance_level=significance_level, hypothesis_population_mean=hypothesis_population_diff_mean,
                                                                       data_type=data_type, values_field=values_field)

        #The result matches with the following scipy.stats method:
        #t_statistic_scipy, p_value_scipy = scipy_stats.ttest_rel(a=dataframe_input.iloc[:, 0], b=dataframe_input.iloc[:, 1])


        t_statistic = np.round(t_statistic, decimals=3)
        t_critical_values = np.round(t_critical_values, decimals=3)
        critical_values = np.round(critical_values, decimals=3)
        t_degrees_of_freedom = np.round(t_degrees_of_freedom, decimals=3)
        #------------------------
        if data_type == "discrete":
            result_data.append([input_columns_y, significance_level, p_value, result_hypothesis_str, null_hypothesis_str, alternative_hypothesis_str,
                                sample_size, hypothesis_population_diff_mean, sample_mean, samples_means[0], samples_means[1], std_error, t_degrees_of_freedom,
                                t_critical_values, critical_values, t_statistic])
        else:
            result_data.append([input_columns_y, significance_level, p_value, result_hypothesis_str, null_hypothesis_str, alternative_hypothesis_str,
                                sample_size, hypothesis_population_diff_mean, sample_mean, dataframe_input.iloc[:, 0].mean(), dataframe_input.iloc[:, 1].mean(), sample_variance, sample_std_dev, std_error, t_degrees_of_freedom,
                                t_critical_values, critical_values, t_statistic])

        pd.option_context('display.max_colwidth', -1)
        result_dataframe = pd.DataFrame(result_data, columns=result_columns).round(decimals=3)

        result["object_result_html"] = "<h4>{0}</h4>".format(section_title)
        result["object_result_html"] = """
        <p><strong>Observations</strong><br>
        p-value has been rounded to 3 decimals<br>
        Variance estimate is Bessel's corrected (i.e. unbiased) <br>
        Student's t-distribution is used to compute the p-values (since population variance is unknown) <br>
        <a href="https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ttest_1samp.html" target="_blank" >Check the documentation for more info</a>
        </p>
        """
        pd.set_option('display.max_colwidth', -1)
        result["object_result_html"] += result_dataframe.to_html(classes="datatable-new-ajax display no-border", escape=False, border=0)
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result

@global_helpers.ignore_warnings
def compute_find_distribution(data_var=[], significance_level=0.05, data_type="continuous"):
    if data_type == "discrete":
        dists_fitdistrplus_names = ["bernoulli", "binom", "unif", "geom", "nbinom", "pois"]
        dists_verbose_names = ["Bernoulli", "Binomial", "Uniform", "Geometric", "Negative Binomial", "Poisson"]
        results_hypothesis_tests = []
        for i, dist_name in enumerate(dists_fitdistrplus_names):
            observed_counts, expected_counts, p_value, params, statistic = chi_square_test_distribution(data_var, dist_name)
            dist_fitted = {
                "Distribution name": dists_verbose_names[i],
                "dist_name_scipy": dist_name,
                "p-value": p_value,
                "Observed values": observed_counts,
                "Expected values": expected_counts,
                "Distribution parameters": params,
                "Statistic": statistic,
            }
            results_hypothesis_tests.append(dist_fitted)

        best_dists = sorted(results_hypothesis_tests, key=lambda item: item["p-value"], reverse=True)
        for a, dist in enumerate(best_dists):
            dist["Rank hypothesis test"] = a + 1
    else:
        scipy_dists = ["norm", "expon", "vonmises", "uniform", "cauchy", "logistic",
                       "exponweib", "chi2", "t", "pareto", "gamma", "beta", "lognorm", "maxwell", "f"]
        scipy_dists_verbose_names = ["Normal (Gaussian)", "Exponential", "Von Mises", "Uniform", "Cauchy", "Logistic",
                                     "Exponentiated Weibull", "Chi2", "Student’s T", "Pareto", "Gamma", "Beta", "Lognormal",
                                     "Maxwell", "Snedecor's F"]
        results_mle = []
        results_hypothesis_tests = []
        results_hypothesis_tests_no_nans = []
        for i, dist_name in enumerate(scipy_dists):
            dist = getattr(scipy_stats, dist_name)
            params = dist.fit(data_var)

            dist_fitted = {
                "Distribution name": scipy_dists_verbose_names[i],
                "dist_name_scipy": dist_name
            }

            # Likelihood
            mle = dist.logpdf(data_var, *params).sum()
            aic = 2*len(params) - 2*mle
            dist_fitted["AIC"] = aic
            results_mle.append(dist_fitted)

            # Hypothesis test
            statistic, p_value = scipy_stats.kstest(data_var, dist_name, args=params)
            dist_fitted["Statistic"] = statistic
            dist_fitted["p-value"] = p_value
            results_hypothesis_tests.append(dist_fitted)
            dist_fitted_no_nans = copy.deepcopy(dist_fitted)
            if math.isnan(p_value):
                dist_fitted_no_nans["p-value"] = float("-inf")
            results_hypothesis_tests_no_nans.append(dist_fitted_no_nans)

            params_rounded = list(np.around(np.array(params),4))
            dist_fitted["Distribution parameters"] = params_rounded

        results_mle = sorted(results_mle, key=lambda item: item["AIC"])
        results_hypothesis_tests_no_nans = sorted(results_hypothesis_tests_no_nans, key=lambda item: item["p-value"], reverse=True)

        for i, dist_mle_a in enumerate(results_mle):
            dist_name = dist_mle_a["Distribution name"]
            for dist_mle in results_mle:
                if dist_mle["Distribution name"] == dist_name:
                    dist_mle["Rank AIC"] = i+1
                    break
            for a, dist_hyp in enumerate(results_hypothesis_tests_no_nans):
                if dist_hyp["Distribution name"] == dist_name:
                    dist_mle["Rank hypothesis test"] = a+1
                    break

        best_dists = sorted(results_mle, key=lambda item: item["Rank hypothesis test"])

    return best_dists


@global_helpers.ignore_warnings
def find_distribution(input_data=[], section_title="Find distribution", significance_level=0.05, input_columns_y=[], data_type="continuous"):
    result = {}

    try:
        result["object_result_html"] = "<h4>{0}</h4>".format(section_title)
        dataframe_input = input_data.loc[:, input_columns_y]

        if data_type == "discrete":
            result_columns = ["Rank hypothesis test", "Distribution name","p-value", "Observed values", "Expected values",
                              "Distribution parameters", "Statistic" ]
            result["object_result_html"] += """
            <p><strong>Observations</strong><br>
        p-value has been rounded to 3 decimals<br>
            <a href="https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.chisquare.html" target="_blank">Chi square test</a> is used to compute the goodness of fit.
            <br>
            This test is invalid when the observed or expected frequencies in each category are too small. A typical rule is that all of the observed and expected frequencies should be at least 5.
            <br>
            </p>
            """
        else:
            result_columns = ["Rank hypothesis test", "Rank AIC", "Distribution name", "AIC", "Statistic" ,"p-value",
                              "Distribution parameters"]
            result["object_result_html"] += """
            <p><strong>Observations</strong><br>
        p-value has been rounded to 3 decimals<br>
            Two goodness of fit measures are taken: <br>
            - MLE (<a href="https://en.wikipedia.org/wiki/Akaike_information_criterion" target="_blank">AIC</a>) <br>
            - Hypothesis test (<a href="https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.stats.kstest.html" target="_blank">Kolmogorov-Smirnov test</a>)
            <br>
            </p>
            """
        #We don't need the Bonferroni correction here because every test is: data fits the distrution VS data doesn't fit the distribution
        # (Bonferroni correction is not applied. You can manually <a href="https://en.wikipedia.org/wiki/Bonferroni_correction" target="_blank">calculate it</a> and change the significance level)

        pd.set_option('display.max_colwidth', -1)

        for i, input_column_y in enumerate(input_columns_y):
            if data_type == "discrete":
                data_var = dataframe_input[input_column_y]
            else:
                data_var = dataframe_input[input_column_y].values

            best_dists = compute_find_distribution(data_var=data_var, significance_level=significance_level, data_type=data_type)

            result_dataframe = pd.DataFrame(best_dists).round(decimals=4)
            result_dataframe = result_dataframe.drop('dist_name_scipy', 1)
            result_dataframe = result_dataframe[result_columns]
            result["object_result_html"] += "<h5>{0}</h5>".format(input_column_y)
            result["object_result_html"] += result_dataframe.to_html(classes="datatable-new-ajax display no-border", escape=False, index=False, border=0)
            result["object_result_html"] += "<br>"

    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result

def chi_square_test_distribution(data_var, hypothesis_test_distribution):
    categories_counts = data_var.astype(str).value_counts()
    categories_counts_index = categories_counts.index.values.astype(str)
    num_categories = len(categories_counts_index)
    sum_counts = np.sum(categories_counts)
    observed_frequencies = categories_counts / sum_counts
    categories_counts = categories_counts.tolist()
    fitdistrplus_dists = ["binom", "unif", "nbinom", "pois", "geom"]
    try:
        if hypothesis_test_distribution in fitdistrplus_dists:
            fitdistrplus_R = importr("fitdistrplus")

            categories_counts = rpy2.robjects.IntVector(categories_counts)

            # std = helpers.supress_stout_stderr()
            if hypothesis_test_distribution == "binom":
                param_sum_counts = rpy2.robjects.ListVector({'size': sum_counts.item()})
                param_start = rpy2.robjects.ListVector({'prob': 0.1})
                output_raw_R = fitdistrplus_R.mledist(data=categories_counts, dist=hypothesis_test_distribution,
                                                      fix_arg=param_sum_counts, start=param_start)
                mle = output_raw_R.rx2("estimate")
                prob = mle.rx2("prob")
                params = [sum_counts, np.array(prob)[0].item()]
            else:
                output_raw_R = fitdistrplus_R.mledist(data=categories_counts, dist=hypothesis_test_distribution)
                mle = output_raw_R.rx2("estimate")
                if hypothesis_test_distribution == "unif":
                    param_min = np.array(mle.rx2("min"))[0].item()
                    param_max = np.array(mle.rx2("max"))[0].item()
                    params = [param_min, param_max]
                elif hypothesis_test_distribution == "nbinom":
                    n = sum_counts.item()
                    p = np.array(mle.rx2("mu"))[0].item() / n
                    params = [n, p]
                elif hypothesis_test_distribution == "pois":
                    lambda_param = np.array(mle.rx2("lambda"))[0].item()
                    params = [lambda_param]
                elif hypothesis_test_distribution == "geom":
                    prob = mle.rx2("prob")
                    params = [np.array(prob)[0].item()]

            # helpers.enable_stout_stderr(std)
        else:
            if hypothesis_test_distribution == "bernoulli":
                sample_mean = sum_counts / num_categories
                p = sample_mean / sum_counts
                params = [p]
        # --------------------------------------------------------------------------------------------
        scipy_dist_name = fitdistrplus_name_to_scipy(hypothesis_test_distribution)
        dist_scipy = getattr(scipy_stats, scipy_dist_name)(*params)
        expected_counts = []
        for i in range(num_categories):
            pmf = dist_scipy.pmf(categories_counts[i])
            expected_count = (sum_counts * pmf).round(2)
            expected_freq = pmf.item()
            expected_counts.append(expected_count)
        statistic, p_value = scipy_stats.chisquare(f_obs=categories_counts,
                                                   f_exp=expected_counts)  # , ddof=number_of_bins - len(param))
        if math.isnan(p_value):
            p_value = float("-inf")
        params = np.round(params, 2)
    except Exception as e:
        p_value = float("-inf")
        error_message = '<p class="error-color p-no-margin">Error:</p> this feature is not suitable for chi square test'
        expected_counts = error_message
        params = error_message
        statistic = error_message

    return categories_counts, expected_counts, p_value, params, statistic

@global_helpers.ignore_warnings
def compute_hypothesis_testing_distribution(data_var=[], hypothesis_test_distribution="norm", hypothesis_test_distribution_verbose="Normal (Gaussian)", significance_level=0.05, data_type="continuous"):
    observed_counts = []
    expected_counts = []
    if data_type == "discrete":
        observed_counts, expected_counts, p_value, params, statistic = chi_square_test_distribution(data_var,hypothesis_test_distribution)
    else:
        dist = getattr(scipy_stats, hypothesis_test_distribution)
        params = dist.fit(data_var)
        if hypothesis_test_distribution=="norm":
            statistic, p_value = scipy_stats.shapiro(data_var)
        else:
            statistic, p_value = scipy_stats.kstest(data_var, hypothesis_test_distribution, args=params)

    null_hypothesis_str = "Data follows a <strong>{0}</strong> distribution".format(hypothesis_test_distribution_verbose)
    alternative_hypothesis_str = "Data does <strong>not</strong> follows a <strong>{0}</strong> distribution".format(hypothesis_test_distribution_verbose)

    if (p_value < significance_level) or np.isnan(p_value):
        result_hypothesis_str = "Reject null hypothesis in favor of alternative hypothesis <br><br> <span class='glyphicon glyphicon-remove icon-margin-right'></span>" \
                                +  alternative_hypothesis_str
    else:
        result_hypothesis_str = "Do not reject null hypothesis. We stick with the null hypothesis <br><br> <span class='glyphicon glyphicon-ok icon-margin-right'></span>" \
                                + null_hypothesis_str


    return null_hypothesis_str, alternative_hypothesis_str, statistic, p_value, result_hypothesis_str, params, observed_counts, expected_counts

@global_helpers.ignore_warnings
def hypothesis_testing_distribution(input_data=[], section_title="Hypothesis test distribution", hypothesis_test_distribution="normal", hypothesis_test_distribution_verbose="Normal (Gaussian)", significance_level=0.05, input_columns_y=[], data_type="continuous"):
    result = {}

    try:
        result_data = []

        if data_type == "discrete":
            result_columns = ["Feature", "Significance level", "p-value", "Hypothesis result", "Null hypothesis",
                              "Alternative hypothesis", "Observed values", "Expected values", "Distribution parameters", "t-statistic"]
            observations = """
            <p><strong>Observations</strong><br>
        p-value has been rounded to 3 decimals<br>
            <a href="https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.chisquare.html" target="_blank">Chi square test</a> is used to compute the goodness of fit.
            <br>
            This test is invalid when the observed or expected frequencies in each category are too small. A typical rule is that all of the observed and expected frequencies should be at least 5.
            <br>
            </p>
            """
        else:
            result_columns = ["Feature", "Significance level", "p-value", "Hypothesis result", "Null hypothesis",
                              "Alternative hypothesis", "t-statistic", ]
            result["object_result_html"] = "<h4>{0}</h4>".format(section_title)
            observations = """
            <p><strong>Observations</strong><br>
        p-value has been rounded to 3 decimals<br>
            For Normal (Gaussian) distribution: <a href="https://docs.scipy.org/doc/scipy-0.19.1/reference/generated/scipy.stats.shapiro.html" target="_blank">Shapiro-Wilk test</a> is used <br>
            For other distributions: <a href="https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.stats.kstest.html" target="_blank">Kolmogorov-Smirnov test</a> is used
            </p>
            """
        result["object_result_html"] = observations

        dataframe_input = input_data.loc[:, input_columns_y]
        for i, input_column_y in enumerate(input_columns_y):
            if data_type == "discrete":
                data_var = dataframe_input[input_column_y]
            else:
                data_var = dataframe_input[input_column_y].values

            null_hypothesis_str, alternative_hypothesis_str, statistic, p_value, result_hypothesis_str, params, \
            observed_values, expected_values = \
                compute_hypothesis_testing_distribution(data_var=data_var, hypothesis_test_distribution=hypothesis_test_distribution,
                                                        hypothesis_test_distribution_verbose=hypothesis_test_distribution_verbose,
                                                        significance_level=significance_level, data_type=data_type)

            if data_type == "discrete":
                result_data.append([input_column_y, significance_level, p_value, result_hypothesis_str, null_hypothesis_str, alternative_hypothesis_str,
                                    observed_values, expected_values, params, statistic,])
            else:
                result_data.append([input_column_y, significance_level, p_value, result_hypothesis_str, null_hypothesis_str, alternative_hypothesis_str,
                                    statistic,])

        result_dataframe = pd.DataFrame(result_data, columns=result_columns).round(decimals=4)


        pd.set_option('display.max_colwidth', -1)
        result["object_result_html"] += result_dataframe.to_html(classes="datatable-new-ajax display no-border", escape=False, border=0)
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result



def fitdistrplus_name_to_scipy(dist_name):
    switcher = {
        "bernoulli": "bernoulli",
        "binom": "binom",
        "unif": "randint",
        "geom": "geom",
        "nbinom": "nbinom",
        "pois": "poisson",
    }

    return switcher[dist_name]

#----------Stats utilities-------------------
def check_min_max_columns_to_plot(num_cols=0, min_necessary_cols=1, max_necessary_cols=0):
    result = {}
    result["error"] = False

    if  num_cols < min_necessary_cols and max_necessary_cols == 0: #max_cols=0 means no max columns
        result["error"] = True
        result["error_message"] = "Select at least {0} features".format(min_necessary_cols)
        return result

    if (num_cols < min_necessary_cols or num_cols > max_necessary_cols) and max_necessary_cols != 0:
        if min_necessary_cols == max_necessary_cols:
            result["error"] = True
            result["error_message"] = "Select {0} features".format(min_necessary_cols)
        else:
            result["error"] = True
            result["error_message"] = "Select between {0} and {1} features".format(min_necessary_cols,
                                                                                   max_necessary_cols)

    return result

