import matplotlib

matplotlib.use('Agg')
import tempfile
import sys
import os
import io

from apps.morpho_analyzer.helpers.stats import stats_helpers

import time
from django.conf import settings
from apps.morpho_analyzer.helpers import helpers
import neurosuite_dj.helpers as global_helpers
from celery import shared_task, current_task
from io import BytesIO
import zipfile
import pandas as pd
import plotly
import plotly.graph_objs as plotly_graph
import plotly.figure_factory as plotly_figures
import numpy as np
import math
from sklearn import preprocessing as sklearn_preprocessing
import random
import math
import plotly.tools as plotly_tools
import datetime
import matplotlib.pyplot as matplotlib_plot
import pandas.plotting as pandas_plotting
import scipy.stats as scipy_stats
from scipy.integrate import nquad
from fitter import Fitter
import colorlover as colorlover
import statsmodels.api as statsmodels
import itertools


def bar_chart_univariate(input_data=[], input_column_x=0, input_columns_y=[], column_class="", group_categories=True):
    result = {}
    subplots = False

    if len(input_columns_y) > 1:
        subplots = True
        num_cols = 3
        figure, counter_row, counter_col, num_rows = get_subplots_rows_cols(input_data=input_columns_y,
                                                                            num_cols=num_cols)
        height_subplot = 550
        height_buffer = 100

    try:
        for i, input_column_y in enumerate(input_columns_y):
            # input_data_y = pd.Series(np.array(list(itertools.chain.from_iterable(input_data_y)))) #To show all values instead of list values as only 1 value
            input_data_y = input_data[input_column_y].round(3)
            if group_categories:
                categories_counts = input_data_y.value_counts()
                input_data_y = input_data_y.astype('str')
                x_vars = categories_counts.index
                y_vars = categories_counts
                label_x_axis = input_column_y
                label_y_axis = input_column_x
            else:
                x_vars = input_data[column_class]
                y_vars = input_data_y
                label_x_axis = input_column_x
                label_y_axis = input_column_y

            sorted_y_vars = pd.Series([x for _, x in sorted(zip(y_vars, x_vars), reverse = True)])

            trace_bar_chart = plotly_graph.Bar(
                x=x_vars,
                y=y_vars,
                name=input_column_y,
                text=input_data_y,
                textposition='outside',
                constraintext="none",
                hoverinfo="name+text",
                marker={
                    'color': y_vars,
                    'colorscale': 'Viridis',
                    'reversescale': True,
                },
            )

            if subplots:
                figure.append_trace(trace_bar_chart, counter_row, counter_col)
                counter_row, counter_col = get_subplot_counter_row_col(counter_row=counter_row, counter_col=counter_col,
                                                                       num_cols=num_cols)
                figure['layout']["yaxis" + str(i + 1)].update(title=label_y_axis, titlefont=dict(size=14))
                figure['layout']["xaxis" + str(i + 1)].update(title=label_x_axis, titlefont=dict(size=14))
            else:
                layout = set_layout(title="Bar Chart", column_x_name=input_column_y, column_y_name=input_column_x,
                                    all_x_labels=1)
                layout["xaxis"]['categoryorder'] = 'array'
                layout["xaxis"]['categoryarray'] = sorted_y_vars
                figure = plotly_graph.Figure(data=[trace_bar_chart], layout=layout)

        if subplots:
            figure["layout"].update(height=num_rows * height_subplot + height_buffer)
            figure['layout'].update(title='Bar charts')

        result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                           output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def pie_chart_univariate(input_data=[], input_column_x=0, input_columns_y=[]):
    result = {}
    subplots = False

    result[
        "object_result_html"] = '<div class="container-center center-plot" style="margin-right: 10px; overflow-x:auto">'

    if len(input_columns_y) > 1:
        num_cols = 2
        counter_col = 0
        subplots = True
        result["object_result_html"] += '<table class="not-datatable">'

    try:
        result["object_result_html"] += '<tr>'
        for i, input_column_y in enumerate(input_columns_y):
            result["object_result_html"] += '<td>'
            input_data_y = input_data[input_column_y]
            input_data_y = input_data_y.astype('str')
            categories_counts = input_data_y.value_counts()

            trace_pie_chart = plotly_graph.Pie(
                labels=categories_counts.index,
                values=categories_counts,
                name=input_column_y,
                textinfo='label+value+percent',
                textfont=dict(
                    size=12,
                    color='rgba(255, 255, 255, 1)',
                ),
                marker=dict(
                    line=dict(
                        color='#000000',
                        width=1)
                ),
            )

            layout = set_layout(title="Pie Chart {0}".format(input_column_y), column_x_name=input_column_y,
                                column_y_name=input_column_x, height=550, all_x_labels=0)
            figure = plotly_graph.Figure(data=[trace_pie_chart], layout=layout)
            annotations = []
            for i in range(0, len(categories_counts.index)):
                annotation = dict(
                    x=categories_counts.index[i],
                    y=categories_counts[i] - 0.6,
                    text=str(categories_counts[i]),
                    font=dict(
                        family='Arial',
                        size=14,
                        color='rgba(255, 255, 255, 1)'
                    ),
                    showarrow=False,
                )
            annotations.append(annotation)
            figure["layout"]["annotations"] = annotations

            result["object_result_html"] += plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                                output_type='div')
            if subplots:
                counter_col += 1
                result["object_result_html"] += '</td>'
                if counter_col >= num_cols:
                    counter_col = 0
                    result["object_result_html"] += '</tr>'
                    result["object_result_html"] += '<tr>'

        if subplots:
            result["object_result_html"] += '</table>'
        result["object_result_html"] += '</div>'
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def histogram_univariate(input_data=[], input_column_x=0, input_columns_y=[]):
    result = {}
    subplots = False

    if len(input_columns_y) > 1:
        subplots = True
        num_cols = 3
        figure, counter_row, counter_col, num_rows = get_subplots_rows_cols(input_data=input_columns_y,
                                                                            num_cols=num_cols)
        height_subplot = 200
        height_buffer = 100

    try:
        for i, input_column_y in enumerate(input_columns_y):
            input_data_y = input_data[input_column_y]

            trace_histogram = plotly_graph.Histogram(
                x=input_data_y,
                name=input_column_y,
                autobinx=True,
            )

            if subplots:
                figure.append_trace(trace_histogram, counter_row, counter_col)
                counter_row, counter_col = get_subplot_counter_row_col(counter_row=counter_row, counter_col=counter_col,
                                                                       num_cols=num_cols)
                figure['layout']["yaxis" + str(i + 1)].update(title=input_column_x, titlefont=dict(size=14))
                figure['layout']["xaxis" + str(i + 1)].update(title=input_column_y, titlefont=dict(size=14))
            else:
                layout = set_layout(title="Histogram", column_x_name=input_column_y, column_y_name=input_column_x,
                                    all_x_labels=0)
                figure = plotly_graph.Figure(data=[trace_histogram], layout=layout)

        if subplots:
            figure["layout"].update(height=num_rows * height_subplot + height_buffer)
            figure['layout'].update(title='Histograms')

        result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                           output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def boxplot_univariate(input_data=[], input_column_x=0, input_columns_y=[], column_class="", label_name=""):
    result = {}
    subplots = False
    if label_name != "":
        colors_points = get_colors_from_labels(input_data, label_name)
        symbols_points = get_symbols_from_labels(input_data, label_name)

    if len(input_columns_y) > 1:
        subplots = True
        num_cols = 3
        figure, counter_row, counter_col, num_rows = get_subplots_rows_cols(input_data=input_columns_y,
                                                                            num_cols=num_cols)
        height_subplot = 250
        height_buffer = 100

    try:
        for i, input_column_y in enumerate(input_columns_y):
            input_data_y = input_data[input_column_y]

            trace = plotly_graph.Box(
                y=input_data_y,
                name=input_column_y,

                text=input_data[column_class],
                boxpoints='all',
                jitter=0.3,
                pointpos=-1.8,
                boxmean=True,
            )
            if label_name != "":
                texts_with_labels = []
                for i, text in enumerate(trace["text"]):
                    new_text = text + "<br>" + label_name + ": " + input_data[label_name][i]
                    texts_with_labels.append(new_text)
                trace["text"] = texts_with_labels
                # trace["marker"]["color"] = colors_points #It does not work in boxplot, it only accepts one colour

            if subplots:
                figure.append_trace(trace, counter_row, counter_col)
                counter_row, counter_col = get_subplot_counter_row_col(counter_row=counter_row, counter_col=counter_col,
                                                                       num_cols=num_cols)
                figure['layout']["yaxis" + str(counter_col)].update(title=input_column_y, titlefont=dict(size=14))
                figure['layout']["xaxis" + str(counter_row)].update(title=input_column_x, titlefont=dict(size=14))
            else:
                layout = set_layout(title="Box plot with points", column_x_name=input_column_x,
                                    column_y_name=input_column_y, all_x_labels=0)
                figure = plotly_graph.Figure(data=[trace], layout=layout)

        if subplots:
            figure["layout"].update(height=num_rows * height_subplot + height_buffer)
            figure['layout'].update(title='Box plots with points')

        result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                           output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def scatter_univariate(input_data=[], input_column_x=0, input_columns_y=[], column_class="", scatter_point_label="",
                       label_name=""):
    result = {}
    subplots = False
    if label_name != "":
        colors_points = get_colors_from_labels(input_data, label_name)
        colors_unique, labels_unique = get_colors_labels_unique(input_data, label_name)

    if len(input_columns_y) > 1:
        subplots = True
        num_cols = 3
        figure, counter_row, counter_col, num_rows = get_subplots_rows_cols(input_data=input_columns_y,
                                                                            num_cols=num_cols)
        height_subplot = 200
        height_buffer = 100

    try:
        for i, input_column_y in enumerate(input_columns_y):
            input_data_y = input_data[input_column_y]

            trace_scatter = plotly_graph.Scattergl(
                x=input_data_y,
                y=input_data_y,
                name=scatter_point_label,
                text=input_data[column_class],
                xaxis='x',
                yaxis='y',
                mode='markers',
                marker=dict(
                    size=10,
                    line=dict(
                        width=0.8,
                        color='rgb(0, 0, 0, .9)',
                    )
                ),
            )

            if label_name != "":
                texts_with_labels = []
                for a, text in enumerate(trace_scatter["text"]):
                    new_text = text + "<br>" + label_name + ": " + input_data[label_name][a]
                    texts_with_labels.append(new_text)
                trace_scatter["text"] = texts_with_labels
                trace_scatter["marker"]["color"] = colors_points

            if subplots:
                figure.append_trace(trace_scatter, counter_row, counter_col)
                counter_row, counter_col = get_subplot_counter_row_col(counter_row=counter_row, counter_col=counter_col,
                                                                       num_cols=num_cols)
                figure['layout']["yaxis" + str(i + 1)].update(title=input_column_y, titlefont=dict(size=14))
                figure['layout']["xaxis" + str(i + 1)].update(title=input_column_y, titlefont=dict(size=14))
            else:
                layout = set_layout(title="Scatter univariate", column_x_name=input_column_y,
                                    column_y_name=input_column_y, all_x_labels=0)
                figure = plotly_graph.Figure(data=[trace_scatter], layout=layout)

        if label_name != "":
            set_legend_annotation_colors(colors_unique, input_column_y, labels_unique, figure["layout"])
            figure["layout"]["showlegend"] = False

        if subplots:
            figure["layout"].update(height=num_rows * height_subplot + height_buffer)
            figure['layout'].update(title="Scatter univariate")

        result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                           output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def bar_chart_bivariate(input_data=[], input_column_x="", input_columns_y=[], pie_chart_type="stack"):
    result = {}
    title_plot = "Bar chart"
    if pie_chart_type == "stack":
        title_plot = "Stacked bar chart"
    elif pie_chart_type == "group":
        title_plot = "Grouped bar chart"
    var_1 = {}
    var_2 = {}

    try:
        # input_data = input_data.astype('str')
        var_1["name"] = input_columns_y[0]
        var_2["name"] = input_columns_y[1]
        var_1["data"] = input_data[var_1["name"]].astype('str')
        var_2["data"] = input_data[var_2["name"]].astype('str')

        categories_counts_1 = var_1["data"].value_counts()
        categories_counts_2 = var_2["data"].value_counts()

        traces_var_2 = []
        for category_var_2 in categories_counts_2.index:
            category_var_2 = str(category_var_2)
            count_category_var_2 = []
            for category_var_1 in categories_counts_1.index:
                category_var_1 = str(category_var_1)
                rows_category_1 = input_data.loc[input_data[var_1["name"]].astype('str') == category_var_1]
                rows_category_2 = rows_category_1.loc[input_data[var_2["name"]].astype('str') == category_var_2]
                size_rows_category_2 = len(rows_category_2)

                count_category_var_2.append(size_rows_category_2)

            trace_bar_chart_2 = plotly_graph.Bar(
                x=categories_counts_1.index,
                y=count_category_var_2,
                name=category_var_2,
                text=count_category_var_2,
                textposition='outside',
                constraintext="none",
                hoverinfo="name+text"
            )
            traces_var_2.append(trace_bar_chart_2)

        layout = set_layout(title=title_plot, column_x_name=var_1["name"], column_y_name=input_column_x, all_x_labels=0)
        layout["barmode"] = pie_chart_type  # group, stack or relative
        figure = plotly_graph.Figure(data=traces_var_2, layout=layout)

        result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                           output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def correlation_matrix_heatmap(input_data=[], input_column_x=0, input_columns_y=[], column_class="",
                               correlation_method="pearson"):
    result = {}
    input_data = input_data.drop([column_class], axis=1)
    try:
        correlation_matrix = input_data.corr(method=correlation_method)
        correlation_matrix = correlation_matrix.round(decimals=2)

        z = []
        for i, input_column_y in reversed(list(enumerate(input_columns_y))):
            row_correlation = []
            for i, correlated_column_y in enumerate(input_columns_y):
                row_correlation.append(correlation_matrix[input_column_y][correlated_column_y])
            z.append(row_correlation)

        figure = plotly_figures.create_annotated_heatmap(z, x=input_columns_y, y=list(reversed(input_columns_y)),
                                                         showscale=True)

        if correlation_method == "pearson":
            min_correlation_value = -1
            max_correlation_value = 1
        elif correlation_method == "kendall":
            min_correlation_value = 0
            max_correlation_value = 1
        elif correlation_method == "spearman":
            min_correlation_value = -1
            max_correlation_value = 1

        margin_annotation_range = 0.7
        if len(input_columns_y) > 15:
            figure['layout']["margin"].update(t=330)
            margin_annotation_range = 1.5
        elif len(input_columns_y) > 3:
            margin_annotation_range = 1

        new_annotation = dict(
            x=(len(input_columns_y) - 1) + margin_annotation_range,
            y=(len(input_columns_y) - 1) + margin_annotation_range,
            text="Range values: [{0}, {1}]".format(min_correlation_value, max_correlation_value),
            xref='x',
            yref='y',
            showarrow=False,
            bordercolor='#000000',
            borderwidth=1,
        )

        old_annotations = list(figure['layout']["annotations"])
        new_annotations = [new_annotation]
        annotations = old_annotations + new_annotations
        figure['layout']["annotations"] = annotations

        figure['layout']["margin"].update(l=180)
        figure['layout']["xaxis"].update(zeroline=False, showgrid=False)
        figure['layout']["yaxis"].update(zeroline=False, showgrid=False)
        figure['layout'].update(height=(len(input_columns_y) * 15) + 400)
        if correlation_method == "pearson":
            figure.layout.title = "Pearson correlation coefficient (Pearson's r)"
        elif correlation_method == "kendall":
            figure.layout.title = "Kendall correlation coefficient"
        elif correlation_method == "spearman":
            figure.layout.title = "Spearman correlation coefficient"

        result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                           output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def scatterplot_matrix(input_data=[], input_column_x=0, input_columns_y=[], column_class="", label_name=""):
    result = {}
    if label_name != "":
        colors_points = get_colors_from_labels(input_data, label_name)
        colors_unique, labels_unique = get_colors_labels_unique(input_data, label_name)

    if len(input_columns_y) > 6:
        result["object_result_html"] = '<div class="container-center" style="margin-right: 10px; overflow-x:auto">'
    else:
        result["object_result_html"] = '<div class="center-plot" style="margin-right: 10px; overflow-x:auto">'

    try:
        features = []
        textd = []
        for i, input_column_y in enumerate(input_columns_y):
            feature = dict(label=input_column_y,
                           values=input_data[input_column_y])
            textd = input_data[column_class]
            features.append(feature)

        trace = plotly_graph.Splom(
            dimensions=features,
            text=textd,
            marker=dict(
                size=7,
                showscale=False,
                line=dict(
                    width=0.8,
                    color='rgb(0, 0, 0, .9)',
                ),
            ),
            diagonal=dict(visible=False),
        )
        if label_name != "":
            texts_with_labels = []
            for i, text in enumerate(trace["text"]):
                new_text = text + "<br>" + label_name + ": " + input_data[label_name][i]
                texts_with_labels.append(new_text)
            trace["text"] = texts_with_labels
            trace["marker"]["color"] = colors_points

        layout = plotly_graph.Layout(
            title="Scatterplot matrix",
            autosize=True,
            height=len(input_columns_y) * 200 + 100,
            width=len(input_columns_y) * 200 + 100,
        )
        figure = plotly_graph.Figure(data=[trace], layout=layout)
        if label_name != "":
            set_legend_annotation_colors(colors_unique, input_column_y, labels_unique, figure["layout"])
            figure["layout"]["showlegend"] = False

        result["object_result_html"] += plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                            output_type='div')
        result["object_result_html"] += '</div>'
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def histogram_2d_Contour_subplots(input_data=[], input_column_x=0, input_columns_y=[], column_class="",
                                  scatter_point_label="", label_name=""):
    result = {}
    if label_name != "":
        colors_points = get_colors_from_labels(input_data, label_name)

    if len(input_columns_y) > 3:
        result["object_result_html"] = '<div class="container-center" style="margin-right: 10px; overflow-x:auto">'
    else:
        result["object_result_html"] = '<div class="center-plot" style="margin-right: 10px; overflow-x:auto">'

    result["object_result_html"] += '<table class="not-datatable">'
    try:
        for i, input_column_y in enumerate(input_columns_y):
            result["object_result_html"] += '<tr>'
            for i, input_compare_column_y in enumerate(input_columns_y):
                result["object_result_html"] += '<td >'
                x_column_name = input_column_y
                y_column_name = input_compare_column_y
                x = input_data[input_column_y]
                y = input_data[input_compare_column_y]

                data = [
                    plotly_graph.Histogram2dContour(
                        x=x,
                        y=y,
                        name="Empty space",
                        xaxis='x',
                        yaxis='y',
                        contours=dict(
                            showlabels=True,
                            labelfont=dict(
                                family='Raleway',
                                color='black'
                            )
                        ),
                    ),
                    plotly_graph.Scatter(
                        x=x,
                        y=y,
                        name=scatter_point_label,
                        text=input_data[column_class],
                        xaxis='x',
                        yaxis='y',
                        mode='markers',
                        marker=dict(
                            size=7,
                            line=dict(
                                width=0.8,
                                color='rgb(0, 0, 0, .9)',
                            )
                        ),
                    ),
                    plotly_graph.Histogram(
                        x=x,
                        yaxis='y2',
                        name=x_column_name,
                    ),
                    plotly_graph.Histogram(
                        y=y,
                        xaxis='x2',
                        name=y_column_name,
                    ),
                ]

                if label_name != "":
                    texts_with_labels = []
                    trace_scatter = data[1]
                    for i, text in enumerate(trace_scatter["text"]):
                        new_text = text + "<br>" + label_name + ": " + input_data[label_name][i]
                        texts_with_labels.append(new_text)
                    trace_scatter["text"] = texts_with_labels
                    trace_scatter["marker"]["color"] = colors_points

                layout = plotly_graph.Layout(
                    autosize=False,
                    title="2D Histogram contour",
                    margin=dict(t=35),
                    xaxis=dict(
                        title=x_column_name,
                        zeroline=False,
                        domain=[0, 0.85],
                        showgrid=False
                    ),
                    yaxis=dict(
                        title=y_column_name,
                        zeroline=False,
                        domain=[0, 0.85],
                        showgrid=False
                    ),
                    xaxis2=dict(
                        title=input_column_x,
                        zeroline=False,
                        domain=[0.85, 1],
                        showgrid=False
                    ),
                    yaxis2=dict(
                        title=input_column_x,
                        zeroline=False,
                        domain=[0.85, 1],
                        showgrid=False
                    ),
                    width=450,
                    height=420,
                    bargap=0,
                    hovermode='closest',
                    showlegend=False
                )

                figure = plotly_graph.Figure(data=data, layout=layout)

                result["object_result_html"] += plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                                    output_type='div')
                result["object_result_html"] += '</td>'

            result["object_result_html"] += '</tr>'

        result["object_result_html"] += '</table>'
        result["object_result_html"] += '</div>'
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def scatterplot_bubble(input_data=[], input_column_x=0, input_columns_y=[], column_class="", scatter_point_label="",
                       colorscale=False, label_name=""):
    result = {}
    x = {}
    y = {}
    z = {}
    if label_name != "":
        colors_points = get_colors_from_labels(input_data, label_name)
        symbols_points = get_symbols_from_labels(input_data, label_name)
        colors_unique, labels_unique = get_colors_labels_unique(input_data, label_name)

    if colorscale and len(input_columns_y) < 4:
        result["error"] = True
        result["error_message"] = "Select at least 4 features to view this plot"
    else:
        try:
            x["name"] = input_columns_y[0]
            y["name"] = input_columns_y[1]
            z["name"] = input_columns_y[2]
            x["data"] = input_data[x["name"]]
            y["data"] = input_data[y["name"]]
            z["data"] = input_data[z["name"]]

            if colorscale:
                color_var = {}
                color_var["name"] = input_columns_y[3]
                color_var["data"] = input_data[color_var["name"]]

            default_point_size = 5
            scale_point_size = 150
            z_sizes = z["data"]
            if z_sizes.max() == z_sizes.min():
                z_sizes_scaled = default_point_size
            else:
                min_max_scaler = sklearn_preprocessing.MinMaxScaler()  # Normalize
                z_sizes_scaled = min_max_scaler.fit_transform(z_sizes.values.reshape(-1, 1))
                z_sizes_scaled = z_sizes_scaled * scale_point_size

            axis_values = []
            for i, x_item in enumerate(x["data"]):
                string_axis_value = scatter_point_label + ": " + str(input_data[column_class][i]) + "<br>" + z[
                    "name"] + ": " + '%1.3f' % z["data"][i]
                if colorscale:
                    string_axis_value += "<br>" + color_var["name"] + ": " + '%1.3f' % color_var["data"][i]

                axis_values.append(string_axis_value)

            data = {
                'y': x["data"],
                'x': y["data"],
                'mode': 'markers',
                'marker': {
                    'size': z_sizes_scaled,
                },
                "line": {
                    "width": 1,
                    # "color": 'rgb(0, 0, 0, .2)',
                },
                "name": z["name"] + " (bubbles size)",
                "text": axis_values
            }
            if label_name != "":
                texts_with_labels = []
                trace_scatter = data
                for i, text in enumerate(trace_scatter["text"]):
                    new_text = text + "<br>" + label_name + ": " + input_data[label_name][i]
                    texts_with_labels.append(new_text)
                trace_scatter["text"] = texts_with_labels
                if colorscale:
                    trace_scatter["marker"]["symbol"] = symbols_points
                else:
                    trace_scatter["marker"]["color"] = colors_points

            layout = plotly_graph.Layout(
                title="Scatter bubble plot",
                showlegend=True,
                autosize=True,
                height=600,
                margin=plotly_graph.layout.Margin(
                ),
                xaxis=dict(
                    title=x["name"],
                    automargin=True,
                    dtick=0,
                ),
                yaxis=dict(
                    title=y["name"],
                    automargin=True,
                ),
            )

            if colorscale:
                data["marker"]["color"] = color_var["data"]
                data["marker"]["showscale"] = True
                data["marker"]["colorbar"] = {}
                data["marker"]["colorbar"]["title"] = color_var["name"] + " (colorscale)"
                layout["legend"] = dict(x=-.1, y=1.1)
                layout["title"] = "Scatter bubble plot with colorscale"

            figure = plotly_graph.Figure([data], layout=layout)
            if label_name != "" and not colorscale:
                set_legend_annotation_colors(colors_unique, np.arange(0, 10), labels_unique, figure["layout"],
                                             yshift=-50)
                # figure["layout"]["showlegend"] = False

            result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                               output_type='div')
        except Exception as e:
            result["error"] = True
            result["error_message"] = "The dataset may contian Nan values. " + str(e)

    return result


def scatterplot_3d(input_data=[], input_column_x=0, input_columns_y=[], column_class="", scatter_point_label="",
                   bubbles_and_colorscale=False, label_name=""):
    result = {}
    x = {}
    y = {}
    z = {}
    bubbles = False
    colorscale = False
    if label_name != "":
        colors_points = get_colors_from_labels(input_data, label_name)
        symbols_points = get_symbols_from_labels(input_data, label_name)
        colors_unique, labels_unique = get_colors_labels_unique(input_data, label_name)

    if bubbles_and_colorscale and len(input_columns_y) < 4:
        result["error"] = True
        result["error_message"] = "Select at least 4 features to view this plot"
    else:
        if bubbles_and_colorscale:
            bubbles = True
        if bubbles_and_colorscale and len(input_columns_y) >= 5:
            colorscale = True

        try:
            x["name"] = input_columns_y[0]
            y["name"] = input_columns_y[1]
            z["name"] = input_columns_y[2]
            x["data"] = input_data[x["name"]]
            y["data"] = input_data[y["name"]]
            z["data"] = input_data[z["name"]]

            if bubbles:
                bubble_var = {}
                bubble_var["name"] = input_columns_y[3]
                bubble_var["data"] = input_data[bubble_var["name"]]

            if colorscale:
                color_var = {}
                color_var["name"] = input_columns_y[4]
                color_var["data"] = input_data[color_var["name"]]

            axis_values = []
            for i, x_item in enumerate(x["data"]):
                string_axis_value = scatter_point_label + ": " + str(input_data[column_class][i]) + "<br>" + x[
                    "name"] + ': ' + '%1.3f' % x_item + '<br>' + y["name"] + ": " + '%1.3f' % y["data"][i] + '<br>' + z[
                                        "name"] + ": " + '%1.3f' % z["data"][i]
                if bubbles:
                    string_axis_value += '<br>' + bubble_var["name"] + ": " + '%1.3f' % bubble_var["data"][i]
                if colorscale:
                    string_axis_value += '<br>' + color_var["name"] + ": " + '%1.3f' % color_var["data"][i]
                axis_values.append(string_axis_value)

            trace_scatterternary = plotly_graph.Scatter3d(
                x=x["data"],
                y=y["data"],
                z=z["data"],
                text=axis_values,
                hoverinfo='text',
                mode='markers',
                marker=dict(
                    size=5,
                    opacity=0.8,
                    line=dict(
                        width=1,
                        color='rgb(0, 0, 0, .7)',
                    ),
                )
            )
            if label_name != "":
                texts_with_labels = []
                trace_scatter = trace_scatterternary
                for i, text in enumerate(trace_scatter["text"]):
                    new_text = text + "<br>" + label_name + ": " + input_data[label_name][i]
                    texts_with_labels.append(new_text)
                trace_scatter["text"] = texts_with_labels
                if colorscale:
                    trace_scatter["marker"]["symbol"] = symbols_points
                else:
                    trace_scatter["marker"]["color"] = colors_points

            layout = plotly_graph.Layout(
                title="3D Scatter Plot",
                autosize=True,
                height=750,
                margin=dict(
                    l=300,
                    r=300,
                    b=0,
                    t=25
                ),
                scene=dict(
                    xaxis=dict(
                        title=x["name"],
                    ),
                    yaxis=dict(
                        title=y["name"],
                    ),
                    zaxis=dict(
                        title=z["name"],
                    )
                )
            )

            if bubbles:
                default_point_size = 5
                scale_point_size = 150
                bubble_var_sizes = bubble_var["data"]
                if bubble_var_sizes.max() == bubble_var_sizes.min():
                    bubble_var_sizes_scaled = default_point_size
                else:
                    min_max_scaler = sklearn_preprocessing.MinMaxScaler()  # Normalize
                    bubble_var_sizes_scaled = min_max_scaler.fit_transform(bubble_var_sizes.values.reshape(-1, 1))
                    bubble_var_sizes_scaled = bubble_var_sizes_scaled * scale_point_size

                trace_scatterternary["marker"]["size"] = bubble_var_sizes_scaled
                layout["annotations"] = [
                    dict(
                        x=-0.1,
                        y=0.95,
                        showarrow=False,
                        text=bubble_var["name"] + " (bubbles size)",
                        xref='paper',
                        yref='paper'
                    )
                ]
                layout["title"] = "3D Scatter Plot with bubbles"

            if colorscale:
                trace_scatterternary["marker"]["color"] = color_var["data"]
                trace_scatterternary["marker"]["showscale"] = True
                trace_scatterternary["marker"]["colorbar"] = {}
                trace_scatterternary["marker"]["colorbar"]["title"] = color_var["name"] + " (colorscale)"
                layout["title"] = "3D Scatter Plot with bubbles and colorscale"

            figure = plotly_graph.Figure(data=[trace_scatterternary], layout=layout)
            if label_name != "" and not colorscale:
                set_legend_annotation_colors(colors_unique, np.arange(0, 10), labels_unique, figure["layout"])
                figure["layout"]["showlegend"] = False

            result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                               output_type='div')
        except Exception as e:
            result["error"] = True
            result["error_message"] = "The dataset may contian Nan values. " + str(e)

    return result


def density_functions(input_data=[], input_column_x=0, input_columns_y=[], column_class="", scatter_point_label="",
                      subplots=False, normalize=False):
    result = {}
    column_y_name = "Probability density function"

    if subplots:
        if len(input_columns_y) > 1:
            num_cols = 3
            figure, counter_row, counter_col, num_rows = get_subplots_rows_cols(input_data=input_columns_y,
                                                                                num_cols=num_cols)
            height_subplot = 200
            height_buffer = 100
        else:
            subplots = False

    try:
        pdfs_traces = []
        for i, input_column_y in enumerate(input_columns_y):
            data_x = input_data[input_column_y].values
            data_x_max = data_x.max()
            contain_nans = np.isnan(data_x).any()
            if contain_nans:
                trace_pdf = plotly_graph.Scattergl(
                    x=[0],
                    y=[0],
                    name=input_column_y + " (Error, contain nans)",
                    mode='markers',
                    marker=dict(
                        size=15,
                    )
                )
            else:
                if data_x_max != data_x.min():
                    if normalize:
                        min_max_scaler = sklearn_preprocessing.MinMaxScaler()
                        data_x = min_max_scaler.fit_transform(data_x.reshape(-1, 1)).ravel()  # Normalize
                    kde = scipy_stats.gaussian_kde(data_x)  # kernel density estimator between points
                    x_range = np.linspace(min(data_x), max(data_x), 500)  # x axis points
                    pdf = kde.evaluate(x_range)

                    trace_pdf = plotly_graph.Scattergl(
                        x=x_range,
                        y=pdf,
                        name=input_column_y,
                        mode='lines',
                        # fill='tozeroy',
                    )
                else:
                    if data_x_max > 0.5:
                        invariant_feature_point = 1
                    else:
                        invariant_feature_point = 0
                    trace_pdf = plotly_graph.Scattergl(
                        x=[invariant_feature_point],
                        y=[0],
                        name=input_column_y + " (Invariant)",
                        mode='markers',
                        marker=dict(
                            size=15,
                        )
                    )
            pdfs_traces.append(trace_pdf)

            if subplots:
                figure.append_trace(trace_pdf, counter_row, counter_col)
                counter_row, counter_col = get_subplot_counter_row_col(counter_row=counter_row, counter_col=counter_col,
                                                                       num_cols=num_cols)
                figure['layout']["yaxis" + str(i + 1)].update(title=column_y_name, titlefont=dict(size=14))
                figure['layout']["xaxis" + str(i + 1)].update(title=input_column_y, titlefont=dict(size=14))

        string_using_normalized_values = ""
        if normalize:
            string_using_normalized_values = " with normalized values"
        if subplots:
            figure["layout"].update(height=num_rows * height_subplot + height_buffer)
            figure['layout'].update(title="Probability density functions (PDF) using Gaussian KDE{0}".format(
                string_using_normalized_values))
        else:
            layout = set_layout(title="Probability density functions (PDF) using Gaussian KDE{0}".format(
                string_using_normalized_values), column_x_name="Features", column_y_name=column_y_name, all_x_labels=0,
                height=600)
            figure = plotly_graph.Figure(data=pdfs_traces, layout=layout)

        figure["layout"].update(showlegend=True)

        result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                           output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def parallel_coordinates(input_data=[], input_column_x=0, input_columns_y=[], column_class="", label_name=""):
    result = {}
    if len(input_columns_y) < 2:
        result["error"] = True
        result["error_message"] = "Select at least 2 features to view this plot"
    else:
        if label_name != "":
            colorscale_custom = get_my_custom_colorscale()

            numbers_labels = []
            labels_unique = pd.Series(input_data[label_name].unique()).sort_values()
            labels_in_numbers = np.arange(len(input_data))
            if len(labels_unique) <= len(labels_in_numbers):
                numbers_map = dict(zip(labels_unique, labels_in_numbers))
                numbers_labels = (input_data[label_name].map(numbers_map)).values.tolist()
            else:
                for i, element in enumerate(labels_unique):
                    numbers_labels.append(labels_in_numbers[i % len(labels_in_numbers)])

            rgb_values = colorlover.scales["11"]['qual']['Paired']
            if len(labels_unique) <= len(rgb_values):
                color_map = dict(zip(labels_unique, rgb_values))
                colors_unique = (labels_unique.map(color_map)).values.tolist()
            else:
                colors_unique = []
                for i, element in enumerate(labels_unique):
                    colors_unique.append(rgb_values[i % len(rgb_values)])

        try:
            if len(input_columns_y) > 5:
                result["object_result_html"] = '<div class="containter-center" style="overflow-x:auto">'
            else:
                result["object_result_html"] = '<div class="center-plot" style="overflow-x:auto">'

            parallel_traces = []
            for i, input_column_y in enumerate(input_columns_y):
                data_x = input_data[input_column_y]
                parallel_coordinate = dict(
                    range=[data_x.max(), data_x.min()],
                    label=input_column_y,
                    values=data_x,

                )

                parallel_traces.append(parallel_coordinate)

            parallel_coordinates_traces = plotly_graph.Parcoords(
                dimensions=list(parallel_traces),
                labelfont=dict(size=15),
            )

            if label_name != "":
                parallel_coordinates_traces["line"]["color"] = numbers_labels
                parallel_coordinates_traces["line"]["colorscale"] = colorscale_custom

            layout = plotly_graph.Layout(
                title="Parallel coordinates",
                autosize=False,
                height=600,
                width=len(input_columns_y) * 250,
                margin=dict(t=120),
                xaxis=dict(
                    automargin=True,
                ),
                yaxis=dict(
                    automargin=True,
                ),
                showlegend=True,
            )
            if label_name != "":
                annotations = []
                layout["margin"] = dict(t=120, r=160)
                x_position_legend = 1
                x_position_legend_shift = (len(input_column_y) * 10)
                for i, label in enumerate(labels_unique):
                    new_annotation = dict(
                        x=x_position_legend,
                        xshift=x_position_legend_shift,
                        y=1 - i * 0.08,
                        text=label,
                        xref='paper',
                        yref='paper',
                        showarrow=False,
                        bordercolor=colors_unique[i],
                        borderwidth=2,
                    )
                    annotations.append(new_annotation)
                layout["annotations"] = annotations

            figure = plotly_graph.Figure(data=[parallel_coordinates_traces], layout=layout)

            result["object_result_html"] += plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                                output_type='div')
            result["object_result_html"] += "</div>"
        except Exception as e:
            result["error"] = True
            result["error_message"] = str(e)

    return result


def set_legend_annotation_colors(colors_unique, input_column_y, labels_unique, layout, yshift=0):
    annotations = []
    layout["margin"] = dict(t=120, r=160)
    x_position_legend = 1
    x_position_legend_shift = (len(input_column_y) * 10)
    for i, label in enumerate(labels_unique):
        new_annotation = dict(
            x=x_position_legend,
            xshift=x_position_legend_shift,
            y=1 - i * 0.08,
            yshift=yshift,
            text=label,
            xref='paper',
            yref='paper',
            showarrow=False,
            bordercolor=colors_unique[i],
            borderwidth=2,
        )
        annotations.append(new_annotation)
    layout["annotations"] = annotations

    return 0


@global_helpers.ignore_warnings
def radviz(input_data=[], input_column_x=0, input_columns_y=[], column_class="", scatter_point_label=""):
    result = {}
    if len(input_columns_y) < 2:
        result["error"] = True
        result["error_message"] = "Select at least 2 features to view this plot"
    else:
        try:
            columns_and_class = input_columns_y.copy()
            columns_and_class.append(column_class)
            input_data_radviz = input_data.loc[:, columns_and_class]

            font = {'family': 'DejaVu Sans',
                    'weight': 'bold',
                    'size': 18}

            matplotlib.rc('font', **font)
            matplotlib_figure = matplotlib_plot.figure()
            ax = pandas_plotting.radviz(input_data_radviz, column_class, color=["blue"], s=150)
            matplotlib_plot.draw()
            ax.legend().set_visible(False)
            plotly_figure = plotly_tools.mpl_to_plotly(matplotlib_figure)

            labels = input_data[column_class].values
            for i, dat in enumerate(plotly_figure['data']):
                t = labels[i]
                dat.update({'name': "", 'text': str(t)})

            plotly_figure['layout']["shapes"] = [
                # unfilled circle
                {
                    'type': 'circle',
                    'xref': 'x',
                    'yref': 'y',
                    'x0': -0.95,
                    'y0': -0.95,
                    'x1': 0.95,
                    'y1': 0.95,
                    'line': {
                        'color': 'rgba(50, 171, 96, 1)',
                    },
                }, ]
            plotly_figure['layout']["margin"].update(l=40, r=10)
            plotly_figure['layout'].update(title="RadViz")
            plotly_figure['layout'].update(width=1000, height=600)

            result["object_result_html"] = '<div class="container-center center-plot">'
            result["object_result_html"] += plotly.offline.plot(plotly_figure, include_plotlyjs=False, show_link=False,
                                                                output_type='div')
            result["object_result_html"] += "</div>"
        except Exception as e:
            result["error"] = True
            result["error_message"] = str(e)

    return result


def andrew_curves(input_data=[], input_column_x=0, input_columns_y=[], column_class="", scatter_point_label="",
                  label_name=""):
    result = {}
    try:
        columns_and_class = input_columns_y.copy()

        if label_name != "":
            label_for_lines_color = label_name
            legend_visible = True
            colors_points = get_colors_from_labels(input_data, label_name)
        else:
            label_for_lines_color = column_class
            legend_visible = False
        columns_and_class.append(label_for_lines_color)
        dataframe_andrew_curves = input_data.loc[:, columns_and_class]
        matplotlib_plot.style.use("ggplot")
        matplotlib_figure = matplotlib_plot.figure()
        matplotlib_plot.draw()

        ax = pandas_plotting.andrews_curves(dataframe_andrew_curves, label_for_lines_color)
        ax.legend().set_visible(False)
        if label_name != "":
            for i, line in enumerate(ax.lines):
                label = input_data.loc[i, label_name]
                line.set_label(label)
                color = colors_points[i]
                color = color.replace("rgb", "")
                color = color.replace("(", "")
                color = color.replace(")", "")
                color_values = color.split(",")
                for i, value in enumerate(color_values):
                    value = float(value)
                    value = value / 255
                    color_values[i] = value
                color_values.append(0.99)
                line.set_color(color_values)
        matplotlib_plot.draw()
        plotly_figure = plotly_tools.mpl_to_plotly(matplotlib_figure)

        plotly_figure['layout']["margin"].update(l=40, r=10)
        plotly_figure['layout'].update(title="Andrew curves")
        plotly_figure['layout'].update(width=1000, height=600)
        plotly_figure['layout']["showlegend"] = legend_visible

        result["object_result_html"] = '<div class="container-center center-plot">'
        result["object_result_html"] += plotly.offline.plot(plotly_figure, include_plotlyjs=False,
                                                            show_link=False, output_type='div')
        result["object_result_html"] += "</div>"
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def radar_charts(input_data=[], input_column_x=0, input_columns_y=[], column_class="", label_name=""):
    result = {}
    if len(input_columns_y) < 2:
        result["error"] = True
        result["error_message"] = "Select at least 2 features to view this plot"
    else:
        if label_name != "":
            colors_points = get_colors_from_labels(input_data, label_name)

        try:
            result["object_result_html"] = '<div class="center-plot" style="overflow-x:auto">'

            traces_radar_charts = []
            dataframe_input = input_data.loc[:, input_columns_y]

            for i in range(len(dataframe_input)):
                data_x = dataframe_input.iloc[i, :]
                name = str(input_data.loc[i, column_class])

                trace_radar_chart = plotly_graph.Scatterpolar(
                    r=data_x,
                    theta=input_columns_y,
                    fill='toself',
                    name=name
                )
                if label_name != "":
                    new_name_text = name + "<br>" + label_name + ": " + str(input_data.loc[i, label_name])
                    trace_radar_chart["name"] = new_name_text
                    trace_radar_chart["line"]["color"] = colors_points[i]

                traces_radar_charts.append(trace_radar_chart)

            layout = plotly_graph.Layout(
                title="Radar charts",
                autosize=False,
                height=800,
                width=1200,
                xaxis=dict(
                    automargin=True,
                ),
                yaxis=dict(
                    automargin=True,
                ),
            )
            figure = plotly_graph.Figure(data=traces_radar_charts, layout=layout)

            result["object_result_html"] += plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                                output_type='div')
        except Exception as e:
            result["error"] = True
            result["error_message"] = str(e)

    return result


def point_estimates_confidence_intervals(input_data=[], column_class="", input_column_x=0, input_columns_y=[],
                                         confidence_level=0.95,
                                         section_title="Point estimates and confidence intervals", column_y_name="",
                                         data_type="continuous", values_field=[]):
    result = {}
    fill_colors = assign_colors_to_list_labels(np.arange(len(input_columns_y)))
    margin_y_between_labels = 50

    try:
        dataframe_input = input_data.loc[:, input_columns_y]
        pdfs_traces = []
        plot_annotations = []
        for i, input_column_y in enumerate(input_columns_y):
            if data_type == "discrete":
                data_var = dataframe_input[input_column_y]
            else:
                data_var = dataframe_input[input_column_y].values

            sample_size, estimate_mean, estimate_variance, estimate_std_dev, t_statistic, t_critical_values, critical_values, \
            std_error, margin_error, t_degrees_of_freedom, t_ppf_all_values, confidence_interval_str, confidence_interval_low, \
            confidence_interval_high = stats_helpers.compute_point_estimates_confidence_intervals(data_var,
                                                                                                  confidence_level,
                                                                                                  data_type=data_type,
                                                                                                  values_field=values_field)

            # ------Plots----------
            pdf_limit_low = estimate_mean - t_ppf_all_values * std_error
            pdf_limit_high = estimate_mean + t_ppf_all_values * std_error

            estimate_mean = estimate_mean.round(decimals=3)
            confidence_interval_low = confidence_interval_low.round(decimals=3)
            confidence_interval_high = confidence_interval_high.round(decimals=3)

            trace_pdf = []
            x_out_confidence_interval_low = np.linspace(pdf_limit_low, confidence_interval_low, 10000)  # x axis points
            if data_type == "discrete":
                pdf_t_dist = scipy_stats.norm.pdf(x_out_confidence_interval_low, loc=estimate_mean, scale=std_error)
            else:
                pdf_t_dist = scipy_stats.t.pdf(x_out_confidence_interval_low, df=t_degrees_of_freedom,
                                               loc=estimate_mean, scale=std_error)
            percentage_out_confidence_level_one_tail = round(((1 - confidence_level) / 2) * 100, 2)
            trace_pdf_out_confidence_interval_low = plotly_graph.Scattergl(
                x=x_out_confidence_interval_low,
                y=pdf_t_dist,
                name=input_column_y + " (Out left confidence interval: {0}%)".format(
                    percentage_out_confidence_level_one_tail),
                mode='lines',
                fill='tozeroy',
                fillcolor=fill_colors[i],
            )
            x_confidence_interval = np.linspace(confidence_interval_low, confidence_interval_high,
                                                10000)  # x axis points
            if data_type == "discrete":
                pdf_t_dist = scipy_stats.norm.pdf(x_confidence_interval, loc=estimate_mean, scale=std_error)
            else:
                pdf_t_dist = scipy_stats.t.pdf(x_confidence_interval, df=t_degrees_of_freedom, loc=estimate_mean,
                                               scale=std_error)
            trace_pdf_confidence_interval = plotly_graph.Scattergl(
                x=x_confidence_interval,
                y=pdf_t_dist,
                name=input_column_y + " (Confidence interval: {0}%)".format(confidence_level * 100),
                mode='lines',
            )
            x_out_confidence_interval_high = np.linspace(confidence_interval_high, pdf_limit_high,
                                                         10000)  # x axis points
            if data_type == "discrete":
                pdf_t_dist = scipy_stats.norm.pdf(x_out_confidence_interval_high, loc=estimate_mean, scale=std_error)
            else:
                pdf_t_dist = scipy_stats.t.pdf(x_out_confidence_interval_high, df=t_degrees_of_freedom,
                                               loc=estimate_mean, scale=std_error)
            trace_pdf_out_confidence_interval_high = plotly_graph.Scattergl(
                x=x_out_confidence_interval_high,
                y=pdf_t_dist,
                name=input_column_y + " (Out right confidence interval: {0}%)".format(
                    percentage_out_confidence_level_one_tail),
                mode='lines',
                fill='tozeroy',
                fillcolor=fill_colors[i],
            )
            trace_pdf += [trace_pdf_out_confidence_interval_low, trace_pdf_confidence_interval,
                          trace_pdf_out_confidence_interval_high]

            pdfs_traces += trace_pdf

            if data_type == "discrete":
                text_mean = "Proportion (mean)"
            else:
                text_mean = "Mean"

            annotation_confidence_intervals = [
                dict(
                    x=confidence_interval_low,
                    y=0,
                    xref='x',
                    yref='y',
                    text='{0} <br> {1} - Margin error <br> ({2})'.format(confidence_interval_low, text_mean,
                                                                         input_column_y),
                    showarrow=True,
                    arrowhead=7,
                    ax=0,
                    ay=30 + i * margin_y_between_labels,
                ),
                dict(
                    x=estimate_mean,
                    y=0,
                    xref='x',
                    yref='y',
                    text='{0} <br> {1} <br> ({2})'.format(estimate_mean, text_mean, input_column_y),
                    showarrow=True,
                    arrowhead=7,
                    ax=0,
                    ay=30 + i * margin_y_between_labels,
                    font=dict(
                        color='#ffffff'
                    ),
                    bgcolor='#ff7f0e',
                ),
                dict(
                    x=confidence_interval_high,
                    y=0,
                    xref='x',
                    yref='y',
                    text='{0} <br> {1} + Margin error <br> ({2})'.format(confidence_interval_high, text_mean,
                                                                         input_column_y),
                    showarrow=True,
                    arrowhead=7,
                    ax=0,
                    ay=30 + i * margin_y_between_labels,
                ),
            ]
            plot_annotations = plot_annotations + annotation_confidence_intervals

        if data_type == "discrete":
            title = "Probability density functions (PDF) of Normal (Gaussian) distribution sampling proportion means"
        else:
            title = "Probability density functions (PDF) of Student's t-distribution sampling means"
        layout = set_layout(
            title=title,
            column_x_name="Features", column_y_name=column_y_name, all_x_labels=0, height=600)
        layout["annotations"] = plot_annotations

        figure = plotly_graph.Figure(data=pdfs_traces, layout=layout)

        result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                           output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def draw_one_plot_hypothesis_one_sample(input_column_y, i, sample_mean, hypothesis_population_mean,
                                        hypothesis_test_type_tails, t_critical_value, t_critical_value_low,
                                        t_critical_value_high, pdf_limit_low, pdf_limit_high, x_range_all,
                                        percentage_null_hypothesis,
                                        percentage_alternative_hypothesis_tails, std_error, t_degrees_of_freedom,
                                        fill_colors, pdfs_traces, data_type="continuous", values_field=[]):
    annotations_hypothesis_test = []
    margin_y_between_labels = 50

    if data_type == "discrete":
        text_mean = "proportion (mean)"
    else:
        text_mean = "mean"

    t_critical_low = dict(
        x=t_critical_value_low,
        y=0,
        xref='x',
        yref='y',
        text='{0} <br> - critical value <br> ({1})'.format(t_critical_value_low, input_column_y),
        showarrow=True,
        arrowhead=7,
        ax=0,
        ay=30 + i * margin_y_between_labels,
    )
    hypothesis_population_mean_annotation = dict(
        x=hypothesis_population_mean,
        y=0,
        xref='x',
        yref='y',
        text='{0} <br> Null hypothesis {1} <br> ({2})'.format(hypothesis_population_mean, text_mean, input_column_y),
        showarrow=False,
        arrowhead=7,
        ax=0,
        ay=20 + i * margin_y_between_labels,
        font=dict(
            color='#ffffff'
        ),
        bgcolor='#ff7f0e',
    )
    t_critical_high = dict(
        x=t_critical_value_high,
        y=0,
        xref='x',
        yref='y',
        text='{0} <br> critical value <br> ({1})'.format(t_critical_value_high, input_column_y),
        showarrow=True,
        arrowhead=7,
        ax=0,
        ay=30 + i * margin_y_between_labels,
    )
    mean_alternative_hypothesis = dict(
        x=sample_mean,
        y=0,
        xref='x',
        yref='y',
        text='{0} <br> Alternative hypothesis {1} <br> ({2})'.format(sample_mean, text_mean, input_column_y),
        showarrow=True,
        arrowhead=7,
        ax=0,
        ay=-110 + i * margin_y_between_labels,
    )

    if hypothesis_test_type_tails == "different":
        x_alternative_hypothesis_low = np.linspace(pdf_limit_low, t_critical_value_low, 10000)  # x axis points
        if data_type == "discrete":
            pdf_t_dist = scipy_stats.norm.pdf(x_alternative_hypothesis_low, loc=hypothesis_population_mean,
                                              scale=std_error)
        else:
            pdf_t_dist = scipy_stats.t.pdf(x_alternative_hypothesis_low, df=t_degrees_of_freedom,
                                           loc=hypothesis_population_mean, scale=std_error)
        trace_pdf_alternative_hypothesis_low = plotly_graph.Scattergl(
            x=x_alternative_hypothesis_low,
            y=pdf_t_dist,
            name=input_column_y + " (Left alternative hypothesis {0}%)".format(
                percentage_alternative_hypothesis_tails),
            mode='lines',
            fill='tozeroy',
            fillcolor=fill_colors[i],
        )

        x_null_hypothesis = np.linspace(t_critical_value_low, t_critical_value_high, 10000)  # x axis points
        if data_type == "discrete":
            pdf_t_dist = scipy_stats.norm.pdf(x_null_hypothesis, loc=hypothesis_population_mean, scale=std_error)
        else:
            pdf_t_dist = scipy_stats.t.pdf(x_null_hypothesis, df=t_degrees_of_freedom,
                                           loc=hypothesis_population_mean, scale=std_error)
        trace_pdf_null_hypothesis = plotly_graph.Scattergl(
            x=x_null_hypothesis,
            y=pdf_t_dist,
            name=input_column_y + " (Null hypothesis {0}%)".format(percentage_null_hypothesis),
            mode='lines',
        )

        x_alternative_hypothesis_high = np.linspace(t_critical_value_high, pdf_limit_high,
                                                    10000)  # x axis points
        if data_type == "discrete":
            pdf_t_dist = scipy_stats.norm.pdf(x_alternative_hypothesis_high, loc=hypothesis_population_mean,
                                              scale=std_error)
        else:
            pdf_t_dist = scipy_stats.t.pdf(x_alternative_hypothesis_high, df=t_degrees_of_freedom,
                                           loc=hypothesis_population_mean, scale=std_error)
        trace_pdf_alternative_hypothesis_high = plotly_graph.Scattergl(
            x=x_alternative_hypothesis_high,
            y=pdf_t_dist,
            name=input_column_y + " (Right alternative hypothesis {0}%)".format(
                percentage_alternative_hypothesis_tails),
            mode='lines',
            fill='tozeroy',
            fillcolor=fill_colors[i],
        )

        pdfs_traces += [trace_pdf_alternative_hypothesis_low, trace_pdf_null_hypothesis,
                        trace_pdf_alternative_hypothesis_high]
        annotations_hypothesis_test += [t_critical_low, hypothesis_population_mean_annotation, t_critical_high,
                                        mean_alternative_hypothesis]
    elif hypothesis_test_type_tails == "greater":
        x_null_hypothesis = np.linspace(pdf_limit_low, t_critical_value_high, 10000)  # x axis points
        if data_type == "discrete":
            pdf_t_dist = scipy_stats.norm.pdf(x_null_hypothesis, loc=hypothesis_population_mean, scale=std_error)
        else:
            pdf_t_dist = scipy_stats.t.pdf(x_null_hypothesis, df=t_degrees_of_freedom,
                                           loc=hypothesis_population_mean, scale=std_error)
        trace_pdf_null_hypothesis = plotly_graph.Scattergl(
            x=x_null_hypothesis,
            y=pdf_t_dist,
            name=input_column_y + " (Null hypothesis {0}%)".format(percentage_null_hypothesis),
            mode='lines',
        )

        x_alternative_hypothesis_high = np.linspace(t_critical_value_high, pdf_limit_high,
                                                    10000)  # x axis points
        if data_type == "discrete":
            pdf_t_dist = scipy_stats.norm.pdf(x_alternative_hypothesis_high, loc=hypothesis_population_mean,
                                              scale=std_error)
        else:
            pdf_t_dist = scipy_stats.t.pdf(x_alternative_hypothesis_high, df=t_degrees_of_freedom,
                                           loc=hypothesis_population_mean, scale=std_error)
        trace_pdf_alternative_hypothesis_high = plotly_graph.Scattergl(
            x=x_alternative_hypothesis_high,
            y=pdf_t_dist,
            name=input_column_y + " (Alternative hypothesis {0}%)".format(
                percentage_alternative_hypothesis_tails),
            mode='lines',
            fill='tozeroy',
            fillcolor=fill_colors[i],
        )

        pdfs_traces += [trace_pdf_null_hypothesis, trace_pdf_alternative_hypothesis_high]
        annotations_hypothesis_test += [hypothesis_population_mean_annotation, t_critical_high,
                                        mean_alternative_hypothesis]
    elif hypothesis_test_type_tails == "lesser":
        x_alternative_hypothesis_low = np.linspace(pdf_limit_low, t_critical_value_low, 10000)  # x axis points
        if data_type == "discrete":
            pdf_t_dist = scipy_stats.norm.pdf(x_alternative_hypothesis_low, loc=hypothesis_population_mean,
                                              scale=std_error)
        else:
            pdf_t_dist = scipy_stats.t.pdf(x_alternative_hypothesis_low, df=t_degrees_of_freedom,
                                           loc=hypothesis_population_mean, scale=std_error)
        trace_pdf_alternative_hypothesis_low = plotly_graph.Scattergl(
            x=x_alternative_hypothesis_low,
            y=pdf_t_dist,
            name=input_column_y + " (Alternative hypothesis {0}%)".format(
                percentage_alternative_hypothesis_tails),
            mode='lines',
            fill='tozeroy',
            fillcolor=fill_colors[i],
        )

        x_null_hypothesis = np.linspace(t_critical_value_low, pdf_limit_high, 10000)  # x axis points
        if data_type == "discrete":
            pdf_t_dist = scipy_stats.norm.pdf(x_null_hypothesis, loc=hypothesis_population_mean, scale=std_error)
        else:
            pdf_t_dist = scipy_stats.t.pdf(x_null_hypothesis, df=t_degrees_of_freedom,
                                           loc=hypothesis_population_mean, scale=std_error)
        trace_pdf_null_hypothesis = plotly_graph.Scattergl(
            x=x_null_hypothesis,
            y=pdf_t_dist,
            name=input_column_y + " (Null hypothesis {0}%)".format(percentage_null_hypothesis),
            mode='lines',
        )

        pdfs_traces += [trace_pdf_alternative_hypothesis_low, trace_pdf_null_hypothesis]
        annotations_hypothesis_test += [t_critical_low, hypothesis_population_mean_annotation,
                                        mean_alternative_hypothesis]

    return pdfs_traces, annotations_hypothesis_test


def hypothesis_testing_one_sample(input_data=[], section_title="Hypothesis test one sample",
                                  hypothesis_test_type_tails="different", input_column_x=0, significance_level=0.05,
                                  hypothesis_population_mean=0, input_columns_y=[], column_class="",
                                  data_type="continuous", values_field=[]):
    result = {}
    column_y_name = "Probability density function"
    fill_colors = assign_colors_to_list_labels(np.arange(len(input_columns_y)))

    if data_type == "discrete":
        title_plot = "Probability density functions (PDF) of Normal (Gaussian)"
    else:
        title_plot = "Probability density functions (PDF) of Student's t-distribution"

    try:
        dataframe_input = input_data.loc[:, input_columns_y]
        pdfs_traces = []
        plot_annotations = []
        for i, input_column_y in enumerate(input_columns_y):
            if data_type == "discrete":
                data_var = dataframe_input[input_column_y]
            else:
                data_var = dataframe_input[input_column_y].values

            null_hypothesis_str, alternative_hypothesis_str, sample_size, sample_mean, sample_variance, sample_std_dev, \
            std_error, t_ppf_all_values, t_degrees_of_freedom, t_critical_values, critical_values, t_statistic, p_value, result_hypothesis_str = stats_helpers.compute_hypothesis_testing_one_sample(
                data_var=data_var, hypothesis_test_type_tails=hypothesis_test_type_tails,
                significance_level=significance_level, hypothesis_population_mean=hypothesis_population_mean,
                data_type=data_type, values_field=values_field)
            sample_mean = sample_mean.round(decimals=2)
            t_critical_value = abs(t_critical_values[0])
            t_critical_value_low = round(hypothesis_population_mean - abs(t_critical_value * std_error), 3)
            t_critical_value_high = round(hypothesis_population_mean + abs(t_critical_value * std_error), 3)
            # ------Plots----------
            pdf_limit_low = hypothesis_population_mean - t_ppf_all_values * std_error
            pdf_limit_high = hypothesis_population_mean + t_ppf_all_values * std_error
            x_range_all = np.linspace(pdf_limit_low, pdf_limit_high, 10000)  # x axis points

            percentage_null_hypothesis = round((1 - significance_level) * 100, 2)
            if hypothesis_test_type_tails == "different":
                percentage_alternative_hypothesis_tails = round(((significance_level) / 2) * 100, 2)
            else:
                percentage_alternative_hypothesis_tails = round(significance_level * 100, 2)

            pdfs_traces, annotations_hypothesis_test = draw_one_plot_hypothesis_one_sample(input_column_y, i,
                                                                                           sample_mean,
                                                                                           hypothesis_population_mean,
                                                                                           hypothesis_test_type_tails,
                                                                                           t_critical_value,
                                                                                           t_critical_value_low,
                                                                                           t_critical_value_high,
                                                                                           pdf_limit_low,
                                                                                           pdf_limit_high, x_range_all,
                                                                                           percentage_null_hypothesis,
                                                                                           percentage_alternative_hypothesis_tails,
                                                                                           std_error,
                                                                                           t_degrees_of_freedom,
                                                                                           fill_colors, pdfs_traces,
                                                                                           data_type=data_type,
                                                                                           values_field=values_field)

            plot_annotations = plot_annotations + annotations_hypothesis_test

            layout = set_layout(
                title=title_plot,
                column_x_name="Features", column_y_name=column_y_name, all_x_labels=0, height=600)
            layout["annotations"] = plot_annotations

            figure = plotly_graph.Figure(data=pdfs_traces, layout=layout)

            result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                               output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def hypothesis_testing_two_independent_samples(input_data=[], section_title="Hypothesis test two samples",
                                               hypothesis_test_type_tails="different", input_column_x=0,
                                               significance_level=0.05, hypothesis_population_mean=0,
                                               input_columns_y=[], column_class="", data_type="continuous",
                                               values_field=[]):
    result = {}
    column_y_name = "Probability density function"
    fill_colors = assign_colors_to_list_labels(np.arange(len(input_columns_y)))
    margin_y_between_labels = 50

    if data_type == "discrete":
        title_plot = "Probability density functions (PDF) of Normal (Gaussian)"
    else:
        title_plot = "Probability density functions (PDF) of Student's t-distribution"

    try:
        pdfs_traces = []
        plot_annotations = []
        input_data[0] = input_data[0].loc[:, input_columns_y]
        input_data[1] = input_data[1].loc[:, input_columns_y]
        for i, input_column_y in enumerate(input_columns_y):
            data_vars = []
            if data_type == "discrete":
                data_vars.append(input_data[0][input_column_y])
                data_vars.append(input_data[1][input_column_y])
            else:
                data_vars.append(input_data[0][input_column_y].values)
                data_vars.append(input_data[1][input_column_y].values)

            null_hypothesis_str, null_hypothesis_mean_differences, alternative_hypothesis_str, mean_hypothesis_differences, \
            samples_sizes, samples_means, samples_variances, samples_stds_devs, std_error, t_ppf_all_values, t_degrees_of_freedom, t_critical_values, critical_values, \
            t_statistic, p_value, result_hypothesis_str = stats_helpers.compute_hypothesis_testing_two_independent_samples(
                data_vars=data_vars, hypothesis_test_type_tails=hypothesis_test_type_tails,
                significance_level=significance_level, data_type=data_type, values_field=values_field)

            samples_means = np.round(samples_means, decimals=3)
            samples_variances = np.round(samples_variances, decimals=3)
            samples_stds_devs = np.round(samples_stds_devs, decimals=3)
            t_statistic = np.round(t_statistic, decimals=3)
            mean_hypothesis_differences = round(mean_hypothesis_differences, 3)
            t_degrees_of_freedom = np.round(t_degrees_of_freedom, decimals=3)
            t_critical_value = abs(t_critical_values[0])
            t_critical_value_low = round(null_hypothesis_mean_differences - abs(t_critical_value * std_error), 3)
            t_critical_value_high = round(null_hypothesis_mean_differences + abs(t_critical_value * std_error), 3)
            # ------Plots----------
            pdf_limit_low = null_hypothesis_mean_differences - t_ppf_all_values * std_error
            pdf_limit_high = null_hypothesis_mean_differences + t_ppf_all_values * std_error

            percentage_null_hypothesis = round((1 - significance_level) * 100, 2)
            if hypothesis_test_type_tails == "different":
                percentage_alternative_hypothesis_tails = round(((significance_level) / 2) * 100, 2)
            else:
                percentage_alternative_hypothesis_tails = round(significance_level * 100, 2)

            annotations_hypothesis_test = []
            t_critical_low = dict(
                x=t_critical_value_low,
                y=0,
                xref='x',
                yref='y',
                text='{0} <br> - critical value <br> ({1})'.format(t_critical_value_low, input_column_y),
                showarrow=True,
                arrowhead=7,
                ax=0,
                ay=30 + i * margin_y_between_labels,
            )
            null_hypothesis_mean_differences_annotation = dict(
                x=null_hypothesis_mean_differences,
                y=0,
                xref='x',
                yref='y',
                text='{0} <br> Null hypothesis mean differences <br> ({1})'.format(null_hypothesis_mean_differences,
                                                                                   input_column_y),
                showarrow=False,
                arrowhead=7,
                ax=0,
                ay=20 + i * margin_y_between_labels,
                font=dict(
                    color='#ffffff'
                ),
                bgcolor='#ff7f0e',
            )
            t_critical_high = dict(
                x=t_critical_value_high,
                y=0,
                xref='x',
                yref='y',
                text='{0} <br> critical value <br> ({1})'.format(t_critical_value_high, input_column_y),
                showarrow=True,
                arrowhead=7,
                ax=0,
                ay=30 + i * margin_y_between_labels,
            )
            mean_differences_alternative_hypothesis = dict(
                x=mean_hypothesis_differences,
                y=0,
                xref='x',
                yref='y',
                text='{0} <br> Alternative hypothesis mean differences <br> ({1})'.format(mean_hypothesis_differences,
                                                                                          input_column_y),
                showarrow=True,
                arrowhead=7,
                ax=0,
                ay=-110 + i * margin_y_between_labels,
            )

            if hypothesis_test_type_tails == "different":
                x_alternative_hypothesis_low = np.linspace(pdf_limit_low, t_critical_value_low, 10000)  # x axis points
                if data_type == "discrete":
                    pdf_t_dist = scipy_stats.norm.pdf(x_alternative_hypothesis_low,
                                                      loc=null_hypothesis_mean_differences, scale=std_error)
                else:
                    pdf_t_dist = scipy_stats.t.pdf(x_alternative_hypothesis_low, df=t_degrees_of_freedom,
                                                   loc=null_hypothesis_mean_differences, scale=std_error)
                trace_pdf_alternative_hypothesis_low = plotly_graph.Scattergl(
                    x=x_alternative_hypothesis_low,
                    y=pdf_t_dist,
                    name=input_column_y + " (Left alternative hypothesis {0}%)".format(
                        percentage_alternative_hypothesis_tails),
                    mode='lines',
                    fill='tozeroy',
                    fillcolor=fill_colors[i],
                )

                x_null_hypothesis = np.linspace(t_critical_value_low, t_critical_value_high, 10000)  # x axis points
                if data_type == "discrete":
                    pdf_t_dist = scipy_stats.norm.pdf(x_null_hypothesis,
                                                      loc=null_hypothesis_mean_differences, scale=std_error)
                else:
                    pdf_t_dist = scipy_stats.t.pdf(x_null_hypothesis, df=t_degrees_of_freedom,
                                                   loc=null_hypothesis_mean_differences, scale=std_error)
                trace_pdf_null_hypothesis = plotly_graph.Scattergl(
                    x=x_null_hypothesis,
                    y=pdf_t_dist,
                    name=input_column_y + " (Null hypothesis {0}%)".format(percentage_null_hypothesis),
                    mode='lines',
                )

                x_alternative_hypothesis_high = np.linspace(t_critical_value_high, pdf_limit_high,
                                                            10000)  # x axis points
                if data_type == "discrete":
                    pdf_t_dist = scipy_stats.norm.pdf(x_alternative_hypothesis_high,
                                                      loc=null_hypothesis_mean_differences, scale=std_error)
                else:
                    pdf_t_dist = scipy_stats.t.pdf(x_alternative_hypothesis_high, df=t_degrees_of_freedom,
                                                   loc=null_hypothesis_mean_differences, scale=std_error)
                trace_pdf_alternative_hypothesis_high = plotly_graph.Scattergl(
                    x=x_alternative_hypothesis_high,
                    y=pdf_t_dist,
                    name=input_column_y + " (Right alternative hypothesis {0}%)".format(
                        percentage_alternative_hypothesis_tails),
                    mode='lines',
                    fill='tozeroy',
                    fillcolor=fill_colors[i],
                )

                pdfs_traces += [trace_pdf_alternative_hypothesis_low, trace_pdf_null_hypothesis,
                                trace_pdf_alternative_hypothesis_high]
                annotations_hypothesis_test += [t_critical_low, null_hypothesis_mean_differences_annotation,
                                                t_critical_high, mean_differences_alternative_hypothesis]
            elif hypothesis_test_type_tails == "greater":
                x_null_hypothesis = np.linspace(pdf_limit_low, t_critical_value_high, 10000)  # x axis points
                if data_type == "discrete":
                    pdf_t_dist = scipy_stats.norm.pdf(x_null_hypothesis,
                                                      loc=null_hypothesis_mean_differences, scale=std_error)
                else:
                    pdf_t_dist = scipy_stats.t.pdf(x_null_hypothesis, df=t_degrees_of_freedom,
                                                   loc=null_hypothesis_mean_differences, scale=std_error)
                trace_pdf_null_hypothesis = plotly_graph.Scattergl(
                    x=x_null_hypothesis,
                    y=pdf_t_dist,
                    name=input_column_y + " (Null hypothesis {0}%)".format(percentage_null_hypothesis),
                    mode='lines',
                )

                x_alternative_hypothesis_high = np.linspace(t_critical_value_high, pdf_limit_high,
                                                            10000)  # x axis points
                if data_type == "discrete":
                    pdf_t_dist = scipy_stats.norm.pdf(x_alternative_hypothesis_high,
                                                      loc=null_hypothesis_mean_differences, scale=std_error)
                else:
                    pdf_t_dist = scipy_stats.t.pdf(x_alternative_hypothesis_high, df=t_degrees_of_freedom,
                                                   loc=null_hypothesis_mean_differences, scale=std_error)
                trace_pdf_alternative_hypothesis_high = plotly_graph.Scattergl(
                    x=x_alternative_hypothesis_high,
                    y=pdf_t_dist,
                    name=input_column_y + " (Alternative hypothesis {0}%)".format(
                        percentage_alternative_hypothesis_tails),
                    mode='lines',
                    fill='tozeroy',
                    fillcolor=fill_colors[i],
                )

                pdfs_traces += [trace_pdf_null_hypothesis, trace_pdf_alternative_hypothesis_high]
                annotations_hypothesis_test += [null_hypothesis_mean_differences_annotation, t_critical_high,
                                                mean_differences_alternative_hypothesis]
            elif hypothesis_test_type_tails == "lesser":
                x_alternative_hypothesis_low = np.linspace(pdf_limit_low, t_critical_value_low, 10000)  # x axis points
                if data_type == "discrete":
                    pdf_t_dist = scipy_stats.norm.pdf(x_alternative_hypothesis_low,
                                                      loc=null_hypothesis_mean_differences, scale=std_error)
                else:
                    pdf_t_dist = scipy_stats.t.pdf(x_alternative_hypothesis_low, df=t_degrees_of_freedom,
                                                   loc=null_hypothesis_mean_differences, scale=std_error)
                trace_pdf_alternative_hypothesis_low = plotly_graph.Scattergl(
                    x=x_alternative_hypothesis_low,
                    y=pdf_t_dist,
                    name=input_column_y + " (Alternative hypothesis {0}%)".format(
                        percentage_alternative_hypothesis_tails),
                    mode='lines',
                    fill='tozeroy',
                    fillcolor=fill_colors[i],
                )

                x_null_hypothesis = np.linspace(t_critical_value_low, pdf_limit_high, 10000)  # x axis points
                if data_type == "discrete":
                    pdf_t_dist = scipy_stats.norm.pdf(x_null_hypothesis,
                                                      loc=null_hypothesis_mean_differences, scale=std_error)
                else:
                    pdf_t_dist = scipy_stats.t.pdf(x_null_hypothesis, df=t_degrees_of_freedom,
                                                   loc=null_hypothesis_mean_differences, scale=std_error)
                trace_pdf_null_hypothesis = plotly_graph.Scattergl(
                    x=x_null_hypothesis,
                    y=pdf_t_dist,
                    name=input_column_y + " (Null hypothesis {0}%)".format(percentage_null_hypothesis),
                    mode='lines',
                )

                pdfs_traces += [trace_pdf_alternative_hypothesis_low, trace_pdf_null_hypothesis]
                annotations_hypothesis_test += [t_critical_low, null_hypothesis_mean_differences_annotation,
                                                mean_differences_alternative_hypothesis]

            plot_annotations = plot_annotations + annotations_hypothesis_test

            layout = set_layout(
                title=title_plot,
                column_x_name="Features", column_y_name=column_y_name, all_x_labels=0, height=600)
            layout["annotations"] = plot_annotations

            figure = plotly_graph.Figure(data=pdfs_traces, layout=layout)

            result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                               output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def hypothesis_testing_two_dependent_samples(input_data=[], section_title="Hypothesis test two dependent samples",
                                             hypothesis_test_type_tails="different", input_column_x=0,
                                             significance_level=0.05, hypothesis_population_mean=0, input_columns_y=[],
                                             column_class="", data_type="continuous", values_field=[]):
    result = {}
    column_y_name = "Probability density function"
    fill_colors = assign_colors_to_list_labels(np.arange(len(input_columns_y)))

    if data_type == "discrete":
        title_plot = "Probability density functions (PDF) of Normal (Gaussian)"
    else:
        title_plot = "Probability density functions (PDF) of Student's t-distribution"

    try:
        pdfs_traces = []
        plot_annotations = []
        if data_type == "discrete":
            dataframe_input = [input_data.loc[:, input_columns_y[0]], input_data.loc[:, input_columns_y[1]]]
            data_var_diff = dataframe_input  # we compute the diff in the compute_hypothesis_testing_one_sample method
        else:
            dataframe_input = input_data.loc[:, input_columns_y]
            data_var_diff = stats_helpers.compute_diff_features(dataframe_input)
        hypothesis_population_diff_mean = 0  # Diff means data var = 0

        null_hypothesis_str, alternative_hypothesis_str, sample_size, sample_mean, sample_variance, sample_std_dev, \
        std_error, t_ppf_all_values, t_degrees_of_freedom, t_critical_values, critical_values, t_statistic, p_value, \
        result_hypothesis_str = stats_helpers.compute_hypothesis_testing_one_sample(data_var=data_var_diff,
                                                                                    hypothesis_test_type_tails=hypothesis_test_type_tails,
                                                                                    hypothesis_test_type="two_dependent_samples",
                                                                                    significance_level=significance_level,
                                                                                    hypothesis_population_mean=hypothesis_population_diff_mean,
                                                                                    data_type=data_type,
                                                                                    values_field=values_field)
        i = 0
        input_column_y = "Diff (" + input_columns_y[0] + ", " + input_columns_y[1] + ")"
        sample_mean = sample_mean.round(decimals=2)
        t_critical_value = abs(t_critical_values[0])
        t_critical_value_low = round(hypothesis_population_mean - abs(t_critical_value * std_error), 3)
        t_critical_value_high = round(hypothesis_population_mean + abs(t_critical_value * std_error), 3)
        # ------Plots----------
        pdf_limit_low = hypothesis_population_mean - t_ppf_all_values * std_error
        pdf_limit_high = hypothesis_population_mean + t_ppf_all_values * std_error
        x_range_all = np.linspace(pdf_limit_low, pdf_limit_high, 10000)  # x axis points

        percentage_null_hypothesis = round((1 - significance_level) * 100, 2)
        if hypothesis_test_type_tails == "different":
            percentage_alternative_hypothesis_tails = round(((significance_level) / 2) * 100, 2)
        else:
            percentage_alternative_hypothesis_tails = round(significance_level * 100, 2)

        pdfs_traces, annotations_hypothesis_test = draw_one_plot_hypothesis_one_sample(input_column_y, i, sample_mean,
                                                                                       hypothesis_population_mean,
                                                                                       hypothesis_test_type_tails,
                                                                                       t_critical_value,
                                                                                       t_critical_value_low,
                                                                                       t_critical_value_high,
                                                                                       pdf_limit_low, pdf_limit_high,
                                                                                       x_range_all,
                                                                                       percentage_null_hypothesis,
                                                                                       percentage_alternative_hypothesis_tails,
                                                                                       std_error, t_degrees_of_freedom,
                                                                                       fill_colors, pdfs_traces,
                                                                                       data_type=data_type,
                                                                                       values_field=values_field)

        plot_annotations = plot_annotations + annotations_hypothesis_test

        layout = set_layout(
            title=title_plot,
            column_x_name="Features", column_y_name=column_y_name, all_x_labels=0, height=600)
        layout["annotations"] = plot_annotations

        figure = plotly_graph.Figure(data=pdfs_traces, layout=layout)

        result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                           output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def find_distribution_pdf(input_data=[], section_title="Find distribution", significance_level=0.05,
                          input_columns_y=""):
    result = {}
    column_y_name = "Probability density function"
    title_plot_common = "PDFs with fitted distributions"
    subplots = False

    result[
        "object_result_html"] = '<div class="container-center center-plot" style="margin-right: 10px; overflow-x:auto">'

    if len(input_columns_y) > 1:
        num_cols = 2
        counter_col = 0
        subplots = True
        result["object_result_html"] += '<table class="not-datatable">'
    try:
        dataframe_input = input_data.loc[:, input_columns_y]

        if subplots:
            result["object_result_html"] += '<tr>'
        for i, input_column_y in enumerate(input_columns_y):
            if subplots:
                result["object_result_html"] += '<td>'
            pdfs_traces = []
            data = dataframe_input[input_column_y].values

            best_dists = stats_helpers.compute_find_distribution(data_var=data, significance_level=significance_level)

            data_max = max(data)
            if data_max != min(data):
                # min_max_scaler = sklearn_preprocessing.MinMaxScaler()
                # data = min_max_scaler.fit_transform(data.reshape(-1, 1)).ravel()  # Normalize
                kde = scipy_stats.gaussian_kde(data)  # kernel density estimator between points
                x_range = np.linspace(min(data), max(data), 10000)  # x axis points
                pdf = kde.evaluate(x_range)

                trace_original_pdf = plotly_graph.Scattergl(
                    x=x_range,
                    y=pdf,
                    name=input_column_y + " (Original)",
                    mode='lines',
                    line=dict(
                        width=4, )
                    # fill='tozeroy',
                )
            else:
                if data_max > 0.5:
                    invariant_feature_point = 1
                else:
                    invariant_feature_point = 0
                trace_original_pdf = plotly_graph.Scattergl(
                    x=[invariant_feature_point],
                    y=[0],
                    name=input_column_y + " (Original)" + " (Invariant)",
                    mode='markers',
                    marker=dict(
                        size=15,
                    )
                )
            pdfs_traces.append(trace_original_pdf)

            # best_dists = best_dists[0:5]
            for best_dist in best_dists:
                dist_to_fit_verbose_name = best_dist["Distribution name"]
                dist_to_fit_name = best_dist["dist_name_scipy"]
                dist_to_fit = getattr(scipy_stats, dist_to_fit_name)
                params_dist_fitted = best_dist["Distribution parameters"]

                arg = params_dist_fitted[:-2]
                loc = params_dist_fitted[-2]
                scale = params_dist_fitted[-1]

                # Get sane start and end points of distribution
                max_data = max(data)
                min_data = min(data)
                size_data = max_data - min_data
                margin_left = size_data / 50
                x_range = np.linspace(min(data) + margin_left, max(data), 10000)  # x axis points
                dist_fitted_pdf = dist_to_fit.pdf(x_range, loc=loc, scale=scale, *arg)

                trace_fitted_pdf = plotly_graph.Scattergl(
                    x=x_range,
                    y=dist_fitted_pdf,
                    name=dist_to_fit_verbose_name,
                    mode='lines',
                    # fill='tozeroy',
                )
                pdfs_traces.append(trace_fitted_pdf)

            title_plot = input_column_y + " " + title_plot_common
            layout = set_layout(title=title_plot, column_x_name="Feature", column_y_name=column_y_name,
                                all_x_labels=0, height=600)
            figure = plotly_graph.Figure(data=pdfs_traces, layout=layout)

            result["object_result_html"] += plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                                output_type='div')
            if subplots:
                counter_col += 1
                result["object_result_html"] += '</td>'
                if counter_col >= num_cols:
                    counter_col = 0
                    result["object_result_html"] += '</tr>'
                    result["object_result_html"] += '<tr>'

        if subplots:
            result["object_result_html"] += '</table>'
        result["object_result_html"] += '</div>'
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def find_distribution_cdf(input_data=[], section_title="Find distribution", significance_level=0.05,
                          input_columns_y=""):
    result = {}
    column_y_name = "Cumulative density function"
    title_plot_common = "CDFs with fitted distributions"
    subplots = False

    result[
        "object_result_html"] = '<div class="container-center center-plot" style="margin-right: 10px; overflow-x:auto">'

    if len(input_columns_y) > 1:
        num_cols = 2
        counter_col = 0
        subplots = True
        result["object_result_html"] += '<table class="not-datatable">'
    try:
        dataframe_input = input_data.loc[:, input_columns_y]

        if subplots:
            result["object_result_html"] += '<tr>'
        for i, input_column_y in enumerate(input_columns_y):
            if subplots:
                result["object_result_html"] += '<td>'
            cdfs_traces = []
            data = dataframe_input[input_column_y].values

            best_dists = stats_helpers.compute_find_distribution(data_var=data, significance_level=significance_level)

            data_max = max(data)
            if data_max != min(data):
                x_range = np.linspace(min(data), max(data), 10000)  # x axis points
                data_sorted = np.sort(data)
                # calculate the proportional values of samples
                cdf = 1. * np.arange(len(data)) / (len(data) - 1)

                trace_original_cdf = plotly_graph.Scattergl(
                    x=data_sorted,
                    y=cdf,
                    name=input_column_y + " (Original)",
                    mode='lines',
                    line=dict(
                        width=4, )
                    # fill='tozeroy',
                )
            else:
                if data_max > 0.5:
                    invariant_feature_point = 1
                else:
                    invariant_feature_point = 0
                trace_original_cdf = plotly_graph.Scattergl(
                    x=[invariant_feature_point],
                    y=[0],
                    name=input_column_y + " (Original)" + " (Invariant)",
                    mode='markers',
                    marker=dict(
                        size=15,
                    )
                )
            cdfs_traces.append(trace_original_cdf)

            # best_dists = best_dists[0:5]
            for best_dist in best_dists:
                dist_to_fit_verbose_name = best_dist["Distribution name"]
                dist_to_fit_name = best_dist["dist_name_scipy"]
                dist_to_fit = getattr(scipy_stats, dist_to_fit_name)
                params_dist_fitted = best_dist["Distribution parameters"]

                arg = params_dist_fitted[:-2]
                loc = params_dist_fitted[-2]
                scale = params_dist_fitted[-1]

                x_range = np.linspace(min(data), max(data), 10000)  # x axis points
                dist_fitted_cdf = dist_to_fit.cdf(x_range, loc=loc, scale=scale, *arg)

                trace_fitted_cdf = plotly_graph.Scattergl(
                    x=x_range,
                    y=dist_fitted_cdf,
                    name=dist_to_fit_verbose_name,
                    mode='lines',
                    # fill='tozeroy',
                )
                cdfs_traces.append(trace_fitted_cdf)

            title_plot = input_column_y + " " + title_plot_common
            layout = set_layout(title=title_plot, column_x_name="Feature", column_y_name=column_y_name,
                                all_x_labels=0, height=600)
            figure = plotly_graph.Figure(data=cdfs_traces, layout=layout)

            result["object_result_html"] += plotly.offline.plot(figure, include_plotlyjs=False, show_link=False,
                                                                output_type='div')
            if subplots:
                counter_col += 1
                result["object_result_html"] += '</td>'
                if counter_col >= num_cols:
                    counter_col = 0
                    result["object_result_html"] += '</tr>'
                    result["object_result_html"] += '<tr>'

        if subplots:
            result["object_result_html"] += '</table>'
        result["object_result_html"] += '</div>'
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def find_distribution_q_q_plots(input_data=[], section_title="Find distribution", significance_level=0.05,
                                input_columns_y="", label_name=""):
    result = {}
    column_y_name = "Empirical quantiles"
    title_plot_common = "Q-Q plot"
    subplots = False

    result[
        "object_result_html"] = '<div class="container-center center-plot" style="margin-right: 10px; overflow-x:auto">'

    num_cols = 2
    counter_col = 0
    subplots = True
    result["object_result_html"] += '<table class="not-datatable">'

    try:
        dataframe_input = input_data.loc[:, input_columns_y]

        if subplots:
            result["object_result_html"] += '<tr>'
        for i, input_column_y in enumerate(input_columns_y):
            data = dataframe_input[input_column_y].values

            best_dists = stats_helpers.compute_find_distribution(data_var=data, significance_level=significance_level)

            best_dists_selected = best_dists[0:2]
            dist_normal_fitted = {}
            for i, dist in enumerate(best_dists):
                if dist["dist_name_scipy"] == "norm":
                    dist_normal_fitted = dist
            best_dists_selected[1] = dist_normal_fitted
            for best_dist in best_dists_selected:
                dist_to_fit_verbose_name = best_dist["Distribution name"]
                dist_to_fit_name = best_dist["dist_name_scipy"]
                dist_to_fit = getattr(scipy_stats, dist_to_fit_name)
                params_dist_fitted = best_dist["Distribution parameters"]

                arg = params_dist_fitted[:-2]
                loc = params_dist_fitted[-2]
                scale = params_dist_fitted[-1]

                data = sorted(data)
                data_np = np.array(data)
                probplot = statsmodels.ProbPlot(data=data_np, dist=dist_to_fit, distargs=tuple(arg), loc=loc,
                                                scale=scale)
                matplotlib_figure_qq = {"title": "Q-Q plot", "plot": probplot.qqplot(line="q")}
                matplotlib_figure_pp = {"title": "P-P plot", "plot": probplot.ppplot(line="45")}
                # matplotlib_figure_prob = {"title": "Probability plot", "plot": probplot.probplot(line="q")}
                prob_plots = [matplotlib_figure_qq, matplotlib_figure_pp]
                # matplotlib_plot.draw()
                # ax.legend().set_visible(False)

                result["object_result_html"] += '<tr>'
                for matplotlib_figure in prob_plots:
                    plotly_figure = plotly_tools.mpl_to_plotly(matplotlib_figure["plot"])

                    plotly_figure["layout"] = dict(
                        title=matplotlib_figure["title"] + " " + input_column_y + " - " + dist_to_fit_verbose_name,
                        width=600,
                        height=600,
                        xaxis=dict(
                            title="Theoretical quantiles ({0})".format(dist_to_fit_verbose_name),
                            automargin=True,
                            zeroline=False,
                            linewidth=1,
                            mirror=True),
                        yaxis=dict(
                            title=column_y_name,
                            automargin=True,
                            zeroline=False,
                            linewidth=1,
                            mirror=True),
                    )
                    texts_with_labels = []
                    for i, text in enumerate(plotly_figure["data"][0]["y"]):
                        new_text = input_data[label_name][i]
                        texts_with_labels.append(new_text)
                    plotly_figure["data"][0]["text"] = texts_with_labels

                    result["object_result_html"] += '<td>'
                    result["object_result_html"] += plotly.offline.plot(plotly_figure, include_plotlyjs=False,
                                                                        show_link=False,
                                                                        output_type='div')
                    result["object_result_html"] += '</td>'
                result["object_result_html"] += '</tr>'
            if subplots:
                result["object_result_html"] += '</tr>'
                result["object_result_html"] += '<tr>'

        if subplots:
            result["object_result_html"] += '</table>'
        result["object_result_html"] += '</div>'
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


def neuron_swc_3D(swc_bytes):
    swc_parser = []
    x = []
    y = []
    z = []

    swc_str = swc_bytes.decode("utf-8")
    swc_lines = swc_str.split("\n")
    node_counter = 1
    for i, line in enumerate(swc_lines):
        node = line.split(" ")
        if node[0] != "#" and (node[0].isdigit() or node[0] == "-1"):
            node_counter += 1
            swc_parser.append(node)
            x.append(node[2])
            y.append(node[3])
            z.append(node[4])

    result = {}

    try:
        trace = [
            plotly_graph.Scattergl3d(
                x=x,
                y=y,
                z=z,
                mode='lines',
                line=dict(color='#0066FF', width=2)
            )
        ]

        figure = plotly_graph.Figure(data=trace)

        result["object_result_html"] = plotly.offline.plot(figure, include_plotlyjs=False,
                                                           show_link=False, output_type='div')
    except Exception as e:
        result["error"] = True
        result["error_message"] = str(e)

    return result


# ------------Plotly utilities--------------
def set_layout(title="", column_x_name="", column_y_name="", all_x_labels=1, height=600, show_y_zero_line=True):
    layout = plotly_graph.Layout(
        title=title,
        autosize=True,
        height=height,
        margin=plotly_graph.layout.Margin(
        ),
        xaxis=dict(
            title=column_x_name,
            automargin=True,
            dtick=all_x_labels,
            zeroline=show_y_zero_line,
        ),
        yaxis=dict(
            title=column_y_name,
            automargin=True,
        ),
    )

    return layout


def get_subplots_rows_cols(input_data=[], num_cols=3, titles=False):
    rows = int(math.ceil(len(input_data) / num_cols))
    if not titles:
        subplot_titles = []
    else:
        subplot_titles = input_data
    figure = plotly_tools.make_subplots(rows=rows, cols=num_cols, subplot_titles=subplot_titles, print_grid=False)
    counter_row = 1
    counter_col = 1

    return figure, counter_row, counter_col, rows


def get_subplot_counter_row_col(counter_row=1, counter_col=1, num_cols=3):
    counter_col += 1
    if counter_col > num_cols:
        counter_col = 1
        counter_row += 1

    return counter_row, counter_col


def get_colors_from_labels(dataframe, label_name):
    labels_unique = pd.Series(dataframe[label_name].unique()).sort_values()
    # colorscales_all = colorlover.scales
    rgb_values = colorlover.scales["11"]['qual']['Paired']
    if len(labels_unique) <= len(rgb_values):
        color_map = dict(zip(labels_unique, rgb_values))
        colors = (dataframe[label_name].map(color_map)).values.tolist()
    else:
        colors = []
        for i, element in enumerate(labels_unique):
            colors.append(rgb_values[i % len(rgb_values)])

    return colors


def get_my_custom_colorscale():
    colorscale = []
    # colorscales_all = colorlover.scales
    rgb_values = colorlover.scales["11"]['qual']['Paired']
    step_range = 1 / len(rgb_values)
    colorscale_range = np.arange(0, 1, step_range)
    for i, value_range in enumerate(colorscale_range):
        scale_value = [value_range, rgb_values[i]]
        colorscale.append(scale_value)

    return colorscale


def assign_colors_to_list_labels(input_list):
    # colorscales_all = colorlover.scales
    input_series = pd.Series(input_list)
    rgb_values = colorlover.scales["11"]['qual']['Paired']
    if len(input_list) <= len(rgb_values):
        color_map = dict(zip(input_series, rgb_values))
        colors = (input_series.map(color_map)).values.tolist()
    else:
        colors = []
        for i, element in enumerate(input_list):
            colors.append(rgb_values[i % len(rgb_values)])

    return colors


def get_symbols_from_labels(dataframe, label_name):
    symbols = []

    num_plotly_symbols = 45
    labels_unique = pd.Series(dataframe[label_name].unique()).sort_values()
    # symbol_values = np.arange(num_plotly_symbols)
    symbol_values = ['circle', 'circle-open', 'square', 'square-open', 'diamond', 'diamond-open', 'cross', 'x']
    if len(labels_unique) <= len(symbol_values):
        symbol_map = dict(zip(labels_unique, symbol_values))
        symbols = (dataframe[label_name].map(symbol_map)).values.tolist()
    else:
        symbols = []
        for i, element in enumerate(labels_unique):
            symbols.append(symbol_values[i % len(symbol_values)])

    return symbols


def get_colors_labels_unique(input_data, label_name):
    numbers_labels = []
    labels_unique = pd.Series(input_data[label_name].unique()).sort_values()
    labels_in_numbers = np.arange(len(input_data))
    if len(labels_unique) <= len(labels_in_numbers):
        numbers_map = dict(zip(labels_unique, labels_in_numbers))
    else:
        for i, element in enumerate(labels_unique):
            numbers_labels.append(labels_in_numbers[i % len(labels_in_numbers)])
    rgb_values = colorlover.scales["11"]['qual']['Paired']
    if len(labels_unique) <= len(rgb_values):
        color_map = dict(zip(labels_unique, rgb_values))
        colors_unique = (labels_unique.map(color_map)).values.tolist()
    else:
        colors_unique = []
        for i, element in enumerate(labels_unique):
            colors_unique.append(rgb_values[i % len(rgb_values)])
    return colors_unique, labels_unique


def density_functions_bn(mean=0, std_deviation=1, evidence_value=None):
    pdfs_traces = []
    shapes = []

    if isinstance(mean, list):
        x_range = np.linspace(min(mean) - 3 * max(std_deviation), max(mean) + 3 * max(std_deviation), 500)  # x axis points
        init_pdf = scipy_stats.norm.pdf(x_range, mean[0], std_deviation[0])
        joint_cond_pdf = scipy_stats.norm.pdf(x_range, mean[1], std_deviation[1])
        trace_pdf = plotly_graph.Scattergl(
            x=x_range,
            y=init_pdf,
            mode='lines',
            name='Original distribution'
            # fill='tozeroy',
        )
        pdfs_traces.append(trace_pdf)
        trace_pdf = plotly_graph.Scattergl(
            x=x_range,
            y=joint_cond_pdf,
            mode='lines',
            name='New conditional distribution',
            # fill='tozeroy',
            line=dict(width=3, color=('rgb(0, 0, 0)'))
        )
        pdfs_traces.append(trace_pdf)
        layout = set_layout(all_x_labels=0, height=180, show_y_zero_line=False)
        """
        if joint_cond_pdf[int(x_range.size / 2)] > init_pdf[int(x_range.size / 2)]:
            annotations_y = joint_cond_pdf[int(x_range.size / 2)]
        else:
            annotations_y = init_pdf[int(x_range.size / 2)]
        """
        annotations_y_pos = max(max(init_pdf), max(joint_cond_pdf))
        annotation_x_margin = x_range[-1]/4
        plot_annotations = [
            dict(
                text="Mean <br> <b>{}</b>  <br> <span style='color: #1f77b4 !important;'>{}</span>".format(round(mean[1], 2), round(mean[0], 2)),
                x=x_range[0] - annotation_x_margin, #min(mean) - 2 * std_deviation[mean.index(min(mean))],
                y=annotations_y_pos,
                xref='x',
                yref='y',
                showarrow=False,
                arrowhead=1,
                ax=0, #0
                ay=1, #1
            ),
            dict(
                text="Std deviation <br> <b>{}</b> <br> <span style='color: #1f77b4 !important;'>{}</span>".format(round(std_deviation[1], 2), round(std_deviation[0], 2)),
                x= x_range[-1] + annotation_x_margin, #max(mean) + 2 * std_deviation[mean.index(max(mean))],
                y=annotations_y_pos,
                xref='x',
                yref='y',
                showarrow=False,
                arrowhead=1,
                ax=0,
                ay=1,
            )
        ]
    else:
        x_range = np.linspace(mean - 3 * std_deviation, mean + 3 * std_deviation, 500)  # x axis points
        pdf = scipy_stats.norm.pdf(x_range, mean, std_deviation)

        trace_pdf = plotly_graph.Scattergl(
            x=x_range,
            y=pdf,
            mode='lines',
            # fill='tozeroy',
        )
        pdfs_traces.append(trace_pdf)
        layout = set_layout(all_x_labels=0, height=150, show_y_zero_line=False)

        x_max_min_vals = (x_range[0], x_range[-1])
        annotations_y_pos = max(pdf)
        annotation_x_margin = x_max_min_vals[1]/4
        annotation_x_mean = x_max_min_vals[0]
        annotation_x_std = x_max_min_vals[1]

        if evidence_value is not None:
            if evidence_value < mean -  3 * std_deviation:
                annotation_x_mean = evidence_value
                annotation_x_margin = evidence_value/10
            elif evidence_value > mean + 3 * std_deviation:
                annotation_x_std = evidence_value
                annotation_x_margin = evidence_value/10

        annotation_x_mean -= annotation_x_margin
        annotation_x_std += annotation_x_margin

        plot_annotations = [
            dict(
                text="Mean <br> {}".format(round(mean, 2)),
                x=annotation_x_mean, #mean - 2 * std_deviation,
                y=annotations_y_pos, #pdf[int(x_range.size / 2)],
                xref='x',
                yref='y',
                showarrow=False,
                arrowhead=1,
                ax=0,
                ay=1,
            ),
            dict(
                text="Std deviation <br> {}".format(round(std_deviation, 2)),
                x=annotation_x_std, #mean + 2 * std_deviation,
                y=annotations_y_pos, #pdf[int(x_range.size / 2)],
                xref='x',
                yref='y',
                showarrow=False,
                arrowhead=1,
                ax=0,
                ay=1,
            )
        ]

        shapes = [
            dict(
                type='line',
                x0=mean,
                y0=0,
                x1=mean,
                y1=pdf[int(x_range.size / 2)],
                xref='x',
                yref='y',
            )
        ]

    if evidence_value is not None:
        plot_annotations += [
            dict(
                text="({}) <br> Evidence".format(round(evidence_value, 2)),
                x=evidence_value,
                y=0,
                xref='x',
                yref='y',
                showarrow=True,
                arrowhead=3,
                ax=0,
                ay=-30,
            ),

        ]
    layout["annotations"] = plot_annotations
    if shapes:
        layout["shapes"] = shapes

    layout["legend"] = dict(orientation="h")

    figure = plotly_graph.Figure(data=pdfs_traces, layout=layout)
    if isinstance(mean, list):
        figure["layout"].update(showlegend=True)
    figure['layout']["margin"].update(l=0, r=10, b=0, t=0)
    result = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False, output_type='div',
                                 config={"displayModeBar": False})

    return result


def density_functions_multi(means=[0], std_devs=[1], mixture_weights=[0], evidence_value=None, structures_ids=[], structures_colors=[]):
    pdfs_traces = []
    plot_annotations = []
    shapes = []
    norm_params = []

    for i, structure_id in enumerate(structures_ids):
        mean = means[i]
        std_dev = std_devs[i]
        mixture_weight = mixture_weights[i]
        norm_params.append([mean, std_dev, mixture_weight])

        x_range = np.linspace(mean - 3 * std_dev, mean + 3 * std_dev, 500)  # x axis points
        pdf = scipy_stats.norm.pdf(x_range, mean, std_dev)

        trace_pdf = plotly_graph.Scattergl(
            x=x_range,
            y=pdf,
            mode='lines',
            name=structure_id + "({}, {})".format(round(mean, 2), round(std_dev, 2)),
            marker=dict(
                color=structures_colors[i]
            ),
            # fill='tozeroy',
        )
        pdfs_traces.append(trace_pdf)

        if i == 0:
            x_min = np.min(x_range)
            x_max = np.max(x_range)
        else:
            if np.min(x_range) < x_min:
                x_min = np.min(x_range)
            if np.max(x_range) > x_max:
                x_max = np.max(x_range)

        """
        plot_annotations += [
            dict(
                text="Mean <br> {}".format(round(mean, 2)),
                x=mean - 2 * std_dev,
                y=pdf[int(x_range.size / 2)],
                xref='x',
                yref='y',
                showarrow=False,
                arrowhead=1,
                ax=0,
                ay=1,
            ),
            dict(
                text="Std deviation <br> {}".format(round(std_dev, 2)),
                x=mean + 2 * std_dev,
                y=pdf[int(x_range.size / 2)],
                xref='x',
                yref='y',
                showarrow=False,
                arrowhead=1,
                ax=0,
                ay=1,
            )
        ]
        """
        """
        shapes += [
            dict(
                type='line',
                x0=mean,
                y0=0,
                x1=mean,
                y1=pdf[int(x_range.size / 2)],
                xref='x',
                yref='y',
            )
        ]
        """

        if evidence_value is not None:
            plot_annotations += [
                dict(
                    text="({}) <br> Evidence".format(round(evidence_value, 2)),
                    x=evidence_value,
                    y=0,
                    xref='x',
                    yref='y',
                    showarrow=True,
                    arrowhead=3,
                    ax=0,
                    ay=-30,
                ),

            ]

    x_range = np.linspace(x_min, x_max, 500)
    mixture_pdfs_traces = np.zeros_like(x_range)
    for loc, scale, mixture_weight in norm_params:
        mixture_pdfs_traces += scipy_stats.norm.pdf(x_range, loc=loc, scale=scale) * mixture_weight

    trace_pdf = plotly_graph.Scattergl(
        x=x_range,
        y=mixture_pdfs_traces,
        mode='lines',
        name="Mixture all",
        line=dict(width=4,color=('rgb(0, 0, 0)'))
    )
    #pdfs_traces.append(trace_pdf)

    layout = set_layout(all_x_labels=0, height=240, show_y_zero_line=False)
    layout["annotations"] = plot_annotations
    layout["shapes"] = shapes
    layout["legend"] = dict(orientation="h")

    figure = plotly_graph.Figure(data=pdfs_traces, layout=layout)
    figure["layout"].update(showlegend=True)
    figure['layout']["margin"].update(l=0, r=10, b=0, t=0)
    result = plotly.offline.plot(figure, include_plotlyjs=False, show_link=False, output_type='div',
                                 config={"displayModeBar": False})

    return result
