import rpy2
from rpy2.rinterface import R_VERSION_BUILD
from rpy2.robjects import r
from rpy2.robjects.packages import importr
from rpy2.robjects import pandas2ri
import rpy2.robjects as robjects
import rpy2.rinterface as rinterface
import datetime
from django.conf import settings
import os
import neurosuite_dj.helpers as global_helpers
import math
from apps.morpho_analyzer.helpers import helpers


def chernoff_faces(input_data=[], input_column_x=0, input_columns_y=[], column_class="", session_id=""):
    result = {}

    if global_helpers.is_free_disk_space(new_upload_bytes=10000):
        pandas2ri.activate()
        try:

            title_plot = "Chernoff faces"
            width_one_face = 520
            height_one_face = 426
            ncol_plot = 5
            nrow_plot = math.ceil(len(input_data[column_class]) / ncol_plot)
            width_page = nrow_plot * height_one_face
            height_page = ncol_plot * width_one_face

            time_miliseconds = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f")
            name_plot_image = "chernoff_" + session_id + time_miliseconds + ".png"
            path_plot_image = settings.PLOTS_TMP_DIR + os.sep + name_plot_image
            # Create dir_output if not exists:
            if not os.path.exists(settings.PLOTS_TMP_DIR):
                os.makedirs(settings.PLOTS_TMP_DIR)
            url_plot_image = settings.PLOTS_TMP_URL + "/" + name_plot_image

            aplpack = importr("aplpack")
            grdevices = importr('grDevices')
            labels_r = robjects.StrVector(input_data[column_class])
            input_data = input_data.loc[:, input_columns_y]
            png_function = grdevices.png(filename=path_plot_image, width=width_one_face*ncol_plot , height=height_one_face*nrow_plot)
            std = helpers.supress_stout_stderr()
            faces_R = aplpack.faces(input_data, face_type=0, labels=labels_r, main=title_plot, ncol_plot=ncol_plot, nrow_plot=nrow_plot)
            helpers.enable_stdout_stderr(std)
            close_dev_png = grdevices.dev_off()

            features_faces_asssigned = faces_R.rx2("info")
            features_faces_asssigned_html = ""
            if features_faces_asssigned == rinterface.NULL:
                result["error"] = True
                result["error_message"] = "Unknown error creating Chernoff faces"
            else:
                features_faces_asssigned_html += "<table class='datatable-new-ajax table-with-footer table-scroll-y container-center table-small-border table-not-scroll-x table-headers-center'" \
                                                 ">"
                features_faces_asssigned_html+= "<thead>"
                features_faces_asssigned_html+= "<tr>"
                features_faces_asssigned_html+= "<th>{0}</th><th>{1}</th>".format("Face feature", "Neuron feature")
                features_faces_asssigned_html+= "</tr>"
                features_faces_asssigned_html+= "</thead>"
                features_faces_asssigned_html+= "<tbody>"
                n_rows = features_faces_asssigned.nrow
                for i in range(1, n_rows):
                    features_faces_asssigned_html += "<tr>"
                    features_faces_asssigned_html += "<td>{0}</td><td>{1}</td>".format(features_faces_asssigned.rx2(i, 1)[0], features_faces_asssigned.rx(i, 2)[0])
                    features_faces_asssigned_html += "</tr>"

                features_faces_asssigned_html+= "</tbody>"
                features_faces_asssigned_html+= "</table>"

            result["object_result_html"] = """
            <div class='container-center r-plot'>
                <h4>{0}</h4>
                <p>Note that only the first 15 features can be mapped in the Chernoff faces <br>
                This plot image will be auto-deleted in one hour. To save it permanently, download the image to your computer
                with Right click -> Save image as.
                </p>
                <p>Features mapped:</p>
                <div class='container-center-limited-40'>
                {1}
                </div>
                <p>Click on the image to view it in full size:</p>
                <a target='_blank' href='{2}'><img class="chernoff-faces-image" src='{2}'></img></a>
            </div>
            """.format(title_plot, features_faces_asssigned_html, url_plot_image)

        except Exception as e:
            result["error"] = True
            result["error_message"] = str(e)
    else:
        result["error"] = True
        result["error_message"] = "The server's disk space is full. Try again later"

    return result
