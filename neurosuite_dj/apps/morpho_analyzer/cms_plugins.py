from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _


@plugin_pool.register_plugin  # register the plugin
class MorphoAnalyzerPluginPublisher(CMSPluginBase):
    module = _("Morpho_analyzer")
    name = _("Morpho_analyzer plugin 1")  # name of the plugin in the interface
    render_template = "cms_plugin1.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context


@plugin_pool.register_plugin  # register the plugin
class MorphoAnalyzerPluginPublisher2(CMSPluginBase):
    module = _("Morpho_analyzer")
    name = _("Morpho_analyzer plugin 2")  # name of the plugin in the interface
    render_template = "cms_plugin2.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

