import io
import json
import logging
import os
import zipfile
import base64
from django.conf import settings
from django.contrib.sessions.models import Session
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.http import HttpResponse
from django.http import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseRedirect,
    Http404
)
import time
import datetime
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.utils.encoding import smart_str
from django.views import View
from django.views.generic.base import TemplateView
import neurosuite_dj.helpers as global_helpers
from io import BytesIO
from io import BytesIO as IO
import requests
from celery.result import AsyncResult
import apps.microscopic_images.tasks as tasks
from celery.task.control import revoke

import neurosuite_dj.helpers_global.datasets_user as datasets_user_helper
from apps.biophysical_models.helpers import io as biophys_io
from apps.biophysical_models.helpers import datasets as biophys_datasets

# Create your views here.

# ----Index----------
def index_biophysical_models(request):
    data = {}

    return render(request, 'index_biophysical_models.html', data)

def select_morpho_electro(request):
    data = {}
    clear_session(request.session)

    data["fields_by_categories"] = biophys_io.AllenSDK.get_fields_by_categories()

    return render(request, 'select_morpho_electro.html', data)

def load_morpho_electro_allen(request):
    data = {}
    query_filters = json.loads(request.POST.get("query-values-json", "{}"))
    custom_filters = json.loads(request.POST.get("custom-options-json", "{}"))

    allensdk = biophys_io.AllenSDK(request.session.session_key)

    morpho_df, electro_df = allensdk.get_morpho_electro_common_dfs()
    #dataframe_morpho = allensdk.get_morpho_dataframe(query_filters)
    #morpho_reconstructions = allensdk.get_morpho_reconstructions(query_filters)
    #dataframe_electro = allensdk.get_electro_dataframe(query_filters)
    #stimuli_data = allensdk.get_electro_stimuli(query_filters)


    dataset_morpho = biophys_datasets.DatasetMorpho({}, "morpho", request.session.session_key, app_name=settings.BIOPHYSICAL_MODELS_APP_NAME)
    dataset_electro = biophys_datasets.DatasetElectro({}, "electro", request.session.session_key, app_name=settings.BIOPHYSICAL_MODELS_APP_NAME)
    dataset_morpho.load(morpho_df)
    dataset_electro.load(electro_df)

    datasets_user = datasets_user_helper.DatasetsUser(settings.BIOPHYSICAL_MODELS_APP_NAME)
    datasets_user.add([dataset_morpho, dataset_electro])
    datasets_user.save(request.session)

    return render(request, 'create_biophys_models.html', data)

def create_biophys_models(request):
    data = {}

    return render(request, 'create_biophys_models.html', data)

def ml_create_model(request):
    data = {}

    return render(request, 'ml_create_model.html', data)

def ml_new_model_ann(request):
    data = {}

    if "datasets_user_" + settings.MORPHO_ANALYZER_APP_NAME in request.session:
        datasets_user = request.session["datasets_user_" + settings.BIOPHYSICAL_MODELS_APP_NAME]

        if "ml_ann" in request.session:
            del request.session['ml_ann']
            request.session.save()
            request.session.modified = True

        data["datasets"] = datasets_user.datasets
        data["features"] = []

    for dataset_name, dataset_value in datasets_user.datasets.items():
        dataframe = dataset_value.get_dataframe()
        path = os.path.join(settings.DATASETS_TMP_PATH, dataset_name)
        dataframe.to_csv(path + ".csv")
        dataframe.to_parquet(path + ".parquet.gzip", engine="fastparquet", compression="gzip")

    return render(request, 'ml_new_model_ann.html', data)

# --------------------------------------------------------------------
# ---------------------General methods-----------------------------------------------
def clear_session(session):
    session.flush()
    session['neurons_user'] = []
    session['datasets_user_' + settings.MORPHO_ANALYZER_APP_NAME] = {}

    return 0