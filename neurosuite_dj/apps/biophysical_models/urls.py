from django.conf.urls import url
from . import views

urlpatterns = [
    # ------Select input data----
    url(r'^select_morpho_electro', views.select_morpho_electro, name='select_morpho_electro'),
    url(r'^load_morpho_electro_allen', views.load_morpho_electro_allen, name='load_morpho_electro_allen'),

    # ------Create biophysical models----
    url(r'^create_biophys_models', views.create_biophys_models, name='create_biophys_models'),

    # ------Machine learning models----
    url(r'^ml_create_model', views.ml_create_model, name='ml_create_model'),
    url(r'^ml_new_model_ann', views.ml_new_model_ann, name='ml_new_model_ann'),

    # ------Root----
    url(r'^$', views.index_biophysical_models, name='index_biophysical_models'),
]
