from django.apps import AppConfig


class BiophysicalModelsConfig(AppConfig):
    name = 'biophysical_models'
