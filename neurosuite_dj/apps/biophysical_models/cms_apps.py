from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


@apphook_pool.register  # register the application
class BiophysicalModelsApphook(CMSApp):
    app_name = "apps.biophysical_models"
    name = _("Biophysical models")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["apps.biophysical_models.urls"]