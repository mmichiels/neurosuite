from .base import  *

class DatasetMorpho(dataset_helper.Dataset):

    def __init__(self, neurons_reconstructions, name, session_id="", dataframe=None, label_column_id="", label_instances="", has_input_dataset=False, extension=".parquet.gzip", app_name="", instances_error=[], delete=False):
        super(DatasetMorpho, self).__init__(name, session_id, dataframe, label_column_id, label_instances, has_input_dataset, extension, app_name, instances_error, delete)
        self.neurons_reconstructions = neurons_reconstructions