from .base import  *

class DatasetElectro(dataset_helper.Dataset):

    def __init__(self, stimuli, name, session_id="", dataframe=None, label_column_id="", label_instances="", has_input_dataset=False, extension=".parquet.gzip", app_name="", instances_error=[], delete=False):
        super(DatasetElectro, self).__init__(name, session_id, dataframe, label_column_id, label_instances, has_input_dataset, extension, app_name, instances_error, delete)
        self.stimuli = stimuli