from .base import  *

from allensdk.core.cell_types_cache import CellTypesCache
from allensdk.api.queries.cell_types_api import CellTypesApi
from allensdk.core.cell_types_cache import ReporterStatus as RS

class AllenSDK(Io):
    def __init__(self, session_key):
        super(AllenSDK, self).__init__()
        self.cell_types_cache_path = os.path.join(settings.ALLEN_CELL_TYPES_CACHE_DIR, session_key)
        manifest_file = os.path.join(self.cell_types_cache_path, "manifest.json")
        self.cell_types_cache = CellTypesCache(manifest_file=manifest_file)
        cells_metadata_with_reconstructions = self.cell_types_cache.get_cells(require_reconstruction=True)
        self.cells_ids_morpho_electro = list(set([cell["id"] for cell in cells_metadata_with_reconstructions])) #Set to remove duplicates


    def get_morpho_electro_common_dfs(self):
        morpho_df = self.get_morpho_dataframe()
        morpho_ids = morpho_df["specimen_id"].values.tolist()

        electro_df = self.get_electro_dataframe()
        electro_df = electro_df.loc[electro_df['specimen_id'].isin(morpho_ids)]
        electro_df.reset_index(drop=True, inplace=True)

        return morpho_df, electro_df

    def get_morpho_dataframe(self, query_filters={}):
        morpho_features = self.cell_types_cache.get_morphology_features()
        morpho_df = pd.DataFrame(morpho_features)

        morpho_df = self.clean_features_dataframe(morpho_df, column_id="specimen_id")

        return morpho_df

    def get_electro_dataframe(self, query_filters={}):
        electro_features = self.cell_types_cache.get_ephys_features()
        electro_df = pd.DataFrame(electro_features)

        electro_df = self.clean_features_dataframe(electro_df, column_id="specimen_id")

        return electro_df

    def clean_features_dataframe(self, dataframe, column_id):

        dataframe = dataframe.loc[:, ~dataframe.columns.duplicated()]
        dataframe.dropna()
        dataframe = dataframe.loc[dataframe[column_id].isin(self.cells_ids_morpho_electro)]
        dataframe.drop_duplicates(keep=False, inplace=True)
        dataframe.drop_duplicates(subset=column_id, keep=False, inplace=True)
        dataframe.sort_values([column_id], axis=0, inplace=True)
        dataframe.reset_index(drop=True, inplace=True)

        return dataframe

    def electro_to_dataframe(self):
        pass

    def morpho_to_dataframe(self):
        pass

    @staticmethod
    def get_fields_by_categories():
        ALLEN_CELL_TYPES_CATEGORIES_FRONTEND = [
            {
                "name": "animal",
                "verbose_name": "Animal",
                "fields":
                    [
                        {
                            "name": "species",
                            "verbose_name": "Species"
                        },
                    ]
            },
            {
                "name": "Reconstruction type",
                "verbose_name": "Completeness",
                "fields":
                    [
                        {
                            "name": "domain",
                            "verbose_name": "Structural domain"
                        }
                    ]
            },
            {
                "name": "morphometry",
                "verbose_name": "Morphometry",
                "fields":
                    [
                        {
                            "name": "surface",
                            "verbose_name": "Surface"
                        },
                        {
                            "name": "shrinkage_corrected",
                            "verbose_name": "Shrinkage corrected"
                        },
                    ]
            },
            {
                "name": "other_fields",
                "verbose_name": "Other fields",
                "fields":
                    [

                    ]
            },
            {
                "name": "custom_options",
                "verbose_name": "Custom options",
                "fields":
                    [
                        {
                            "name": "limit_number_results",
                            "verbose_name": "Limit the number of results",
                            "custom_option": True
                        },
                        {
                            "name": "sort_results",
                            "verbose_name": "Sort results by",
                            "custom_option": True
                        },
                    ]
            },
        ]

        return ALLEN_CELL_TYPES_CATEGORIES_FRONTEND