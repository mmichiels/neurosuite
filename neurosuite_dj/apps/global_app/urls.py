from django.conf.urls import url
from . import views

urlpatterns = [

    # ------Root----
    url(r'^$', views.index_global, name='index_global'),
]
