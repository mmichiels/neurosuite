from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


@apphook_pool.register  # register the application
class GlobalAppApphook(CMSApp):
    app_name = "apps.global_app"
    name = _("GlobalApp")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["apps.global_app.urls"]