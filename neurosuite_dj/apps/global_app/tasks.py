from celery import task,shared_task,current_task
import os
from numpy import random
from celery.contrib import rdb
import json
import pydevd
from datetime import datetime, timezone
import subprocess
from django.core import management
from django.conf import settings
from django.contrib.sessions.models import Session
from django.contrib.sessions.backends.db import SessionStore
import neurosuite_dj.helpers_global.dataset as dataset_helper
import neurosuite_dj.helpers_global.datasets_user as datasets_user_helper
from apps.morpho_analyzer.helpers.machine_learning.bayesian_networks import models as bn_models

@task
def cleanup_expired_sessions():
    print("Clearing all expired sessions")

    """1st. Custom cleanup of session data:"""
    datetime_now_utc = datetime.now(timezone.utc)  # UTC time
    datetime_now = datetime_now_utc.astimezone()  # local time
    expired_sessions = Session.objects.filter(expire_date__lt=datetime_now)
    for session_expired in expired_sessions:
        datasets_user_helper.clean_datasets_folder(session_expired.session_key)
        bn_models.BayesianNetwork.clean_inference_tmp_user_folder(session_expired.session_key)

    """2nd. Cleanup of session entries in the session database table using the Django management command."""
    if len(expired_sessions) > 0:
        manage_py_path_exec = os.path.join(settings.PROJECT_ROOT, "manage.py")
        manage_py_full_command = ["python3", manage_py_path_exec, "clearsessions"]
        manage_py_cmd = ' '.join(manage_py_full_command) #python3 /neurosuite/neurosuite_dj/manage.py clearsessions
        output = subprocess.Popen(manage_py_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output_out, output_err  = output.communicate()
        print("output_out clearsessions: ", output_out)
        print("output_err clearsessions: ", output_err)

    print("Expired session cleared!")

    return 0
