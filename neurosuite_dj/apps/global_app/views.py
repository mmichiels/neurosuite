import io
import json
import logging
import os
import zipfile
import base64
from django.conf import settings
from django.contrib.sessions.models import Session
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.http import HttpResponse
from django.http import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseRedirect,
    Http404
)
import time
import datetime
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.utils.encoding import smart_str
from django.views import View
from django.views.generic.base import TemplateView
import neurosuite_dj.helpers as global_helpers
from io import BytesIO
from io import BytesIO as IO
import requests
from celery.result import AsyncResult
import apps.global_app.tasks as tasks
from celery.task.control import revoke


# Create your views here.

# ----Index----------
def index_global(request):
    data = {}
    set = settings.BASE_DIR
    set2 = settings.PROJECT_ROOT
    tasks.cleanup_expired_sessions()

    return render(request, 'index_global.html', data)
