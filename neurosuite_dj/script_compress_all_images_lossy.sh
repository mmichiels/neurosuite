#!/bin/bash
#This script compress lossy all JPEG, PNG and GIF images in the project

WORKDIR=/neurosuite/neurosuite_dj

function run_lossy_compression() {
    cd $WORKDIR

    #mozjpeg:
    gdebi --n mozjpeg_3.1_amd64.deb
    ln -s /opt/mozjpeg/bin/jpegtran /usr/bin/mozjpegtran
    ln -s /opt/mozjpeg/bin/cjpeg  /usr/bin/mozcjpeg
    ln -s /opt/mozjpeg/bin/djpeg /usr/bin/mozdjpeg

    RED='\033[0;31m'
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color
    EXTENSION_OUTPUT_JPG='COMPRESSED_MOZJPEG'
    find ./ -name "*.jpg" | while read fname; do
      printf "${GREEN}Compressing${NC} ${fname}\n"
      mozdjpeg $fname | mozcjpeg -quality 75 | mozjpegtran -copy none -optimize > $fname.$EXTENSION_OUTPUT_JPG
      if [ -f $fname.$EXTENSION_OUTPUT_JPG ]; then
        rm $fname
        mv $fname.$EXTENSION_OUTPUT_JPG $fname
      fi
    done

    #pngquant:
    apt-get install -y pngquant

    find ./ -iname "*.png" -type f \
    -exec pngquant --ext .png --force 256 {} \;

    #gifsicle:
    apt-get install -y gifsicle

    find ./ -iname '*.gif' -type f \
    -exec gifsicle -O3 --colors 16 -b {} \;

}

run_lossy_compression