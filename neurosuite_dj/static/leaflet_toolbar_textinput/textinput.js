L.textInput = L.Toolbar2.Action.extend({
    options: {
        toolbarIcon: {
            html: '<span class="fa fa-comment-o"></span>',
            tooltip: 'Name'
        }
    },
    initialize: function(map, shape, options) {
        this._shape = shape;

        L.setOptions(this, options);
        L.Toolbar2.Action.prototype.initialize.call(this, map, options);
    },
    addHooks: function () {
        //map.setView([48.85815, 2.29420], 19);
        this.disable();
        map.removeLayer(this.toolbar);

        var keyShape = this._shape._configuration._mykeyB;
        var leafletId = this._shape._leaflet_id;
        var nameShape = "";
        if (this._shape._configuration !== undefined && this._shape._configuration.name !== undefined) {
            var nameShape = this._shape._configuration.name;
        }

        var idIW = L.popup();
        var content = '<span><b>Shape Name</b></span><br/><input id="shapeName" type="text" value="' + nameShape + '" /><br/><input type="button" id="okBtnPopup" leafletId=' + leafletId + ' keyShape="' + keyShape + '" value="Save">';
        //var content = '<span><b>Shape Name</b></span><br/><input id="shapeName" type="text" placeholder="' + nameShape + '" /><br/><br/><span><b>Shape Description<b/></span><br/><textarea id="shapeDesc" cols="25" rows="2"></textarea><br/><br/><input type="button" id="okBtnPopup" leafletId=' + leafletId + ' keyShape="' + keyShape + '" value="Save">';
        idIW.setContent(content);
        if (this._shape instanceof L.Polyline || this._shape instanceof L.Polygon || this._shape instanceof L.Rectangle) {
            var coordinates = this._shape.getLatLngs();
        } else {
            var coordinates  = this._shape.getLatLng();
        }
        this._shape.bindPopup(idIW);
        this._shape.openPopup();
        //idIW.setLatLng(coordinates); //calculated based on the e.layertype
        //idIW.openOn(map);
    },

});