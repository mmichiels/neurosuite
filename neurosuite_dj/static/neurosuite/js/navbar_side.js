/**
 * Created by mario on 29/12/17.
 */
<!-- jQuery CDN -->
var fixed_menu = true;
window.jQuery = window.$ = jQuery;

$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {

        if ($('.sidebar-neurosuite').hasClass("active")) {
            if( screen.width > 768 ) {
                $(".column-content").css("margin-left", 250);
            } else {
                $(".column-content").css("margin-left", 140);
            }
        } else {
            if( screen.width > 768 ) {
                $(".column-content").css("margin-left", 140);
            } else {
                $(".column-content").css("margin-left", 0);
            }
        }

        $('.sidebar-neurosuite').toggleClass('active');
    });
    $('#sidebar ul.not-collapse > li').on('click', function () {
        if ($(this).find('ul.collapse').length) {
            $("ul.not-collapse").find( '> li.active' ).removeClass( 'active' );
        } else {
            $("ul.not-collapse").find( 'li.active' ).removeClass( 'active' );
        }


        $(this).addClass('active');
    });

    $('#sidebar ul.collapse-li > li').on('click', function () {
        $("#sidebar").find( 'li.active' ).removeClass( 'active' );
        $(this).parent().addClass('active');
        $(this).addClass('active');
    });



    $(window).scroll(function () {
        if( screen.width <= 463 ) {
            var navbar_top = $("#navbar-top-neurosuite").height();
            var navbar_top_up = $(".menu_block").height();

            if ($(window).scrollTop() >= navbar_top_up/2) {
                $(".sidebar-neurosuite").css("top", 140);
                if ($(window).scrollTop() >= navbar_top/2+40) {
                    $(".sidebar-neurosuite").css("top", 0);
                    $(".sidebar-neurosuite").css("padding-top", 40);
                }
            } else {
                $(".sidebar-neurosuite").css("top", 270);
                $(".sidebar-neurosuite").css("padding-top", 40);
            }
        }

        if( screen.width > 463 &&  screen.width <= 597) {
            var navbar_top = $("#navbar-top-neurosuite").height();
            var navbar_top_up = $(".menu_block").height();

            if ($(window).scrollTop() >= navbar_top_up/2) {
                $(".sidebar-neurosuite").css("top", 80);
                if ($(window).scrollTop() >= navbar_top/2+40) {
                    $(".sidebar-neurosuite").css("top", 0);
                    $(".sidebar-neurosuite").css("padding-top", 40);
                }
            } else {
                $(".sidebar-neurosuite").css("top", 180);
                $(".sidebar-neurosuite").css("padding-top", 40);
            }
        }

        if( screen.width > 597 &&  screen.width <= 768) {
            var navbar_top = $("#navbar-top-neurosuite").height();
            var navbar_top_up = $(".menu_block").height();

            if ($(window).scrollTop() >= navbar_top_up/2) {
                $(".sidebar-neurosuite").css("top", 10);
                if ($(window).scrollTop() >= navbar_top/2+40) {
                    $(".sidebar-neurosuite").css("top", 0);
                    $(".sidebar-neurosuite").css("padding-top", 40);
                }
            } else {
                $(".sidebar-neurosuite").css("top", 110);
                $(".sidebar-neurosuite").css("padding-top", 40);
            }
        }

    });

});