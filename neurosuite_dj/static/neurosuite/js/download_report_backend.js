$(document).ready( function () {

    $(".button-export").click(function() {
        var export_section = $(this).parents('.buttons-download-report:first');
        var app_id = export_section.attr("app_id");
        var section_report_id = export_section.attr("section_report_id");
        var format = $(this).attr("format")

        var form = $("#report-" + section_report_id);
        var form_id = form.attr("id");
        form.attr("action", "/morpho/export_dataframe/");

        $("#" + form_id + "> #app").val(app_id);
        $("#" + form_id + "> #section").val(section_report_id);
        $("#" + form_id + "> #format").val(format);
        form.submit();
    });

});