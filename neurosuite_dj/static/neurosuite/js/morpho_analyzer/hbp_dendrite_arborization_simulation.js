
$(document).ready( function () {
    var csrftoken = $("#csrftoken").val();

    $("#simulate-neurons-button").click(function() {

        var data_submit = {
            "csrfmiddlewaretoken": csrftoken,
            "input_number_simulated_neurons": $("#input_number_simulated_neurons").val(),
        };

        $.ajax({
            type: "POST",
            url: "/morpho/morpho_simulate_dendrite_arborization/",
            data: data_submit,
            dataType: "json",
            beforeSend: function() {
                $("#results-waiting").hide();
                $("#results-done").hide();
                $("#results-error").hide();
                $("#results-waiting-response").removeClass("hide-element");
                $("#results-waiting-response").show();
            },
            success: function (data) {
                $("#results-done").removeClass("hide-element");
                $("#results-done").show();
                $("#results-waiting").hide();
                $("#results-waiting-response").hide();
                $("#results-error").hide();
            },
            error: function (error, status) {
                console.log("error in morpho_simulate_dendrite_arborization", status);
                $("#results-done").hide();
                $("#results-waiting").hide();
                $("#results-waiting-response").hide();
                $("#results-error").removeClass("hide-element");
                $("#results-error").show();
                $("#text-error").text(error.responseText);
            }
        });

    });

});