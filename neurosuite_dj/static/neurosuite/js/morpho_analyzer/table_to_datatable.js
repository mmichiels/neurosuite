$(document).ready( function () {

    $('table').each(function(){
        if (!$(this).hasClass("not-to-auto-datatable")) {
            create_datatable($(this));
        }
    });

} );


function create_datatable_by_id(id) {
    var table_html = $("#" + id);
    create_datatable(table_html);
}

function create_searchable_strings_column_fields(table_html) {
    if (table_html.hasClass("table-dataframe-html-with-stats")) {
        table_html.append($('<tfoot/>').append(table_html.find("thead tr").clone()));

        var counter = 0;
        table_html.find('tfoot th').each(function (i) {
            var title = $(this).text();
            if (i > 0) {
                $(this).html('<button id="stats-' + counter + '" field_stats="' + counter + '" data-type="continuous" type="button" class="btn btn-primary custom-stats-button"><span class="fa fa-bar-chart icon-margin-right"></span>Stats</button>');
                counter += 1;
            }
        });
    }

    if (table_html.hasClass("table-with-stats")) {
        table_html.find("tfoot").prepend(table_html.find("thead tr").clone());

        table_html.find('tfoot tr:eq(0) th').each(function () {
            var title = $(this).text();
            $(this).html('<input class="search-field-table" type="text" placeholder="' + title + '" />');
        });
    } else {
        table_html.append($('<tfoot/>').append(table_html.find("thead tr").clone()));

        table_html.find('tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input class="search-field-table" type="text" placeholder="' + title + '" />');
        });
    }
}

function create_searchable_range_num_column_fields(table_html) {
    table_html.append($('<tfoot/>').append(table_html.find("thead tr").clone()));

    table_html.find('tfoot th').each(function (i) {
        var title = $(this).text();
        var slider_elem_id = "slider-search-" + title;
        $(this).html('<div id="' + slider_elem_id + '" class="search-field-table search-field-table-range-num" />');

        /*    $(this).html('<div class="slider-inputs-div"><div id="' + slider_elem_id + '" class="search-field-table search-field-table-range-num" />' +
        '<span><input class="input-below-slider" id="' + slider_input_left_id + '" max="255" min="0" step="10" type="number" value="200">' +
        '<input class="input-below-slider icon-margin-left" id="' + slider_input_right_id + '" max="255" min="0" step="10" type="number" value="200">' +
        '</span></div>');*/

        var slider_col = $("#" + slider_elem_id)[0];

        noUiSlider.create(slider_col, {
            start: [0, 1],
            connect: true,
            format: wNumb({
                decimals: 2
            }),
            range: {
                'min': 0,
                'max': 1,
            },
            tooltips: [wNumb({decimals: 1}), true],
            step: 0.01,
            pips: {
                mode: 'count',
                values: 3,
                density: 1,
                format: wNumb({
                    decimals: 1,
                }),
            }
        });
    });

}


function update_searchable_range_num_column_fields(table_html_original, cols_ranges) {
    var table_html = $(".dataTables_scrollHead").find("table");
    var cols_with_numeric_ranges = Object.keys(cols_ranges);

    table_html.find('thead th').each(function (i) {
        i = i.toString();
        var title = $(this).text();

        var slider_elem_id = "slider-search-" + title;
        var slider_col = $("#" + slider_elem_id)[0];
        var col_range = cols_ranges[i];

        if (slider_col.noUiSlider) {
            slider_col.noUiSlider.destroy();
        }

        if (cols_with_numeric_ranges.indexOf(i) !== -1) {
            noUiSlider.create(slider_col, {
                start: [col_range["current_min"], col_range["current_max"]],
                connect: true,
                format: wNumb({
                    decimals: 2
                }),
                range: {
                    'min': col_range["min"],
                    'max': col_range["max"],
                },
                tooltips: [true, true],
                step: 0.01,
                pips: {
                    mode: 'count',
                    values: 3,
                    density: 3,
                    format: wNumb({
                        decimals: 2,
                    }),
                }
            });
            var tooltip_right = $("#" + slider_elem_id).find('.noUi-handle-upper');
            var tooltip_left = $("#" + slider_elem_id).find('.noUi-handle-lower');
            tooltip_right.empty();
            tooltip_left.empty();

            tooltip_right.html($('<div class="noUi-tooltip"><input class="input-below-slider" value="' +  col_range["current_max"]+ '"></div>'));
            tooltip_left.html($('<div class="noUi-tooltip"><input class="input-below-slider" value="' +  col_range["current_min"]+ '"></div>'));
        }
    });
}


function create_datatable(table_html) {
    table_html.removeClass( "dataTable dataframe" );
    table_html.addClass("display table-with-footer");

    var number_columns = table_html.find("tbody > tr:first > td").length;
    var scrollX = false;
    create_searchable_strings_column_fields(table_html);

    if (table_html.hasClass("table-not-scroll-x")) {
        scrollX = false;
    } else {
        scrollX = true;
    }

    if (number_columns > 8 && !table_html.hasClass("table-not-scroll-x")) {
        scrollX = true;
    }

    if (table_html.hasClass("table-scroll-y")) {

        var table = table_html.DataTable( {
            dom: 'lBrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
            ],
            deferRender:    true,
            scrollY:        400,
            scrollCollapse: true,
            scroller:       true,
            paging:         false,
            scrollX: scrollX,
            "aaSorting": [],
            "processing": true,

            "bAutoWidth": false,
            initComplete: function () {
                $('.buttons-copy').addClass('btn btn-md btn-default');
                $('.buttons-copy').html('<span><span></span>Copy</span>');
                $('.buttons-csv').addClass('btn btn-md btn-default');
                $('.buttons-csv').html('<span><span class="fa fa-file-text-o icon-submenu"></span>CSV</span>');
                $('.buttons-excel').addClass('btn btn-md btn-default');
                $('.buttons-excel').html('<span><span class="fa fa-file-excel-o icon-submenu"></span>Excel</span>');
                $('.buttons-pdf').addClass('btn btn-md btn-default');
                $('.buttons-pdf').html('<span><span class="fa fa-file-pdf-o icon-submenu"></span>PDF</span>');
            }
        } );
    } else {
        var table = table_html.DataTable( {
            dom: 'lBrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
            ],
            "pageLength": 10,
            scrollX: scrollX,
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "aaSorting": [],
            "processing": true,
            initComplete: function () {
                $('.buttons-copy').addClass('btn btn-md btn-default');
                $('.buttons-copy').html('<span><span></span>Copy</span>');
                $('.buttons-csv').addClass('btn btn-md btn-default');
                $('.buttons-csv').html('<span><span class="fa fa-file-text-o icon-submenu"></span>CSV</span>');
                $('.buttons-excel').addClass('btn btn-md btn-default');
                $('.buttons-excel').html('<span><span class="fa fa-file-excel-o icon-submenu"></span>Excel</span>');
                $('.buttons-pdf').addClass('btn btn-md btn-default');
                $('.buttons-pdf').html('<span><span class="fa fa-file-pdf-o icon-submenu"></span>PDF</span>');
            }
        } );
    }
    // Apply the search
    table.columns().every(function () {
        var that = this;
        var footer = this.footer();

        $('input', footer).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });


    if (table_html.hasClass("table-with-booleans")) {
        table.cells().every( function () {
            if (this.data() == "True") {
                this.data('<p class="hide-element">Yes</p><span class="glyphicon glyphicon-ok"></span>');
            }
            else if (this.data() == "False") {
                this.data('<p class="hide-element">No</p><span class="glyphicon glyphicon-remove"></span>');
            }
        });
    };

    if (table_html.hasClass("table-with-nones")) {
        table.cells().every( function () {
            if (this.data() == "None") {
                this.data('<p class="hide-element">None</p><span class=\'fa fa-ellipsis-h\'></span>');
            }
        });
    };
}