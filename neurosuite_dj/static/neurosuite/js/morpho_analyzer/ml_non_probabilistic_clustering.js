$(document).ready(function () {
    var csrf_token = $("#csrf_token").val();
    console.log("CSRF: " + csrf_token);

    $(".select-dataset").on("change", function (e) {
        var values_chosen = $(this).val();
        var select_id = $(this).attr("id");
        if (values_chosen.length !== 0 && values_chosen !== "") {
            var value_str = "";
            var choose_value_elem = $('#' + $(this).attr("choose_value_elem"));
            var selectizes_value_elem = $("#" + $(this).attr("selectize_value_elem"));
            var selectizes_value_elem_single_val = false;
            if (selectizes_value_elem.hasClass("is-selectize-single-val")) {
                selectizes_value_elem_single_val = true;
            }

            var waiting_div_elem = $('#' + $(this).attr("waiting_div_elem"));
            choose_value_elem.removeClass("hide-element");
            choose_value_elem.show();

            selectizes_value_elem.each(function( index ) {
                $(this).selectize()[0].selectize.destroy();
                $(this).empty();
            });
            var data_client_json = {
                "datasets_names": values_chosen,
                "include_ids": false,
                "model_name": $("#model_name").val()
            };
            data_client_json = JSON.stringify(data_client_json)
            var data_send = {
                "csrfmiddlewaretoken": csrf_token,
                "data_client_json": data_client_json
            };
            $.ajax({
                type: "POST",
                url: "/morpho/get_features_datasets/",
                data: data_send,
                dataType: 'json',
                beforeSend: function () {
                    waiting_div_elem.removeClass("hide-element");
                    waiting_div_elem.show();
                },
                success: function (data) {
                    waiting_div_elem.hide();
                    $("#error-upload").empty();

                    var values = data["values"];
                    var maxItems = values.length;
                    if (selectizes_value_elem_single_val) {
                        maxItems = 1;
                    }
                    var maxOptions = values.length;
                    var items = values.map(function(x) { return { item: x }; });

                    selectizes_value_elem.selectize({
                        maxItems: maxItems,
                        maxOptions: maxOptions,
                        labelField: "item",
                        valueField: "item",
                        searchField: "item",
                        options: items,
                    });

                },
                error: function () {
                    waiting_div_elem.hide();
                    $("#error-upload").val("Server error");
                }
            });
        }
    });

    $("#select-dataset-discretize-method").on("change", function (e) {
        var method_chosen = $(this).val();
        if( method_chosen == "fayyad_irani" ) {
            $("#section-nbins-for-equal-width-discretization").addClass("hide-element")
        } else {
            $("#section-nbins-for-equal-width-discretization").removeClass("hide-element")
        }
    });

    $("#continue-button-features-selection").click(function(e){
        var filling_na_algorithm = $("#select-filling-algorithm").val();
        var remove_non_variant_features = $("#remove-non-variant-features").prop('checked');
        var normalize_values = $("#checkbox-normalize-values").prop('checked');
        var normalization_method = $("#select-normalizing-algorithm").val();
        var dataset_discretize = $("#checkbox-dataset-discretize").prop('checked');
        var dataset_discretize_method = $("#select-dataset-discretize-method").val();
        var nbins_for_equal_width_discretization = $("#nbins-for-equal-width-discretization").val();
        var apply_feature_subset_selection = $("#checkbox-apply-feature-selection").prop('checked');
        var feature_selection_algorithm = $("#select-feature-selection-algorithm").val();
        var alpha_feature_selection = $("#alpha-feature-selection").val();
        var apply_dimentionality_reduction = $("#checkbox-apply-dimentionality-reduction").prop('checked');
        var pca_algorithm = $("#select-dimensionality-reduction-algorithm").val();
        var when_apply_pca = $("#select-when-dimesionality-reduction").val();
        var n_components = $("#top_features_dimensionality_reduction").val();
        var features_preprocessing = {};
        var datasets_features_names = $("#select-dataset-predictor").val();
        var dataframes_features = {};
        var select_dataframes_features = $("#select-dataset-predictor-features");
        var selectize_dataframes_features = select_dataframes_features[0].selectize;
        var dataframes_features_all = select_dataframes_features.attr("selected-all");

        features_preprocessing["filling_na_algorithm"] = filling_na_algorithm;
        features_preprocessing["remove_non_variant_features"] = remove_non_variant_features;
        features_preprocessing["normalize_values"] = normalize_values;
        features_preprocessing["normalization_method"] = normalization_method;
        features_preprocessing["dataset_discretize"] = dataset_discretize;
        features_preprocessing["discretize_method"] = dataset_discretize_method;
        features_preprocessing["n_bins"] = nbins_for_equal_width_discretization;
        features_preprocessing["apply_feature_subset_selection"] = apply_feature_subset_selection;
        features_preprocessing["feature_selection_algorithm"] = feature_selection_algorithm;
        features_preprocessing["alpha"] = alpha_feature_selection;
        features_preprocessing["apply_dimentionality_reduction"] = apply_dimentionality_reduction;
        features_preprocessing["pca_algorithm"] = pca_algorithm;
        features_preprocessing["when_apply_pca"] = when_apply_pca;
        features_preprocessing["n_components"] = n_components;

        dataframes_features["values"] = select_dataframes_features.val();
        dataframes_features["selected_all"] = false;
        if (dataframes_features_all === "true") {
            dataframes_features["selected_all"] = true;
        }

        if (dataframes_features["values"] == "" && dataframes_features["selected_all"] == false) {
            alert("Features must not be empty")
        } else {
            var progress_elem_html_id = "progress-features-selection";
            var progress_elem_html = $("#" + progress_elem_html_id);
            var waiting_div_elem = $("#waiting-continue-features-selection");
            var error_elem = $("#error-continue-features-selection");
            var success_elem = $("#success-continue-features-selection");
            var next_section_elem = $("#section-learning-algorithm");

            var data_client_json = {
                "features_preprocessing": features_preprocessing,
                "datasets_features_names": datasets_features_names,
                "dataframes_features": dataframes_features,
            };
            data_client_json = JSON.stringify(data_client_json)
            var data_send = {
                "csrfmiddlewaretoken": csrf_token,
                "data_client_json": data_client_json,
            };

            $.ajax({
                type: "POST",
                url: "/morpho/ml_non_probabilistic_features_selection",
                data: data_send,
                dataType: 'json',
                beforeSend: function () {
                    waiting_div_elem.removeClass("hide-element");
                    waiting_div_elem.show();
                    progress_elem_html.show();
                },
                success: function (data_task) {
                    var data_processed = data_task
                    //var data_processed = run_worker(data_task, progress_elem_html_id, function (data_processed) {
                        waiting_div_elem.hide();
                        if (data_processed["error"]) {
                            error_elem.removeClass("hide-element");
                            error_elem.show();
                            error_elem.text("Error: " + data_processed["error_message"]);
                            success_elem.hide();
                        } else {
                            error_elem.empty();
                            success_elem.removeClass("hide-element");
                            success_elem.show();
                            next_section_elem.removeClass("hide-element");
                            next_section_elem.show();
                            $('html, body').animate({
                                scrollTop: next_section_elem.offset().top
                            }, 1000);
                            $('.nav-tabs a[href="#section-algorithm"]').tab('show');
                            $("#select-learning-algorithm").trigger('change');
                            $("#agglomerative_criteria").trigger('change');
                        }
                    //})
                },
                error: function () {
                    waiting_div_elem.hide();
                    success_elem.show();
                    error_elem.text("Server error");
                }
            });
        }
    });

    $("#select-learning-algorithm").on("change", function (e) {
        var algorithm_chosen = $(this).val();
        show_algorithms_parameters(algorithm_chosen);
    });

    $("#agglomerative_criteria").on("change", function (e) {
        var criteria_chosen = $(this).val();
        if (criteria_chosen == "auto") {
            $(".criteria-param").hide();
        }
        if (criteria_chosen == "k") {
            $(".d-param").hide();
            $(".k-param").removeClass("hide-element").show();
        }
        if (criteria_chosen == "distance") {
            $(".k-param").hide();
            $(".d-param").removeClass("hide-element").show();
        }
    });

    $("#spectral_clusters_selection").on("change", function (e) {
        var method_chosen = $(this).val();
        if( method_chosen == "auto" ) {
            $(".optional-k").addClass("hide-element")
        } else {
            $(".optional-k").removeClass("hide-element")
        }
    });

    $("#continue-learn-algorithms").click(function(e) {
        var selected_algorithm = $("#select-learning-algorithm").val();
        var learning_algorithm = {};
        var current_section_elem = $("#section-learning-algorithm");
        var waiting_div_elem = $("#waiting-continue-learn-algorithms");
        var progress_elem_html_id = "progress-supervised-class-learning-algorithms";
        var progress_elem_html = $("#" + progress_elem_html_id);
        var error_elem = $("#error-continue-learn-algorithms");
        var success_elem = $("#success-continue-learn-algorithms");
        var next_section_elem_1 = $("#section-cluster-results");
        var summary_elem = $("#cluster-results")
        var plot_elem = $("#cluster-plot")


        var id_algorithm = selected_algorithm;
        learning_algorithm= {};
        learning_algorithm['algorithm_name'] = id_algorithm
        $("#learning-parameters-" + id_algorithm + " .learning-parameter").each(function() {
            var parameter_name = $(this).attr("id");
            var input_type = $(this).attr("type");
            if (parameter_name !== undefined) {
                var parameter_value = $(this).val();
                if (input_type == "checkbox") {
                    parameter_value = $(this).is(":checked");
                }
                learning_algorithm[parameter_name] = parameter_value;
            }
        });


        var data_client_json = {
            "learning_algorithm": learning_algorithm,
            "model_name": $("#model_name").val()
        };
        data_client_json = JSON.stringify(data_client_json)
        var data_send = {
            "csrfmiddlewaretoken": csrf_token,
            "data_client_json": data_client_json,
        };

        $.ajax({
            type: "POST",
            url: "/morpho/ml_non_probabilistic_class_learn/",
            data: data_send,
            dataType: 'json',
            beforeSend: function () {
                waiting_div_elem.removeClass("hide-element");
                waiting_div_elem.show();
                progress_elem_html.show();
                error_elem.hide();
            },
            success: function (data_task) {
                var data_processed = data_task
                //var data_processed = run_worker(data_task, progress_elem_html_id, function(data_processed){
                    waiting_div_elem.hide();
                    if (data_processed["error"]) {
                        error_elem.removeClass("hide-element").show();
                        error_elem.text("Error: " + data_processed["error_message"]);
                    } else {
                        success_elem.removeClass("hide-element").show();
                        next_section_elem_1.removeClass("hide-element").show();
                        current_section_elem.removeClass("last-section-page");

                        $('html, body').animate({
                            scrollTop: next_section_elem_1.offset().top
                        }, 1000);

                        $('#plot').remove();
                        $('#dendrogram').remove();
                        $('#elbow').remove();
                        $('#table-clusters > thead').children().remove();
                        $('#table-clusters > tbody').children().remove();
                        $('#big-table-clusters > thead').children().remove();
                        $('#big-table-clusters > tbody').children().remove();
                        $('#table-centroids > thead').children().remove();
                        $('#table-centroids > tbody').children().remove();
                        console.log("helow");

                        clusters = JSON.parse(data_task.summary.clusters);

                        if(selected_algorithm == "k-means" || selected_algorithm == "agglomerative") {
                            $('#table-clusters > thead').append("<th>" + "Cluster" +"</th>" +"<th>" + "Number of instances" +"</th>" +"<th>" + "Percentage" +"</th>")
                            for (var key in clusters) {
                                // check if the property/key is defined in the object itself, not in parent
                                if (clusters.hasOwnProperty(key)) {
                                    cluster = key;
                                    instances = clusters[key];
                                    $('#table-clusters > tbody').append( "<tr>" + "<td>"
                                + cluster +"</td>" + "<td>" + instances[0] +"</td>" + "<td>" + instances[1] + " %" +"</td>" + "</tr>");
                                }
                            }
                        } else {
                            $('#big-table-clusters > thead').append("<th>" + "Cluster" +"</th>" +"<th>" + "Number of instances" +"</th>" +"<th>" + "Percentage" +"</th>")
                            for (var key in clusters) {
                                // check if the property/key is defined in the object itself, not in parent
                                if (clusters.hasOwnProperty(key)) {
                                    cluster = key;
                                    instances = clusters[key];
                                    $('#big-table-clusters > tbody').append( "<tr>" + "<td>"
                                + cluster +"</td>" + "<td>" + instances[0] +"</td>" + "<td>" + instances[1] + " %" +"</td>" + "</tr>");
                                }
                            }
                        }



                        if( selected_algorithm == "k-means") {

                            features = JSON.parse(data_task.summary.features);
                            features_row = "<tr><th>" + "Cluster" +"</th>"
                            for (var feature in features) {
                                features_row += "<th>" + features[feature] +"</th>";
                            }
                            features_row += "</tr>";
                            $('#table-centroids > thead').append(features_row);

                            centroids = JSON.parse(data_task.summary.centroids);
                            for (var cluster in centroids) {
                                var features = centroids[cluster];
                                centroid_row = "<tr><td>" + (parseInt(cluster) + 1).toString() +"</td>";
                                for (var feature in features) {
                                    centroid_row += "<td>" + features[feature] +"</td>";
                                }
                                centroid_row += "</tr>";
                                $('#table-centroids > tbody').append(centroid_row);
                            }

                            var elbow = document.createElement('img');
                            elbow.src = 'data:image/png;base64,' + data_task.summary.elbow_method_plot;
                            elbow.id = 'elbow'
                            $('#cluster-options').append(elbow);
                        }
                        if( selected_algorithm == "agglomerative") {

                            var dendrogram = document.createElement('img');
                            dendrogram.src = 'data:image/png;base64,' + data_task.summary.dendrogram;
                            dendrogram.id = 'dendrogram'
                            $('#section-cluster-plot').append(dendrogram);

                            var elbow = document.createElement('img');
                            elbow.src = 'data:image/png;base64,' + data_task.summary.elbow_method_plot;
                            elbow.id = 'elbow'
                            $('#cluster-options').append(elbow);
                        }
                        var plot = document.createElement('img');
                        plot.src = 'data:image/png;base64,' + data_task.summary.plot;
                        plot.id = 'plot'
                        $('#section-cluster-plot').append(plot);

                        $(".form-empty-csrfmiddlewaretoken").each(function(index) {
                            $(this).val(csrf_token);
                        });
                        $('.nav-tabs a[href="#section-results"]').tab('show');
                    }
                //});
            },
            error: ( function(e) {
                waiting_div_elem.hide();
                success_elem.hide();
                error_elem.removeClass("hide-element").show();
                error_elem.text("Error: check if the selected columns have NaN or empty values");
            })
        });
    });
});

function show_algorithms_parameters(algorithm_chosen) {
    var placeholder_algorithm_paremeters = $("#learning-parameters-" + algorithm_chosen);

    $("#section-select-algorithm-parameters").removeClass("hide-element").show();

    $(".learning-algorithm-parameters").hide(); //Close old algorithm parameters input
    placeholder_algorithm_paremeters.show();
}