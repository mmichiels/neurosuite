$(document).ready( function () {

    console.log("My NEURON");

    // Draw canvas

    $('.renderCanvas').each(function(i, elem) {
        console.log("DEBUG");
        var canvas_id = $(this).attr("id");
        var canvas = document.getElementById(canvas_id);
        var neuron_name_input = $(this).attr("neuron_name");

        // Drawer
        var drawer;
        // Controls
        var controls;
        // Current reconstruction
        var reconstruction;

        // Modules
        var core;
        var reader;
        var babylon;
        var control;

        System.import('@neuroviewer/core').then(function (m) {
            core = m;
            System.import('@neuroviewer/reader').then(function (m) {
                reader = m;
                System.import('@neuroviewer/babylon-drawer').then(function (m) {
                    babylon = m;
                    System.import('@neuroviewer/control').then(function (m) {
                        control = m;

                        drawer = new babylon.BabylonDrawer(canvas);
                        drawer.init();

                        var input, file, fr, ext, parser;

                        if (reconstruction)
                            reconstruction.dispose();

                        if (controls)
                            controls.dispose();


                        if (typeof window.FileReader !== 'function') {
                            alert("The file API isn't supported on this browser yet.");
                            return;
                        }

                        //file = input.files[0];
                        var neuron_name = $("#neuron_name_" + neuron_name_input).val();
                        var ext = $("#neuron_data_file_extension_" + neuron_name_input).val().substring(1); //substring(1) is used to delete the dot of the extension
                        var blob_b64 = $("#neuron_data_file_" + neuron_name_input).val();
                        var file = b64toBlob(blob_b64, "text/plain");
                        parser = reader.parserByFileExtension(ext);
                        fr = new FileReader();

                        fr.onload = function receivedText(e) {
                            try {
                                lines = e.target.result;
                                reconstruction = parser.read(lines, null);
                                reconstruction.attachDrawer(drawer);
                                //reconstruction.singleElementDraw = true;
                                reconstruction.linearDrawing = true;

                                reconstruction.draw();
                            } catch (e) {
                                console.log("Error drawing the neuron ", e);
                                $("#error_loading_neuron").show();
                                $("#renderCanvas").hide();
                            }

                            // Reset camera
                            drawer.resetCamera();

                            // Normalize scene
                            drawer.normalizeScene();

                            // Optimize
                            // drawer.optimize(0);

                            // Create grid
                            drawer.createGrid(babylon.default_config_grid);

                            // Create controls
                            controls = new control.Control(drawer, reconstruction);
                            controls.showOptions();
                            controls.showNeuron();
                            var control_layer = $("#ControlLayer");
                            control_layer.attr("id", control_layer.attr("id")+i);
                            control_layer.addClass("ControlLayer");
                            var counter = i;

                            $("#ControlLayer" + i).find("*").each(function (k, elem) {
                                if ($(this).attr("id")) {
                                    if ($(this).attr("id") !== "animate_btn" && $(this).attr("id") !== "camera_alpha" && $(this).attr("id") !== "camera_beta" && $(this).attr("id") !== "reconstruction_list") {
                                        $(this).attr("id", $(this).attr("id") + counter);
                                    }
                                }
                                if ($(this).attr("target")) {
                                    $(this).attr("target", $(this).attr("target") + counter);
                                }
                                if ($(this).attr("data-target")) {
                                    $(this).attr("data-target", $(this).attr("data-target") + counter);
                                }
                            });
                        };

                        fr.readAsText(file);

                    });
                });
            });
        });


    });





} );



function fix_ids() {

}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}
