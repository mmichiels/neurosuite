$(document).ready( function () {
    var csrf_token = $("#csrf_token").val();

    $("#select-dataset").on("change", function (e) {
        $("#continue-export-buttons").removeClass("hide-element").show();
    });

    $(".button-export-dataset").click(function(e) {
        var format_file = $(this).attr("file-format");
        var dataset_name = $("#select-dataset").val();

        var waiting_div_elem = $('#waiting-export-dataset');
        var error_elem = $('#error-export-dataset');
        if (dataset_name.length !== 0 && dataset_name !== "") {
            var data_client_json = {
                "format_file": format_file,
                "dataset_name": dataset_name,
            };
            data_client_json = JSON.stringify(data_client_json)
            var data_send = {
                "csrfmiddlewaretoken": csrf_token,
                "data_client_json": data_client_json
            };
            $.ajax({
                type: "POST",
                url: "/morpho/morpho_run_export_datasets/",
                data: data_send,
                dataType: 'json',
                beforeSend: function () {
                    waiting_div_elem.removeClass("hide-element");
                    waiting_div_elem.show();
                },
                success: function (data) {
                    waiting_div_elem.hide();
                    $("#dataframe_exported_download").attr("href", data["dataframe_exported_url"])
                    $("#dataframe_exported_download")[0].click()
                },
                error: function (error) {
                    waiting_div_elem.hide();
                    error_elem.removeClass('hide-element').show().text("Error exporting the dataset. Try again later.");
                }
            });
        } else {
            error_elem.removeClass('hide-element').show().text("Error: no datasets selected.");
        }

    });

});