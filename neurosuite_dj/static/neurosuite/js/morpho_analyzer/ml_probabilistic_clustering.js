var prob_clustering_table = {};
var datatable_is_empty = false;

$(document).ready(function () {
    var csrf_token = $("#csrf_token").val();

    $("#upload-clustering-structures").fileinput({
        hideThumbnailContent: true, // hide image, pdf, text or other content in the thumbnail preview
        theme: "explorer",
        uploadUrl: "/morpho/ml_upload_probabilistic_clustering_model_graph/",
        fileActionSettings: {
            showDownload: false,
            showUpload: false,
            indicatorNew: '',
            removeIcon: '<i class="glyphicon glyphicon-trash fa-2x" aria-hidden="true"></i>',
            zoomIcon: '<i class="glyphicon glyphicon-zoom-in fa-2x"></i>',
        },
        previewClass: "file-input-small container-center",
        layoutTemplates: {
            main1: "<div class=\'container-center\'>\n" +
                "{preview}\n" +
                "<div class=\'input-group {class} file-input-browse-small\'>\n" +
                "   <div class=\'input-group-btn\ input-group-prepend file-input-button-small'>\n" +
                "       {browse}\n" +
                "       {upload}\n" +
                "       {remove}\n" +
                "   </div>\n" +
                "   {caption}\n" +
                "</div>" +
                "</div>"
        },
        browseIcon: "<i class=\"glyphicon glyphicon-folder-open icon-margin-right\"></i> ",
        uploadExtraData: function (previewId, index) {
            var obj = {};
            obj['csrfmiddlewaretoken'] = csrf_token;
            obj['structure'] = 1;
            return obj;
        },
        uploadAsync: false
    });

    $('#upload-clustering-structures').on('filebatchpreupload', function () {
        $("#select-group-graph").addClass("hide-element").hide();
        $("#select-nodes-graph-evidence-effect").addClass("hide-element").hide();
        $("#select-group-graph-evidence-effect").addClass("hide-element").hide();
    });

    $('#upload-clustering-structures').on('filebatchuploadcomplete', function () {
        $("#upload-complete-label").text("Structure upload complete!");
        $("#glasso-params-section").hide();
        $("#continue-button-upload-bn").trigger("click");
    });

    $("#upload-clustering-files").fileinput({
        hideThumbnailContent: true, // hide image, pdf, text or other content in the thumbnail preview
        theme: "explorer",
        uploadUrl: "/morpho/ml_upload_probabilistic_clustering_model/",
        fileActionSettings: {
            showDownload: false,
            showUpload: false,
            indicatorNew: '',
            removeIcon: '<i class="glyphicon glyphicon-trash fa-2x" aria-hidden="true"></i>',
            zoomIcon: '<i class="glyphicon glyphicon-zoom-in fa-2x"></i>',
        },
        previewClass: "file-input-small container-center",
        layoutTemplates: {
            main1: "<div class=\'container-center\'>\n" +
                "{preview}\n" +
                "<div class=\'input-group {class} file-input-browse-small\'>\n" +
                "   <div class=\'input-group-btn\ input-group-prepend file-input-button-small'>\n" +
                "       {browse}\n" +
                "       {upload}\n" +
                "       {remove}\n" +
                "   </div>\n" +
                "   {caption}\n" +
                "</div>" +
                "</div>"
        },
        browseIcon: "<i class=\"glyphicon glyphicon-folder-open icon-margin-right\"></i> ",
        uploadExtraData: function (previewId, index) {
            var obj = {};
            obj['csrfmiddlewaretoken'] = csrf_token;
            obj['structure'] = 1;
            return obj;
        },
        uploadAsync: false
    });

    $('#upload-clustering-files').on('filebatchpreupload', function () {
        $("#select-group-graph").addClass("hide-element").hide();
        $("#select-nodes-graph-evidence-effect").addClass("hide-element").hide();
        $("#select-group-graph-evidence-effect").addClass("hide-element").hide();
    });

    $('#upload-clustering-files').on('filebatchuploadcomplete', function () {
        $("#upload-complete-label").text("Structure upload complete!");
        $("#glasso-params-section").removeClass("hide-element").show();
        $("#continue-button-upload-bn").trigger("click");
    });

    $(".load-prob-clustering-example").click(function(e){
        var waiting_div_elem = $("#waiting-load-prob-clustering");
        var error_elem = $("#error-load-prob-clustering");

        var data_client_json = {
            "model_example_name": $(this).attr("model-example-name"),
        };
        data_client_json = JSON.stringify(data_client_json);
        var data_send = {
            "csrfmiddlewaretoken": $("#csrf_token").val(),
            "data_client_json": data_client_json,
        };

        $.ajax({
            type: "POST",
            url: "/morpho/ml_prob_clustering_load_example/",
            data: data_send,
            dataType: 'json',
            beforeSend: function () {
                waiting_div_elem.removeClass("hide-element").show();
                error_elem.hide();
            },
            success: function (data) {
                waiting_div_elem.hide();

                if (!data["error"]) {
                    error_elem.hide();
                    $("#glasso-params-section").removeClass("hide-element").show();
                    $("#continue-button-upload-bn").trigger("click");
                } else {
                    error_elem.text(data["error_message"]);
                    error_elem.removeClass("hide-element").show();
                }
            },
            error: function () {
                waiting_div_elem.hide();
                error_elem.text("Server error");
                error_elem.removeClass("hide-element").show();

            }
        });
    });


    var prob_clustering_table = create_prob_clustering_datatable([]);

    $("#select-all-columns-clustering-table").click(function(e){
        datatable_is_empty = false;
        var columns_names = all_cols_names.replace(/'/g, '"');
        columns_names = JSON.parse(columns_names);

        restart_prob_clustering_datatable(columns_names);
    });

    $("#deselect-all-columns-clustering-table").click(function(e) {
        restart_prob_clustering_empty_datatable();
    });

    $("#select-columns-clustering-table").change(function(e){
        var columns_names_individuals = $(this).val();
        var group_id = "Custom groups";
        var categories_ids = $("#select-columns-category-clustering-table").val();

        get_cols_in_category(group_id, categories_ids, function(data) {
            var columns_names_categories = [];
            if (!data["error"]) {
                columns_names_categories = data["nodes_in_categories"];
            }
            var columns_names = concatenate_arrays_no_duplicates(columns_names_individuals, columns_names_categories)
            update_prob_clustering_table(columns_names);
        });
    });

});

function create_prob_clustering_datatable(columns_names) {
    var datatable_html = $("#prob-clustering-table");

    create_searchable_range_num_column_fields(datatable_html);

    var prob_clustering_table = $('#prob-clustering-table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/morpho/ml_prob_clustering_table/',
            type: 'POST',
            data: function(data) {
                data.csrfmiddlewaretoken = $("#csrf_token").val();
                data.is_empty = datatable_is_empty;
                data.cols_names = columns_names;
            },
            dataType: 'json',
            dataFilter: update_table_external_elems,
        },
        dom: '<"top"f>rt<"bottom"ip><"clear">',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
        ],
        scrollX: true,
        initComplete: function (response_json) {
            $('.buttons-copy').addClass('btn btn-md btn-default');
            $('.buttons-copy').html('<span><span></span>Copy</span>');
            $('.buttons-csv').addClass('btn btn-md btn-default');
            $('.buttons-csv').html('<span><span class="fa fa-file-text-o icon-submenu"></span>CSV</span>');
            $('.buttons-excel').addClass('btn btn-md btn-default');
            $('.buttons-excel').html('<span><span class="fa fa-file-excel-o icon-submenu"></span>Excel</span>');
            $('.buttons-pdf').addClass('btn btn-md btn-default');
            $('.buttons-pdf').html('<span><span class="fa fa-file-pdf-o icon-submenu"></span>PDF</span>');
        },
        "language": {
            "search": "Search string:"
        }
        //scrollY: 620,
    });

    return prob_clustering_table
}

function update_table_external_elems(response) {
    var response_json = JSON.parse(response);
    var datatable_html = $("#prob-clustering-table");

    $("#cols-total").text(response_json["colsTotal"]);
    $("#cols-filtered").text(response_json["colsFiltered"]);

    update_searchable_range_num_column_fields(datatable_html, response_json["colsRanges"]);

    var cols_style = response_json["colsStyle"];
    for (let [key, value] of Object.entries(cols_style)) {
        $("#col-" + key).css("background-color",  value["color"]);
    }

    $("#prob-clustering-table").DataTable().columns().every(function (col) {
        //console.log("click col every: ", col);
        var that = this;
        var footer_col = $('.search-field-table-range-num')[col];
        // var col_id = ""
        // $("#col-" + col).css("background-color",  cols_style[col]["color"]);

        if (footer_col !== undefined) {
            var slider_col = $('.search-field-table-range-num')[col].noUiSlider;
            var slider_col_elem = $('.search-field-table-range-num').eq(col);
            var slider_col_input_right = $('.search-field-table-range-num').eq(col).find('.noUi-handle-upper .noUi-tooltip').find('input');
            var slider_col_input_left = $('.search-field-table-range-num').eq(col).find('.noUi-handle-lower .noUi-tooltip').find('input');

            if (slider_col !== undefined) {
                $(".input-below-slider").on("click", function(e) {
                    $(this).val("");
                    e.preventDefault();
                });
                var is_new_input_val = false;
                var is_enter_clicked = false;

                slider_col_input_right.on("blur", function(e){
                    e.preventDefault();
                    if (is_enter_clicked) {
                        is_enter_clicked = false;
                        is_new_input_val = false;
                    } else {
                        is_new_input_val = true;
                    }
                    slider_col_input_left.trigger("change");
                });

                slider_col_input_right.on("keydown", function (e) {
                    var key = e.which;
                    if(key == 13)  // the enter key code
                    {
                        is_enter_clicked = true;
                        $(this).trigger('change');
                        return false;
                    }
                });

                slider_col_input_left.on("keydown", function (e) {
                    var key = e.which;
                    if(key == 13)  // the enter key code
                    {
                        is_enter_clicked = true;
                        $(this).trigger('change');
                        return false;
                    }
                });

                slider_col_input_right.on("change", function(e) {
                    slider_col.set([null, this.value]);
                });
                slider_col_input_left.on("change", function(e) {
                    slider_col.set([this.value, null]);
                });

                slider_col.off("slide");
                slider_col.on('slide', function (values, handle) {
                    is_new_input_val = true;
                    slider_col_input_right.val(values[1]);
                    slider_col_input_left.val(values[0]);
                });

                slider_col.off("set");
                slider_col.on('set', function (values, handle) {
                    if ((is_new_input_val || is_enter_clicked)) {
                        console.log("backend");
                        is_new_input_val = false;
                        if (that.search() !== values) {
                            that
                                .search(values)
                                .draw();
                        }
                    }
                });
            }
        }
    });

    return response;
}

function destroy_datatable_prob_clustering() {
    var datatable_html_id = "prob-clustering-table";
    var datatable_html = $("#" + datatable_html_id);
    datatable_html.DataTable().destroy();

    $("#" + datatable_html_id + " thead tr").empty();
    $("#" + datatable_html_id + " tfoot tr").empty();
    var body_table = $("#" + datatable_html_id + " tbody");
    body_table.empty();
    body_table.append("<tr><td></td></tr>");
}


function restart_prob_clustering_table_html() {
    destroy_datatable_prob_clustering();
    var datatable_html = $("#prob-clustering-table");
    var header_table = datatable_html.find("thead tr");
    var col_id = $("#prob_clustering_table_col_id").val();

    header_table.append("<th id='" + col_id + "'>" + col_id + "</th>");

    return header_table;
}

function restart_prob_clustering_empty_datatable() {
    datatable_is_empty = true;
    var columns_names = [];
    restart_prob_clustering_table_html();
    create_prob_clustering_datatable(columns_names);
    $("#cols-filtered").text(1);
}

function restart_prob_clustering_datatable(columns_names) {
    datatable_is_empty = false;

    var header_table = restart_prob_clustering_table_html();
    for (let i = 0; i < columns_names.length; i++) {
        header_table.append("<th id='col-" + columns_names[i] + "'>" + columns_names[i] + "</th>");
    }
    create_prob_clustering_datatable(columns_names);
}

function update_prob_clustering_table(columns_names) {
    if (columns_names.length == 0) {
        restart_prob_clustering_empty_datatable();
    }
    else {
        restart_prob_clustering_datatable(columns_names);
    }
}

function initialize_selectize_structures(structures_info, filter_by) {
    var selectize_structure = $("#selectize-bn-structure")[0].selectize;
    var selectize_multi_structures = $("#selectize-multi-structures")[0].selectize;
    selectize_structure.clearOptions();
    selectize_multi_structures.clearOptions();

    var list_elems = [];
    var filter_greater = true;

    if (filter_by === "structure_id") {
        filter_greater = false;
    }

    for (const [structure_id, structure_data] of Object.entries(structures_info)) {
        let val = structure_id;
        if (filter_by !== "structure_id") {
            val = structure_data[filter_by];
        }

        list_elems.push({"key": structure_id, "value": val})
    }
    list_elems.sort(function (a, b) {
        if (filter_greater) {
            return a.value < b.value ;
        } else {
            return a.value > b.value ;
        }
    });

    selectize_structure.addOption({value: "all", text: "All"});
    // selectize_structure.addOption({value: "common", text: "Common edges (in > 1 clusters)"});
    // selectize_structure.addOption({value: "common-all", text: "Common edges (in all clusters)"});

    for (const [key, data] of Object.entries(list_elems)) {
        let structure_id = data["key"];
        let color_structure = structures_info[structure_id]["color"];
        let val = structures_info[structure_id]["percentage_instances"];
        let select_option = {
            value: structure_id, text: "Cluster " + structure_id,
            additional_text: val,
            color: color_structure};
        selectize_structure.addOption(select_option);
        selectize_multi_structures.addOption(select_option);
    }

    selectize_structure.addItem("all");
}


function init_update_selectize_datatable_cols_groups() {
    var select_groups_cols_input = $("#select-columns-group-clustering-table");
    var selectize_groups_cols_input = select_groups_cols_input[0].selectize;
    var section_category_columns_clustering_table = $("#category-columns-clustering-table");
    var select_categories_cols_input = $("#select-columns-category-clustering-table");
    var selectize_categories_cols_input = select_categories_cols_input[0].selectize;

    var data_client_json = {
        "model_name": $("#model_name").val()
    };
    data_client_json = JSON.stringify(data_client_json);
    var data_send = {
        "csrfmiddlewaretoken": $("#csrf_token").val(),
        "data_client_json": data_client_json,
    };

    $.ajax({
        type: "POST",
        url: "/morpho/ml_bn_get_groups/",
        data: data_send,
        dataType: 'json',
        success: function (data) {
            var categories_in_group = data["groups"];
            var items = categories_in_group.map(function (x) {
                return {item: x};
            });
            items.push({"item": "No group"});

            if (select_groups_cols_input.selectize()[0].selectize) {
                for (i = 0; i < items.length; i++) {
                    selectize_groups_cols_input.addOption({value: categories_in_group[i], text: categories_in_group[i]});
                }
            } else {
                selectize_groups_cols_input.destroy();
                select_groups_cols_input.empty();
                select_groups_cols_input.selectize({
                    maxOptions: categories_in_group.length + 1,
                    labelField: "item",
                    valueField: "item",
                    searchField: "item",
                    options: items,
                });
            }

            select_groups_cols_input.unbind('change');
            select_groups_cols_input.on("change", function (e) {
                var group_id = $(this).val();
                if (group_id === "No group") {
                    section_category_columns_clustering_table.hide();
                    selectize_clear_items(selectize_categories_cols_input);
                } else {
                    set_categories_in_group(group_id);
                }
            });
        }
    });
}

function set_categories_in_group(group_id) {
    var section_category_columns_clustering_table = $("#category-columns-clustering-table");
    var select_categories_cols_input = $("#select-columns-category-clustering-table");
    var selectize_categories_cols_input = select_categories_cols_input[0].selectize;

    var data_client_json = {
        "group_id": group_id,
        "model_name": $("#model_name").val()
    };
    data_client_json = JSON.stringify(data_client_json);
    var data_send = {
        "csrfmiddlewaretoken": $("#csrf_token").val(),
        "data_client_json": data_client_json,
    };

    $.ajax({
        type: "POST",
        url: "/morpho/ml_bn_category_values_in_group/",
        data: data_send,
        dataType: 'json',
        success: function (data) {
            section_category_columns_clustering_table.removeClass("hide-element").show();

            var categories_in_group = data["categories_in_group"];
            var items = categories_in_group.map(function (x) {
                return {item: x};
            });

            if (select_categories_cols_input.selectize()[0].selectize) {
                for (i = 0; i < items.length; i++) {
                    selectize_categories_cols_input.addOption({value: categories_in_group[i], text: categories_in_group[i]});
                }
            } else {
                selectize_categories_cols_input.destroy();
                select_categories_cols_input.empty();
                select_categories_cols_input.selectize({
                    maxOptions: categories_in_group.length + 1,
                    labelField: "item",
                    valueField: "item",
                    searchField: "item",
                    options: items,
                });
            }

            select_categories_cols_input.unbind('change');
            select_categories_cols_input.on("change", function (e) {
                var categories_ids = $(this).val();
                get_cols_in_category(group_id, categories_ids, function(data){
                    var columns_names_categories = [];
                    if (!data["error"]) {
                        columns_names_categories = data["nodes_in_categories"];
                    }
                    var columns_names_individuals = $("#select-columns-clustering-table").val();
                    var columns_names = concatenate_arrays_no_duplicates(columns_names_individuals, columns_names_categories)
                    update_prob_clustering_table(columns_names);
                });
            });
        }
    });
}


function get_cols_in_category(group_id, categories_ids, callback) {
    var data_client_json = {
        "group_id": group_id,
        "categories_ids": categories_ids,
        "model_name": $("#model_name").val()
    };
    data_client_json = JSON.stringify(data_client_json);
    var data_send = {
        "csrfmiddlewaretoken": $("#csrf_token").val(),
        "data_client_json": data_client_json,
    };

    $.ajax({
        type: "POST",
        url: "/morpho/ml_bn_get_nodes_in_categories/",
        data: data_send,
        dataType: 'json',
        success: function (data) {
            callback(data);
        },
        error: function () {
        }
    });
}

function concatenate_arrays_no_duplicates(array1, array2) {
    var result = [];

    result = array1.concat(array2.filter(function (item) {
        return array1.indexOf(item) < 0;
    }));

    return result;
}
