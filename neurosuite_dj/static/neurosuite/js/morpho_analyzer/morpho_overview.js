$(document).ready(function () {

    $.ajax({
        url: '/morpho/load_data_morpho_overview',
        beforeSend: function () {
            $("#loading-overview-data").show();
        },
        success: function(data) {
            $("#loading-overview-data").hide();
            $('#table-neurons-overview').html(data);
            create_datatable_by_id("table-neurons-overview");

            $('.table-neurons-overview tbody').on('click', '.neuron_png_cell_table', function () {
                $('.neuron_png_full').attr('src', $(this).attr('src'));
                $('#neuron_png_modal').modal('show');
                var id_neuron = $(this).closest('tr').attr('id');
                $(".text_neuron_png_modal").html(id_neuron);
            });
        }, error:  function(data) {
            //$("#loading-overview-data").hide();
            console.log("ERROR", data);
            $("#loading-overview-data").html("<span><strong>Error. Try again later</strong></span>")
        }


    });


})