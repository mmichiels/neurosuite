$(document).ready( function () {

    $('.convert_format_neuron').click(function () {
        var form = $(this.form);

        var format_convert = $(this).attr("format_convert");
        var neuron_name = $(this).attr("neuron-name");
        $("#output-format-form").val(format_convert);
        $("#neuron-name-form").val(neuron_name);

        form.submit();
    });

    $(".convert_format_all").click(function(){
        var format_convert = $(this).attr("format_convert");
        var form = $(this.form);
        var output_format = $("#output-format-all");
        output_format.val(format_convert);
        form.submit();
    });


} );