$(document).ready(function () {




    $('.sidebar-header').on('click', '.neuron_png_full', function () {
        $('.neuron_png_full').attr('src', $(this).attr('src'));
        $('#neuron_png_modal').modal('show');
        var id_neuron = $("#neuron_name").val();
        $(".text_neuron_png_modal").html(id_neuron);
    });

    my_jsons = [];
    $(".neurostr_json_result_string").each(function() {
        var json_string = $(this).val();
        var data = JSON.parse(json_string);
        my_jsons.push(data);
        $(this).next(".json-renderer").jsonViewer(data,  {collapsed: true});
    });


    $('.convert_format_neuron').click(function () {
        var form = $(this.form);

        var format_convert = $(this).attr("format_convert");
        var neuron_name = $(this).attr("neuron-name");
        $("#output-format-form").val(format_convert);
        $("#neuron-name-form").val(neuron_name);

        form.submit();
    });



})