$(document).ready( function () {
    var csrf_token = $("#csrf_token").val();
    var text_copied_to_clipboard = "";

    $("#select-field-bivariate-modal").selectize({
        maxItems: 1,
        create: false,
        onInitialize: function() {
            this.setValue("");
        },
    });


    $("#select-distribution-hypothesis-distribution").selectize({
        maxItems: 1,
        create: false,
        onInitialize: function() {
            this.setValue("norm");
        },
    });

    $("#select-distribution-hypothesis-distribution-discr").selectize({
        maxItems: 1,
        create: false,
        onInitialize: function() {
            this.setValue("binom");
        },
    });


    $(".load-selectize-features").click(function(e){
        var select_elem_id = $(this).attr("select-elem");
        var select_elem = $("#" + select_elem_id);
        var data_loaded = select_elem.hasClass("data-loaded");
        var buttons_all = $("#all-" + select_elem_id);
        var maxItems = parseInt($(this).attr("max-items"));

        if (!data_loaded) {
            var data_client_json = {
                "dataset_name": $("#dataset_name").val(),
                "include_ids": false,
                "csrfmiddlewaretoken": csrf_token,
            };
            var data_client_json = JSON.stringify(data_client_json);
            var data_to_send = {
                "data_client_json": data_client_json,
                "csrfmiddlewaretoken": csrf_token,
            };
            var that = $(this);
            $.ajax({
                type: "POST",
                url: "/morpho/get_features_dataset/",
                data: data_to_send,
                dataType: 'json',
                beforeSend: function () {

                },
                success: function (data) {
                    that.hide();
                    select_elem.removeClass("hide-element");
                    select_elem.show();
                    buttons_all.removeClass("hide-element");
                    buttons_all.show();
                    var values = data["values"];
                    if (maxItems === 0) {
                        maxItems = null;
                    }
                    var maxOptions = values.length;
                    var items = values.map(function(x) { return { item: x }; });
                    var selectize_elem = select_elem.selectize({
                        maxItems: maxItems,
                        maxOptions: maxOptions,
                        labelField: "item",
                        valueField: "item",
                        searchField: "item",
                        options: items,
                    });
                    select_elem.selectize()[0].selectize.refreshOptions();
                    select_elem.addClass("data-loaded");
                },
                error: function (error) {
                    console.log("Error: ", error)
                }
            });
        }
    });

    $(".load-selectize-discrete-features").click(function(e){
        var select_elem_id = $(this).attr("select-elem");
        var select_elem = $("#" + select_elem_id);
        var data_loaded = select_elem.hasClass("data-loaded");

        if (!data_loaded) {
            var data_client_json = {
                "dataset_name": $("#dataset_name").val(),
                "include_ids": false,
                "csrfmiddlewaretoken": csrf_token,
            };
            var data_client_json = JSON.stringify(data_client_json);
            var data_to_send = {
                "data_client_json": data_client_json,
                "csrfmiddlewaretoken": csrf_token,
            };
            var that = $(this);
            $.ajax({
                type: "POST",
                url: "/morpho/get_features_discrete_dataset/",
                data: data_to_send,
                dataType: 'json',
                beforeSend: function () {

                },
                success: function (data) {
                    that.hide();
                    select_elem.removeClass("hide-element");
                    select_elem.show();
                    var values = data["values"];
                    var maxItems = values.length;
                    var maxOptions = values.length;
                    var items = values.map(function(x) { return { item: x }; });
                    var selectize_elem = select_elem.selectize({
                        maxItems: 1,
                        maxOptions: maxOptions,
                        labelField: "item",
                        valueField: "item",
                        searchField: "item",
                        options: items,
                    });
                    select_elem.selectize()[0].selectize.refreshOptions();
                    select_elem.addClass("data-loaded");
                },
                error: function (error) {
                    console.log("Error: ", error)
                }
            });
        }
    });


    $("#select-type-hypothesis-two-independent-samples-discr").change(function (e) {
        var value_chosen = $(this).val();
        var value_str = "";
        if (value_chosen === "different") {
            value_str = "!=";
            //$(".select-type-null-hypothesis-two-independent-samples").text("=");
        } else if (value_chosen === "greater") {
            value_str = ">";
            //$(".select-type-null-hypothesis-two-independent-samples").text("<=");
        }  else if (value_chosen === "lesser") {
            value_str = "<";
            //$(".select-type-null-hypothesis-two-independent-samples").text(">=");
        }
        $("#select-type-hypothesis-two-independent-samples-discr-text").text(value_str);
    });

    $("#select-type-hypothesis-two-independent-samples").change(function (e) {
        var value_chosen = $(this).val();
        var value_str = "";
        if (value_chosen === "different") {
            value_str = "!=";
            //$(".select-type-null-hypothesis-two-independent-samples").text("=");
        } else if (value_chosen === "greater") {
            value_str = ">";
            //$(".select-type-null-hypothesis-two-independent-samples").text("<=");
        }  else if (value_chosen === "lesser") {
            value_str = "<";
            //$(".select-type-null-hypothesis-two-independent-samples").text(">=");
        }
        $("#select-type-hypothesis-two-independent-samples-text").text(value_str);
    });

    $("#select-type-hypothesis-dependent-samples").change(function (e) {
        var value_chosen = $(this).val();
        var value_str = "";
        if (value_chosen === "different") {
            value_str = "!=";
            //$(".select-type-null-hypothesis-dependent-samples").text("=");
        } else if (value_chosen === "greater") {
            value_str = ">";
            //$(".select-type-null-hypothesis-dependent-samples").text("<=");
        }  else if (value_chosen === "lesser") {
            value_str = "<";
            //$(".select-type-null-hypothesis-dependent-samples").text(">=");
        }
        $("#select-type-hypothesis-dependent-samples-text").text(value_str);
    });

    $("#select-type-hypothesis-dependent-samples-discr").change(function (e) {
        var value_chosen = $(this).val();
        var value_str = "";
        if (value_chosen === "different") {
            value_str = "!=";
            //$(".select-type-null-hypothesis-dependent-samples").text("=");
        } else if (value_chosen === "greater") {
            value_str = ">";
            //$(".select-type-null-hypothesis-dependent-samples").text("<=");
        }  else if (value_chosen === "lesser") {
            value_str = "<";
            //$(".select-type-null-hypothesis-dependent-samples").text(">=");
        }
        $("#select-type-hypothesis-dependent-samples-discr-text").text(value_str);
    });

    $("#select-distribution-hypothesis-distribution").change(function (e) {
        var value_chosen = $(this).text();
        $("#hypothesis-distribution-selected").text(value_chosen);
    });

    $("#select-distribution-hypothesis-distribution-discr").change(function (e) {
        var value_chosen = $(this).text();
        $("#hypothesis-distribution-discr-selected").text(value_chosen);
    });


    $("#checkbox-find-distribution").change(function(e){
        var is_checked = $(this).is(":checked")
        if (is_checked) {
            $("#hypothesis-check-distribution-hypothesis").hide();
            //
        } else {
            $("#hypothesis-check-distribution-hypothesis").show();
        }
    });

    $("#checkbox-find-distribution-discr").change(function(e){
        var is_checked = $(this).is(":checked")
        if (is_checked) {
            $("#hypothesis-check-distribution-hypothesis-discr").hide();
            //
        } else {
            $("#hypothesis-check-distribution-hypothesis-discr").show();
        }
    });

    $(".select-field-to-load-values").on("change", function (e) {
        var value_chosen = $(this).val();
        if (value_chosen.length !== 0 && value_chosen !== "") {
            var value_str = "";
            var choose_value_elem = $('#' + $(this).attr("choose_value_elem"));
            var selectize_value_elem = $('#' + $(this).attr("selectize_value_elem"));
            var waiting_div_elem = $('#' + $(this).attr("waiting_div_elem"));
            var analysis_run_elem = $('#' + $(this).attr("analysis_run_elem"));
            choose_value_elem.removeClass("hide-element");
            choose_value_elem.show();

            selectize_value_elem.selectize()[0].selectize.destroy();
            selectize_value_elem.empty();
            var data_send = {
                "csrfmiddlewaretoken": csrf_token,
                "custom_option": "false",
                "subgroups_selected": "0",
                "neuron_field_id": value_chosen,
                "neuron_field_name": value_chosen,
                "dataset_name": $("#dataset_name").val(),
            };
            $.ajax({
                type: "POST",
                url: "/morpho/get_local_session_values_neuron_field/",
                data: data_send,
                dataType: 'json',
                beforeSend: function () {
                    waiting_div_elem.removeClass("hide-element");
                    waiting_div_elem.show();
                },
                success: function (data) {
                    waiting_div_elem.hide();
                    selectize_value_elem.show();

                    var neuron_field_id = data["field"];
                    var values = data["values"];
                    var maxItems = 1000
                    var items = values.map(function(x) { return { item: x }; });
                    selectize_value_elem.selectize({
                        maxItems: maxItems,
                        labelField: "item",
                        valueField: "item",
                        searchField: "item",
                        options: items,
                    });

                    analysis_run_elem.removeClass("hide-element");
                    analysis_run_elem.show();
                },
                error: function () {
                    waiting_div_elem.hide();
                    choose_value_elem.append("<p>Error loading values</p>")
                }
            });
        }
    });

    //univariate plot
    $(".stats-button").click(function (e) {
        var field_stats = $(this).attr("field_stats");
        var columns_names = [field_stats];
        var data_type = $(this).attr("data-type");
        $("#stats-title-field").text(field_stats);
        $(".feature_selected_versus").text(field_stats);
        $("#modal-stats-field").modal("show");

        //Plots
        var waiting_object = $("#waiting-plot-modal");
        var placeholder_object_html = $("#univariate-plots-modal");
        var url_process = "/morpho/generate_multi_univariate_plots_html/";
        var additional_args = {};
        additional_args["label_name"] = "";
        additional_args["data_type"] = data_type;
        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);

        //Tabular stats
        var waiting_object = $("#waiting-stats-modal");
        var placeholder_object_html = $("#univariate-stats-modal");
        var url_process = "/morpho/univariate_descriptive_stats/";
        additional_args["data_type"] = data_type;
        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-multi-univariate").click(function (e) {
        var columns_names = $("#select-field-multi-univariate").val();
        var url_process = "/morpho/generate_multi_univariate_plots_html/";
        var waiting_object = $("#waiting-plot-multi-univariate");
        var placeholder_object_html = $("#multi-univariate-plots");
        var additional_args = {};
        additional_args["label_name"] = $("#select-label-multi-univariate").val();

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-stats-multi-univariate").click(function (e) {
        var columns_names = $("#select-field-multi-univariate").val();
        var url_process = "/morpho/univariate_descriptive_stats/";
        var waiting_object = $("#waiting-multi-univariate-stats");
        var placeholder_object_html = $("#multi-univariate-stats");

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html);
    });

    $("#generate-stats-multi-univariate-discr").click(function (e) {
        var columns_names = $("#select-field-multi-univariate-discr").val();
        var url_process = "/morpho/univariate_descriptive_stats/";
        var waiting_object = $("#waiting-multi-univariate-discr-stats");
        var placeholder_object_html = $("#multi-univariate-discr-stats");
        var additional_args = {};
        additional_args["data_type"] = "discrete";

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-multi-univariate-discr").click(function (e) {
        var columns_names = $("#select-field-multi-univariate-discr").val();
        var url_process = "/morpho/generate_multi_univariate_plots_html/";
        var waiting_object = $("#waiting-plots-multi-univariate-discr");
        var placeholder_object_html = $("#multi-univariate-discr-plots");
        var additional_args = {};
        additional_args["data_type"] = "discrete";

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-stats-bivariate").click(function (e) {
        var columns_names = $("#select-field-bivariate").val();
        var url_process = "/morpho/bivariate_stats/";
        var waiting_object = $("#waiting-stats-bivariate");
        var placeholder_object_html = $("#bivariate-stats");

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html);
    });


    $("#generate-plots-bivariate").click(function (e) {
        var columns_names = $("#select-field-bivariate").val();
        var url_process = "/morpho/generate_bivariate_plots_html/";
        var waiting_object = $("#waiting-plot-bivariate");
        var placeholder_object_html = $("#bivariate-plots");
        var additional_args = {};
        additional_args["label_name"] = $("#select-label-bivariate").val();

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-stats-bivariate-discr").click(function (e) {
        var columns_names = $("#select-field-bivariate-discr").val();
        var url_process = "/morpho/bivariate_stats/";
        var waiting_object = $("#waiting-stats-bivariate-discr");
        var placeholder_object_html = $("#bivariate-discr-stats");
        var additional_args = {};
        additional_args["data_type"] = "discrete";

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-bivariate-discr").click(function (e) {
        var columns_names = $("#select-field-bivariate-discr").val();
        var url_process = "/morpho/generate_bivariate_plots_html/";
        var waiting_object = $("#waiting-plots-bivariate-discr");
        var placeholder_object_html = $("#bivariate-discr-plots");
        var additional_args = {};
        additional_args["data_type"] = "discrete";

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });



    $("#generate-plots-trivariate").click(function (e) {
        var columns_names = $("#select-field-trivariate").val();
        var url_process = "/morpho/generate_trivariate_plots_html/";
        var waiting_object = $("#waiting-plot-trivariate");
        var placeholder_object_html = $("#trivariate-plots");
        var additional_args = {};
        additional_args["label_name"] = $("#select-label-trivariate").val();

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });


    $("#generate-plots-multivariate").click(function (e) {
        var columns_names = $("#select-field-multivariate").val();
        var url_process = "/morpho/generate_multivariate_plots_html/";
        var waiting_object = $("#waiting-plot-multivariate");
        var placeholder_object_html = $("#multivariate-plots");
        var additional_args = {};
        additional_args["label_name"] = $("#select-label-multivariate").val();

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-stats-point-interval-estimates").click(function (e) {
        var columns_names = $("#select-field-point-interval-estimates").val();
        var url_process = "/morpho/point_interval_estimates_stats/";
        var waiting_object = $("#waiting-stats-point-interval-estimates");
        var placeholder_object_html = $("#point-interval-estimates-stats");
        var additional_args = {};
        additional_args["confidence_level"] = $("#point-estimates-confidence-level").val();

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-point-interval-estimates").click(function (e) {
        var columns_names = $("#select-field-point-interval-estimates").val();
        var url_process = "/morpho/point_interval_estimates_plots_html/";
        var waiting_object = $("#waiting-plots-point-interval-estimates");
        var placeholder_object_html = $("#point-interval-estimates-plots");
        var additional_args = {};
        additional_args["confidence_level"] = $("#point-estimates-confidence-level").val();

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-stats-point-interval-estimates-discr").click(function (e) {
        var columns_names = $("#select-field-point-interval-estimates-discr").val();
        var values_field = $("#selectize-value-point-interval-estimates-discr").val();
        var url_process = "/morpho/point_interval_estimates_stats/";
        var waiting_object = $("#waiting-stats-point-interval-estimates-discr");
        var placeholder_object_html = $("#point-interval-estimates-discr-stats");
        var additional_args = {};
        additional_args["confidence_level"] = $("#point-estimates-discr-confidence-level").val();
        additional_args["data_type"] = "discrete";
        additional_args["values_field"] = values_field;

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-point-interval-estimates-discr").click(function (e) {
        var columns_names = $("#select-field-point-interval-estimates-discr").val();
        var values_field = $("#selectize-value-point-interval-estimates-discr").val();
        var url_process = "/morpho/point_interval_estimates_plots_html/";
        var waiting_object = $("#waiting-plots-point-interval-estimates-discr");
        var placeholder_object_html = $("#point-interval-estimates-discr-plots");
        var additional_args = {};
        additional_args["confidence_level"] = $("#point-estimates-discr-confidence-level").val();
        additional_args["data_type"] = "discrete";
        additional_args["values_field"] = values_field;

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });


    $("#generate-stats-hypothesis-one-sample").click(function (e) {
        var columns_names = $("#select-field-hypothesis-one-sample").val();
        var url_process = "/morpho/hypothesis_testing_stats/";
        var waiting_object = $("#waiting-stats-hypothesis-one-sample");
        var placeholder_object_html = $("#hypothesis-one-sample-stats");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-one-sample-significance-level").val();
        additional_args["hypothesis_test_type"] = "one_sample";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-one-sample").val();
        additional_args["hypothesis_population_mean"] = $("#hypothesis-population-mean").val();


        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-hypothesis-one-sample").click(function (e) {
        var columns_names = $("#select-field-hypothesis-one-sample").val();
        var url_process = "/morpho/hypothesis_testing_plots_html/";
        var waiting_object = $("#waiting-plot-hypothesis-one-sample");
        var placeholder_object_html = $("#hypothesis-one-sample-plots");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-one-sample-significance-level").val();
        additional_args["hypothesis_test_type"] = "one_sample";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-one-sample").val();
        additional_args["hypothesis_population_mean"] = $("#hypothesis-population-mean").val();

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-stats-hypothesis-one-sample-discr").click(function (e) {
        var columns_names = $("#select-field-hypothesis-one-sample-discr").val();
        var values_field = $("#selectize-value-hypothesis-one-sample-discr").val();
        var url_process = "/morpho/hypothesis_testing_stats/";
        var waiting_object = $("#waiting-stats-hypothesis-one-sample-discr");
        var placeholder_object_html = $("#hypothesis-one-sample-discr-stats");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-one-sample-discr-significance-level").val();
        additional_args["hypothesis_test_type"] = "one_sample";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-one-sample-discr").val();
        additional_args["hypothesis_population_mean"] = $("#hypothesis-population-mean").val();
        additional_args["data_type"] = "discrete";
        additional_args["values_field"] = values_field;

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-hypothesis-one-sample-discr").click(function (e) {
        var columns_names = $("#select-field-hypothesis-one-sample-discr").val();
        var values_field = $("#selectize-value-hypothesis-one-sample-discr").val();
        var url_process = "/morpho/hypothesis_testing_plots_html/";
        var waiting_object = $("#waiting-plots-hypothesis-one-sample-discr");
        var placeholder_object_html = $("#hypothesis-one-sample-discr-plots");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-one-sample-discr-significance-level").val();
        additional_args["hypothesis_test_type"] = "one_sample";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-one-sample-discr").val();
        additional_args["hypothesis_population_mean"] = $("#hypothesis-population-mean").val();
        additional_args["data_type"] = "discrete";
        additional_args["values_field"] = values_field;

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-stats-hypothesis-two-independent-samples").click(function (e) {
        var columns_names = $("#select-field-hypothesis-two-independent-samples").val();
        var url_process = "/morpho/hypothesis_testing_stats/";
        var waiting_object = $("#waiting-stats-hypothesis-two-independent-samples");
        var placeholder_object_html = $("#hypothesis-two-independent-samples-stats");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-two-independent-samples-significance-level").val();
        additional_args["hypothesis_test_type"] = "two_independent_samples";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-two-independent-samples").val();

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-hypothesis-two-independent-samples").click(function (e) {
        var columns_names = $("#select-field-hypothesis-two-independent-samples").val();
        var url_process = "/morpho/hypothesis_testing_plots_html/";
        var waiting_object = $("#waiting-plot-hypothesis-two-independent-samples");
        var placeholder_object_html = $("#hypothesis-two-independent-samples-plots");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-two-independent-samples-significance-level").val();
        additional_args["hypothesis_test_type"] = "two_independent_samples";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-two-independent-samples").val();

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-stats-hypothesis-two-independent-samples-discr").click(function (e) {
        var columns_names = $("#select-field-hypothesis-two-independent-samples-discr").val();
        var values_field = $("#selectize-value-hypothesis-two-independent-samples-discr").val();
        var url_process = "/morpho/hypothesis_testing_stats/";
        var waiting_object = $("#waiting-stats-hypothesis-two-independent-samples-discr");
        var placeholder_object_html = $("#hypothesis-two-independent-samples-discr-stats");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-two-independent-samples-discr-significance-level").val();
        additional_args["hypothesis_test_type"] = "two_independent_samples";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-two-independent-samples-discr").val();
        additional_args["data_type"] = "discrete";
        additional_args["values_field"] = values_field;

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-hypothesis-two-independent-samples-discr").click(function (e) {
        var columns_names = $("#select-field-hypothesis-two-independent-samples-discr").val();
        var values_field = $("#selectize-value-hypothesis-two-independent-samples-discr").val();
        var url_process = "/morpho/hypothesis_testing_plots_html/";
        var waiting_object = $("#waiting-plots-hypothesis-two-independent-samples-discr");
        var placeholder_object_html = $("#hypothesis-two-independent-samples-discr-plots");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-two-independent-samples-discr-significance-level").val();
        additional_args["hypothesis_test_type"] = "two_independent_samples";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-two-independent-samples-discr").val();
        additional_args["data_type"] = "discrete";
        additional_args["values_field"] = values_field;

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-stats-hypothesis-dependent-samples").click(function (e) {
        var columns_names = $("#select-field-hypothesis-dependent-samples").val();
        var url_process = "/morpho/hypothesis_testing_stats/";
        var waiting_object = $("#waiting-stats-hypothesis-dependent-samples");
        var placeholder_object_html = $("#hypothesis-dependent-samples-stats");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-dependent-samples-significance-level").val();
        additional_args["hypothesis_test_type"] = "two_dependent_samples";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-dependent-samples").val();

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-hypothesis-dependent-samples").click(function (e) {
        var columns_names = $("#select-field-hypothesis-dependent-samples").val();
        var url_process = "/morpho/hypothesis_testing_plots_html/";
        var waiting_object = $("#waiting-plots-hypothesis-dependent-samples");
        var placeholder_object_html = $("#hypothesis-dependent-samples-plots");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-dependent-samples-significance-level").val();
        additional_args["hypothesis_test_type"] = "two_dependent_samples";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-dependent-samples").val();

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-stats-hypothesis-dependent-samples-discr").click(function (e) {
        var columns_name_1 = $("#select-field-hypothesis-dependent-samples-1-discr").val();
        var columns_name_2 = $("#select-field-hypothesis-dependent-samples-2-discr").val();
        var columns_names = [columns_name_1, columns_name_2];
        var values_field_1 = $("#selectize-value-hypothesis-dependent-samples-1-discr").val();
        var values_field_2 = $("#selectize-value-hypothesis-dependent-samples-2-discr").val();
        var url_process = "/morpho/hypothesis_testing_stats/";
        var waiting_object = $("#waiting-stats-hypothesis-dependent-samples-discr");
        var placeholder_object_html = $("#hypothesis-dependent-samples-discr-stats");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-dependent-samples-discr-significance-level").val();
        additional_args["hypothesis_test_type"] = "two_dependent_samples";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-dependent-samples-discr").val();
        additional_args["data_type"] = "discrete";
        additional_args["values_field_1"] = values_field_1;
        additional_args["values_field_2"] = values_field_2;

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-hypothesis-dependent-samples-discr").click(function (e) {
        var columns_name_1 = $("#select-field-hypothesis-dependent-samples-1-discr").val();
        var columns_name_2 = $("#select-field-hypothesis-dependent-samples-2-discr").val();
        var columns_names = [columns_name_1, columns_name_2];
        var values_field_1 = $("#selectize-value-hypothesis-dependent-samples-1-discr").val();
        var values_field_2 = $("#selectize-value-hypothesis-dependent-samples-2-discr").val();
        var url_process = "/morpho/hypothesis_testing_plots_html/";
        var waiting_object = $("#waiting-plots-hypothesis-dependent-samples-discr");
        var placeholder_object_html = $("#hypothesis-dependent-samples-discr-plots");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-dependent-samples-discr-significance-level").val();
        additional_args["hypothesis_test_type"] = "two_dependent_samples";
        additional_args["hypothesis_test_type_tails"] = $("#select-type-hypothesis-dependent-samples-discr").val();
        additional_args["data_type"] = "discrete";
        additional_args["values_field_1"] = values_field_1;
        additional_args["values_field_2"] = values_field_2;

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });


    $("#generate-stats-hypothesis-distribution").click(function (e) {
        var columns_names = $("#select-field-hypothesis-distribution").val();
        var url_process = "/morpho/hypothesis_testing_distribution_stats/";
        var waiting_object = $("#waiting-stats-hypothesis-distribution");
        var placeholder_object_html = $("#hypothesis-distribution-stats");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-distribution-significance-level").val();
        if ($("#checkbox-find-distribution").is(":checked")) {
            additional_args["hypothesis_test_distribution"] = "most_probable_dist";
            additional_args["hypothesis_test_distribution_verbose"] = "Most probable distribution";
        } else {
            additional_args["hypothesis_test_distribution"] = $("#select-distribution-hypothesis-distribution").val();
            additional_args["hypothesis_test_distribution_verbose"] = $("#hypothesis-distribution-selected").text();
        }
        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-hypothesis-distribution").click(function (e) {
        var columns_names = $("#select-field-hypothesis-distribution").val();
        var url_process = "/morpho/hypothesis_testing_distribution_plots_html/";
        var waiting_object = $("#waiting-plots-hypothesis-distribution");
        var placeholder_object_html = $("#hypothesis-distribution-plots");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-distribution-significance-level").val();
        if ($("#checkbox-find-distribution").is(":checked")) {
            additional_args["hypothesis_test_distribution"] = "most_probable_dist";
            additional_args["hypothesis_test_distribution_verbose"] = "Most probable distribution";
        } else {
            additional_args["hypothesis_test_distribution"] = $("#select-distribution-hypothesis-distribution").val();
            additional_args["hypothesis_test_distribution_verbose"] = $("#hypothesis-distribution-selected").text();
        }

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-stats-hypothesis-distribution-discr").click(function (e) {
        var columns_names = $("#select-field-hypothesis-distribution-discr").val();
        var url_process = "/morpho/hypothesis_testing_distribution_stats/";
        var waiting_object = $("#waiting-stats-hypothesis-distribution-discr");
        var placeholder_object_html = $("#hypothesis-distribution-discr-stats");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-distribution-discr-significance-level").val();
        if ($("#checkbox-find-distribution-discr").is(":checked")) {
            additional_args["hypothesis_test_distribution"] = "most_probable_dist";
            additional_args["hypothesis_test_distribution_verbose"] = "Most probable distribution";
        } else {
            additional_args["hypothesis_test_distribution"] = $("#select-distribution-hypothesis-distribution-discr").val();
            additional_args["hypothesis_test_distribution_verbose"] = $("#hypothesis-distribution-discr-selected").text();
        }
        additional_args["data_type"] = "discrete";

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $("#generate-plots-hypothesis-distribution-discr").click(function (e) {
        var columns_names = $("#select-field-hypothesis-distribution-discr").val();
        var url_process = "/morpho/hypothesis_testing_distribution_plots_html/";
        var waiting_object = $("#waiting-plots-hypothesis-distribution-discr");
        var placeholder_object_html = $("#hypothesis-distribution-discr-plots");
        var additional_args = {};
        additional_args["significance_level"] = $("#hypothesis-distribution-discr-significance-level").val();
        if ($("#checkbox-find-distribution-discr").is(":checked")) {
            additional_args["hypothesis_test_distribution"] = "most_probable_dist";
            additional_args["hypothesis_test_distribution_verbose"] = "Most probable distribution";
        } else {
            additional_args["hypothesis_test_distribution"] = $("#select-distribution-hypothesis-distribution-discr").val();
            additional_args["hypothesis_test_distribution_verbose"] = $("#hypothesis-distribution-discr-selected").text();
        }
        additional_args["data_type"] = "discrete";

        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });








});

//columns_names must be an array []
function get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args)  {
    var data = {
        "dataset_name": $("#dataset_name").val(),
        "label_column_id": $("#label_column_id").val(),
        "label_instances": $("#label_instances").val(),
        "columns_names": columns_names
    };

    if (additional_args !== undefined) {
        for (var key in additional_args) {
            var value = additional_args[key];
            if (value === undefined) {
                value = "";
            }
            data[key] = value;
        }
    }

    var input_data_json = JSON.stringify(data);
    var that = this;


    $.ajax({
        type: "POST",
        url: url_process,
        data: {
            "csrfmiddlewaretoken": $("#csrf_token").val(),
            "input_data_json": input_data_json,
        },
        datatype: "json",
        beforeSend: function() {
            placeholder_object_html.empty();
            waiting_object.removeClass("hide-element");
            waiting_object.show();
        },
        success: function(data) {
            waiting_object.hide();
            placeholder_object_html.empty();

            if (!data["error"]) {
                for (var key in data["objects"]) {
                    var value = data["objects"][key];
                    if (value["error"] === true) {
                        append_error_message(key_error=key, placeholder_object_html, error_message=value["error_message"])
                    } else {
                        placeholder_object_html.append(value["object_result_html"]);

                        $(placeholder_object_html).find(".plotly-graph-div").on('plotly_click', function(data, points){
                            var text_point = points.points[0].text;
                            if (text_point !== this.text_copied_to_clipboard) {
                                this.text_copied_to_clipboard = text_point;
                                if (text_point !== undefined) {
                                    copyToClipboard(text_point);
                                    var notify_object = print_notify_message(permanent=false, type="success", icon='glyphicon glyphicon-ok', title="Text copied to clipboard!", message=text_point, delay=1000, additional_message="");
                                }
                            }

                        });
                    }
                }
                //In case any new tables were returned
                $(placeholder_object_html).find('.datatable-new-ajax').each(function(){
                    create_datatable($(this));
                });
            } else {
                append_error_message(key_error="Fatal error: ", placeholder_object_html, error_message=data["error_message"])
            }

        },
        error: function(data) {
            waiting_object.hide();
            append_error_message(key_error="Exception error", placeholder_object_html, error_message="error generating the plots/stats")
        }
    });
}

function append_error_message(key_error, placeholder_object_html, error_message) {
    placeholder_object_html.append("<h4>" + key_error + "</h4><p class='error-color'><strong>Error:</strong> " + error_message + ". Try again later</p>");
}