
$(document).ready(function () {
    var csrf_token = $("#csrf_token").val();
    console.log("CSRF: " + csrf_token);

    $(".load-dataset-example").click(function(e){
        var waiting_div_elem = $("#waiting-continue-features-selection");
        var error_elem = $("#error-continue-features-selection");
        var success_elem = $("#success-continue-features-selection");
        var next_section_elem = $("#section-train-val-test");

        var data_client_json = {
            "dataset_type": $(this).attr("dataset-type"),
            "model_name": $("#model_name").val()
        };
        data_client_json = JSON.stringify(data_client_json);
        var data_send = {
            "csrfmiddlewaretoken": csrf_token,
            "data_client_json": data_client_json,
        };

        $.ajax({
            type: "POST",
            url: "/morpho/ml_supervised_class_load_dataset_example/",
            data: data_send,
            dataType: 'json',
            beforeSend: function () {
                waiting_div_elem.removeClass("hide-element");
                waiting_div_elem.show();
            },
            success: function (data) {
                waiting_div_elem.hide();
                if (data["error"]) {
                    error_elem.text(data["error_message"]);
                    success_elem.hide();
                } else {
                    error_elem.empty();
                    success_elem.removeClass("hide-element");
                    success_elem.show();
                    next_section_elem.removeClass("hide-element");
                    next_section_elem.show();

                    $('html, body').animate({
                        scrollTop: next_section_elem.offset().top
                    }, 1000);
                    //$('.dropup').addClass('open'); // substitute with your own selector
                    //$('.dropdown-toggle').attr('aria-expanded', true).focus();
                }
            },
            error: function () {
                waiting_div_elem.hide();
                success_elem.show();
                error_elem.text("Server error");
            }
        });
    });

    $(".select-dataset").on("change", function (e) {
        var values_chosen = $(this).val();
        var select_id = $(this).attr("id");
        if (values_chosen.length !== 0 && values_chosen !== "") {
            var value_str = "";
            var choose_value_elem = $('#' + $(this).attr("choose_value_elem"));
            var selectizes_value_elem = $("#" + $(this).attr("selectize_value_elem"));
            var selectizes_value_elem_single_val = false;
            if (selectizes_value_elem.hasClass("is-selectize-single-val")) {
                selectizes_value_elem_single_val = true;
            }

            var waiting_div_elem = $('#' + $(this).attr("waiting_div_elem"));
            choose_value_elem.removeClass("hide-element");
            choose_value_elem.show();

            selectizes_value_elem.each(function( index ) {
                $(this).selectize()[0].selectize.destroy();
                $(this).empty();
            });
            var data_client_json = {
                "datasets_names": values_chosen,
                "include_ids": false,
                "model_name": $("#model_name").val()
            };
            data_client_json = JSON.stringify(data_client_json)
            var data_send = {
                "csrfmiddlewaretoken": csrf_token,
                "data_client_json": data_client_json
            };
            $.ajax({
                type: "POST",
                url: "/morpho/get_features_datasets/",
                data: data_send,
                dataType: 'json',
                beforeSend: function () {
                    waiting_div_elem.removeClass("hide-element");
                    waiting_div_elem.show();
                },
                success: function (data) {
                    waiting_div_elem.hide();
                    $("#error-upload").empty();

                    var values = data["values"];
                    var maxItems = values.length;
                    if (selectizes_value_elem_single_val) {
                        maxItems = 1;
                    }
                    var maxOptions = values.length;
                    var items = values.map(function(x) { return { item: x }; });

                    selectizes_value_elem.selectize({
                        maxItems: maxItems,
                        maxOptions: maxOptions,
                        labelField: "item",
                        valueField: "item",
                        searchField: "item",
                        options: items,
                    });

                },
                error: function () {
                    waiting_div_elem.hide();
                    $("#error-upload").val("Server error");
                }
            });
        }
    });

    $("#select-dataset-discretize-method").on("change", function (e) {
        var method_chosen = $(this).val();
        if( method_chosen == "fayyad_irani" ) {
            $("#section-nbins-for-equal-width-discretization").addClass("hide-element")
        } else {
            $("#section-nbins-for-equal-width-discretization").removeClass("hide-element")
        }
    });

    $("#select-feature-selection-algorithm").on("change", function (e) {
        var method_chosen = $(this).val();
        if( method_chosen == "m_i" ) {
            $("#select-feature-selection-filter option[value='fwe']").remove();
            $("#select-feature-selection-filter").trigger('change');
        } else {
            if ($('#select-feature-selection-filter > option').length == 2) {
                $("#select-feature-selection-filter").append('<option value="fscore">family wise error test</option>');
            }
        }
    });

    $("#select-feature-selection-filter").on("change", function (e) {
        var method_chosen = $(this).val();
        if( method_chosen == "kbest" ) {
            $("#criteria-k").removeClass("hide-element")
            $("#criteria-percentile").addClass("hide-element")
            $("#criteria-test").addClass("hide-element")
        }
        if( method_chosen == "percentile" ) {
            $("#criteria-k").addClass("hide-element")
            $("#criteria-percentile").removeClass("hide-element")
            $("#criteria-test").addClass("hide-element")
        }
        if( method_chosen == "fwe" ) {
            $("#criteria-k").addClass("hide-element")
            $("#criteria-percentile").addClass("hide-element")
            $("#criteria-test").removeClass("hide-element")
        }
    });

    $("#select-dataset-class-features").on("change", function (e) {
        var vals = $(this).val();
        if( vals.length > 1 ) {
            $("#section-learning-method").removeClass("hide-element")
            $("#section-selected-learning-algorithms").addClass("hide-element")
            $("#section-features-subset-selection").addClass("hide-element")
            //$(".cross-validation-section").addClass("hide-element")
            //$('.checkbox-cross-validation').each(function() { $(this).prop('checked', false) });
        } else {
            $("#section-learning-method").addClass("hide-element")
            $("#section-selected-learning-algorithms").removeClass("hide-element")
            $("#section-features-subset-selection").removeClass("hide-element")
            //$("#cross-validation-section").removeClass("hide-element")
        }
    });

    $("#continue-button-features-selection").click(function(e){
        var filling_na_algorithm = $("#select-filling-algorithm").val();
        var remove_non_variant_features = $("#remove-non-variant-features").prop('checked');
        var normalize_values = $("#checkbox-normalize-values").prop('checked');
        var normalization_method = $("#select-normalizing-algorithm").val();
        var dataset_discretize = $("#checkbox-dataset-discretize").prop('checked');
        var dataset_discretize_method = $("#select-dataset-discretize-method").val();
        var nbins_for_equal_width_discretization = $("#nbins-for-equal-width-discretization").val();
        var apply_feature_subset_selection = $("#checkbox-apply-feature-selection").prop('checked');
        var feature_selection_algorithm = $("#select-feature-selection-algorithm").val();
        var filter = $("#select-feature-selection-filter").val();
        var k_best = $("#k-feature-selection").val();
        var percentile = $("#percentile-feature-selection").val();
        var alpha_feature_selection = $("#alpha-feature-selection").val();
        var apply_dimentionality_reduction = $("#checkbox-apply-dimentionality-reduction").prop('checked');
        var pca_algorithm = $("#select-dimensionality-reduction-algorithm").val();
        var when_apply_pca = $("#select-when-dimesionality-reduction").val();
        var n_components = $("#top_features_dimensionality_reduction").val();
        var features_preprocessing = {};
        var datasets_class_names = $("#select-dataset-class").val();
        var datasets_features_names = $("#select-dataset-predictor").val();
        var dataframes_features = {};
        var dataframes_features_classes = {};
        var select_dataframes_features = $("#select-dataset-predictor-features");
        var select_dataframes_features_classes = $("#select-dataset-class-features");
        var selectize_dataframes_features = select_dataframes_features[0].selectize;
        var dataframes_features_all = select_dataframes_features.attr("selected-all");
        var dataframes_features_classes_all = select_dataframes_features_classes.attr("selected-all");

        features_preprocessing["filling_na_algorithm"] = filling_na_algorithm;
        features_preprocessing["remove_non_variant_features"] = remove_non_variant_features;
        features_preprocessing["normalize_values"] = normalize_values;
        features_preprocessing["normalization_method"] = normalization_method;
        features_preprocessing["dataset_discretize"] = dataset_discretize;
        features_preprocessing["discretize_method"] = dataset_discretize_method;
        features_preprocessing["n_bins"] = nbins_for_equal_width_discretization;
        features_preprocessing["apply_feature_subset_selection"] = apply_feature_subset_selection;
        features_preprocessing["filter"] = filter;
        features_preprocessing["k_best"] = k_best;
        features_preprocessing["percentile"] = percentile;
        features_preprocessing["feature_selection_algorithm"] = feature_selection_algorithm;
        features_preprocessing["alpha"] = alpha_feature_selection;
        features_preprocessing["apply_dimentionality_reduction"] = apply_dimentionality_reduction;
        features_preprocessing["pca_algorithm"] = pca_algorithm;
        features_preprocessing["when_apply_pca"] = when_apply_pca;
        features_preprocessing["n_components"] = n_components;

        dataframes_features["values"] = select_dataframes_features.val();
        dataframes_features["selected_all"] = false;
        if (dataframes_features_all === "true") {
            dataframes_features["selected_all"] = true;
        }

        dataframes_features_classes["values"] = select_dataframes_features_classes.val();
        dataframes_features_classes["selected_all"] = false;
        if (dataframes_features_classes_all === "true") {
            dataframes_features_classes["selected_all"] = true;
        }

        var progress_elem_html_id = "progress-features-selection";
        var progress_elem_html = $("#" + progress_elem_html_id);
        var waiting_div_elem = $("#waiting-continue-features-selection");
        var error_elem = $("#error-continue-features-selection");
        var success_elem = $("#success-continue-features-selection");
        var next_section_elem = $("#section-train-val-test");

        var data_client_json = {
            "features_preprocessing": features_preprocessing,
            "datasets_class_names": datasets_class_names,
            "datasets_features_names": datasets_features_names,
            "dataframes_features": dataframes_features,
            "dataframes_features_classes": dataframes_features_classes,
        };
        data_client_json = JSON.stringify(data_client_json)
        var data_send = {
            "csrfmiddlewaretoken": csrf_token,
            "data_client_json": data_client_json,
        };

        $.ajax({
            type: "POST",
            url: "/morpho/ml_supervised_class_features_selection/",
            data: data_send,
            dataType: 'json',
            beforeSend: function () {
                waiting_div_elem.removeClass("hide-element");
                waiting_div_elem.show();
                progress_elem_html.show();
            },
            success: function (data_task) {
                data_processed = data_task
                //var data_processed = run_worker(data_task, progress_elem_html_id, function (data_processed) {
                    waiting_div_elem.hide();
                    if (data_processed["error"]) {
                        error_elem.removeClass("hide-element");
                        error_elem.show();
                        error_elem.text("Error: " + data_processed["error_message"]);
                        success_elem.hide();
                    } else {
                        error_elem.empty();
                        success_elem.removeClass("hide-element");
                        success_elem.show();
                        next_section_elem.removeClass("hide-element");
                        next_section_elem.show();

                        $('#feature-selection-values-list-table').children().remove();
                        if ("features_values" in data_task.summary) {
                            var i = 0;
                            values_list = JSON.parse(data_task.summary.features_values);
                            for( i = 0; i < values_list.length; i ++) {
                                var feature = values_list[i][1];
                                var value = values_list[i][0];
                                $('#feature-selection-values-list-table').append("<div class='row'>" + "<div class='col-md-6' style=\"text-align: right;\"" + ">"
                                + feature +"</div>" + "<div class='col-md-6'>" + value +"</div>" + "</div>");
                            }
                        }

                        $('#dimentionality-reduction-explained-variance-ratio-div').html("");
                        if ("explained_variance_ratio" in data_task.summary) {
                            $('#dimentionality-reduction-explained-variance-ratio-div').append("variance ratio explained: " + data_task.summary.explained_variance_ratio);
                        }

                        $('#dimentionality-reduction-plot-div').children().remove();
                        if ("dimentionality_reduction_plot" in data_task.summary) {
                            var img = document.createElement('img');
                            img.src = 'data:image/png;base64,' + data_task.summary.dimentionality_reduction_plot;
                            $('#dimentionality-reduction-plot-div').append(img);
                        }


                        $('html, body').animate({
                            scrollTop: next_section_elem.offset().top
                        }, 1000);
                        $('.nav-tabs a[href="#section-train-val"]').tab('show');
                    }
                //})
            },
            error: function (e) {
                waiting_div_elem.hide();
                success_elem.show();
                error_elem.text("Server error");
            }
        });
    });

    $("#continue-train-val-test").click(function(e) {
        var waiting_div_elem = $("#waiting-continue-train-val-test");
        var error_elem = $("#error-continue-train-val-test");
        var success_elem = $("#success-continue-train-val-test");
        var next_section_elem = $("#section-learning-algorithm");

        var k_folds = $("#k-folds").val();
        var percentage_test_set = $("#percentage-test-set").val();
        var shuffle_instances = $("#shuffle-instances-checkbox").is(":checked");

        var data_client_json = {
            "k_folds": k_folds,
            "percentage_test_set": percentage_test_set,
            "shuffle_instances": shuffle_instances,
        };
        data_client_json = JSON.stringify(data_client_json)
        var data_send = {
            "csrfmiddlewaretoken": csrf_token,
            "data_client_json": data_client_json,
        };

        $.ajax({
            type: "POST",
            url: "/morpho/ml_supervised_class_set_sets/",
            data: data_send,
            dataType: 'json',
            beforeSend: function () {
                waiting_div_elem.removeClass("hide-element");
                waiting_div_elem.show();
                error_elem.hide();
            },
            success: function (data_processed) {
                waiting_div_elem.hide();
                if (data_processed["error"]) {
                    error_elem.removeClass("hide-element").show();
                    error_elem.text("Error: " + data_processed["error_message"]);
                } else {
                    success_elem.removeClass("hide-element").show();
                    next_section_elem.removeClass("hide-element").show();

                    $('html, body').animate({
                        scrollTop: next_section_elem.offset().top
                    }, 1000);
                    $('.nav-tabs a[href="#section-algorithm"]').tab('show');
                }
            },
            error: function(e){
                waiting_div_elem.hide();
                success_elem.hide();
                error_elem.removeClass("hide-element").show();
                error_elem.text("Error: check if the input values are valid");
            }
        });

    });

    $("#select-learning-algorithm").on("change", function (e) {
        var algorithms_chosen = $(this).val();
        var last_algorithm_chosen = algorithms_chosen[algorithms_chosen.length-1];
        show_algorithms_parameters(last_algorithm_chosen);
    });

    $(document).on('click', '#section-selected-learning-algorithms .item', function (e) {
        var algorithm_chosen = $(this).data()["value"];
        show_algorithms_parameters(algorithm_chosen);
    });

    $(".multi-label-classification-algorithm").on("change", function (e) {
        var algorithm_chosen = $(this).val();
        console.log("aqui viene el método:")
        console.log(algorithm_chosen)
        if(algorithm_chosen != "") {
            show_algorithms_parameters(algorithm_chosen);
        }
    });

    $("#select-learning-method").on("change", function (e) {
        var algorithms_chosen = $(this).val();
        var last_algorithm_chosen = algorithms_chosen[algorithms_chosen.length-1];
        console.log(last_algorithm_chosen)
        show_methods_parameters(last_algorithm_chosen);
    });

    $(document).on('click', '#section-learning-method .item', function (e) {
        var algorithm_chosen = $(this).data()["value"];
        console.log(algorithm_chosen)
        show_methods_parameters(algorithm_chosen);
    });

    $(".checkbox-cross-validation").on("change", function (e) {
        var algorithm_id = $(this).attr("algorithm");
        var cross_validation_checked = $(this).is(":checked");
        var algorithm_parameters_section = $("#" + algorithm_id + "-parameters")
        if (cross_validation_checked) {
            algorithm_parameters_section.addClass("disabled-div");
        } else {
            algorithm_parameters_section.removeClass("disabled-div");
        }
    });


    $("#continue-learn-algorithms").click(function(e) {
        var selected_learning_methods = $("#select-learning-method").val();
        var learning_methods = {};
        var selected_algorithms = $("#select-learning-algorithm").val();
        var learning_algorithms = {};
        var current_section_elem = $("#section-learning-algorithm");
        var waiting_div_elem = $("#waiting-continue-learn-algorithms");
        var progress_elem_html_id = "progress-supervised-class-learning-algorithms";
        var progress_elem_html = $("#" + progress_elem_html_id);
        var error_elem = $("#error-continue-learn-algorithms");
        var success_elem = $("#success-continue-learn-algorithms");
        var next_section_elem_1 = $("#section-models-evaluation");
        var next_section_elem_2 = $("#section-prediction-new-dataset");

        if (selected_learning_methods.length > 0) {
            selected_algorithms = []
        }
        // it applies just for multi label classification problems, it means more than one target features
        for (var i=0; i<selected_learning_methods.length; i++) {
            var id_method = selected_learning_methods[i];
            learning_methods[id_method] = {};
            $("#method-parameters-" + id_method + " .learning-parameter").each(function() {
                var parameter_name = $(this).attr("id");
                var input_type = $(this).attr("type");
                if (parameter_name !== undefined) {
                    var parameter_value = $(this).val();
                    if (input_type == "checkbox") {
                        parameter_value = $(this).is(":checked");
                    }
                    learning_methods[id_method][parameter_name] = parameter_value;
                }
            });

            $("#method-parameters-" + id_method + " .algorithm-parameter-select").each(function() {
                var parameter_name = $(this).attr("id");
                if (parameter_name !== undefined) {
                    var parameter_value = $(this).val();
                    selected_algorithms.push(parameter_value);
                }
            });
        }

        for (var i=0; i<selected_algorithms.length; i++) {
            var id_algorithm = selected_algorithms[i];
            learning_algorithms[id_algorithm] = {};
            $("#learning-parameters-" + id_algorithm + " .learning-parameter").each(function() {
                var parameter_name = $(this).attr("id");
                var input_type = $(this).attr("type");
                if (parameter_name !== undefined) {
                    var parameter_value = $(this).val();
                    if (input_type == "checkbox") {
                        parameter_value = $(this).is(":checked");
                    }
                    learning_algorithms[id_algorithm][parameter_name] = parameter_value;
                }
            });
        }

        var data_client_json = {
            "learning_methods": learning_methods,
            "learning_algorithms": learning_algorithms,
            "model_name": $("#model_name").val()
        };
        data_client_json = JSON.stringify(data_client_json)
        var data_send = {
            "csrfmiddlewaretoken": csrf_token,
            "data_client_json": data_client_json,
        };

        $.ajax({
            type: "POST",
            url: "/morpho/ml_supervised_class_learn/",
            data: data_send,
            dataType: 'json',
            beforeSend: function () {
                waiting_div_elem.removeClass("hide-element");
                waiting_div_elem.show();
                progress_elem_html.show();
                error_elem.hide();
            },
            success: function (data_task) {
                var data_processed = run_worker(data_task, progress_elem_html_id, function(data_processed){
                    waiting_div_elem.hide();
                    if (data_processed["error"]) {
                        error_elem.removeClass("hide-element").show();
                        error_elem.text("Error: " + data_processed["error_message"]);
                    } else {
                        success_elem.removeClass("hide-element").show();
                        next_section_elem_1.removeClass("hide-element").show();
                        next_section_elem_2.removeClass("hide-element").show();
                        current_section_elem.removeClass("last-section-page");

                        $('html, body').animate({
                            scrollTop: next_section_elem_1.offset().top
                        }, 1000);

                        $('.nav-tabs a[href="#section-evaluation"]').tab('show');

                        var placeholder_table_html = $("#models-evaluation-training-val-table-placeholder");
                        placeholder_table_html.empty();
                        placeholder_table_html.append(data_processed["models_evaluation_training_val_table"]);
                        placeholder_table_html.find('.datatable-new-ajax').each(function(){
                            create_datatable($(this));
                        });

                        var placeholder_table_html = $("#models-evaluation-test-table-placeholder");
                        placeholder_table_html.empty();
                        placeholder_table_html.append(data_processed["models_evaluation_test_table"]);
                        placeholder_table_html.find('.datatable-new-ajax').each(function(){
                            create_datatable($(this));
                        });

                        $(".form-empty-csrfmiddlewaretoken").each(function(index) {
                            $(this).val(csrf_token);
                        });
                    }
                });
            },
            error: function (e) {
                waiting_div_elem.hide();
                success_elem.hide();
                error_elem.removeClass("hide-element").show();
                error_elem.text("Error: check if the selected columns have NaN or empty values");
            }
        });

    });

    $(document).on('click', '.custom-stats-button', function (e) {
        var table_placeholder = $(this).closest(".table-placeholder");
        var table_placeholder_id = table_placeholder.attr("id");
        var set_name = table_placeholder.attr("set-name");
        var col_idx = $(this).attr("field_stats");
        var columns_names = [col_idx];
        var additional_args = {"set_name": set_name};

        var col_name =  table_placeholder.find("table").DataTable().columns(parseInt(col_idx)+1).header().to$().text();
        $("#stats-title-field").text(col_name);
        $("#modal-stats-field").modal("show");

        //Plots
        var waiting_object = $("#waiting-plot-modal");
        var placeholder_object_html = $("#univariate-plots-modal");
        var url_process = "/morpho/ml_supervised_class_evaluation_plots/";
        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);

        //Tabular stats
        var waiting_object = $("#waiting-stats-modal");
        var placeholder_object_html = $("#univariate-stats-modal");
        var url_process = "/morpho/ml_supervised_class_evaluation_stats_table/";
        get_plots_stats_from_server(url_process, columns_names, waiting_object, placeholder_object_html, additional_args);
    });

    $(document).on('click', '.model-explanation-button', function (e) {
        var algorithm = $(this).attr("algorithm");
        var set = $(this).attr("set");
        var waiting_div_elem = $("#model-explanation-waiting");
        var error_elem = $("#model-explanation-error");
        var current_section_elem = $("#model-explanation-content");
        var current_section_elem_text = $("#model-explanation-content-text");
        var current_section_elem_plot = $("#model-explanation-content-plot");
        var modal = $("#modal-model-explanation");

        $("#model-explanation-title-field").text(algorithm);

        current_section_elem_text.empty();
        current_section_elem_plot.empty();

        var data_client_json = {
            "algorithm": algorithm,
            "set": set
        };
        data_client_json = JSON.stringify(data_client_json);
        var data_send = {
            "csrfmiddlewaretoken": csrf_token,
            "data_client_json": data_client_json,
        };

        $.ajax({
            type: "POST",
            url: "/morpho/ml_supervised_class_model_explanation/",
            data: data_send,
            dataType: 'json',
            beforeSend: function () {
                modal.modal('show');
                waiting_div_elem.removeClass("hide-element");
                waiting_div_elem.show();
                error_elem.hide();
            },
            success: function (response) {
                waiting_div_elem.hide();
                if (response["error"]) {
                    error_elem.removeClass("hide-element").show();
                    error_elem.text("Error: " + response["error_message"]);
                } else {
                    if (response["text"] !== "") {
                        current_section_elem_text.html(response["text"]);

                        current_section_elem_text.find('.datatable-new-ajax').each(function(){
                            create_datatable($(this));
                        });
                    }

                    if (response["plot"] !== "" && response["plot"] != null) {
                        //current_section_elem_plot.append(response["plot"]);

                        var img = document.createElement('img');
                        img.src = 'data:image/png;base64,' + response["plot"];
                        current_section_elem_plot.append(img);
                    }
                }
            },
            error: function (e) {
                waiting_div_elem.hide();
                error_elem.removeClass("hide-element").show();
                error_elem.text("Error: " + e);
            }
        });
    });

    $("#upload-new-prediction-dataset").fileinput({
        hideThumbnailContent: true, // hide image, pdf, text or other content in the thumbnail preview
        theme: "explorer",
        uploadUrl: "/morpho/ml_supervised_class_upload_prediction_new_dataset/",
        fileActionSettings: {
            showDownload: false,
            showUpload: false,
            indicatorNew: '',
            removeIcon: '<i class="glyphicon glyphicon-trash fa-2x" aria-hidden="true"></i>',
            zoomIcon: '<i class="glyphicon glyphicon-zoom-in fa-2x"></i>',
        },
        previewClass: "file-input-small container-center",
        layoutTemplates: {
            main1: "<div class=\'container-center\'>\n" +
                "{preview}\n" +
                "<div class=\'input-group {class} file-input-browse-small\'>\n" +
                "   <div class=\'input-group-btn\ input-group-prepend file-input-button-small'>\n" +
                "       {browse}\n" +
                "       {upload}\n" +
                "       {remove}\n" +
                "   </div>\n" +
                "   {caption}\n" +
                "</div>" +
                "</div>"
        },
        browseIcon: "<i class=\"glyphicon glyphicon-folder-open icon-margin-right\"></i> ",
        uploadExtraData: function(previewId, index) {
            var obj = {};
            obj['csrfmiddlewaretoken'] = csrf_token;
            return obj;
        },
        uploadAsync: false
    });

    $('#upload-new-prediction-dataset').on('filebatchpreupload', function() {
        $("#waiting-new-prediction-dataset").removeClass("hide-element").show();
    });

    $('#upload-new-prediction-dataset').on('filebatchuploadcomplete', function() {
        $("#waiting-new-prediction-dataset").hide();
        $("#upload-new-prediction-dataset-complete").removeClass("hide-element").show();
        $("#new-prediction-dataset-download-section").removeClass("hide-element").show();

        var select_learning_algorithms = $("#select-learning-algorithm");
        var selectize_learning_algorithms = $("#select-learning-algorithm")[0].selectize;
        var selectize_learned_algorithms = $("#select-learning-algorithm-new-prediction-dataset")[0].selectize;
        var all_algorithms_learned = selectize_learning_algorithms.options;
        var current_algorithms_learned = $("#select-learning-algorithm").val();

        selectize_learned_algorithms.clearOptions();

        for (var option_key in all_algorithms_learned) {
            let option = all_algorithms_learned[option_key];
            let current_algorithm_was_selected = false;
            let j = 0;

            while (j < current_algorithms_learned.length && !current_algorithm_was_selected) {
                if (option["value"] == current_algorithms_learned[j]) {
                    current_algorithm_was_selected = true;
                }
                j++;
            }

            if (current_algorithm_was_selected) {
                selectize_learned_algorithms.addOption(option);
            }
        }
    });

    $(".download-predictions-new-prediction-dataset").click(function(e) {
        var form = $("#new-prediction-dataset-download-form");
        var algorithms = $("#select-learning-algorithm-new-prediction-dataset").val();

        var data_client_json = {
            "algorithms": algorithms,
            "format": $(this).attr("format")
        };
        data_client_json = JSON.stringify(data_client_json);
        $("#new-prediction-dataset-download-data-client-json").val(data_client_json);

        if (algorithms.length == 0) {
            $("#new-prediction-dataset-download-error").removeClass("hide-element").show();
        } else {
            $("#new-prediction-dataset-download-error").hide();
            form.submit();
        }

    });


    $("#select-learning-algorithm").on("change", function (e) {
        var selectize_learning_algorithms = $(this)[0].selectize;
        var all_algorithms_learned = selectize_learning_algorithms.options;
        var learning_algorithms_selected =  $(this).val();
        var forbidden_algorithms = ["bagging", "boosting", "stacking"];
        var bagging_estimator_selectize = $("#bagging_base_estimator")[0].selectize;
        var boosting_estimator_selectize = $("#boosting_base_estimator")[0].selectize;
        var stacking_classifiers = $("#stacking_classifiers")[0].selectize;
        var stacking_meta_classifier = $("#stacking_meta_classifier")[0].selectize;

        bagging_estimator_selectize.clearOptions();
        boosting_estimator_selectize.clearOptions();
        stacking_classifiers.clearOptions();
        stacking_meta_classifier.clearOptions();

        for (let i=0; i<learning_algorithms_selected.length; i++) {
            let option_key = learning_algorithms_selected[i];
            if (forbidden_algorithms.indexOf(option_key) === -1) {
                let option = all_algorithms_learned[option_key];
                bagging_estimator_selectize.addOption(option);
                boosting_estimator_selectize.addOption(option);
                stacking_classifiers.addOption(option);
                stacking_meta_classifier.addOption(option);
            }
        }

    });

});

function getTableColumnIdx (table) {
    var title = table.columns( colIdx ).header().to$().text();
}

function show_algorithms_parameters(algorithm_chosen) {
    var placeholder_algorithm_paremeters = $("#learning-parameters-" + algorithm_chosen);

    $("#section-select-algorithm-parameters").removeClass("hide-element").show();

    $(".learning-algorithm-parameters").hide(); //Close old algorithm parameters input
    placeholder_algorithm_paremeters.show();
}

function show_methods_parameters(algorithm_chosen) {
    console.log("desde la funcion")
    console.log(algorithm_chosen)

    var placeholder_method_paremeters = $("#method-parameters-" + algorithm_chosen);

    $("#section-select-method-parameters").removeClass("hide-element").show();

    $(".learning-method-parameters").hide(); //Close old algorithm parameters input
    placeholder_method_paremeters.show();

    if(algorithm_chosen == "mltsvm" || algorithm_chosen == "mlknn") {
        $(".learning-algorithm-parameters").hide();
    } else {
        $("#method-parameters-" + algorithm_chosen + " .algorithm-parameter-select").each(function() {
            $(this).trigger("change");
        });
    }

}