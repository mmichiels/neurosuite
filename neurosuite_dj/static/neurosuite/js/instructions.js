/**
 * Created by mario on 29/12/17.
 */
<!-- jQuery CDN -->
var fixed_menu = true;
window.jQuery = window.$ = jQuery;

function hide_all() {
    $('.app').each( function() {
        app = $(this);
        app.hide();
        app.addClass("hide-element");
    })
}

function show_all() {
    $('.app').each( function() {
        app = $(this);
        app.show();
        app.removeClass("hide-element").show();
    })
}

function show_class(class_) {
    console.log('1')
    $('.'+class_).each( function() {
        console.log('2')
        app = $(this);
        app.show();
        app.removeClass("hide-element").show();
    })
}

$(document).ready(function () {

    $('.btn').on('click', function () {
        id = $(this).attr('id');
        if( id == "all") {
            console.log("desde todos")
            show_all();
        } else {
            console.log("desde el else")
            hide_all();
            show_class(id);
        }
    });
});