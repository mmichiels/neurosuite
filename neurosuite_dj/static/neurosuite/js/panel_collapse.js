
$(document).ready(function () {


    $(document).on('click', '.panel-heading.clickable', function(e){
        var $this = $(this);
        if(!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            if ($this.attr('id') === "panel-heading-neuroviewer") {
                $('#ControlLayer').hide();
            }
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            if ($this.attr('id') === "panel-heading-neuroviewer") {
                $('#ControlLayer').show();
            }
        }
    })

})