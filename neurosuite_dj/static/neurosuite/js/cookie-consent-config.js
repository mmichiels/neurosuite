window.addEventListener("load", function(){
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#383b75"
            },
            "button": {
                "background": "#f1d600"
            }
        },
        "content": {
            "message": "This website uses cookies to ensure you get the best experience on our website. By navigating this site, you agree to allow us to use cookies.",
            "dismiss": "Ok",
            "href": "https://neurosuites.com/cookie-policy"
        }
    })});