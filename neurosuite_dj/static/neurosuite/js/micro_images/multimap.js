
$(document).ready( function () {
    var csrftoken = $("#csrftoken").val();
    var configuration_json_received_string = $("#configuration_json_received").val();

    var default_options = {
        "controls": {
            "zoom": false,
            "draw": {
                position: 'bottomleft',
                draw: {
                    polyline: false,
                    marker: {},
                    circlemarker: false,
                    polygon: {
                        allowIntersection: false
                    },
                    rectangle: true,
                    circle: {}
                },
                edit: {
                    allowIntersection: false
                }
            },
            "layers": {
                "autoZIndex": false,
                "sortLayers": false
            },
            "attribution": {
                "prefix": "<a href= 'https://github.com/gherardovarando/leaflet-map-builder'> leaflet-map-builder</a>"
            }
        },
        "tooltip": {
            "marker": true,
            "polyline": true,
            "polygon": true,
            "circle": true,
            "rectangle": true
        },
        "popup": {
            "marker": false,
            "polyline": false,
            "polygon": false,
            "circle": false,
            "rectangle": false
        }
    };

    var editActions = [
        L.textInput,
        L.Toolbar2.Action.extendOptions({
            toolbarIcon: {
                className: 'leaflet-color-picker',
                html: '<span class="fa fa-eyedropper"></span>',
                tooltip: 'Color'
            },
            subToolbar: new L.Toolbar2({ actions: [
                    L.ColorPicker.extendOptions({ color: '#db1d0f' }),
                    L.ColorPicker.extendOptions({ color: '#025100' }),
                    L.ColorPicker.extendOptions({ color: '#ffff00' }),
                    L.ColorPicker.extendOptions({ color: '#0000ff' }),
                    L.ColorPicker.extendOptions({ color: '#000000' }),
                    L.ColorPicker.extendOptions({ color: '#ffffff' }),
                ]})
        })
    ];


    initializeMap();

    var builder = L.mapBuilder();
    builder.setMap(map);
    builder.setOptions(default_options);

    var configuration_name = "";

    if (configuration_json_received_string !== "") {
        this.configuration_map_json = JSON.parse(configuration_json_received_string);
        load_json_check_errors(this.configuration_map_json);
    }

    function getUpdatedConfiguration(builder) {
        var my_builder_config = builder._configuration;
        if (builder._drawnItems !== null && builder._drawnItems !== undefined) {
            var drawing_layers = builder._drawnItems._configuration.layers;
            my_builder_config.layers["drawnItems"] = {
                "name": "drawnItems",
                "role": "drawnItems",
                "type": "featureGroup",
                "layers": drawing_layers
            };
        }

        return my_builder_config;
    }

    $("#export-map-json").click(function() {
        var my_builder_config = getUpdatedConfiguration(builder);
        var file_name = "map_exported.json"
        saveGenericJson(my_builder_config, file_name);
    });

    $("#export-drawing-layer-json").click(function() {
        var my_builder_config = getUpdatedConfiguration(builder);
        var config_drawing_layer = my_builder_config.layers["drawnItems"]
        var file_name = "drawing_layer_exported.json"
        saveGenericJson(config_drawing_layer, file_name);
    });


    $("#input-file-map-json").change(function() {
        var configuration = $(this)[0].files[0];
        $("#map-filename").text(configuration.name);
        $("#map-filename").show();
        configuration_name = configuration.name;
        var fr = new FileReader();
        fr.onload = onReaderLoadMap;
        fr.readAsText(configuration);
    });

    function onReaderLoadMap(event){
        this.configuration_map_json = JSON.parse(event.target.result);
        $(".buttons-hide-without-map").removeClass("hide-element");
        $("#load-drawing-layer-json").attr('style', 'display: none !important');
        $("#load-drawing-layer-json").hide();
        loadMap(this.configuration_map_json);
    }

    function loadMap(configuration_json) {
        this.configuration_map_json = configuration_json;
        initializeMap();
        builder = L.mapBuilder();
        builder.setMap(map);
        builder.setOptions(default_options);
        builder.setConfiguration(configuration_json);

        var drawing_layers = builder._drawnItems._layers;
        for (var key in drawing_layers) {
            let layer = drawing_layers[key];
            layer.unbindTooltip();
            layer.bindTooltip(layer._configuration.name, {"permanent": true});

            layer.on('click', function(event) {
                editColorObjectLayer(event, layer);
            });
        };


        map.on('draw:created', function (e) {
            var type = e.layerType,
                layer = e.layer,
                options = e.layer.options;
            builder._drawnItems.addLayer(layer);

            layer.on('click', function(event) {
                editColorObjectLayer(event, layer);
            });

            var new_key = generateId(6);

            builder._drawnItems._configuration.layers[new_key] = {
                type : type,
                options: options,
                "details": "",
                "_mykeyB": new_key
            }

            if (layer instanceof L.Polyline || layer instanceof L.Polygon || layer instanceof L.Rectangle) {
                builder._drawnItems._configuration.layers[new_key]["latlngs"] = layer.getLatLngs();
            } else {
                builder._drawnItems._configuration.layers[new_key]["latlng"] = layer.getLatLng();
            }

            layer["_configuration"] = {};
            layer["_configuration"]["_mykeyB"] = new_key
        });

        map.on('draw:edited', function (e) {
            var layers = e.layers;
            layers.eachLayer(function (layer) {
                layer.unbindTooltip();
                layer.bindTooltip(layer._configuration.name, {"permanent": true});
            });
        });

        map.on("draw:deleted", function(e) {
            var layers = e.layers._layers;
            for (var key in layers) {
                var new_key = layers[key]._configuration._mykeyB;
                delete builder._drawnItems._configuration.layers[new_key];
            };
        });
        map.addLayer(builder._drawnItems);
    }

    function loadLayer(configuration_json) {
        builder.loadLayer(configuration_json);
    }



    //Rename object layer
    $('body').on('keypress', '#shapeName', function(args) {
        if (args.keyCode == 13) {
            $('#okBtnPopup').click();
            return false;
        }
    });

    $("body").on("click", "#okBtnPopup", function(element) {
        var element_target = element.target;
        var sName = $('#shapeName').val();
        var sDesc = $('#shapeDesc').val();

        var key_layer  = $(element_target).attr("keyShape");
        builder._drawnItems._configuration.layers[key_layer]["name"] = sName;

        var leaflet_id  = $(element_target).attr("leafletId");
        var drawing_layers = builder._drawnItems._layers;
        var layer_object = drawing_layers[leaflet_id];
        if (layer_object._configuration === undefined) {
            layer_object["_configuration"] = {};
        }
        layer_object._configuration["name"] = sName;
        layer_object.closePopup();
        layer_object.unbindPopup();
        layer_object.closePopup();

        layer_object.unbindTooltip();
        layer_object.bindTooltip(sName, {"permanent": true});


    });


    function editColorObjectLayer(event, layer) {
        new L.Toolbar2.Popup(event.latlng, {
            actions: editActions
        }).addTo(map, layer);
    }


    function addMiniMap(layer) {
        var options_minimap = _.clone(layer.options); //Clone to not copy by reference and not modify the original layer object
        options_minimap["level"] = 1; //TODO optimization: Get current level instead of the 1st level by default
        var layer_minimap = new L.TileLayer(layer._url, options_minimap);

        if (this.miniMap) {
            if (this.change_map) {
                this.miniMap = new L.Control.MiniMap(layer_minimap, {zoomLevelOffset: -13}).addTo(map);
                this.change_map = false;
            } else {
                this.miniMap.changeLayer(layer_minimap);
            }
        } else {
            this.miniMap = new L.Control.MiniMap(layer_minimap, {zoomLevelOffset: -13}).addTo(map);
        }

    }

    function initializeMap() {
        if(this.map != undefined || this.map != null){
            this.map.remove();
            $("#main_map").html("");
            $("#premap").empty();
            $("<div id=\"main_map\" style=\"height: 500px;\"></div>").appendTo("#premap");
            this.change_map = true;
        }

        this.map = L.map("main_map", {
            //Leaflet.fullscreen plugin by mapbox
            fullscreenControl: true,
            //Default options
            zoomControl: false, //use the builder to put controls
            attributionControl: false, //use the builder to put controls
            crs: L.CRS.Simple,
            multilevel: true,
            levelControl: {
                position: 'bottomleft'
            },
            //easyPrint plugin:
            editable: true,
            printable: true,
            downloadable: true,
            //zoominfoControl plugin:
            zoominfoControl: true,
        });

        //-------Plugins------
        //Leaflet-MiniMap
        map.on('baselayerchange', function (e) {
            addMiniMap(e.layer);
        });

        //Leaflet.Coordinates
        L.control.coordinates({
            position:"bottomleft", //optional default "bootomright"
            decimals:2, //optional default 4
            labelTemplateLat:"Latitude: {y}", //optional default "Lat: {y}"
            labelTemplateLng:"Longitude: {x}", //optional default "Lng: {x}"
        }).addTo(map);

        //Leaflet print export

        map.on('layeradd', function(object) {
            if (object.layer._configuration !== undefined && object.layer._configuration.image_id !== null && object.layer._configuration.image_id !== undefined && object.layer._configuration.baseLayer) {
                $("#actual_image_id").val(object.layer._configuration.image_id);
                $("#actual_image_name").val(object.layer._configuration.name);
            }
        });


        map.on('overlayadd', function(object) {
            if (object.layer._configuration.image_id !== null && object.layer._configuration.image_id !== undefined) {
                $("#actual_image_id").val(object.layer._configuration.image_id);
                $("#actual_image_name").val(object.layer._configuration.name);
                addMiniMap(object.layer);
            }
        });

        map.on('overlayremove', function(object) {
            if (object.layer._configuration.image_id !== null && object.layer._configuration.image_id !== undefined) {
                var base_layer = {}
                map.eachLayer(function (layer) {
                    if (layer._configuration !== undefined && layer._configuration.baseLayer) {
                        base_layer = layer;
                    }
                })

                if (base_layer._configuration !== undefined) {
                    $("#actual_image_id").val(base_layer._configuration.image_id);
                }
                addMiniMap(base_layer);
            }
        });
    }



    function saveGenericJson(obj, name_file) {
        var str = JSON.stringify(obj);
        var data = encode_json_string( str );

        var blob = new Blob( [ data ], {
            type: 'application/octet-stream'
        });

        var url = URL.createObjectURL( blob );
        var link = document.createElement( 'a' );
        link.setAttribute( 'href', url );
        link.setAttribute( 'download', name_file);
        var event = document.createEvent( 'MouseEvents' );
        event.initMouseEvent( 'click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent( event );
    }


    function encode_json_string ( s ) {
        var out = [];
        for ( var i = 0; i < s.length; i++ ) {
            out[i] = s.charCodeAt(i);
        }
        return new Uint8Array( out );
    }

// dec2hex :: Integer -> String
    function dec2hex (dec) {
        return ('0' + dec.toString(16)).substr(-2)
    }

// generateId :: Integer -> String
    function generateId (len) {
        var arr = new Uint8Array((len || 40) / 2)
        window.crypto.getRandomValues(arr)
        return Array.from(arr, dec2hex).join('')
    }

















    var md5 = "",
        csrf = $("input[name='csrfmiddlewaretoken']")[0].value,
        form_data = [{"name": "csrfmiddlewaretoken", "value": csrf}];
    function calculate_md5(file, chunk_size) {
        var slice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice,
            chunks = chunks = Math.ceil(file.size / chunk_size),
            current_chunk = 0,
            spark = new SparkMD5.ArrayBuffer();
        function onload(e) {
            spark.append(e.target.result);  // append chunk
            current_chunk++;
            if (current_chunk < chunks) {
                read_next_chunk();
            } else {
                md5 = spark.end();
            }
        };
        function read_next_chunk() {
            var reader = new FileReader();
            reader.onload = onload;
            var start = current_chunk * chunk_size,
                end = Math.min(start + chunk_size, file.size);
            reader.readAsArrayBuffer(slice.call(file, start, end));
        };
        read_next_chunk();
    }

    var chunkSizeBytes = 1000000 // Chunks of 1 MB
    $("#chunked_upload_map").fileupload({
        url: "/micro/api_chunked_upload",
        dataType: "json",
        maxChunkSize: chunkSizeBytes,
        formData: form_data,
        add: function(e, data) {
            chunked_upload_add(e, data);
        },
        chunkdone: function(e, data) {
            chunked_upload_chunkdone(e, data);
        },
        done: function(e, data) {
            chunked_upload_complete_done(e, data, url_process = "/micro/load_map_image_computer_multimap");
        },
        error: function(e, data) {
            chunked_upload_error(e, data);
        }
    });

    $("#chunked_upload_layer").fileupload({
        url: "/micro/api_chunked_upload",
        dataType: "json",
        maxChunkSize: chunkSizeBytes,
        formData: form_data,
        add: function(e, data) {
            chunked_upload_add(e, data);
        },
        chunkdone: function(e, data) {
            chunked_upload_chunkdone(e, data);
        },
        done: function(e, data) {
            chunked_upload_complete_done(e, data, url_process="/micro/load_layer_image_computer_multimap");
        },
        error: function(e, data) {
            chunked_upload_error(e, data);
        }
    });


    $("#load-drawing-layer-json").fileupload({
        url: "/micro/api_chunked_upload",
        dataType: "json",
        maxChunkSize: chunkSizeBytes,
        formData: form_data,
        add: function(e, data) {
            chunked_upload_add(e, data);
        },
        chunkdone: function(e, data) {
            chunked_upload_chunkdone(e, data);
        },
        done: function(e, data) {
            chunked_upload_complete_done(e, data, url_process="/micro/load_drawing_layer_multimap");
        },
        error: function(e, data) {
            chunked_upload_error(e, data);
        }
    });

    function chunked_upload_add(e, data) {
        // Called before starting upload
        // If this is the second file you're uploading we need to remove the
        // old upload_id and just keep the csrftoken (which is always first).
        $(".chunked_upload").hide();
        $(".loading_map_image").removeClass("hide-element");
        $(".loading_map_image").show();
        $(".status_load_map_image").show();
        $(".progress_bar_status_load_map_image").removeClass("hide-element");
        $(".progress_bar_status_load_map_image").show();
        $(".loading_map_image_spinner").show();
        $(".status_text_load_map_image").text("Step 1/2: Uploading image to the server...");

        form_data.splice(1);
        calculate_md5(data.files[0], chunkSizeBytes);  // Again, chunks of 100 kB
        data.submit();
    }

    $("#form-load-layer-image-url").on('click', '#load-layer-image-url-submit', function(e) {
        e.preventDefault();

        $("#configuration_json_in_html").val(JSON.stringify(getUpdatedConfiguration(builder)));
        $("#configuration_name_in_html").val(configuration_name);

        //Submit form at the end if you want
        $("#form-load-layer-image-url").submit();
    });

    function chunked_upload_chunkdone(e, data) { // Called after uploading each chunk
        if (form_data.length < 2) {
            form_data.push(
                {"name": "upload_id", "value": data.result.upload_id}
            );
        }
        var progress = parseInt(data.loaded / data.total * 100.0, 10);
        jQuery('.bar').css({'width': progress + '%'});
        jQuery('.bar-text').html(progress + '%');
    }

    function chunked_upload_complete_done(e, data, url_process) {
        // Called when the file has completely uploaded
        $(".progress_bar_status_load_map_image").hide();
        $(".status_text_load_map_image").text("Step 2/2: Processing image...");
        $(".status_extra_load_map_image").show();
        $(".status_extra_load_map_image").text("This should usually take less than a minute.");
        var configuration_map_json =  JSON.stringify(getUpdatedConfiguration(builder));
        $.ajax({
            type: "POST",
            url: url_process,
            data: {
                csrfmiddlewaretoken: csrf,
                upload_id: data.result.upload_id,
                configuration_map_json: JSON.stringify(getUpdatedConfiguration(builder)),
                configuration_name: configuration_name,
                md5: md5
            },
            dataType: "json",
            success: function(data) {
                $(".loading_map_image").hide();
                $(".load_map_image_text_complete").show();
                $(".load_map_image_text_complete").text("Completed!");
                $(".load_map_image_text_complete").hide();
                $(".status_extra_load_map_image").hide();
                $(".status_load_map_image").hide();
                $(".modal").modal('hide');
                $(".chunked_upload").show();

                load_json_check_errors(data);
            },
            error: function(data) {
                var error_text = getAjaxErrorText(data)
                printLoadError(error_text);
            }
        });
    }

    function chunked_upload_error(e, data) {
        var error_text = getAjaxErrorText(data)
        if (error_text === "") {
            if (e.hasOwnProperty('responseJSON') && e.responseJSON.hasOwnProperty('detail') && e.responseJSON.detail !== "") {
                error_text = e.responseJSON.detail;
            } else {
                error_text = "Error";
            }
        }
        printLoadError(error_text);
    }

    function load_json_check_errors(json_data) {
        if (json_data.error === "") {
            if (json_data.configuration_warning === undefined || json_data.configuration_warning == "") {
                json_data.configuration_warning = "";
                var notify_object = print_notify_message(permanent=false, type="success", icon='glyphicon glyphicon-ok', title="New layer/s added", message="Click on the layer controls to change the layer", delay=10000, additional_message="<a class='leaflet-control-layers-toggle'></a>");
            } else {
                var notify_object = print_notify_message(permanent=true, type="danger", icon='glyphicon glyphicon-remove', title="Error", message=json_data.configuration_warning, delay=10000);
            }
            $("#map-filename").removeClass("hide-element");
            $("#map-filename").text(json_data.configuration_name + " - This map will auto-delete in 1 hour");
            configuration_name = json_data.configuration_name;
            $("#map-filename").show();
            this.configuration_map_json = json_data.configuration_json;
            $(".buttons-hide-without-map").removeClass("hide-element");
            $("#load-drawing-layer-json").attr('style', 'display: none !important');
            $("#load-drawing-layer-json").hide();
            loadMap(this.configuration_map_json);
        } else {
            printLoadError(json_data.error);
        }
    }

    function getAjaxErrorText(e, data) {
        var error_text = "";
        if (data !== undefined) {
            if (data.responseJSON !== undefined && data.responseJSON.detail !== undefined) {
                error_text = data.responseJSON.detail;
            } else {
                error_text = data.responseText;
            }
        }
        return error_text;
    }

    function printLoadError(error_text) {
        $(".loading_map_image").hide();
        var start_error_text = "Error: ";
        var help_text = ". Please retry later."
        $(".status_extra_load_map_image").hide();
        $(".modal").modal('hide');
        $(".chunked_upload").show();
        var show_error_text = start_error_text + error_text + help_text;
        $("#map-filename").removeClass("hide-element");
        $("#map-filename").text(show_error_text);
        $("#map-filename").show();
        $("#load-drawing-layer-json").attr('style', 'display: none !important');
        $("#load-drawing-layer-json").hide();
    }

    $("#tools-points-continue").click(function(e) {
        var form = $("#form-tool-points-multimap");
        var configuration_map_updated = getUpdatedConfiguration(builder);
        var configuration_map_json = JSON.stringify(configuration_map_updated);
        $("#configuration_map_json").val(configuration_map_json);
        $("#configuration_name_json").val(configuration_name);
        form.submit();
    });

    $("#export-map-image").click(function(e) {
        var form = $("#form-export-map-image");

        var configuration_map_updated = getUpdatedConfiguration(builder);
        var configuration_map_json = JSON.stringify(configuration_map_updated);
        $("#configuration_map_json_image").val(configuration_map_json);
        $("#configuration_name_json_image").val(configuration_name);
        var element = document.getElementById("premap")
        var full_html_client = document.documentElement.innerHTML;
        full_html_client = btoa(unescape(encodeURIComponent( full_html_client )));
        $("#full_html_client").val(full_html_client);

        form.submit();
    });


    $(".panel-heading-recommended-sources").each(function (index, element) {
        $(this).parents('.panel').find('.panel-body').slideUp();
        $(this).addClass('panel-collapsed');
        $(this).find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
    });

    function print_notify_message(permanent, type, icon, title, message, delay, additional_mesage) {
        if (permanent) {
            delay = 0;
        }
        $.notify({
            // options
            icon: icon,
            title: title,
            message: message
        },{
            // settings
            type: type,
            offset: {
                x: 10,
                y: 100
            },
            allow_dismiss: true,
            delay: delay,
            template: "<div data-notify=\"container\" class=\"col-xs-11 col-sm-3 alert alert-{0}\" role=\"alert\">" +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                "<span data-notify=\"icon\" class='notify-icon'></span>" +
                "<span data-notify=\"title\"><strong>{1}</strong></span><br>" +
                "<span data-notify=\"message\">{2}</span>" +
                additional_mesage +
                "</div>" +
                ""
        });
    }

});