$(document).ready(function () {
    var poll_xhr;
    var process_started = false;
    var prev_progress_percent = 0;
    var prev_estimated_time_finish = 0;

    function poll() {
        var json_dump = $("#data").val();
        var task_id = $("#task_id").val();

        console.log(task_id);
        poll_xhr = $.ajax({
            url: $("#task_url_on_loading").val(),
            type: 'POST',
            data: {
                task_id: task_id,
                csrfmiddlewaretoken: $("#csrf_token").val(),
            },
            success: function (result) {
                console.log("RESULT DONE: ", result.done);
                $("#task_state").html(result);

                if (result.processed_django) {
                    document.getElementById("user-count").textContent = "DONE";
                    jQuery('.bar').css({'width': 100 + '%'});
                    jQuery('.bar-text').html(100 + '%');
                    var form = $("#form-task-done-frontend");
                    form.submit();
                } else {
                    if (typeof result.process_percent === 'undefined' && !process_started) {
                        jQuery('.bar').css({'width': 0 + '%'});
                        jQuery('.bar-text').html(0 + '%');
                        document.getElementById("user-count").textContent = "STARTING...";
                        $("#estimated-time-finish").text("Calculating");
                        console.log("Test");
                    } else {
                        process_started = true;
                        if (typeof result.process_percent !== 'undefined' || result.progress_percent !== undefined) {
                            prev_progress_percent = result.process_percent;
                            prev_estimated_time_finish = result.estimated_time_finish;
                        } else {
                            console.log("Changing value");
                            result.process_percent = prev_progress_percent;
                            result.estimated_time_finish = prev_estimated_time_finish;
                        }
                        console.log("prev_progress_percent", prev_progress_percent);
                        console.log("prev_estimated_time_finish", prev_estimated_time_finish);
                        console.log("Result process percent", prev_progress_percent);
                        jQuery('.bar').css({'width': prev_progress_percent+ '%'});
                        jQuery('.bar-text').html(prev_progress_percent + '%');
                        document.getElementById("user-count").textContent = "PROCESSING...";
                        var estimated_time_finish = prev_estimated_time_finish;
                        console.log("estimated_time_finish", estimated_time_finish);
                        var duration = moment.duration(estimated_time_finish, 'seconds');
                        console.log("estimated_time_finish SECONDS", duration);
                        var formatted = duration.format("hh[h]:mm[m]:ss[s]");
                        $("#estimated-time-finish").text(formatted);
                    }

                }

            }
        });
    };

    setInterval(function () {
        poll();
    }, 500);

    window.onbeforeunload = function (e) {
        var e = e || window.event;
        console.log("Revoking task...");
        cancel_possible = $("#cancel_possible").val()
        if ($("#cancel_possible").val() === "1") {
            revoke_worker_task(exit=false);

            // For IE and Firefox
            if (e) {
                e.returnValue = 'Are you sure to go back?';
            }

            // For Safari
            return 'Are you sure to go back?';
        }

    };

    $("#button-cancel-task").click(function () {
        console.log("Revoking task...");
        revoke_worker_task(exit=true);
    });

});


function revoke_worker_task(exit) {
    var task_id = $("#task_id").val();

    if (task_id !== undefined && task_id !== "") {
        $.ajax({
            url: "/morpho/revoke_worker_task/",
            type: 'POST',
            data: {
                task_id: task_id,
                csrfmiddlewaretoken: $("#csrf_token").val(),
            },
            success: function (result) {
                console.log("Task succesfully revoked", result)
                if (exit) {
                    window.history.back();
                }
            },
            error: function (result) {
                console.log("Error revoking the task", result)
                if (exit) {
                    window.history.back();
                }
            },
        });
    }

}
