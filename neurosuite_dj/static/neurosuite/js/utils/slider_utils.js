

function set_input_left_for_slider(slider_id, slider, min_val, callback) {
    slider = slider.noUiSlider;
    var tooltip_left =  $("#" + slider_id).find('.noUi-handle-lower');
    tooltip_left.empty();
    tooltip_left.html($('<div class="noUi-tooltip"><input class="input-below-slider input-below-slider-left" value="' +   min_val + '"></div>'));

    var slider_input = $("#" + slider_id).find('.input-below-slider-left');

    var is_new_input_val = false;
    var is_enter_clicked = false;

    $("#" + slider_id).find(".input-below-slider-left").on("click", function(e) {
        $(this).val("");
        e.preventDefault();
    });

    slider_input.on("blur", function(e){
        e.preventDefault();
        if (is_enter_clicked) {
            is_enter_clicked = false;
            is_new_input_val = false;
        } else {
            is_new_input_val = true;
        }
        slider_input.trigger("change");
    });

    slider_input.on("keydown", function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            is_enter_clicked = true;
            $(this).trigger('change');
            return false;
        }
    });

    slider_input.on("change", function(e) {
        slider.set(this.value);
    });

    slider.off("slide");
    slider.on('slide', function (values, handle) {
        is_new_input_val = true;
        slider_input.val(values[0]);
    });

    slider.off("set");
    slider.on('set', function (values, handle) {
        if ((is_new_input_val || is_enter_clicked)) {
            is_new_input_val = false;
            callback();
        }
    });
}

function set_input_rigth_for_slider(slider_id, slider, max_val, callback) {
    slider = slider.noUiSlider;
    var tooltip_right =  $("#" + slider_id).find('.noUi-handle-upper');
    tooltip_right.empty();
    tooltip_right.html($('<div class="noUi-tooltip"><input class="input-below-slider input-below-slider-right" value="' +   max_val + '"></div>'));

    var slider_input_left = $("#" + slider_id).find('.input-below-slider-left');
    var slider_input_right = $("#" + slider_id).find('.input-below-slider-right');

    var is_new_input_val = false;
    var is_enter_clicked = false;

    $("#" + slider_id).find(".input-below-slider-right").on("click", function(e) {
        $(this).val("");
        e.preventDefault();
    });

    slider_input_right.on("blur", function(e){
        e.preventDefault();
        if (is_enter_clicked) {
            is_enter_clicked = false;
            is_new_input_val = false;
        } else {
            is_new_input_val = true;
        }
        slider_input_right.trigger("change");
    });

    slider_input_right.on("keydown", function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            is_enter_clicked = true;
            $(this).trigger('change');
            return false;
        }
    });

    slider_input_right.on("change", function(e) {
        slider.set([null, this.value]);
    });

    slider_input_left.on("blur", function(e){
        e.preventDefault();
        if (is_enter_clicked) {
            is_enter_clicked = false;
            is_new_input_val = false;
        } else {
            is_new_input_val = true;
        }
        slider_input_left.trigger("change");
    });

    slider_input_left.on("keydown", function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            is_enter_clicked = true;
            $(this).trigger('change');
            return false;
        }
    });

    slider_input_left.on("change", function(e) {
        slider.set([this.value, null]);
    });

    slider.off("slide");
    slider.on('slide', function (values, handle) {
        is_new_input_val = true;
        slider_input_left.val(values[0]);
        slider_input_right.val(values[1]);
    });

    slider.off("set");
    slider.on('set', function (values, handle) {
        if ((is_new_input_val || is_enter_clicked)) {
            is_new_input_val = false;
            callback();
        }
    });
}