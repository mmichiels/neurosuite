
function print_notify_message(permanent, type, icon, title, message, delay, additional_mesage, position) {
    if (position === undefined) {
        position = "top-right";
    }
    var placement = get_placement_notify(position);

    if (additional_mesage === undefined) {
        additional_mesage = "";
    }
    if (permanent) {
        delay = 0;
    }
    var notify_object = $.notify({
        // options
        icon: icon,
        title: title,
        message: message
    }, {
        // settings
        type: type,
        placement: {
            from: placement["from"],
            align: placement["align"],
        },
        offset: placement["offset"],
        allow_dismiss: true,
        delay: delay,
        template: "<div data-notify=\"container\" class=\"col-xs-11 col-sm-3 alert alert-{0}\" role=\"alert\">" +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            "<span data-notify=\"icon\" class='notify-icon'></span>" +
            "<span data-notify=\"title\"><strong>{1}</strong></span><br>" +
            "<span data-notify=\"message\">{2}</span>" +
            additional_mesage +
            "</div>" +
            ""
    });

    return notify_object
}

function get_placement_notify(position) {
    var placement = {
        "from": "top",
        "align": "right",
        "offset": {
            x: 10,
            y: 100,
        },
    };

    switch(position) {
        case "top-right":
            placement = {
                "from": "top",
                "align": "right",
                "offset": {
                    x: 10,
                    y: 100,
                },
            };
            break;
        case "top-left":
            placement = {
                "from": "top",
                "align": "left",
                "offset": {
                    x: 10,
                    y: 100,
                },
            };
            break;
        case "bottom-right":
            placement = {
                "from": "bottom",
                "align": "right",
                "offset": {
                    x: 10,
                    y: 60,
                },
            };
            break;
        case "bottom-left":
            placement = {
                "from": "bottom",
                "align": "left",
                "offset": {
                    x: 10,
                    y: 60,
                },
            };
            break;
    }

    return placement;
}