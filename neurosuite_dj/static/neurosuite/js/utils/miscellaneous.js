function copyToClipboard(text) {
    var dummy_input_copy = document.createElement("input");
    document.body.appendChild(dummy_input_copy);
    dummy_input_copy.setAttribute("id", "dummy_input_copy_id");
    $("#dummy_input_copy_id").val(text);
    dummy_input_copy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy_input_copy);
}