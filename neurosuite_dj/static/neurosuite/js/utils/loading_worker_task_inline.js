var poll_xhr;
var process_started = false;
var prev_progress_percent = 0;
var prev_estimated_time_finish = 0;
var task_id = "";
var interval;

function run_worker(data_task, id_elem_html, callback) {
    task_id = data_task["id"];

    $("#" + id_elem_html + "-cancel-task").click(function () {
        revoke_worker_task(callback);
    });

    window.onbeforeunload = function (e) {
        var e = e || window.event;

        revoke_worker_task(callback);
    };

    interval = setInterval(function () {
        poll(data_task, id_elem_html, callback);
    }, 500);
}

function poll(data_task, id_elem_html, callback) {
    poll_xhr = $.ajax({
        url: data_task["url_on_loading"],
        type: 'POST',
        data: {
            task_id: task_id,
            csrfmiddlewaretoken: $("#csrf_token").val(),
        },
        success: function (result) {
            if (result.done) {
                $("#" + id_elem_html + "-bar").css({'width': 100 + '%'});
                $("#" + id_elem_html + "-bar-text").html(100 + '%');

                clearInterval(interval);
                callback(result.data_processed)
            } else {
                if (typeof result.process_percent === 'undefined' && !process_started) {
                    $("#" + id_elem_html + "-bar").css({'width': 0 + '%'});
                    $("#" + id_elem_html + "-bar-text").html(0 + '%');
                    $("#" + id_elem_html + "-estimated-time-finish").text("Calculating");
                } else {
                    process_started = true;
                    if (typeof result.process_percent !== 'undefined' || result.process_percent !== undefined) {
                        prev_progress_percent = result.process_percent;
                        prev_estimated_time_finish = result.estimated_time_finish;
                    } else {
                        result.process_percent = prev_progress_percent;
                        result.estimated_time_finish = prev_estimated_time_finish;
                    }
                    $("#" + id_elem_html + "-bar").css({'width': prev_progress_percent + '%'});
                    $("#" + id_elem_html + "-bar-text").html(prev_progress_percent + '%');
                    var estimated_time_finish = prev_estimated_time_finish;
                    if (!isNaN(estimated_time_finish)) {
                        var duration = moment.duration(estimated_time_finish, 'seconds');
                        var estimated_time_finish = duration.format("hh[h]:mm[m]:ss[s]");
                    }
                    $("#" + id_elem_html + "-estimated-time-finish").text(estimated_time_finish);
                }

            }

        }
    });
};


function revoke_worker_task(callback) {
    clearInterval(interval);

    if (task_id !== undefined && task_id !== "") {
        var data_processed = {"error": true, "error_message": "Task revoked"};

        $.ajax({
            url: "/morpho/revoke_worker_task/",
            type: 'POST',
            data: {
                task_id: task_id,
                csrfmiddlewaretoken: $("#csrf_token").val(),
            },
            success: function (result) {
                callback(data_processed);
            },
            error: function (result) {
                callback(data_processed);
            }
        });
    }

}
