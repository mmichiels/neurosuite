$(document).ready( function () {


    $("#select-field-three-values").selectize({
        allowEmptyOption: true,
        create: false,
        maxOptions: 100000000,
        maxItems: 3,
        onInitialize: function () {
            this.setValue("");
        },
    });


    $(".selectize-two-values").selectize({
        maxItems: 2,
        create: false,
        maxOptions: 100000000,
        onInitialize: function () {
            this.setValue("");
        },
    });


    $(".selectize-single-value").selectize({
        maxItems: 1,
        create: false,
        maxOptions: 100000000,
        onInitialize: function () {
            this.setValue("");
        },
        render: {
            option: function (data, escape) {
                var padding = 55;
                return render_option_custom(data, escape, padding)
            },
            item : function(data, escape) {
                return render_item_custom(data, escape);
            }
        },
        /*
        plugins: {
            'dropdown_header': {
                title: '<button class="btn full-width ng-binding" data-options="modal">Sort by:</button>'
            }
        }
        */
    });



    $(".selectize-single-value-initialized").selectize({
        maxItems: 1,
        create: false,
        maxOptions: 100000000,
    });




    $(".selectize-multivalue").selectize({
        allowEmptyOption: true,
        create: false,
        maxOptions: 100000000,
        maxItems: 1000,
        onInitialize: function () {
            this.setValue("");
        },
        render: {
            option: function (data, escape) {
                var padding = 15;
                return render_option_custom(data, escape, padding)
            },
            item : function(data, escape) {
                return render_item_multi_custom(data, escape);
            }
        },
    });

    function render_option_custom(data, escape, padding) {
        if (data.hasOwnProperty("color")) {
            if (data.hasOwnProperty("additional_text")) {
                return '<div class="option"><span style="float:left; background-color:' + data.color + ';color:#ffffff; ' +
                    'padding: 5px ' + padding + 'px 5px ' + padding + 'px; border: 1px solid #0073bb; font-weight: 700; border-radius: 3px;">' +
                    escape(data.text) + '</span><span style="float:right; min-width: 40px; padding: 5px 5px 5px 5px;' +
                    ' margin-left: 3px; border: 1px solid #0073bb; font-weight: 700; border-radius: 3px;">' +
                    escape(data.additional_text) + '</span></div>';
            } else {
                return '<div class="option" style="background-color:' + data.color + ';color:#ffffff;' +
                    ' font-weight: 700; margin: 3px 3px 3px 3px; border: 1px solid #0073bb; border-radius: 3px;">' +
                    escape(data.text) + '</div>';
            }
        }
        return '<div class="option">' + escape(data.text) + '</div>';
    }

    function render_item_custom(data, escape) {
        if (data.hasOwnProperty("color")) {
            if (data.hasOwnProperty("additional_text")) {
                return '<div class="item"><span style="background-color:' + data.color + ' !important; background-image: none !important;' +
                    ' color:#ffffff; padding: 5px 55px 5px 55px; font-weight: 700; border: 1px solid #0073bb; border-radius: 3px;">' +
                    escape(data.text) + '</span><span style="min-width: 30px; padding: 5px 5px 5px 5px;' +
                    ' margin-left: 3px; margin-right: 16px; border: 1px solid #0073bb; font-weight: 700; border-radius: 3px;">' +
                    escape(data.additional_text) + '</span></div>';
            } else {
                return '<div class="item" style="background-color:' + data.color + ' !important; background-image: none !important;' +
                    ' color:#ffffff; padding: 5px 55px 5px 55px; font-weight: 700; border: 1px solid #0073bb; border-radius: 3px;">' +
                    escape(data.text) + '</div>';
            }
        }
        return '<div class="item">' + escape(data.text) + '</div>';
    }

    function render_item_multi_custom(data, escape) {
        if (data.hasOwnProperty("color")) {
            var div_result = '<div class="item" style="background-color:' + data.color + ' !important; background-image: none !important;' +
                ' color:#ffffff; font-weight: 700; ">' + escape(data.text);
            if (data.hasOwnProperty("additional_text")) {
                div_result += " (" + escape(data.additional_text) + ")";
            }
            div_result += "</div>"
            return div_result;
        }

        return '<div class="item">' + escape(data.text) + '</div>';
    }

    $(".selectize-multivalue-allow-new").selectize({
        allowEmptyOption: true,
        create: true,
        maxOptions: 100000000,
        maxItems: 1000,
        onInitialize: function () {
            this.setValue("");
        },
    });


    $('.select-all-button').click(function (e) {
        var select_field_id = $(this).attr("select-field");
        var el = $('#' + select_field_id)[0].selectize;
        var optKeys = Object.keys(el.options);
        console.log(optKeys)


        optKeys.forEach(function (key, index) {
            el.addItem(key);
        });
    });

    $('.select-all-lazy-button').click(function (e) {
        var select_field_id = $(this).attr("select-field");
        var select_field = $('#' + select_field_id);
        var select_field_all_id = $(this).attr("select-field") + "-all";
        var select_field_all =  $('#' + select_field_all_id);
        var selectize = select_field[0].selectize;

        selectize.clear();

        selectize.disable();
        select_field.attr("selected-all", true);
        select_field_all.removeClass("hide-element").show();
    });

    $('.deselect-all-lazy-button').click(function (e) {
        var select_field_id = $(this).attr("select-field");
        var select_field = $('#' + select_field_id);
        var select_field_all_id = $(this).attr("select-field") + "-all";
        var select_field_all =  $('#' + select_field_all_id);
        var selectize = select_field[0].selectize;

        selectize.enable();
        select_field_all.hide();

        selectize.clear();
        select_field.attr("selected-all", false);
    });

    $('.deselect-all-button').click(function (e) {
        var select_field_id = $(this).attr("select-field");
        var selectize_elem = $('#' + select_field_id)[0].selectize;
        selectize_clear_items(selectize_elem);
    });

});

function selectize_clear_items(selectize_elem) {
    var optKeys = Object.keys(selectize_elem.options);
    optKeys.forEach(function (key, index) {
        selectize_elem.removeItem(key);
    });
}