<!-- must load the font-awesome.css for this example -->

$(document).ready(function () {
    var csrftoken = $("#csrftoken").val();
    var label_column_id = "";

    (function() {
        var send = XMLHttpRequest.prototype.send,
            token = $('meta[name=csrf-token]').attr('content');
        XMLHttpRequest.prototype.send = function(data) {
            this.setRequestHeader('X-CSRFToken', csrftoken);
            return send.apply(this, arguments);
        };
    }());

    $("#checkbox-has-column-id").on("change", function (e) {
        var checked = $(this).is(":checked");
        if (checked) {
            $("#input-select-column-id").removeClass("hide-element").show();
        } else {
            $("#input-select-column-id").hide();
        }
    });


    //-----------------------------------------------------

    $("#input-ke-upload").fileinput({
        hideThumbnailContent: true, // hide image, pdf, text or other content in the thumbnail preview
        theme: "explorer",
        uploadUrl: "/morpho/process_upload_neurons/",
        fileActionSettings: {
            showDownload: false,
            showUpload: false,
            indicatorNew: '',
            removeIcon: '<i class="glyphicon glyphicon-trash fa-2x" aria-hidden="true"></i>',
            zoomIcon: '<i class="glyphicon glyphicon-zoom-in fa-2x"></i>',
        },
        uploadExtraData:{'csrfmiddlewaretoken': csrftoken },
        uploadAsync: false
    });

    $('#input-ke-upload').on('filebatchpreupload', function(event, data) {
        console.log('File batch pre upload');
    });

    $('#input-ke-upload').on('filebatchuploadcomplete', function(event, files, extra) {
        console.log('File batch upload complete');
        window.location.href="/morpho/index_morpho_analyzer/";
    });

    var UppyCore = Uppy.Core;
    var UppyDashboard = Uppy.Dashboard;
    var UppyGoogleDrive = Uppy.GoogleDrive;
    var UppyTus = Uppy.Tus;

    var uppy = UppyCore({
        debug: false,
        autoProceed: false,
        restrictions: {
            maxFileSize: 5000000000, //5 GB,
            maxNumberOfFiles: 1,
            minNumberOfFiles: 1,
            allowedFileTypes: ['.csv', '.gzip']
        }
    })
        .use(UppyDashboard, {
            inline: true,
            target: '#upload-csv-uppy',
            replaceTargetContent: true,
            showProgressDetails: true,
            note: 'Only CSV (.csv) or Apache Parquet (.parquet.gzip) files are allowed. Max file size: 5 GB',
            height: 300,
            width: '100%',
            browserBackButtonClose: true,
            showLinkToFileUploadResult: false,
            hideProgressAfterFinish: false,
        })
        //.use(UppyGoogleDrive, { target: UppyDashboard, serverUrl: '/api_uppy_gdrive/' })
        .use(UppyTus, {
                endpoint: $("#tus_upload_url_api_js").val(),
                resume: false,
                chunkSize: 2000000, //10000000
                retryDelays: [0, 1000, 3000, 5000],
            }
        ) //10000000

    uppy.on('complete', function(result) {
        if ($(result.succesful)) {
            var waiting_div_elem = $("#waiting-continue-upload-csv");
            var label_column_id = $("#label-column-id").val();
            if (! $("#checkbox-has-column-id").is(":checked")) {
                label_column_id = "__None__"
            }
            console.log("label column: ", label_column_id);

            var data_client_json = {
                "upload-id": result.upload_id,
                "label-column-id": label_column_id,
                "label-instances": $("#label-instances").val(),
            };
            data_client_json = JSON.stringify(data_client_json)
            var data_send = {
                "csrfmiddlewaretoken": csrftoken,
                "data_client_json": data_client_json,
            };

            $.ajax({
                type: "POST",
                url: "/morpho/upload_dataset_done/",
                data: data_send,
                dataType: 'json',
                beforeSend: function () {
                    waiting_div_elem.removeClass("hide-element");
                },
                success: function (data) {
                    waiting_div_elem.hide();

                    if (data["error"]) {
                        $("#upload-csv-error").removeClass("hide-element").show();
                        $("#upload-csv-error").text("Error: ", data["error_message"]);
                    } else {
                        $("#upload-csv-uppy").hide();
                        $("#upload-csv-text-complete").removeClass("hide-element").show();
                        $("#upload-csv-text-complete").text("Upload and processing complete! Please wait to be redirected.");

                        window.location.href="/morpho/index_morpho_analyzer/";
                    }
                },
                error: function (data) {
                    waiting_div_elem.hide();
                    $("#upload-csv-error").removeClass("hide-element").show();
                    $("#upload-csv-error").text("Error: ", data["error_message"]);
                }
            });
        } else {
            $("#upload-csv-error").removeClass("hide-element").show();
            $("#upload-csv-error").text("Error uploading the file");
        }
    })

});
