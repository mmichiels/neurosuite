$(document).ready(function () {
    var csrf_token = $("#csrf_token").val();
    const NUM_SUBGROUPS = 2;
    var subgroups_selected = 0;
    var subgroup_active = 0;
    check_min_max_fields();

    var query_values_json = [];
    var custom_options_json = [];

    $("body").keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
        }
    });

    $(document).on('click', '.items-search-panel .list-group-item.clickable', function(e) {
        var $this = $(this);
        var neuron_field_id  = $this.attr("id"); //neuron field frontend id
        var neuron_field_name  = $this.attr("field-name"); //neuron field backend id
        var custom_option  = $this.attr("custom_option");
        var submenu = $this.next('.list-group-submenu');
        if(!$this.hasClass('collapsed')) {
            //Open

            submenu.slideDown();
            $this.addClass("collapsed");
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');

            if(!$this.hasClass('data-loaded')) {
                var data_send = {
                    "csrfmiddlewaretoken": csrf_token,
                    "custom_option": custom_option,
                    "subgroups_selected": subgroups_selected,
                    "neuron_field_id": neuron_field_id,
                    "neuron_field_name": neuron_field_name,
                    "dataset_name": $("#dataset_name").val(),
                };
                $.ajax({
                    type: "POST",
                    url: "/morpho/get_local_session_values_neuron_field/",
                    data: data_send,
                    dataType: 'json',
                    beforeSend: function () {
                        $("#loading-" + neuron_field_id).show();
                    },
                    success: function (data) {
                        $this.addClass("data-loaded");
                        createInputElement(data);
                    },
                    error: function () {
                        // failed request; give feedback to user
                        $("#content-" + neuron_field_id).append("Error loading values")
                    }
                });

            }



        } else {
            //Close
            submenu.slideUp();
            $this.removeClass("collapsed");
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');

        }
    });



    $(document).on('change', '.radio-range-numeric-field', function(e){
        //console.log("CLICKED range radio", $(this));
        var item_parent = $(this).parent().closest("div .list-group-submenu");
        var neuron_field_id = item_parent.attr("id");
        var range_values = $("#wrap-range-numeric-values-" + neuron_field_id);
        var exact_value = $("#wrap-select-values-" + neuron_field_id);
        exact_value.hide();
        range_values.show();
    });

    $(document).on('change', '.radio-exact-numeric-field', function(e){
        //console.log("CLICKED exact radio", $(this));
        var item_parent = $(this).parent().closest("div .list-group-submenu");
        var neuron_field_id = item_parent.attr("id");
        var range_values = $("#wrap-range-numeric-values-" + neuron_field_id);
        var exact_value = $("#wrap-select-values-" + neuron_field_id);
        range_values.hide();
        exact_value.show();
    });


    $(document).on('change', '.radio-range-date-field', function(e){
        //console.log("CLICKED range radio", $(this));
        var item_parent = $(this).parent().closest("div .list-group-submenu");
        var neuron_field_id = item_parent.attr("id");
        var range_values = $("#wrap-datepicker-range-field-" + neuron_field_id);
        var exact_value = $("#wrap-select-values-" + neuron_field_id);
        exact_value.hide();
        range_values.show();
    });

    $(document).on('change', '.radio-exact-date-field', function(e){
        //console.log("CLICKED exact radio", $(this));
        var item_parent = $(this).parent().closest("div .list-group-submenu");
        var neuron_field_id = item_parent.attr("id");
        var range_values = $("#wrap-datepicker-range-field-" + neuron_field_id);
        var exact_value = $("#wrap-select-values-" + neuron_field_id);
        range_values.hide();
        exact_value.show();
    });


    $("#half-first-sample-mean").click(function (e) {
        var query_values_json= {};
        var custom_options_json= {};
        var half_filter = true;
        var subgroup_id = 1;
        var all_others = false;
        send_filter_query(query_values_json, custom_options_json, half_filter, all_others, subgroup_id);
    });

    $("#others-second-sample-mean").click(function (e) {
        var query_values_json= {};
        var custom_options_json= {};
        var half_filter = false;
        var all_others = true;
        var subgroup_id = 2;
        send_filter_query(query_values_json, custom_options_json, half_filter, all_others, subgroup_id);
    });

    $(".filter-sample-mean").click(function (e) {
        var subgroup_id = $(this).attr("subgroup-id");
        subgroup_active = subgroup_id;
        $("#modal-filter-sample-" + subgroup_id).modal('show');
    });

    $("#sample-1-remove").click(function (e) {
        subgroup_id = 1;
        $(".sample-1-selected").hide();
        $("#sample-1-selected-error").hide();
        $(".sample-1-unselected").show();
        subgroups_selected = 0;

        remove_sample(subgroup_id);
    });

    $("#sample-2-remove").click(function (e) {
        subgroup_id = 2;
        $(".sample-2-selected").hide();
        $("#sample-2-selected-error").hide();
        $(".sample-2-unselected").show();
        subgroups_selected = 1;

        remove_sample(subgroup_id);
    });

    function remove_sample(subgroup_id) {
        var data_send = {
            "csrfmiddlewaretoken": csrf_token,
            "subgroup_id": subgroup_id,
            "dataset_name": $("#dataset_name").val(),
        };
        $.ajax({
            type: "POST",
            url: "/morpho/remove_datasets_subgroup/",
            data: data_send,
            dataType: 'json',
            success: function (data) {
            },
            error: function () {
            },
        });
    }

    function sample_1_selected(filters_summary, number_neurons_new_sample) {
        $(".sample-1-selected").removeClass("hide-element");
        $(".sample-1-selected").show();
        $("#sample-1-selected-error").hide();
        $(".sample-1-unselected").hide();

        if (filters_summary) {
            $("#sample-1-half-selected").hide();
            $("#sample-1-filter-selected").removeClass("hide-element");
            $("#sample-1-filter-selected").show();
        } else {
            $("#sample-1-half-selected").removeClass("hide-element");
            $("#sample-1-half-selected").show();
            $("#sample-1-filter-selected").hide();
        }

        $("#sample-1-number-neurons-new-sample").text("(" + number_neurons_new_sample + " instances taken)");
        subgroups_selected += 1;
    }

    function sample_2_selected(filters_summary, number_neurons_new_sample) {
        $(".sample-2-selected").removeClass("hide-element");
        $(".sample-2-selected").show();
        $("#sample-2-selected-error").hide();
        $(".sample-2-unselected").hide();

        if (filters_summary) {
            $("#sample-2-other-values").hide();
            $("#sample-2-filter-selected").removeClass("hide-element");
            $("#sample-2-filter-selected").show();
        } else {
            $("#sample-2-other-values").removeClass("hide-element");
            $("#sample-2-other-values").show();
            $("#sample-2-filter-selected").hide();
        }

        $("#sample-2-number-neurons-new-sample").text("(" + number_neurons_new_sample + " instances taken)");
        subgroups_selected += 1;
    }

    function sample_1_error(error_message) {
        $("#sample-1-selected-error").removeClass("hide-element");
        $("#sample-1-selected-error").show();
        $("#sample-1-error-message").text(error_message);
    }

    function sample_2_error(error_message) {
        $("#sample-2-selected-error").removeClass("hide-element");
        $("#sample-2-selected-error").show();
        $("#sample-2-error-message").text(error_message);
    }

    for (i = 1; i <= NUM_SUBGROUPS; i++) {
        $("#continue-select-subgroup-" + i).click(function (e) {
            var subgroup_id = $(this).attr("subgroup-id");
            //var form = $("#form-filter-query");

            //Process fields values
            //console.log("Processing fields values");

            //Get open fields values
            query_values_json = []; //Reset the array
            custom_options_json = []; //Reset the array
            $("#modal-filter-sample-" + subgroup_id + " .content-submenu:visible").each(function () {
                //console.log("Content submenus:", $(this));
                var field_id = $(this).attr("id-field"); //Frontend id
                var field_name = $(this).attr("name-field"); //Backend id
                var field_verbose_name = $(this).attr("field-verbose-name");
                var field_category_class = $(this).attr("category-class");
                var field_category_verbose_name = $(this).attr("category-verbose-name");
                custom_option = false;
                var data_type_values = "discrete";
                var select_input = $("#select-values-" + field_id);
                var range_numeric_input = $("#range-numeric-values-" + field_id);
                var range_date_input = $("#datepicker-values-" + field_id);

                var values;
                if ($("#wrap-select-values-" + field_id).is(":visible")) {
                    //console.log("Select " + field_id + " is visible");
                    values = select_input.val();
                }

                else if ($("#wrap-range-numeric-values-" + field_id).is(":visible")) {
                    values = [];
                    values.push($("#input-range-numeric-1-" + field_id).val());
                    values.push($("#input-range-numeric-2-" + field_id).val());
                    data_type_values = "interval";
                } else if ($("#wrap-datepicker-range-field-" + field_id).is(":visible")) {
                    values = range_date_input.val();
                }

                else if ($("#wrap-" + field_id).is(":visible") && field_name === "limit_number_results") {
                    custom_option = true;
                    //console.log("limit_number_results is visible");
                    var value_limit_results = $("#input-" + field_id).val();
                    if (value_limit_results !== undefined && value_limit_results !== "") {
                        values = value_limit_results;
                    }
                    else {
                        values = undefined;
                    }
                } else if ($("#wrap-" + field_id).is(":visible") && field_name === "sort_results") {
                    custom_option = true;
                    //console.log("sort_results is visible");
                    var order = $("#select-values-order-" + field_id).val();
                    var values_sort = select_input.val();
                    if (order !== undefined && order !== "" && values_sort.length > 0) {
                        values = {
                            "order": order,
                            "values": values_sort,
                        }
                    } else {
                        values = undefined;
                    }
                }

                if (custom_option) {
                    if (values !== undefined) {
                        custom_options_json.push({
                            "id": field_name,
                            "verbose_name": field_verbose_name,
                            "category": field_category_verbose_name,
                            "category_class": field_category_class,
                            "values": values,
                            "data_type_values": data_type_values
                        });
                    }
                } else {
                    if (values !== undefined && values.length > 0) {
                        query_values_json.push({
                            "id": field_name,
                            "verbose_name": field_verbose_name,
                            "category": field_category_verbose_name,
                            "category_class": field_category_class,
                            "values": values,
                            "data_type_values": data_type_values
                        });
                    }
                }
            });

            if (query_values_json.length > 0 || custom_options_json.length > 0) {
                $("#filter-sample-" + subgroup_id + "-selected").show();
                $("#sample-" + subgroup_id + "-error-empty-query").hide();
            } else {
                $("#filter-sample-" + subgroup_id + "-selected").hide();
                $("#sample-" + subgroup_id + "-error-empty-query").show();
            }

            if (query_values_json.length > 0) {
                $("#sample-" + subgroup_id + "-query-filters").show();
            } else {
                $("#sample-" + subgroup_id + "-query-filters").hide();
            }

            if (custom_options_json.length > 0) {
                $("#sample-" + subgroup_id + "-query-custom-options").show();
            } else {
                $("#sample-" + subgroup_id + "-query-custom-options").hide();
            }

            $("#sample-" + subgroup_id + "-query-list > tr").remove();
            query_values_json.forEach(function (element, i) {
                i = i + 1;
                element["order"] = i;

                var values_field = "";
                element.values.forEach(function (element_value, i) {
                    values_field += "<span class='value-field-order'>" + element_value + "</span>"
                });

                var filter_element = "<tr id='query-field-'" + element.id + " neuron-id='" + element.id + "'><td class='order'>" + i + "</td><td><span class='name-category-order " + element.category_class + "'>" + element.category + "</span></td><td><span class='name-field-order'>" + element.verbose_name + "</span></td><td>" + values_field + "</td></tr>";
                $("#sample-" + subgroup_id + "-query-list").append(filter_element);
            });


            $("#sample-" + subgroup_id + "-query-custom-options-list > tr").remove();
            custom_options_json.forEach(function (element, i) {
                i = i + 1;
                element["order"] = i;

                var values_field = "";
                if (typeof element.values === "object") {
                    values_field += "<span><strong>Order: </strong>" + element.values["order"] + "</span><br><br>"
                    values_sort = "";
                    element.values["values"].forEach(function (element_value, i) {
                        values_sort += "<span class='value-field-order'>" + element_value + "</span>"
                    });
                    values_field += "<span><strong>Sort by: </strong>" + values_sort + "</span>"
                } else {
                    values_field = element.values;
                }

                var filter_element = "<tr id='query-field-'" + element.id + " neuron-id='" + element.id + "'><td class='order'>" + i + "</td><td><span class='name-field-order'>" + element.verbose_name + "</span></td><td><strong>" + values_field + "</strong></td></tr>";
                $("#sample-" + subgroup_id + "-query-custom-options-list").append(filter_element);
            });

            var half_filter = false;
            var all_others = false;
            send_filter_query(query_values_json, custom_options_json, half_filter, all_others, subgroup_id);
        });
    }

    //-----------------Sort query order table--------------------------
    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index)
        {
            $(this).width($originals.eq(index).width())
        });
        return $helper;
    };



    //Renumber table rows
    function renumber_table(tableID) {
        $(tableID + " tr").each(function() {
            count = $(this).parent().children().index($(this)) + 1;
            $(this).find('.order').html(count);
        });
    }

    //--------------------------------------------------------


    function send_filter_query(query_values_json, custom_options_json, half_filter, all_others, subgroup_id) {
        $("#modal-filter-sample-" + subgroup_id).modal('hide');


        var query_values_json_send = JSON.stringify(query_values_json);
        var custom_options_json_send = JSON.stringify(custom_options_json);

        //To debug the form values:
        //var values_form = form.serializeArray();
        //console.log("Form", values_form);
        var data_send = {
            "csrfmiddlewaretoken": csrf_token,
            "query-values-json": query_values_json_send,
            "custom-options-json": custom_options_json_send,
            "half-filter": half_filter,
            "all-others": all_others,
            "subgroup-id": subgroup_id,
            "dataset_name": $("#dataset_name").val(),
            "data-type": $("#data-type").val(),
            "label-column-id": $("#label_column_id").val(),
            "is-uploaded-dataset": $("#has_input_dataset").val(),
        };

        var waiting_object = $("#waiting-select-sample-" + subgroup_id + "-independent");

        $.ajax({
            type: "POST",
            url: "/morpho/filter_session_neurons_query/",
            data: data_send,
            dataType: 'json',
            beforeSend: function() {
                waiting_object.removeClass("hide-element");
                waiting_object.show();
            },
            success: function (data) {
                waiting_object.hide();

                var filters_summary = true;
                if ((isEmpty(query_values_json) && isEmpty(custom_options_json)) || all_others || half_filter) {
                    filters_summary = false;
                }
                var number_neurons_new_sample = data.number_neurons_new_sample
                if (data["error"]) {
                    if (subgroup_id == 1) {
                        sample_1_error(data["error_message"]);
                    }
                    else if (subgroup_id == 2) {
                        sample_2_error(data["error_message"]);
                    }
                }
                else {
                    if (subgroup_id == 1) {
                        sample_1_selected(filters_summary, number_neurons_new_sample);
                    }
                    else if (subgroup_id == 2) {
                        sample_2_selected(filters_summary, number_neurons_new_sample);
                    }
                }
            },
            error: function (e) {
                waiting_object.hide();

                // failed request; give feedback to user
                if (subgroup_id == 1)
                    sample_1_error(e.statusText);
                else if (subgroup_id == 2) {
                    sample_2_error(e.statusText);
                }
            }
        });
    };


});



function createInputElement(data) {
    var neuron_field_id = data["field"];
    var values = data["values"];

    $("#loading-" + neuron_field_id).hide();
    $("#content-" + neuron_field_id).show();

    if (neuron_field_id === "limit_number_results") {
        $("#wrap-" + neuron_field_id).show();
    } else if (neuron_field_id === "sort_results") {
        order_values = ["Ascending", "Descending"];
        maxItems = 1;
        setupSelectize(order_values, "order-" + neuron_field_id, maxItems);
        setupSelectize(values, neuron_field_id);
        $("#wrap-" + neuron_field_id).show();
    } else{
        if (values.length === 0) {
            $("#content-" + neuron_field_id).append("No data retrieved");
        }

        if (data["type"] === "text") {
            setupSelectize(values, neuron_field_id);
        } else if (data["type"] === "numeric") {
            setupSelectize(values, neuron_field_id);
            setupRangeSlider(values, neuron_field_id);
            $("#wrap-select-values-" + neuron_field_id).hide();
            $("#radio-numeric-values-" + neuron_field_id).show();
            $("#wrap-range-numeric-values-" + neuron_field_id).show();
            //$(".noUi-pips").hide();
        } else if (data["type"] === "date") {
            setupSelectize(values, neuron_field_id);
            setupDatepicker(values, neuron_field_id);
            $("#wrap-select-values-" + neuron_field_id).hide();
            $("#radio-date-values-" + neuron_field_id).show();
            $("#wrap-datepicker-range-field-" + neuron_field_id).show();
        }

    }





}

function setupSelectize(values, neuron_field_id, maxItems){
    if (maxItems === undefined) {
        maxItems = null;
    }
    var items = values.map(function(x) { return { item: x }; });
    $("#select-values-" + neuron_field_id).selectize({
        maxItems: maxItems,
        labelField: "item",
        valueField: "item",
        searchField: "item",
        options: items,
    });
}

function setupRangeSlider(values, neuron_field_id) {
    var element_slider = document.getElementById("range-numeric-values-" + neuron_field_id);
    var min_val = Math.min.apply(null, values);
    var max_val = Math.max.apply(null, values);

    noUiSlider.create(element_slider, {
        start: [min_val, max_val],
        connect: true,
        format: wNumb({
            decimals: 2
        }),
        range: {
            'min': min_val,
            'max': max_val,
        },
        pips: {
            mode: 'count',
            values: 5,
            density: 4
        }
    });
    var inputNumber1 = document.getElementById("input-range-numeric-1-" + neuron_field_id);
    var inputNumber2 = document.getElementById("input-range-numeric-2-" + neuron_field_id);

    element_slider.noUiSlider.on('update', function( values, handle ) {

        var value = values[handle];

        if ( handle ) {
            inputNumber2.value = value;
        } else {
            inputNumber1.value = value;
        }
    });

    inputNumber1.addEventListener('change', function(){
        element_slider.noUiSlider.set([this.value, null]);
    });

    inputNumber2.addEventListener('change', function(){
        element_slider.noUiSlider.set([null, this.value]);
    });
}

function setupDatepicker(values, neuron_field_id) {
    var startDate = values[0];
    var endDate = values[values.length-1];

    $("#datepicker-values-" + neuron_field_id).daterangepicker({
        "showDropdowns": true,
        ranges: {
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment()],
            'This Year': [moment().startOf('year'), moment().endOf('year')],
            'Last Year': [moment().subtract(1, 'year').startOf('year'), moment()],
            'Last 2 Years': [moment().subtract(2, 'year').startOf('year'), moment()],
            'Last 5 Years': [moment().subtract(5, 'year').startOf('year'), moment()],
            'Last 10 Years': [moment().subtract(10, 'year').startOf('year'), moment()],
            'From the beginning': [startDate, moment()],
        },
        locale: {
            format: 'YYYY-MM-DD'
        },
        "linkedCalendars": true,
        "showCustomRangeLabel": true,
        "startDate": startDate,
        "endDate": moment(),
        "opens": "center",
        "minDate": startDate,
        "maxDate": moment(),
    }, function(start, end, label) {
        //console.log("New date range selected: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        $("#datepicker-range-value-" + neuron_field_id).html(label);
    });

}


function check_min_max_fields() {
    var min_weight_field = $("li#min_weight");
    var min_weight_content = $("div #content-min_weight");
    var max_weight_field = $("li#max_weight");
    var max_weight_label = $("#label-li-max_weight");
    var max_weight_content = $("div #content-max_weight");

    var min_age_field = $("li#min_age");
    var min_age_content = $("div #content-min_age");
    var max_age_field = $("li#max_age");
    var max_age_label = $("#label-li-max_age");
    var max_age_content = $("div #content-max_age");


    if (min_weight_field.length && max_weight_field.length) {
        min_weight_field.remove();
        min_weight_content.remove();
        max_weight_field.attr("name", "weight");
        max_weight_field.val("weight");
        max_weight_label.html("Weight");
    }

    if (min_age_field.length && max_age_field.length) {
        min_age_field.remove();
        min_age_content.remove();
        max_age_field.attr("name", "age");
        max_age_field.val("age");
        max_age_label.html("Age");
    }
}



function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}


