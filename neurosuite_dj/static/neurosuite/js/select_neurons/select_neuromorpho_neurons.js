$(document).ready(function () {
    var csrftoken = getCookie('csrftoken');
    check_min_max_fields();

    var query_values_json = [];
    var custom_options_json = [];

    $("body").keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
        }
    })

    $(document).on('click', '.items-search-panel .list-group-item.clickable', function(e){
        var $this = $(this);
        var neuron_field_id  = $this.attr("id");
        var custom_option  = $this.attr("custom_option");
        var submenu = $this.next('.list-group-submenu');
        if(!$this.hasClass('collapsed')) {
            //Open

            submenu.slideDown();
            $this.addClass("collapsed");
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');

            if(!$this.hasClass('data-loaded')) {
                var data = {
                    "custom_option": custom_option
                };
                $.ajax({
                    type: "GET",
                    url: "/morpho/get_values_neuron_field/" + neuron_field_id,
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        $("#loading-" + neuron_field_id).show();
                    },
                    success: function (data) {
                        $this.addClass("data-loaded");
                        createInputElement(data);
                    },
                    error: function () {
                        // failed request; give feedback to user
                        $("#content-" + neuron_field_id).append("Error loading values")
                    }
                });

            }



        } else {
            //Close
            submenu.slideUp();
            $this.removeClass("collapsed");
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');

        }
    });



    $(document).on('change', '.radio-range-numeric-field', function(e){
        //console.log("CLICKED range radio", $(this));
        var item_parent = $(this).parent().closest("div .list-group-submenu");
        var neuron_field_id = item_parent.attr("id");
        var range_values = $("#wrap-range-numeric-values-" + neuron_field_id);
        var exact_value = $("#wrap-select-values-" + neuron_field_id);
        exact_value.hide();
        range_values.show();
    });

    $(document).on('change', '.radio-exact-numeric-field', function(e){
        //console.log("CLICKED exact radio", $(this));
        var item_parent = $(this).parent().closest("div .list-group-submenu");
        var neuron_field_id = item_parent.attr("id");
        var range_values = $("#wrap-range-numeric-values-" + neuron_field_id);
        var exact_value = $("#wrap-select-values-" + neuron_field_id);
        range_values.hide();
        exact_value.show();
    });


    $(document).on('change', '.radio-range-date-field', function(e){
        //console.log("CLICKED range radio", $(this));
        var item_parent = $(this).parent().closest("div .list-group-submenu");
        var neuron_field_id = item_parent.attr("id");
        var range_values = $("#wrap-datepicker-range-field-" + neuron_field_id);
        var exact_value = $("#wrap-select-values-" + neuron_field_id);
        exact_value.hide();
        range_values.show();
    });

    $(document).on('change', '.radio-exact-date-field', function(e){
        //console.log("CLICKED exact radio", $(this));
        var item_parent = $(this).parent().closest("div .list-group-submenu");
        var neuron_field_id = item_parent.attr("id");
        var range_values = $("#wrap-datepicker-range-field-" + neuron_field_id);
        var exact_value = $("#wrap-select-values-" + neuron_field_id);
        range_values.hide();
        exact_value.show();
    });


    $(".continue-button").click(function (e) {
        //var form = $("#form-search-neuromorpho-query");

        //Process fields values
        //console.log("Processing fields values");

        //Get open fields values
        query_values_json = []; //Reset the array
        custom_options_json = []; //Reset the array
        $( ".content-submenu:visible" ).each(function() {
            //console.log("Content submenus:", $(this));
            var field_id = $(this).attr("id-field");
            var field_verbose_name = $(this).attr("field-verbose-name");
            var field_category_class = $(this).attr("category-class");
            var field_category_verbose_name = $(this).attr("category-verbose-name");
            custom_option = false;
            var select_input = $("#select-values-" + field_id);
            var range_numeric_input = $("#range-numeric-values-" + field_id);
            var range_date_input = $("#datepicker-values-" + field_id);

            var values;
            if ($("#wrap-select-values-" + field_id).is(":visible")) {
                //console.log("Select " + field_id + " is visible");
                values = select_input.val();
            } /*

                else if ($("#wrap-range-numeric-values-" + field_id).is(":visible")) {
                    console.log("Range numeric " + field_id + " is visible");
                    values = [];
                    values.push($("#input-range-numeric-1-" + field_id).val());
                    values.push($("#input-range-numeric-2-" + field_id).val());
                } else if ($("#wrap-datepicker-range-field-" + field_id).is(":visible")) {
                    console.log("Range date " + field_id + " is visible");
                    values = range_date_input.val();

            }
             */
            else if ($("#wrap-limit_number_results").is(":visible") && field_id === "limit_number_results") {
                custom_option = true;
                //console.log("limit_number_results is visible");
                var value_limit_results = $("#input-" + field_id).val();
                if (value_limit_results !== undefined && value_limit_results !== "") {
                    values = value_limit_results;
                }
                else {
                    values = undefined;
                }
            } else if ($("#wrap-sort_results").is(":visible")  && field_id === "sort_results") {
                custom_option = true;
                //console.log("sort_results is visible");
                var order = $("#select-values-order-" + field_id).val();
                var values_sort = select_input.val();
                if (order !== undefined && order !== "" && values_sort.length > 0) {
                    values = {
                        "order": order,
                        "values": values_sort,
                    }
                } else {
                    values = undefined;
                }
            }

            if (custom_option) {
                if (values !== undefined) {
                    custom_options_json.push({
                        "id": field_id,
                        "verbose_name": field_verbose_name,
                        "category": field_category_verbose_name,
                        "category_class": field_category_class,
                        "values": values,
                    });
                }
            } else {
                if (values !== undefined && values.length > 0) {
                    query_values_json.push({
                        "id": field_id,
                        "verbose_name": field_verbose_name,
                        "category": field_category_verbose_name,
                        "category_class": field_category_class,
                        "values": values,
                    });
                }
            }
        });

        if (query_values_json.length > 0 || custom_options_json.length > 0) {
            $("#select-query-order").show();
            $("#warning-empty-query").hide();
        } else {
            $("#select-query-order").hide();
            $("#warning-empty-query").show();
        }

        if (query_values_json.length > 0) {
            $("#neuromorpho-query-filters").show();
        } else {
            $("#neuromorpho-query-filters").hide();
        }

        if (custom_options_json.length > 0) {
            $("#neuromorpho-query-custom-options").show();
        } else {
            $("#neuromorpho-query-custom-options").hide();
        }

        $("#neuromorpho-query-list > tr").remove();
        query_values_json.forEach(function(element, i) {
            i = i+1;
            element["order"] = i;

            var values_field = "";
            element.values.forEach(function(element_value, i) {
                values_field += "<span class='value-field-order'>" + element_value + "</span>"
            });

            var filter_element = "<tr id='query-field-'" + element.id + " neuron-id='" + element.id + "'><td class='order'>" + i +  "</td><td><span class='name-category-order " + element.category_class + "'>" + element.category + "</span></td><td><span class='name-field-order'>" + element.verbose_name +"</span></td><td>" + values_field + "</td><td><a class='btn btn-delete btn-danger'>Delete</a></td></tr>";
            $("#neuromorpho-query-list").append(filter_element);
        });


        $("#neuromorpho-query-custom-options-list > tr").remove();
        custom_options_json.forEach(function(element, i) {
            i = i+1;
            element["order"] = i;

            var values_field = "";
            if (typeof element.values === "object") {
                values_field += "<span><strong>Order: </strong>" + element.values["order"] + "</span><br><br>"
                values_sort = "";
                element.values["values"].forEach(function(element_value, i) {
                    values_sort += "<span class='value-field-order'>" + element_value + "</span>"
                });
                values_field += "<span><strong>Sort by: </strong>" + values_sort + "</span>"
            } else {
                values_field = element.values;
            }

            var filter_element = "<tr id='query-field-'" + element.id + " neuron-id='" + element.id + "'><td class='order'>" + i +  "</td><td><span class='name-field-order'>" + element.verbose_name +"</span></td><td><strong>" + values_field + "</strong></td><td><a class='btn btn-delete btn-danger'>Delete</a></td></tr>";
            $("#neuromorpho-query-custom-options-list").append(filter_element);
        });

        $("#modal-order-neuromorpho").modal('show');

    });

    //-----------------Sort query order table--------------------------
    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index)
        {
            $(this).width($originals.eq(index).width())
        });
        return $helper;
    };

    /*
    //Make table sortable
    $("#neuromorpho-query-table tbody").sortable({
        helper: fixHelperModified,
        stop: function(event,ui) {renumber_table('#neuromorpho-query-table')}
    }).disableSelection();
    */

    //Delete button in table rows
    $('table').on('click','.btn-delete',function() {
        tableID = '#' + $(this).closest('table').attr('id');
        r = confirm('Delete this item?');
        if(r) {
            $(this).closest('tr').remove();
            renumber_table(tableID);
        }
    });

    //Renumber table rows
    function renumber_table(tableID) {
        $(tableID + " tr").each(function() {
            count = $(this).parent().children().index($(this)) + 1;
            $(this).find('.order').html(count);
        });
    }

    //--------------------------------------------------------


    $("#send-neuromorpho-query").click(function (e) {
        $("#modal-order-neuromorpho").modal('hide');

        var form = $("#form-search-neuromorpho-query");
        var query_values_json_send = {};
        $("#neuromorpho-query-table tbody tr").each(function(i, row) {
            i = i+1;
            var neuron_id = $(this).attr("neuron-id");
            var filter_element = query_values_json.find(function(row) {
                return row["id"] == neuron_id;
            });
            query_values_json_send[neuron_id] = filter_element["values"]
        });


        var custom_options_json_send = {};
        $("#neuromorpho-query-custom-options-table tbody tr").each(function(i, row) {
            i = i+1;
            var custom_option_id = $(this).attr("neuron-id");
            var custom_option = custom_options_json.find(function(row) {
                return row["id"] == custom_option_id;
            });
            custom_options_json_send[custom_option_id] = custom_option["values"]
        });

        query_values_json = []; //Reset the array
        custom_options_json = []; //Reset the array
        query_values_json_send = JSON.stringify(query_values_json_send);
        custom_options_json_send = JSON.stringify(custom_options_json_send);
        $("#query-values-json").val(query_values_json_send);
        $("#custom-options-json").val(custom_options_json_send);

        //To debug the form values:
        //var values_form = form.serializeArray();
        //console.log("Form", values_form);

        form.submit();
    });


});



function createInputElement(data) {
    var neuron_field_id = data["field"];
    var values = data["values"];

    $("#loading-" + neuron_field_id).hide();
    $("#content-" + neuron_field_id).show();

    if (neuron_field_id === "limit_number_results") {
        $("#wrap-" + neuron_field_id).show();
    } else if (neuron_field_id === "sort_results") {
        order_values = ["Ascending", "Descending"];
        maxItems = 1;
        setupSelectize(order_values, "order-" + neuron_field_id, maxItems);
        setupSelectize(values, neuron_field_id);
        $("#wrap-" + neuron_field_id).show();
    } else{
        if (values.length === 0) {
            $("#content-" + neuron_field_id).append("No data retrieved");
        }

        if (data["type"] === "text") {
            setupSelectize(values, neuron_field_id);
        } else if (data["type"] === "numeric") {
            $("#wrap-select-values-" + neuron_field_id).hide();
            $("#radio-numeric-values-" + neuron_field_id).show();
            $("#wrap-range-numeric-values-" + neuron_field_id).hide();
            //$(".noUi-pips").hide();
        } else if (data["type"] === "date") {
            $("#wrap-select-values-" + neuron_field_id).hide();
            $("#radio-date-values-" + neuron_field_id).hide();
            $("#wrap-datepicker-range-field-" + neuron_field_id).show();
        }


        /*
        else if (data["type"] === "numeric") {
            setupSelectize(values, neuron_field_id);
            setupRangeSlider(values, neuron_field_id);
            $("#wrap-select-values-" + neuron_field_id).hide();
            $("#radio-numeric-values-" + neuron_field_id).show();
            $("#wrap-range-numeric-values-" + neuron_field_id).show();
            //$(".noUi-pips").hide();
        } else if (data["type"] === "date") {
            setupSelectize(values, neuron_field_id);
            setupDatepicker(values, neuron_field_id);
            $("#wrap-select-values-" + neuron_field_id).hide();
            $("#radio-date-values-" + neuron_field_id).show();
            $("#wrap-datepicker-range-field-" + neuron_field_id).show();
        }
        */
    }





}

function setupSelectize(values, neuron_field_id, maxItems){
    if (maxItems === undefined) {
        maxItems = null;
    }
    var items = values.map(function(x) { return { item: x }; });
    $("#select-values-" + neuron_field_id).selectize({
        maxItems: maxItems,
        labelField: "item",
        valueField: "item",
        searchField: "item",
        options: items,
    });
}

function setupRangeSlider(values, neuron_field_id) {
    var element_slider = document.getElementById("range-numeric-values-" + neuron_field_id);

    noUiSlider.create(element_slider, {
        start: [ parseFloat(values[0]), parseFloat(values[values.length-1])],
        connect: true,
        format: wNumb({
            decimals: 2
        }),
        range: {
            'min': parseFloat(values[0]),
            'max': parseFloat(values[values.length-1])
        },
        pips: {
            mode: 'count',
            values: 5,
            density: 4
        }
    });
    var inputNumber1 = document.getElementById("input-range-numeric-1-" + neuron_field_id);
    var inputNumber2 = document.getElementById("input-range-numeric-2-" + neuron_field_id);

    element_slider.noUiSlider.on('update', function( values, handle ) {

        var value = values[handle];

        if ( handle ) {
            inputNumber2.value = value;
        } else {
            inputNumber1.value = value;
        }
    });

    inputNumber1.addEventListener('change', function(){
        element_slider.noUiSlider.set([this.value, null]);
    });

    inputNumber2.addEventListener('change', function(){
        element_slider.noUiSlider.set([null, this.value]);
    });
}

function setupDatepicker(values, neuron_field_id) {
    var startDate = values[0];
    var endDate = values[values.length-1];

    $("#datepicker-values-" + neuron_field_id).daterangepicker({
        "showDropdowns": true,
        ranges: {
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment()],
            'This Year': [moment().startOf('year'), moment().endOf('year')],
            'Last Year': [moment().subtract(1, 'year').startOf('year'), moment()],
            'Last 2 Years': [moment().subtract(2, 'year').startOf('year'), moment()],
            'Last 5 Years': [moment().subtract(5, 'year').startOf('year'), moment()],
            'Last 10 Years': [moment().subtract(10, 'year').startOf('year'), moment()],
            'From the beginning': [startDate, moment()],
        },
        locale: {
            format: 'YYYY-MM-DD'
        },
        "linkedCalendars": true,
        "showCustomRangeLabel": true,
        "startDate": startDate,
        "endDate": moment(),
        "opens": "center",
        "minDate": startDate,
        "maxDate": moment(),
    }, function(start, end, label) {
        //console.log("New date range selected: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        $("#datepicker-range-value-" + neuron_field_id).html(label);
    });

}


function check_min_max_fields() {
    var min_weight_field = $("li#min_weight");
    var min_weight_content = $("div #content-min_weight");
    var max_weight_field = $("li#max_weight");
    var max_weight_label = $("#label-li-max_weight");
    var max_weight_content = $("div #content-max_weight");

    var min_age_field = $("li#min_age");
    var min_age_content = $("div #content-min_age");
    var max_age_field = $("li#max_age");
    var max_age_label = $("#label-li-max_age");
    var max_age_content = $("div #content-max_age");


    if (min_weight_field.length && max_weight_field.length) {
        min_weight_field.remove();
        min_weight_content.remove();
        max_weight_field.attr("name", "weight");
        max_weight_field.val("weight");
        max_weight_label.html("Weight");
    }

    if (min_age_field.length && max_age_field.length) {
        min_age_field.remove();
        min_age_content.remove();
        max_age_field.attr("name", "age");
        max_age_field.val("age");
        max_age_label.html("Age");
    }


}

//---Get csrftoken (to prevent those attacks)-------
// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}