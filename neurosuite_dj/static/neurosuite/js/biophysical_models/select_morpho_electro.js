$(document).ready(function () {
    var csrftoken = $("#csrftoken").val();

    var query_values_json = [];
    var custom_options_json = [];

    $("body").keypress(function (e) {
        if (e.keyCode == '13') {
            e.preventDefault();
        }
    });

    $(document).on('click', '.items-search-panel .list-group-item.clickable', function (e) {
        var $this = $(this);
        var neuron_field_id = $this.attr("id");
        var custom_option = $this.attr("custom_option");
        var submenu = $this.next('.list-group-submenu');
        if (!$this.hasClass('collapsed')) {
            //Open

            submenu.slideDown();
            $this.addClass("collapsed");
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');

            if (!$this.hasClass('data-loaded')) {
                var data = {
                    "custom_option": custom_option
                };
                $.ajax({
                    type: "GET",
                    url: "/morpho/get_values_neuron_field/" + neuron_field_id,
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        $("#loading-" + neuron_field_id).show();
                    },
                    success: function (data) {
                        $this.addClass("data-loaded");
                        createInputElement(data);
                    },
                    error: function () {
                        // failed request; give feedback to user
                        $("#content-" + neuron_field_id).append("Error loading values")
                    }
                });

            }


        } else {
            //Close
            submenu.slideUp();
            $this.removeClass("collapsed");
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');

        }
    });

    $(".continue-button").click(function (e) {
        var form = $("#form-load-morpho-electro-allen");

        var query_values_json_send = {};
        $("#morpho-electro-allen-query-table tbody tr").each(function(i, row) {
            i = i+1;
            var neuron_id = $(this).attr("neuron-id");
            var filter_element = query_values_json.find(function(row) {
                return row["id"] == neuron_id;
            });
            query_values_json_send[neuron_id] = filter_element["values"]
        });


        var custom_options_json_send = {};
        $("#morpho-electro-allen-query-custom-options-table tbody tr").each(function(i, row) {
            i = i+1;
            var custom_option_id = $(this).attr("neuron-id");
            var custom_option = custom_options_json.find(function(row) {
                return row["id"] == custom_option_id;
            });
            custom_options_json_send[custom_option_id] = custom_option["values"]
        });

        query_values_json = []; //Reset the array
        custom_options_json = []; //Reset the array
        query_values_json_send = JSON.stringify(query_values_json_send);
        custom_options_json_send = JSON.stringify(custom_options_json_send);
        $("#query-values-json").val(query_values_json_send);
        $("#custom-options-json").val(custom_options_json_send);

        //To debug the form values:
        //var values_form = form.serializeArray();
        //console.log("Form", values_form);

        form.submit();
    });


});