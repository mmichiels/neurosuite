$(document).ready(function () {
    var csrftoken = $("#csrftoken").val();


    $("#select-model-type").on("change", function (e) {
        var model_chosen = $(this).val();
        var placeholder_model_paremeters = $("#model-parameters-" + model_chosen);

        $(".model-parameters").hide(); //Close old methods parameters input
        placeholder_model_paremeters.removeClass("hide-element").show();

        $("#section-continue").removeClass("hide-element").show();
        $("#section-select-parameters-optimization").removeClass("last-section-page");
    });


    $(".continue-button").click(function (e) {
        var model_type_chosen = $("#select-model-type").val();
        if (model_type_chosen == "no_model") {
            window.location.href = "/biophys/ml_create_model/"
        } else {
            var form = $("#form-create-biophys-model");

            var model_parameters_json = {}; //Reset the array
            model_parameters_json = JSON.stringify(model_parameters_json);
            $("#model-parameters-json").val(model_parameters_json);
        }
    });
});