#!/bin/bash
#This script compress lossless all JPEG, PNG and GIF images in the project

WORKDIR=$(pwd)

function run_lossless_compression() {
    cd $WORKDIR

    #zopflipng
    cd /
    git clone https://github.com/google/zopfli.git
    cd zopfli
    make zopflipng

    cd $WORKDIR

    EXTENSION_OUTPUT_PNG='COMPRESSED_ZOPFLIPNG'
    find ./ -name "*.png" | while read fname; do
      printf "${GREEN}Compressing zopflipng lossless PNG: ${NC} ${fname}\n"
      /zopfli/zopflipng $fname $fname.$EXTENSION_OUTPUT_PNG
      if [ -f $fname.$EXTENSION_OUTPUT_PNG ]; then
        rm $fname
        mv $fname.$EXTENSION_OUTPUT_PNG $fname
      fi
    done
}

run_lossless_compression
