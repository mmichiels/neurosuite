System.set('babylonjs', System.newModule(BABYLON));
console.log("Loading system.config.js");

System.config({
  transpiler: 'typescript',
  defaultJSExtensions: true,
  paths: {
    'nv:' : '/static/neuroviewer/example/dist/scripts/neuroviewer/'
  },
  map: {
    '@neuroviewer/core': "nv:core/",
    '@neuroviewer/reader': "nv:reader/",
    '@neuroviewer/babylon-drawer': "nv:babylon-drawer/",
    '@neuroviewer/control': "nv:control/"
  },
  packages: {
   '@neuroviewer/core': {
      "defaultExtension": "js",
      "main": "index.js"
    },
    '@neuroviewer/reader': {
      "defaultExtension": "js",
      "main": "index.js"
    },
    '@neuroviewer/babylon-drawer': {
      "defaultExtension": "js",
      "main": "index.js"
    },
    '@neuroviewer/control': {
      "defaultExtension": "js",
      "main": "index.js"
    }
  }
});

console.log("system.config.js loaded");
