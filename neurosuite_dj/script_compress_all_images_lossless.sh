#!/bin/bash
#This script compress lossless all JPEG, PNG and GIF images in the project

WORKDIR=/neurosuite/neurosuite_dj

function run_lossless_compression() {
    cd $WORKDIR

    #mozjpeg:
    gdebi --n mozjpeg_3.1_amd64.deb
    rm -f mozjpeg_3.1_amd64.deb
    ln -s /opt/mozjpeg/bin/jpegtran /usr/bin/mozjpegtran
    ln -s /opt/mozjpeg/bin/cjpeg  /usr/bin/mozcjpeg
    ln -s /opt/mozjpeg/bin/djpeg /usr/bin/mozdjpeg

    RED='\033[0;31m'
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color
    find ./ -name "*.jpg" | while read fname; do
      printf "${GREEN}Compressing mozjpeg lossless JPEG: ${NC} ${fname}\n"
      mozjpegtran -copy none -optimize $fname > $fname._COMPRESSED_MOZJPEG
      if [ -f ${fname}._COMPRESSED_MOZJPEG ]; then
        rm $fname
        mv $fname._COMPRESSED_MOZJPEG $fname
      fi
    done

    #zopflipng
    cd /
    git clone https://github.com/google/zopfli.git
    cd zopfli
    make zopflipng
    cd $WORKDIR

    EXTENSION_OUTPUT_PNG='COMPRESSED_ZOPFLIPNG'
    find ./ -name "*.png" | while read fname; do
      printf "${GREEN}Compressing zopflipng lossless PNG: ${NC} ${fname}\n"
      /zopfli/zopflipng $fname $fname.$EXTENSION_OUTPUT_PNG
      if [ -f $fname.$EXTENSION_OUTPUT_PNG ]; then
        rm $fname
        mv $fname.$EXTENSION_OUTPUT_PNG $fname
      fi
    done
}

run_lossless_compression

