"""
WSGI config for neurosuite_dj project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "neurosuite_dj.settings.dev")


#-----Django startup code--------------
try:
    msg_no_superuser_created = "No django superuser created. To create a superuser add django_admin_user.key, django_admin_email.key',django_admin_password.key to ./credentials_secrets/base_py_settings/"
    from django.conf import settings

    if not settings.DJANGO_SUPERUSER_CREATED:
        import django
        django.setup()
        if settings.DJANGO_ADMIN_USER != "" and settings.DJANGO_ADMIN_EMAIL != "" and settings.DJANGO_ADMIN_PASSWORD != "":
            from django.contrib.auth.models import User
            User.objects.filter(email=settings.DJANGO_ADMIN_EMAIL).delete()
            User.objects.create_superuser(settings.DJANGO_ADMIN_USER, settings.DJANGO_ADMIN_EMAIL, settings.DJANGO_ADMIN_PASSWORD)
            print ("Django superuser created. If possible, avoid creating a superuser in production. To avoid creating a superuser, remove django_admin_user.key, django_admin_email.key',django_admin_password.key from ./credentials_secrets/base_py_settings/ ")
            settings.DJANGO_SUPERUSER_CREATED = True
        else:
            print (msg_no_superuser_created)
    else:
        print ("Django superuser already created")
except Exception as e:
    print (msg_no_superuser_created)
#⨪-------------------------------------



application = get_wsgi_application()
