from __future__ import absolute_import, unicode_literals
import os
from datetime import timedelta
import django
from celery import Celery
from django.conf import settings
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'neurosuite_dj.settings.dev')

celery_app = Celery('neurosuite_dj',  backend='rpc')
celery_app.config_from_object('django.conf:settings', namespace='CELERY')
celery_app.autodiscover_tasks()

