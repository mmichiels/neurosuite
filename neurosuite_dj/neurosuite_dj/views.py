from django.shortcuts import render
from django.core.mail import send_mail
from django.conf import settings
from django.http import JsonResponse
from django.dispatch import receiver
import django_tus.signals

def error_404(request):
    data = {}
    data["url_error"] = request.build_absolute_uri(request.path)
    return render(request, 'error_404.html', data)


def error_500(request):
    data = {}
    return render(request, 'error_500.html', data)

@receiver(django_tus.signals.tus_upload_finished_signal)
def upload_finished(sender, **kwargs):
    request = kwargs["request"]
    request.session["tus_upload_filename"] = kwargs["filename"]
    request.session.save()
    request.session.modified = True

    return 0