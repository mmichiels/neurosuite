import os
import csv
from django.conf import settings
from django.core.files.temp import NamedTemporaryFile
import pandas as pd
import numpy as np
import dask.dataframe as dask_df
import dask.multiprocessing
import secrets

class Dataset():
    def __init__(self, name, session_id="", dataframe=None, label_column_id="", label_instances="", has_input_dataset=False, extension=".parquet.gzip", app_name="", instances_error=[], delete=False):
        self.name = name
        self.session_id = session_id
        self.dataframe = dataframe
        self.label_column_id = label_column_id
        self.label_instances = label_instances
        self.has_input_dataset = has_input_dataset
        self.extension = extension
        self.app_name = app_name
        self.filename = self.name + self.extension
        self.instances_error = instances_error
        self.delete = delete
        if session_id is None:
            session_id = secrets.token_hex(16)
        if self.app_name != "":
            self.folder_path = os.path.join(settings.DATASETS_TMP_PATH, session_id)
            self.folder_path = os.path.join(self.folder_path, app_name)
        else:
            self.folder_path = os.path.join(settings.DATASETS_TMP_PATH, session_id)
        self.file_path = os.path.join(self.folder_path, self.filename)
        self.url = "{}/{}/{}/{}".format(settings.DATASETS_TMP_URL, self.session_id, self.app_name, self.filename)
        if dataframe is None:
            self.modified = False
            self.columns = None
        else:
            self.modified = True
            self.columns = dataframe.columns.values
            self.shape = dataframe.shape
        if not os.path.exists(self.folder_path):
            os.makedirs(self.folder_path)


    def save(self):
        self.dataframe.to_parquet(self.file_path, engine="fastparquet", compression="gzip")
        self.dataframe = None
        self.modified = False

    def load(self, dataframe, label_column_id="", label_instances="", instances_error=[]):
        self.dataframe = dataframe
        self.modified = True
        self.instances_error = instances_error

        self.columns = self.dataframe.columns.values
        self.shape = dataframe.shape

        self.label_column_id = get_proper_column_id(label_column_id, self.columns)
        self.label_instances = get_proper_label_instances(label_instances)

        return 0

    def update_and_save(self, dataframe_updated):
        self.dataframe = dataframe_updated
        self.save()

    def delete_dataset(self):
        self.dataframe = None
        os.remove(self.file_path)

    def get_dataframe(self, columns=None, include_ids=True):
        if columns is not None and include_ids:
            columns = [self.label_column_id] + columns

        if columns is None and not include_ids:
            try:
                columns = list(self.columns)
                columns.remove(self.label_column_id)
            except Exception as e:
                pass #The dataframe doesn't have the column id

        if self.dataframe is None:
            dataframe = pandas_read_dataset(self.file_path, columns)
        else:
            dataframe = self.dataframe

        return dataframe

def get_label_column_id_and_instances(dataframe, label_column_id, label_instances):
    columns = dataframe.columns.values

    if label_column_id == "__None__":
        label_column_id = "id"
        dataframe[label_column_id] = dataframe.index
        cols = dataframe.columns.tolist()
        cols = cols[-1:] + cols[:-1]
        dataframe = dataframe[cols]
    else:
        label_column_id = get_proper_column_id(label_column_id, columns)

    label_instances = get_proper_label_instances(label_instances)

    return dataframe, label_column_id, label_instances

def get_proper_column_id(label_column_id, columns):
    if label_column_id == "" or label_column_id not in columns:
        label_column_id = columns[0]

    return label_column_id

def get_proper_label_instances(label_instances=""):
    if label_instances == "":
        label_instances = "Instances"

    return label_instances

def pandas_read_dataset(path, columns=None):
    #path = os.path.join(settings.CHUNKED_UPLOAD_PATH, "clean_full_brain.csv")
    #path = os.path.join(settings.CHUNKED_UPLOAD_PATH, "clean_full_brain.parquet.gzip")
    #path = os.path.join(settings.CHUNKED_UPLOAD_PATH, "clean_full_brain.pickle")

    file_name = os.path.splitext(path)[0]
    file_extension = os.path.splitext(path)[1]
    if file_extension == ".gzip":
        file_extension = os.path.splitext(file_name)[1] + file_extension

    if file_extension == ".csv":
        pd_dataframe = pandas_read_csv(path, columns)
    elif file_extension == ".parquet.gzip":
        pd_dataframe = pandas_read_parquet(path, columns)
    else:
        raise Exception("File extension not supported")

    return pd_dataframe

def fix_last_character_encoding_tus(path):
    with open(path, 'rb+') as filehandle:
        filehandle.seek(-1, os.SEEK_END)
        filehandle.truncate()

    return 0

def pandas_read_csv(path, columns):
    pd_first_rows = pd.read_csv(path, nrows=10, na_filter=False, low_memory=False)
    guessed_dtypes = pd_first_rows.dtypes
    guessed_dtypes = guessed_dtypes.to_dict()

    try:
        pd_dataframe = pd.read_csv(path, usecols=columns, na_filter=False, dtype=guessed_dtypes, low_memory=False)
    except Exception as e:
        pd_dataframe = pd.read_csv(path, usecols=columns, low_memory=False)

    return pd_dataframe

def dask_read_csv(path, columns):
    #Dask:
    dask.config.set(scheduler='threads')
    dask_dataframe = dask_df.read_csv(path, usecols=columns, na_filter=False, dtype=np.float64, low_memory=False)
    pd_dataframe = dask_dataframe.compute()  # convert to pandas in parallel

    return pd_dataframe

def pandas_read_parquet(path, columns):
    pd_dataframe = pd.read_parquet(path, columns=columns, engine="fastparquet")

    return pd_dataframe


def export_dataframe(dataframe, session_key, app_folder, format_file):
    session_folder = os.path.join(settings.DATASETS_TMP_PATH, session_key)
    session_app_folder = os.path.join(session_folder, app_folder)
    path = os.path.join(session_app_folder, "exported_dataset_tmp.{}".format(format_file))

    if format_file == "csv":
        dataframe.to_csv(path)
    elif format_file == "parquet.gzip":
        dataframe.to_parquet(path, engine="fastparquet", compression="gzip")
    else:
        raise Exception("File format not supported")

    return path

def initialize_all_dataframes_data(session_key, datasets_user, dataframe_input_all):
    dataset_discrete_name = datasets_user.input_dataset_discrete_name
    dataset_continuous_name = datasets_user.input_dataset_continuous_name
    label_column_id = datasets_user.global_label_column_id

    dataset_discrete, dataset_continuous = initialize_datasets_discr_cont(session_key, datasets_user, dataframe_input_all, dataset_discrete_name, dataset_continuous_name, label_column_id)

    return 0

def initialize_datasets_discr_cont(session_key, datasets_user, dataframe_input_all, dataset_discrete_name, dataset_continuous_name, label_column_id):
    dataframe_discrete, dataframe_continuous = get_dataframe_discr_cont(dataframe_input_all, label_column_id)

    dataset_discrete = Dataset(dataset_discrete_name, session_key, dataframe_discrete, label_column_id, datasets_user.global_label_instances, app_name=datasets_user.app_name)
    dataset_continuous = Dataset(dataset_continuous_name, session_key, dataframe_continuous, label_column_id, datasets_user.global_label_instances, app_name=datasets_user.app_name)
    datasets_user.add([dataset_discrete, dataset_continuous])

    return dataset_discrete, dataset_continuous


def get_dataframe_discr_cont(dataframe, label_column_id):
    dataframe_ints = dataframe.loc[:, dataframe.columns != label_column_id].select_dtypes(include=['int']) #Select all int columns but label_column_id
    dataframe_ints = dataframe_ints.apply(discretize_ints, axis=0)
    dataframe.update(dataframe_ints)

    dataframe_discrete = dataframe.select_dtypes(include=['object', 'bool', 'category'])
    dataframe_discrete = dataframe_add_label_id(dataframe_discrete, dataframe, label_column_id)

    dataframe_continuous = dataframe.select_dtypes(include=[np.number])
    dataframe_continuous = dataframe_add_label_id(dataframe_continuous, dataframe, label_column_id)

    return dataframe_discrete, dataframe_continuous

def discretize_ints(col, **kwargs):
    num_categories = len(col.value_counts().values)
    if (num_categories < 10): #Threshold value for column to be considered continuous
        col = col.astype(str)

    return col


def dataframe_add_label_id(dataframe, dataframe_all, label_column_id):
    if label_column_id not in dataframe.columns:
        dataframe[label_column_id] = dataframe_all.loc[:, label_column_id].astype(str)
    columns_with_label_id = list(dataframe.columns.values)
    columns_with_label_id.remove(label_column_id)
    columns_with_label_id.insert(0, label_column_id)
    dataframe = dataframe[columns_with_label_id]

    return dataframe

def dataframe_get_type(dataframe_dtypes):
    is_number = np.vectorize(lambda x: np.issubdtype(x, np.number))
    if all(is_number(dataframe_dtypes)):
        data_type = "continuous"
    elif (not any(is_number(dataframe_dtypes))) and (any(dataframe_dtypes == "object") or any(dataframe_dtypes == "bool")):
        data_type = "discrete"
    else:
        data_type = "hybrid"

    return data_type

def get_dataframe_same_rows(dataframe_1, dataframe_2):
    indices_dataframe_1 = list(dataframe_1.index)

    try:
        dataframe_2_same_rows = dataframe_2.loc[indices_dataframe_1, :]
    except Exception as e:
        dataframe_2_same_rows = pd.DataFrame(columns=dataframe_2.columns.values)

    return dataframe_2_same_rows

def merge_session_dataframes(datasets_user, datasets_names=[], dataset_columns=None, include_ids=True):
    columns_get = None
    if dataset_columns is not None:
        columns_all = set(list(datasets_user.datasets[datasets_names[0]].columns))
        columns_get = list(set(dataset_columns) & columns_all)
        dataset_columns = list(set(dataset_columns) - set(columns_get)) #Remove columns already taken

    merged_dataframes = datasets_user.datasets[datasets_names[0]].get_dataframe(columns_get, include_ids)
    for i, dataset_name in enumerate(datasets_names):
        if i != 0:
            columns_get = None
            if dataset_columns is not None:
                columns_all = set(list(datasets_user.datasets[dataset_name].columns))
                columns_get = list(set(dataset_columns) & columns_all)
                dataset_columns = list(set(dataset_columns) - set(columns_get))  #Remove columns already taken

            dataframe = datasets_user.datasets[dataset_name].get_dataframe(columns_get, include_ids)
            cols_to_use = dataframe.columns.difference(merged_dataframes.columns)
            merged_dataframes = pd.merge(merged_dataframes, dataframe[cols_to_use], left_index=True, right_index=True,
                                         how='outer')

    return merged_dataframes

def merge_session_dataframes_columns(datasets_user, datasets_names=[], include_ids=False):
    merged_dataframes_columns = []
    for i, dataset_name in enumerate(datasets_names):
        dataframe_chosen = datasets_user.datasets[dataset_name].get_dataframe()
        dataframe_columns = list(dataframe_chosen.columns.values)
        in_first = set(merged_dataframes_columns)
        in_second = set(dataframe_columns)
        in_second_but_not_in_first = in_second - in_first

        merged_dataframes_columns = merged_dataframes_columns + list(in_second_but_not_in_first)

    merged_dataframes_columns.sort()

    if not include_ids:
        label_column_id = datasets_user.global_label_column_id
        if label_column_id in merged_dataframes_columns:
            merged_dataframes_columns.remove(label_column_id)

    return merged_dataframes_columns

def remove_label_column_id(dataset):
    columns = dataset.columns

    try:
        columns.remove(dataset.label_column_id)
    except Exception as e:
        pass #Dataframe doesn't have the column id

    return columns