import numpy as np
import math

def to_datatable_json(received_info, df, cols_special):
    num_rows = len(df)
    input_params = {}

    for param, val in received_info.items():
        if not process_param_from_array(input_params, param, val):
            input_params[param] = parse_input_param(param, val)

    # Filtering columns:
    cols_total = len(df.columns)
    col_id = df.columns[0]
    cols_names = [col_id]
    if "is_empty" in input_params and input_params["is_empty"]:
        pass
    else:
        if "cols_names" in input_params:
            cols_names += input_params["cols_names"]
        else:
            all_cols = list(df.columns)
            max_cols = min(len(all_cols), 150+1)
            cols_names = all_cols[:max_cols]

    cols_filtered = len(cols_names)
    df = df.loc[:, cols_names]

    cols_style = {}
    cols_style[col_id] = {
        "color": "#66ffff"
    }
    for col in cols_special:
        cols_style[col] = {
            "color": "#98e698"
        }

    #Get cols ranges (to filter in the frontend):
    mins = df.min().tolist()
    maxs = df.max().tolist()
    cols_ranges = {}
    cols_ranges_tuples = zip(mins, maxs)
    for i, (col_min, col_max) in enumerate(cols_ranges_tuples):
        if isinstance(col_min, np.generic):
            col_min = col_min.item()
        if isinstance(col_max, np.generic):
            col_max = col_max.item()

        try:
            col_min = round(float(col_min), 2)
            col_max = round(float(col_max), 2)
            is_numeric_range = True

            if (math.isnan(col_min) or math.isnan(col_max)) or col_min == col_max:
                is_numeric_range = False
        except Exception as e:
            is_numeric_range = False

        if is_numeric_range:
            cols_ranges[i] = {
                "min": col_min,
                "current_min": col_min,
                "current_max": col_max,
                "max": col_max
            }

    # Sorting:
    df = df_sorting(df, input_params["order"])
    df = df.round(2)

    # Filtering rows:
    for col_idx, col_info in input_params["columns"].items():
        col_idx = int(col_idx)
        val_search = col_info["value"]
        if len(val_search) > 0:
            min_val = val_search[0]
            max_val = val_search[1]
            if min_val > max_val:
                min_val = max_val
            df = df[df.iloc[:, col_idx].between(min_val, max_val)]
            if col_idx in cols_ranges:
                cols_ranges[col_idx]["current_min"] = round(min_val, 2)
                cols_ranges[col_idx]["current_max"] = round(max_val, 2)

    df.replace(np.nan, 'NaN', regex=True, inplace=True)
    search_global_val = input_params["search"]["value"]
    if len(search_global_val) > 0:
        df = df[df.applymap(lambda x: search_global_val in x if isinstance(x, str) else False).any(axis=1)]

    num_rows_filtered = df.shape[0]

    # Pagination:
    df = df_pagination(df, input_params["start"], input_params["length"])

    data_json = df.values.tolist()
    datatable_json = {
        "draw": input_params["draw"],
        "recordsTotal": num_rows,
        "recordsFiltered": num_rows_filtered,
        "data": data_json,
        "colsTotal": cols_total,
        "colsFiltered": cols_filtered,
        "colsNames": list(df.columns),
        "colsRanges": cols_ranges,
        "colsStyle": cols_style,
    }

    return datatable_json


def min_max_series(x):
    return pd.Series(index=['min','max'],data=[x.min(),x.max()])

def process_param_from_array(input_params, param, val):
    is_array_param = True

    if param == "cols_names[]":
        input_params["cols_names"] = val
        return True

    additional_params = [p.split(']')[0] for p in param.split('[') if ']' in p]

    if param.startswith("columns"):
        param = "columns"
        idx = additional_params.pop(0)
        # name = additional_params[1]
        # search_param = additional_params[1]
    elif param.startswith("order"):
        param = "order"
        idx = additional_params.pop(0)
    elif param.startswith("search"):
        param = "search"
        idx = additional_params.pop(0)
    else:
        return False

    if param not in input_params:
        input_params[param] = {}
    if idx not in input_params[param]:
        input_params[param][idx] = {}

    val = parse_string_val(val[0])

    if len(additional_params) > 0:
        for additional_param in additional_params:
            if param == "columns" and additional_param == "value" and len(val) > 0:
                val = [float(val_i) for val_i in val.split(',')]
            input_params[param][idx][additional_param] = val
    else:
        input_params[param][idx] = val

    return is_array_param

def parse_string_val(val):
    if val == "true" or val == "True":
        return True
    elif val == "false" or val == "False":
        return False

    return val


def parse_input_param(param, val):
    type = ""
    val = val[0]
    param_converted = ""

    datatable_params_types = {'draw': "integer",
                              'start': "integer",
                              'length': "integer",
                              'pageLength': "integer",
                              'column': "integer",
                              'dir': "string",
                              'data': "string",
                              'name': "string",
                              'searchable': "bool",
                              'orderable': "bool",
                              'value': "string",
                              'regex': "bool",
                              'csrfmiddlewaretoken': ['X20yEHS41pqecEN7zzSMGOKmo54ZWq6AtIFH54rfLqx0MFFDQYanzJYsy7GMTucK'],
                              }
    try:
        type = datatable_params_types[param]
    except KeyError as e:
        type = "string"
        param_converted = parse_string_val(val)
        return param_converted

    if type == "integer":
        param_converted = int(val)
    elif type == "bool":
        param_converted = bool(val)
    elif type == "string":
        param_converted = str(val)

    return param_converted

def df_pagination(df, start, length):
    df_page = df.iloc[start:start+length, :].copy()

    return df_page

def df_sorting(df, order_info):
    for order_idx, order_cmd in order_info.items():
        col_idx = int(order_cmd["column"])
        col_name = df.columns[col_idx]
        ascending = False
        if order_cmd["dir"] == "asc":
            ascending = True

        df.sort_values(by=col_name, axis=0, ascending=ascending, inplace=True)

    return df