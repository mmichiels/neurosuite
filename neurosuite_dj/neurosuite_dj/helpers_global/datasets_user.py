from django.conf import settings
import os
import shutil
import pandas as pd

class DatasetsUser():
    def __init__(self, app_name="", datasets={}, input_dataset_name="input_data", has_input_dataset=True, has_morpho_reconstructions=False):
        self.datasets = datasets
        self.input_dataset_name = "input_data"
        self.input_dataset_discrete_name = self.input_dataset_name + "_discrete"
        self.input_dataset_continuous_name = self.input_dataset_name + "_continuous"
        self.app_name = app_name
        self.name = "datasets_user_" + app_name
        self.global_label_column_id = ""
        self.global_label_instances = ""
        self.has_input_dataset = has_input_dataset
        self.has_morpho_reconstructions = has_morpho_reconstructions
        self.modified = True

    def get_input_dataframe(self):
        dataframe_discrete = self.datasets[self.input_dataset_discrete_name].get_dataframe()
        dataframe_continuous = self.datasets[self.input_dataset_continuous_name].get_dataframe()

        cols_to_use = dataframe_continuous.columns.difference(dataframe_discrete.columns)
        merged_dataframes = pd.merge(dataframe_discrete, dataframe_continuous[cols_to_use], left_index=True, right_index=True,
                                     how='outer')

        return merged_dataframes

    def save(self, session):
        if not self.name in session:
            self.modified = True

        for key, elem in self.datasets.items():
            if elem.modified:
                self.datasets[key].save()
                self.modified = True
            self.datasets[key].dataframe = None

        if self.modified:
            session[self.name] = self
            self.modified = False

            session.save()
            session.modified = True

    def clean_all_memory_and_modified(self, session):
        for key, elem in self.datasets.items():
            self.datasets[key].modified = False
            self.datasets[key].dataframe = None

        self.save(session)

    def clean_old_datasets(self, session):
        for key, elem in list(self.datasets.items()):
            if elem.delete:
                self.modified = True #This will allow us to save automatically the datasets_user object in the session
                self.datasets[key].delete_dataset()
                del self.datasets[key]

        self.save(session)

    def add(self, datasets):
        for dataset in datasets:
            self.datasets[dataset.name] = dataset
            dataset.modified = True

        return 0

def clean_datasets_folder(session_key):
    path_datasets_session = os.path.join(settings.DATASETS_TMP_PATH, session_key)

    try:
        shutil.rmtree(path_datasets_session)
        print("Delete session: datasets deleted from disk")
    except Exception as e:
        print("Delete session: no datasets available to delete")
        return 1

    return 0