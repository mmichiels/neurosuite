import pandas as pd
import os
from django.conf import settings
import warnings
from functools import wraps
from dateutil.parser import parse
from celery import shared_task, current_task

def update_progress_worker(current_task, percent, estimated_time_finish=""):
    if current_task:
        process_percent = int(percent)
        if not isinstance(estimated_time_finish, str):
            estimated_time_finish = round((estimated_time_finish), 2)
        elif estimated_time_finish == "":
            estimated_time_finish = "unknown"
        current_task.update_state(state='PROGRESS', meta={'process_percent': process_percent,
                                                          'estimated_time_finish': estimated_time_finish})