"""neurosuite_dj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import os
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from django.conf.urls import handler404, handler500
import neurosuite_dj.views as global_views
from django_tus.views import TusUpload

#-----Pydev (Python debugger client) to debug from Pycharm on host machine---
import sys
sys.path.append('pycharm-debug-py3k.egg')  # replace by pycharm-debug.egg for Python 2.7
import pydevd
# the following line can be copied from "Run/Debug Configurations" dialog
if settings.DEBUG:
    pydevd.settrace(settings.HOST_MACHINE_IP, port=settings.DEBUG_PORT, stdoutToServer=True, stderrToServer=True)
#------------------------------------------------------------------------------


#==========================Django rest framework ========================
# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        #fields = ('url', 'username', 'email', 'is_staff')
        fields = ('url', 'username', 'is_staff')

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    url(r'^manage/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^global/', include('apps.global_app.urls')),
    url(r'^morpho/', include('apps.morpho_analyzer.urls')),
    url(r'^biophys/', include('apps.biophysical_models.urls')),
    url(r'^micro/', include('apps.microscopic_images.urls')),

    url(r'^tus_upload/$', TusUpload.as_view(), name='tus_upload'),
    url(r'^tus_upload/(?P<resource_id>[0-9a-z-]+)$', TusUpload.as_view(), name='tus_upload_chunks'),

    url(r'^', include('cms.urls')),
]


handler404 = global_views.error_404
handler500 = global_views.error_500



if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
                      url(r'^__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns
