import pandas as pd
import os
from django.conf import settings
import warnings
from functools import wraps
from dateutil.parser import parse
from chunked_upload.exceptions import ChunkedUploadError
from celery import shared_task, current_task
import zipfile
import random


def ignore_warnings(f):
    @wraps(f)
    def inner(*args, **kwargs):
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("ignore")
            response = f(*args, **kwargs)
        return response
    return inner

def pandas_dataframe_to_lists(dataframe=None, remove_columns=[], append_columns=[], index_column=True):
    result_columns = []
    result_list = []
    if dataframe is not None:
        dataframe = dataframe.drop(remove_columns, axis=1)

        result_columns = dataframe.columns.get_values().tolist()
        result_columns.extend(append_columns)
        if index_column:
            result_columns.insert(0, "Id")
            result_list = dataframe.reset_index().values.tolist()
        else:
            result_list = dataframe.values.tolist()

    return result_columns, result_list


def is_free_disk_space(new_upload_bytes=None):
    statvfs = os.statvfs('/')
    free_available_disk_space = statvfs.f_frsize * statvfs.f_bavail  # Number of free bytes that ordinary users are allowed to use (excl. reserved space)
    if new_upload_bytes is not None:
        free_available_disk_space -= new_upload_bytes
    if (free_available_disk_space < settings.LIMIT_FREE_DISK_SPACE):
        return False
    else:
        return True

def validate_disk_space_chunked_upload(new_upload_bytes=None):
    statvfs = os.statvfs('/')
    free_available_disk_space = statvfs.f_frsize * statvfs.f_bavail  # Number of free bytes that ordinary users are allowed to use (excl. reserved space)
    if new_upload_bytes is not None:
        free_available_disk_space -= new_upload_bytes
    if (free_available_disk_space < settings.LIMIT_FREE_DISK_SPACE):
        raise ChunkedUploadError(status=400, detail='Low space on the server disk')

def create_chunked_upload_folder():
    #Create CHUNKED_UPLOAD_PATH if not exists:
    if not os.path.exists(settings.CHUNKED_UPLOAD_PATH):
        os.makedirs(settings.CHUNKED_UPLOAD_PATH)

def get_type_values(values):
    type = "text" #Default value

    is_number = True
    for item in values:
        if item is not None:
            if (not isinstance(item,dict) and not is_numeric(item)) or isinstance(item,dict):
                is_number = False
                break

    if is_number:
        type = "numeric"
    else:
        is_date = True
        for item in values:
            if item is not None:
                if (not isinstance(item,dict) and not string_is_date(item)) or isinstance(item,dict):
                    is_date = False
                    break

        if is_date:
            type = "date"

    return type

def string_is_date(string):
    try:
        parse(string)
        return True
    except ValueError:
        return False

def is_numeric(string):
    if string_is_float(string) or string_is_integer(string):
        return True
    else:
        return False

def isdigit(string):
    if string.isdigit() or string.isnumeric():
        return True
    else:
        return False

def string_is_float(string):
    try:
        float(string)
        return True
    except:
        return False

def string_is_integer(string):
    try:
        int(string)
        return True
    except:
        return False

def verbosize_string(input_string):
    verbose_string = input_string[:1].upper() + input_string[1:]  # Capitalize the 1st character
    verbose_string = verbose_string.replace("_", " ")

    return verbose_string

def zip_inmemoryfiles_iterator(zip_file):
    with zipfile.ZipFile(zip_file, "r", zipfile.ZIP_STORED) as openzip:
        filelist = openzip.infolist()
        for f in filelist:
            yield (f.filename, openzip.read(f))

def zip_filepath_iterator(zip_filepath):
    with zipfile.ZipFile(zip_filepath, "r") as openzip:
        filelist = openzip.infolist()
        for f in filelist:
            yield (f.filename, openzip.read(f))

def generate_random_color():
    random_color = lambda: random.randint(0, 255)
    random_color_hex = ('#%02X%02X%02X' % (random_color(), random_color(), random_color()))
    return random_color_hex