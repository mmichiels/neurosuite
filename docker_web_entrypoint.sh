#!/bin/bash
source ~/.bashrc
conda activate conda_py364

readonly HOST='db'
readonly PORT=5432

rstudio-server stop
rstudio-server start

until nc -z $HOST $PORT; do
    echo "$(date) - waiting for postgres..."
    sleep 1
done
echo "$(date) - Postgres is UP!"


readonly WORKDIR=/neurosuite/neurosuite_dj

$WORKDIR/fix_aldryn_bootstrap3_docker.sh
echo "Migrating...(If DEBUG=True -> check debugger has started and is working)"
python $WORKDIR/manage.py makemigrations        # Create new database migrations
python $WORKDIR/manage.py migrate        # Apply database migrations
echo "Collecting static"
python $WORKDIR/manage.py collectstatic --noinput  # collect static files
echo "Creating django_cache_table in db"
python $WORKDIR/manage.py createcachetable django_cache_table

#configure_rstudio_server
rstudio-server stop
rstudio-server start
userdel rstudio
adduser --disabled-password --gecos "" rstudio
echo rstudio:rstudio | chpasswd

Xvfb :7 -screen 0 1280x1024x24 &

#Run shiny-server
/usr/bin/shiny-server > /dev/null 2>&1 &

#Install rpy2. #We install this in the docker_web_entrypoint.sh because it needs to be installed after the installation of python and R
pip install rpy2==2.9.2

#Fix django-tus url with no port:
cp $WORKDIR/neurosuite_dj/helpers_global/django_tus_views.py /opt/conda/envs/conda_py364/lib/python3.6/site-packages/django_tus/views.py


#$WORKDIR/script_compress_images.sh #Uncomment when you need to compress JPG, PNG and GIF files

uwsgi --ini $WORKDIR/neurosuite_uwsgi_docker.ini