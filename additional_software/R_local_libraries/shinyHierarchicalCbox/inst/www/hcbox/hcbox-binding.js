
  // Create a generic output binding instance
  var hcboxBinding = new Shiny.InputBinding();

  // Our output el class is : shinyD3Bn
  var HCBOX_CONT_CLASS = ".shiny-hcbox-container";
  var HCBOX_CLASS = ".shiny-hcbox";

  // Finding method: Using jQuery find method
  hcboxBinding.find = function(scope) {
    return $(scope).find(HCBOX_CLASS);
  };
  
  hcboxBinding.getValue = function(el) {
    
    var $el = $(el);
    
    // Get jstree
    var jstree =  $el.jstree();
    
    // Get data variable
    var treedata = window[ $el.attr('id') + "_data"] ;
    
    // Get functions are different if checkbox plugin is loaded
    if(treedata.plugins.indexOf("checkbox") > -1 ){
      if(treedata.extra.skipTopLevels)
        return( jstree.get_bottom_checked() );
      else
        return( jstree.get_checked() );
    }
    else{
      if(treedata.extra.skipTopLevels)
        return( jstree.get_bottom_selected() );
      else
        return( jstree.get_selected() );
    }
  };
  
  // Suscribe function: Calls callback function when a change occurs
  // We call the callback function whenever a node is (un)checked
  // debounding policy rate applies
  hcboxBinding.subscribe = function(el, callback) {
    
    // Get element
    var $el = $(el);
    
    // save callback
    $el.data("callback",callback);
    
    // Subscribe on change.jstree event.
    // NOTE: jstree docs says that there is a check_node event, but i can make it work.
    $el.on("changed.jstree", function (e, data) {
      callback(true); // TRUE: rate policy applies.
    });
    
  };
  
  // Unsuscribe, pretty strightforward
  hcboxBinding.unsubscribe = function(el) {
    
    var $el =  $(el);
    
    $el.off('.jstree');
    
    // Get jstree
    var jstree =  $el.jstree();
    
    // Destroy it
    jstree.destroy();
    
  };
  
  /* Auxiliar function: copies values into jstree */
  updateObject = function( prevObj, newObj ){
    // If newobj is null, keep previous value
    if( newObj === null || (typeof newObj == 'undefined') ){
      return prevObj;
    }
    // If it is not an object, return the new value (or the key value doenst exist in the previous val)
    else if ( (typeof newObj != 'object') || ( typeof prevObj == 'undefined' ) 
             || ( $.isArray(prevObj) ) || ( prevObj === null )  ){
      return newObj;
    }
    else{ 
      
      // Update each key in newObj
      for( var key in newObj ){
        prevObj[key] = updateObject( prevObj[key], newObj[key] );
      }
      
      return(prevObj);
    }
  };
  
  // Function to call when a message from the server
  // Actually this function is called from updateHCBox()
  hcboxBinding.receiveMessage = function(el, data) {
  
    // Get container
    var $el =  $(el);
    
    // Get jstree
    var jstree =  $el.jstree(true);
    
    // Get data variable
    var treedata = window[ $el.attr('id') + "_data"] ;
    
    if(data.action == 'rebuild'){
      // Remove previous
      jstree.destroy();
      
      // Unsubsbcribe
      $el.off('.jstree');
      
      // Update jstree
      window[ $el.attr('id') + "_data"] = updateObject( treedata, data.values );
      
      // Rebuild tree
      jstree = $el.jstree( window[ $el.attr('id') + "_data"] );
      
      /// Subscribe again
      var callback = $el.data("callback");
      $el.on("changed.jstree", function (e, data) {
        callback(true); // TRUE: rate policy applies.
      });
    }
    else if (data.action == 'update'){
      
      // change theme
      if( data.values.name !== null ) jstree.set_theme( data.values.name );
      // change icons
      if( data.values.icons !== null ){
        if( jstree.values.icons ) jstree.show_icons();
        else tree.hide_icons();
      }
      // change stripes
      if( data.values.stripes !== null ){
        if( jstree.values.stripes) jstree.show_stripes();
        else jstree.hide_stripes();
      }
    }
  };
  
  // Get current state (data)
  hcboxBinding.getState = function(el){
    $(el).jstree(true).get_json();
  };
  
  // Initializer function that creates the jstree
  hcboxBinding.initialize = function(el) {
    
      var $el = $(el);
      var data = window[ $el.attr('id') + "_data"] ;
      
      // Create tree
      var tree = $el.jstree( data );
  };
  
  // Debounce rate policy: wait 250 ms wo updates to trigger the call
  hcboxBinding.getRatePolicy = function() {
    return {
      policy: 'debounce',
      delay: 250
    };
  };

// Register our new binding
Shiny.inputBindings.register(hcboxBinding, "shiny.hcbox");


