
createMenu = function(nodeData, menuEls){
  
  var menu = [];
  
  if(menuEls){
    menuEls.forEach (function(x){
      // Create menu item
      menu.push( { title: x.title, action: function(el,d,i){ ($(el)).trigger("change", {actionType : x.actionType, nodeId : d.id } )} });
    });
  }
  return (menu);
};
