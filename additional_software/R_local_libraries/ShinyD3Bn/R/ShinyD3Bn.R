#' Adds the content of www to BnD3
#' 
#' @importFrom shiny addResourcePath
#' 
#' @noRd
.onAttach <- function(...) {
  addResourcePath('BnD3', system.file('www', package='ShinyD3Bn'))
}

#' Builds a BnD3 input/output in the ui
#'
#' To be called from ui.R. Creates whatever its necessary to create
#' a D3 Bn input/output in the user interface (web)
#'
#'@export
BnD3IO <- function( inputId, width = "100%" ,height = "500px"){
  
  # Convert units and validate. Create style string
  style <- sprintf("width: %s; height: %s;",
    validateCssUnit(width), validateCssUnit(height))

  tagList(
    # Include CSS/JS dependencies. Use "singleton" to make sure that even
    # if multiple lineChartOutputs are used in the same page, we'll still
    # only include these chunks once.
    singleton(tags$head(
      # D3
      tags$script(src="BnD3/d3/d3.min.js"),
      tags$script(src="BnD3/d3/d3.tip.v0.6.3.js"),
      # Jquery
      # tags$script(src="BnD3/jquery/jquery-2.1.4.min.js"),
      # d3-context-menu
      tags$script(src="BnD3/d3-context-menu/js/d3-context-menu.js"),
      tags$link(rel="stylesheet", type="text/css", href="BnD3/d3-context-menu/css/d3-context-menu.css"),
      # D3Bn Binding
      tags$script(src="BnD3/D3Bn/D3Bn_aux_functions.js"),
      tags$link(rel="stylesheet", type="text/css", href="BnD3/css/BnD3.css"),
      tags$link(rel="stylesheet", type="text/css", href="BnD3/css/d3tip.css"),
      tags$script(src="BnD3/D3Bn/D3Bn-binding.js"),
      tags$script(src="BnD3/D3Bn/d3Bn-contextMenu.js")
      
    )),
    tagList(
      
      # The actual Div tag that will host our bn
      div(id=inputId, class="shinyD3Bn", style=style),
      
      # bsModal to show up stuff (no title, no "link" nor button triggered)
      # Create div with class d3BnModal inside
      bsModal(id = paste0(inputId,"__modal"),NULL,NULL,
              div(id=paste0(inputId,"__modal_content"),class="d3BnModal"),
              size="large")
    )
  )
}

#' Renders a BN in a BnD3 input/output given
#'
#' To be called from server.R (Updates the input object)
#'
#'@export
renderBnD3 <- function(session, inputId , d3Bn, 
                       # SVG params (can be different from div)
                       svg.width = "500",
                       svg.height = "500",
                       svg.others = list(),
                       
                       # Layout Params
                       layout=c("force","hierachical"),
                       force.charge       = -1000,
                       force.linkdist     = 150,
                       force.avoidOverlap = TRUE,
                       layout.others = list(),
                       
                       # Node Params
                       node.color = c("group","class","type"),
                       node.radius = 30,
                       node.padding = 30,
                       node.showTooltip = T,
                       node.subgraph = NA,
                       node.others = list(),
                       
                       # Arc Params
                       arc.width = 2,
                       arc.others = list(),
                       
                       #menu
                       menu.options = list(
                         list( title = "Expand node", actionType = "expand"),
                         list( title = "Show details", actionType = "details"),
                         list( title = "Set evidence", actionType = "evidence")
                       )
                       
                       ){
  
  # Match options
  layout <- match.arg(layout)
  node.color <- match.arg(node.color)

  # Get list of nodes and arcs
  edgeNodeList <- d3Bn$getEdgeNodeLists()
  
  # Select only given nodes 
  if( !is.na( node.subgraph )){
    
    # selected nodes
    newnodes <- lapply(edgeNodeList$nodes, function(x,subg){
      if(x$name %in% subg) return (x)
      else
        return(NULL)
    }, node.subgraph)
    
    # Remove null
    if(length(newnodes) > 0 )
      edgeNodeList$nodes <- newnodes[ sapply(newnodes, function(x) length(x) > 0)]
    else
      edgeNodeList$nodes <- list()
    
    # Re-set Ids
    previousPosition <- sapply( edgeNodeList$nodes, function(x)x$id  )
    
    # Repeat for edges
    selids <- sapply(edgeNodeList$nodes, function(x) x$id)
    
    newedges <- lapply(edgeNodeList$arcs, function(x,subg,ids){
      if( (x$source %in% subg) && (x$target %in% subg) ){
        # Re-set IDs
        x$source <- which.max(ids == x$source )
        x$target <- which.max(ids == x$target )
        return (x)
      }
      else
        return(NULL)
    }, selids,previousPosition)
    
    # Remove null
    if(length(newedges) > 0 )
      edgeNodeList$arcs <- newedges[ sapply(newedges, function(x) length(x) > 0)]
    else
      edgeNodeList$arcs <- list()
  }
  
  # Fix arc indexes
  edgeNodeList$arcs <- lapply(edgeNodeList$arcs,function(x){ x$target = x$target-1;  x$source = x$source-1; return(x)})
  
    
  # Output options
  params <- list()
  # SVG
  params$svg = c( list( width = svg.width,
                      height = svg.height),
                svg.others)
  
  #Layout
  params$layout = c(list(layout=layout,
                        charge = force.charge,
                        linkdist = force.linkdist,
                        avoidOverlap = force.avoidOverlap),
                  layout.others)
  # Node
  params$node = c( list( color = node.color,
                       radius = node.radius,
                       padding = node.padding,
                       tooltip = node.showTooltip),
                 node.others)
  
  # Menu
  params$menu = menu.options
  
  # Arc
  params$arc = c( list( width = arc.width ), arc.others)
                      
  # Retrun value for JSON
  session$sendInputMessage(inputId,c(edgeNodeList, params = list(params) ))
}

# Process input from bn and returns its value
#
# TO BE DEFINED
registerInputHandler("shiny.BnD3", function(data, ...) {
  return(data)
}, force = TRUE)