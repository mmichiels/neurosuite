#!/bin/bash

readonly HOST_DB='db'
readonly PORT_DB=5432

echo "Backups container started!"
echo "log postgresql backups in ./backups/pg_backups/log_pg_backups.log"
echo "log django backups in ./backups/pg_backups/log_pg_backups.log"


#--------Define cleanup procedure--------------
cleanup() {
    echo "Bye!"
    /neurosuite_backups/pg_backups/script_pg_backup_overwrite.sh
    /neurosuite_backups/dj_backups/script_dj_backup.sh
}
#Trap SIGTERM
trap 'cleanup' SIGTERM
trap 'cleanup' SIGQUIT

#rsyslogd #To log cron jobs
cron -f
