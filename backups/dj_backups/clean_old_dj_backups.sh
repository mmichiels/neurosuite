#!/bin/bash
LOGS_TO_DOCKER_LOG='/proc/1/fd/1' #For Docker

DATE_NOW=`date +%d_%m_%Y_%H_%M_%S`

mkdir -p /neurosuite_backups/dj_backups/dj_backups_files

exec &>> $LOGS_TO_DOCKER_LOG
echo "" &>> $LOGS_TO_DOCKER_LOG

#Examples:
#Remove files older than 10 days:
# -mtime +9
#Remove files older than 24 hours (1440 minutes):
# -mmin +1439

if [[ $(find '/neurosuite_backups/dj_backups/dj_backups_files' -mtime +9 -print) ]]; then
  echo "Cleaning old dj backups"
  find '/neurosuite_backups/dj_backups/dj_backups_files' -mtime +9 -type f -delete
  echo $DATE_NOW
  echo "Old dj_backups logs cleaned"
fi
