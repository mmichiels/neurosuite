#!/bin/bash
#LOG_DJ_BACKUPS='/neurosuite_backups/dj_backups/dj_backups_files/log_dj_backups.log'
LOGS_TO_DOCKER_LOG='/proc/1/fd/1' #For Docker
DATE_NOW=`date +%d_%m_%Y_%H_%M_%S`

mkdir -p /neurosuite_backups/dj_backups/dj_backups_files

exec &>> $LOGS_TO_DOCKER_LOG
echo "" &>> $LOGS_TO_DOCKER_LOG

echo $DATE_NOW >> $LOG_DJ_BACKUPS
tar -czpvf /neurosuite_backups/dj_backups/dj_backups_files/neurosuite_dj_backup_$DATE_NOW.tar.gz /neurosuite_dj/
echo "neurosuite_dj_backup automatically DONE" >> $LOG_DJ_BACKUPS
echo "" >> $LOG_DJ_BACKUPS
