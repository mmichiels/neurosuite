#!/bin/bash
LOG_PG_BACKUPS='./backups/pg_backups/pg_backups_files/log_pg_backups.log'
DATE_NOW=`date +%d_%m_%Y_%H_%M_%S`

mkdir -p ./backups/pg_backups/pg_backups_files

exec &>> $LOG_PG_BACKUPS
echo "" &>> $LOG_PG_BACKUPS

docker-compose exec db pg_dump neurosuite_db_psql > ./backups/pg_backups/pg_backups_files/neurosuite_db_psql_backup.sql
echo "neurosuite_db_psql_backup manually DONE at $DATE_NOW"
