#!/bin/bash
#LOG_PG_BACKUPS='/neurosuite_backups/pg_backups/pg_backups_files/log_pg_backups.log'
LOGS_TO_DOCKER_LOG='/proc/1/fd/1' #For Docker


DATE_NOW=`date +%d_%m_%Y_%H_%M_%S`

mkdir -p /neurosuite_backups/pg_backups/pg_backups_files

exec &>> $LOGS_TO_DOCKER_LOG
echo "" &>> $LOGS_TO_DOCKER_LOG

echo $DATE_NOW
pg_dump neurosuite_db_psql -U postgres -h db > /neurosuite_backups/pg_backups/pg_backups_files/neurosuite_db_psql_backup_$DATE_NOW.sql
echo "neurosuite_db_psql_backup automatically DONE"
