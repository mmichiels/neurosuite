#!/bin/bash
echo "NeuroSuites startup DEVELOPMENT starts"
echo "IMPORTANT: If this is your first run: Rename neurosuite_dj/dummy_credentials_secrets/ to neurosuite_dj/credentials_secrets/* and fill the text in each file with your own users/passwords. Django_admin files are not neccesary if you don't want a Django superuser account to be created, so delete or rename them if you don't want a Django admin account to be created."
echo "If you are with DEBUG=True and you have just to do a migration, maybe you need to uncomment the line 10 of manage.py in order to continue the DEBUG breakpoint and not hangs up"
echo "If you need to generate a new TLS certificate for a new domain, please use the docker-compose.yml file and nginx.tmpl that end in '-new-tls-certificate'"

echo "Stopping and removing all containers..."
./new_docker_daemon.sh &
#docker -H unix:///root/altdocker.socket swarm leave --force
docker-compose -H unix:///root/altdocker.socket down --remove-orphans

#echo "Removing  all docker data containers..."
#Neccesary when the app is deployed in a different computer or different docker daemon
#docker -H unix:///root/altdocker.socket volume rm neurosuite_pg_data #Sometimes this fails and must be run manually (running the previous lines and the running this line in the terminal)
#docker -H unix:///root/altdocker.socket volume rm neurosuite_prometheus_data
#docker -H unix:///root/altdocker.socket volume rm neurosuite_logs_data
docker -H unix:///root/altdocker.socket volume rm neurosuite_nginx_proxy_conf
docker -H unix:///root/altdocker.socket volume rm neurosuite_nginx_proxy_vhost
docker -H unix:///root/altdocker.socket volume rm neurosuite_nginx_proxy_html


#Necessary when nginx, rabbitMQ, rstudio-server, postgresql and shiny-server are deployed in the host machine:
sudo /etc/init.d/nginx stop
systemctl stop rabbitmq-server
systemctl stop rstudio-server
service postgresql stop
service shiny-server stop

#Enable ufw (Ubuntu's firewall) to block all incoming traffic except the Docker traffic
echo "Enabling firewall: deny all incoming traffic except Docker traffic"
echo "Warning: ssh port will also be blocked. To change this behaviour, edit this startup.sh file commenting the ufw lines"
ufw enable
ufw default deny incoming
ufw allow 4444 #To debug Python code inside the Docker container
ufw allow ssh #The server needs ssh for another user tasks not related with Docker nor this software
ufw reload

#When dclient is configured:
#systemctl restart ddclient #commented to avoid assigning a DNS type A to our domain (now we only assign a CNAM pointing to upm.es subdomain)

echo "Deploying neurosuite..."
docker-compose -H unix:///root/altdocker.socket up --remove-orphans --build --force-recreate
echo "Neurosuite finished!"
