FROM python:3.6.4-stretch
#The base OS of this container is Debian stretch

RUN apt-get update && \
    apt-get install -y && \
    apt-get install -y netcat software-properties-common gdebi-core gcc g++ make && \
    apt-get install -y libeigen3-dev && \
    apt-get install -y python-pygraphviz graphviz libgraphviz-dev pkg-config python-dev libxml2-dev libxslt-dev unzip libudunits2-dev mpich

RUN mkdir /neurosuite

#--------------------Boost C++ package installation--------------------
#RUN wget https://dl.bintray.com/boostorg/release/1.66.0/source/boost_1_66_0.tar.gz \
#  && tar xvf boost_1_66_0.tar.gz \
#  && rm boost_1_66_0.tar.gz \
#  && cd boost_1_66_0 \
#  && ./bootstrap.sh --prefix=/usr --with-libraries=all \
#  && ./b2 -j 8  \
#  && ./b2 install


#----------------R and Rstudio installation----------------------
RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends wget r-cran-rgl libglu1-mesa-dev \
   libatlas3-base libx11-dev libcgal-dev libeigen3-dev g++ python-dev autotools-dev libicu-dev build-essential \
   libbz2-dev xvfb xauth xfonts-base xserver-xorg freeglut3 freeglut3-dev libgdal-dev libgeos++-dev libssl-dev

#----To simulate that we have X11-----
RUN apt-get update
RUN Xvfb :7 -screen 0 1280x1024x24 &
RUN export DISPLAY=:7

#-----R installation----
RUN echo "deb http://cloud.r-project.org/bin/linux/debian stretch-cran34/" >> /etc/apt/sources.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys FCAE2A0E115C3D8A
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y r-base r-base-dev
RUN echo 'options(repos = c(CRAN = "https://cran.rstudio.com"))' > ~/.Rprofile

#-----RStudio-----
RUN echo "deb http://ftp.de.debian.org/debian jessie main" >> /etc/apt/sources.list
RUN apt-get update && apt-get upgrade && apt-get install -y openssl libssl1.0.0

RUN wget https://download2.rstudio.org/rstudio-server-1.1.442-amd64.deb
RUN gdebi --n rstudio-server-1.1.442-amd64.deb
RUN rm -f rstudio-server-1.1.442-amd64.deb
RUN rstudio-server start

#-----Shiny server-----
RUN wget https://download3.rstudio.org/ubuntu-12.04/x86_64/shiny-server-1.5.6.875-amd64.deb
RUN gdebi --n shiny-server-1.5.6.875-amd64.deb
RUN rm -f shiny-server-1.5.6.875-amd64.deb
COPY ./shiny-server.conf /etc/shiny-server/shiny-server.conf
#RUN /usr/bin/shiny-server > /dev/null 2>&1 &

#----------Copy R libraries already installed (to speed the startup)-----------------
COPY ./packages/R_libraries/ /usr/local/lib/R/site-library/

#----------Common R packages installation------------------------------
RUN mkdir -p /additional_software/R_local_libraries
COPY ./additional_software/R_local_libraries/ /additional_software/R_local_libraries/
RUN R --no-save -e 'install.packages(c("RJSONIO", "colourpicker", "snow", "threejs", "devtools", "cowplot", "Rcpp", "rjson", "Rvcg", "geometry", "Morpho", "data.table", "dbscan", "bnlearn", "msm", "Hmisc", "mclust"))'
RUN R --no-save -e 'devtools::install_github("lrodriguezlujan/multiStepPage")'
RUN R --no-save -e 'devtools::install_github("lrodriguezlujan/threejswrapper")'
RUN R --no-save -e 'source("http://bioconductor.org/biocLite.R"); biocLite(c("graph", "RBGL", "Rgraphviz"))'
RUN R --no-save -e 'install.packages("/additional_software/R_local_libraries/shinyHierarchicalCbox", repos = NULL, type="source")'
RUN R --no-save -e 'install.packages("/additional_software/R_local_libraries/ShinyD3Bn", repos = NULL, type="source")'

#----------3DBasalRM installation------------------------------
RUN apt-get update
RUN mkdir -p /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/3DBasalRM
COPY ./neurosuite_dj/apps/morpho_analyzer/helpers/3DBasalRM/ /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/3DBasalRM
WORKDIR /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers
#Install 3DBasalRM library: the result library is: "basalrm" so to import it we must type library(basalrm) in our scripts
RUN R CMD INSTALL --no-multiarch --with-keep.source 3DBasalRM

#----------gabaclassifier installation------------------------------
RUN mkdir -p /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/gabaclassifier
COPY ./neurosuite_dj/apps/morpho_analyzer/helpers/gabaclassifier/ /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/gabaclassifier/
RUN R --no-save -e 'devtools::install_github("ComputationalIntelligenceGroup/neurostrr")'
RUN R --no-save -e 'devtools::install_github("ComputationalIntelligenceGroup/neurostrplus")'
WORKDIR /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers
RUN R CMD INSTALL --no-multiarch --with-keep.source gabaclassifier

#-----------Shiny server installation----------------------------
RUN mkdir -p /srv/shiny-server/apps
RUN chown -R shiny:shiny /srv/shiny-server/apps
RUN rm -f /srv/shiny-server/index.html
RUN rm -f -R /srv/shiny-server/sample-apps
#RUN xvfb-run R --no-save -e 'install.packages(c("foreign", "rgl", "ROSE", "MASS", "tmvtnorm", "ggplot2", "MixSim", "scales", "shinyRGL"))'

#----------spineSimulation installation------------------------------
RUN mkdir -p /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/spineSimulation
COPY ./neurosuite_dj/apps/morpho_analyzer/helpers/spineSimulation/ /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/spineSimulation/
RUN ln -s /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/spineSimulation/ /srv/shiny-server/apps/spineSimulation
WORKDIR /srv/shiny-server/apps/
RUN R CMD INSTALL --no-multiarch --with-keep.source spineSimulation

#----------3DSynapsesSA installation------------------------------
RUN mkdir -p /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/3DSynapsesSA
COPY ./neurosuite_dj/apps/morpho_analyzer/helpers/3DSynapsesSA/ /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/3DSynapsesSA/
WORKDIR /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers
RUN R --no-save -e 'devtools::install_github("ComputationalIntelligenceGroup/3DSynapsesSA")'
RUN R CMD INSTALL --no-multiarch --with-keep.source 3DSynapsesSA
RUN ln -s /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/3DSynapsesSA/inst/dashboard_UI /srv/shiny-server/apps/3DSynapsesSA

#----------3DSomaMS installation------------------------------
RUN mkdir -p /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/3DSomaMS
COPY ./neurosuite_dj/apps/morpho_analyzer/helpers/3DSomaMS/ /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/3DSomaMS/
RUN R --no-save -e 'devtools::install_github("ComputationalIntelligenceGroup/3DSomaMS")'
WORKDIR /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers
RUN R CMD INSTALL --no-multiarch --with-keep.source 3DSomaMS

#----------3DSomaMS-shiny installation------------------------------
RUN mkdir -p /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/3DSomaMS-shiny
COPY ./neurosuite_dj/apps/morpho_analyzer/helpers/3DSomaMS-shiny/ /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/3DSomaMS-shiny/
RUN ln -s /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/3DSomaMS-shiny/R /srv/shiny-server/apps/3DSomaMS-shiny

#----------hbp-dendrite-arborization-simulation installation------------------------------
RUN mkdir -p /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/hbp-dendrite-arborization-simulation
COPY ./neurosuite_dj/apps/morpho_analyzer/helpers/hbp-dendrite-arborization-simulation/ /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/hbp-dendrite-arborization-simulation/
WORKDIR /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers
RUN R --no-save -e 'install.packages(c("units", "sf", "rbmn", "pracma", "compoisson", "stats", "spdep", "jsonlite", "FastKNN", "FNN", "entropy"))'
RUN R --no-save -e 'devtools::install_github("lrodriguezlujan/mvCircular", ref = "master")'
RUN R CMD INSTALL --no-multiarch --with-keep.source hbp-dendrite-arborization-simulation
#RUN ln -s /neurosuite/neurosuite_dj/apps/morpho_analyzer/helpers/hbp-dendrite-arborization-simulation/R /srv/shiny-server/apps/hbp-dendrite-arborization-simulation

#----------aplpack installation (for some plot like Chernoff faces------------------------------
RUN R --no-save -e 'install.packages(c("aplpack"))'

#----------Stats and bayesian networks R libraries------------------------------
RUN R --no-save -e 'install.packages(c("fitdistrplus", "bnlearn", "sparsebn"))'

#-------------Java 8 OPEN JDK-------------------------------------
RUN apt-get -y update && \
    apt-get install -y python-software-properties debconf-utils && \
    apt-get install -y libxtst6 libxtst-dev && \
    apt-get install -y openjdk-8-jdk openjdk-8-jre

#----------Libvips installation (for MultiMap, proccessing images)-------------------
RUN apt-get -y update
RUN apt-get install -y --no-install-recommends libvips42 libvips-dev libvips-tools python3-gi gir1.2-vips-8.0

#----------------------ImageJ and plugins-------------------------------------
RUN cd / && \
   mkdir -p additional_software/ImageJ

COPY ./additional_software/ImageJ /additional_software/ImageJ/

#----To simulate that we have X11-----
RUN apt-get update
RUN Xvfb :7 -screen 0 1280x1024x24 &
RUN export DISPLAY=:7

#----------------------Node JS------------------------------------
RUN curl -sL https://deb.nodesource.com/setup_10.x| sudo -E bash - && \
    apt-get install -y nodejs

#----------------------Firefox (for headless web browser)------------------------------------
RUN cd / && \
    wget https://ftp.mozilla.org/pub/firefox/releases/59.0/linux-x86_64/en-US/firefox-59.0.tar.bz2 && \
    tar -xjf firefox-59.0.tar.bz2 && \
    rm firefox-59.0.tar.bz2 && \
    rm -f -R /usr/bin/firefox && \
    mv ./firefox /usr/bin/firefox && \
    apt-get install -y libgtk-3-dev

#---------------------Geckodriver (to connecto Firefox with Selenium)------------------------------------
RUN cd / && \
    rm -R -f /geckodriver && \
    mkdir -p geckodriver && \
    cd /geckodriver && \
    wget https://github.com/mozilla/geckodriver/releases/download/v0.20.1/geckodriver-v0.20.1-linux64.tar.gz && \
    tar -xzf geckodriver-v0.20.1-linux64.tar.gz && \
    rm -f geckodriver-v0.20.1-linux64.tar.gz

#---------------------Conda installations with packages (Django and others)------------------------------------
COPY neurosuite_dj/conda_environment.yml /neurosuite/neurosuite_dj/

RUN cd / && \
    wget https://repo.anaconda.com/miniconda/Miniconda3-4.5.11-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    /opt/conda/bin/conda clean -tipsy && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    export PATH=/opt/conda/bin:$PATH && \
    conda env create python=3.6.4 -f neurosuite/neurosuite_dj/conda_environment.yml && \
    echo "conda activate conda_py364" >> ~/.bashrc

RUN /bin/bash -c "source ~/.bashrc; conda activate conda_py364"


WORKDIR /
COPY ./docker_celery_entrypoint.sh ./docker_celery_entrypoint.sh

CMD ["./docker_celery_entrypoint.sh"]