#!/bin/bash

function install_everything() {
  echo "Starting NeuroSuites install_everything..."

  #Comment this if you don't want to install supervisor
  ./install_docker_and_supervisor.sh

  cd /home
  mkdir -p workspace/PRODUCTION_NEUROSUITES
  cd workspace/PRODUCTION_NEUROSUITES

  git clone https://gitlab.com/mmichiels/neurosuite.git
  echo "IMPORTANT: If this is your first run, configure the credentials_secrets: Rename neurosuite_dj/dummy_credentials_secrets/ to neurosuite_dj/credentials_secrets/* and fill the text in each file with your own users/passwords. Django_admin files are not neccesary if you don't want a Django superuser account to be created, so delete or rename them if you don't want a Django admin account to be created."

  echo "Finished! Now configure the credentials_secrets (check the documentation about it). Change the DEBUG variable to False in neurosuite_dj/neurosuite_dj/settings/base.py. Then configure supervisor to run NeuroSuites always automatically or run ./startup.sh to start using NeuroSuites now (or ./startup-PRODUCTION.sh for the production site)"
}

install_everything
